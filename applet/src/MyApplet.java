import netscape.javascript.JSException;
import netscape.javascript.JSObject;

import java.applet.Applet;
import java.awt.*;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

/**
 * @author TMP-mmendonca
 */
public class MyApplet extends Applet {
    /**
     * Initialization method that will be called after the applet is loaded into
     * the browser.
     */
    public void init() {
        // TODO start asynchronous download of heavy resources
    }

    public void paint(Graphics g) {
        try {
            JSObject window = JSObject.getWindow(this);
            window.call("write", new Object[]{getDados()});
        } catch (JSException jse) {
        }
        g.drawString(getDados(),10,25);
    }

    private String getDados() {
        String dados = "";
        String dadosAdicionais = "";
        String adicionais = "";
        int count = 0;
        try {
            dados = "\"" + InetAddress.getLocalHost().toString() + "\"";
            Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
            for (; n.hasMoreElements(); ) {
                NetworkInterface e = n.nextElement();
                Enumeration<InetAddress> a = e.getInetAddresses();
                for (; a.hasMoreElements(); ) {
                    InetAddress addr = a.nextElement();
                    if (!addr.getHostAddress().equals("")) {
                        dadosAdicionais += "\"" + (count++) + " - " + addr.getCanonicalHostName() + "\"" + ":" + "\"" + addr.getHostAddress() + "\",";
                    }
                }
            }
            adicionais = "{" + dadosAdicionais.substring(0, dadosAdicionais.length() - 1) + "}";
        } catch (Exception e) {
            System.out.printf("Exception");
            e.printStackTrace();
        }
        try {
            MCrypt m = new MCrypt();
            String base = "{\"dados\":" + dados + ",\"dadosAdicionais\":" + adicionais + "}";
            return MCrypt.bytesToHex(m.encrypt(base));
        } catch (Exception e) {
            return "Não Disponível";
        }
    }
}