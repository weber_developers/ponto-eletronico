﻿<?php
session_start();
if ($_SESSION["sessao_id_usrs"] == '') {
    ?>
    <script language="JavaScript">
        window.location.href = 'lista.php';
    </script>
    <?php
    header("Location: lista.php");
    die;
}

require_once("funcoes/conexao.php");

$numr_ip = $_SERVER[HTTP_X_FORWARDED_FOR] . "-" . $_SERVER[REMOTE_ADDR];
$matricula = '001';
$nome_arquivo = $_POST["nome_arquivo"];
$id_usr = 4;
$data_registro = date('Y') . "/" . date('m') . "/" . date('d') . " " . date("H") . ":" . date("i") . ":" . date("s");

$dia = date("d");
$mes = date("m");
$ano = date("Y");
$hora = date("H");

$sql_consult = "insert into acessos (id_usr,data_acesso,entrada,numr_ip) values (4000,'" . date("Y/m/d") . "','" . date("H:i:s") . "','" . $numr_ip . "')";
$dados_consult = mysqli_query( $conexao, $sql_consult);


$sql_consulta_ponto = "select data_acesso,entrada,saida,numr_ip,nome,pma from acessos inner join usuarios on acessos.id_usr = usuarios.id_usr where (ho = 4 or usuarios.id_usr = 4) and day(data_acesso) = " . $dia . " and month(data_acesso) = " . $mes . " and year(data_acesso) = " . $ano . " order by data_acesso desc";
$dados_consulta_ponto = mysqli_query( $conexao, $sql_consulta_ponto);



?>
<script language="JavaScript" type="text/javascript">
    function fncconfirma() {
        var vConfirm = window.confirm("Deseja realmente excluir?");
        if (vConfirm == true) {
            janela.location.href = 'exclui_registros.php';
        }

    }
</script>
<style>
    .botao {
        background: rgb(79, 149, 255);

        padding: 1px 1px 1px;
        border-radius: 34px;
        border: 1px solid rgb(186, 186, 186);
        box-shadow: 0px 3px 7px #333;
        cursor: pointer;
    }

    .botao2 {
        background: rgb(0, 165, 0);

        padding: 1px 1px 1px;
        border-radius: 34px;
        border: 1px solid rgb(186, 186, 186);
        box-shadow: 0px 3px 7px #333;
        cursor: pointer;
    }

    .botao4 {
        background: rgb(255, 36, 36);

        padding: 1px 1px 1px;
        border-radius: 34px;
        border: 1px solid rgb(186, 186, 186);
        box-shadow: 0px 3px 7px #333;
        cursor: pointer;
    }
</style>
<center>
    <strong><font style="font-size:60px;" face="Arial, Helvetica, sans-serif"> Acessos </font></strong></center>
<table width="100%" border="0" align="center" cellpadding="5" cellspacing="5">
    <?php while ($resultado = mysqli_fetch_array($dados_consulta_ponto)) { ?>
        <tr>
            <td height="30"
                <?php
                if (substr($resultado[entrada], 0, 2) < 12) {
                    echo "bgcolor='#36E703'";
                }
                if (substr($resultado[entrada], 0, 2) >= 12 && substr($resultado[entrada], 0, 2) < 18) {
                    echo "bgcolor='#ff6600'";
                }
                if (substr($resultado[entrada], 0, 2) >= 18) {
                    echo "bgcolor='#FF48A4'";
                }
                ?>

                style='border-top:solid #ffffff 2pt;'><font size="6"
                                                            face="Arial, Helvetica, sans-serif"><?php echo $resultado[nome] . "<br>" . $resultado[entrada] . " - " . $resultado[numr_ip] . " - ";
                    if ($resultado[pma] == 1) {
                        echo "Master";
                    }
                    if ($resultado[pma] == 0) {
                        echo "Próprio";
                    }
                    ?></font></td>
        </tr>
    <?php } ?>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="71%" height="150" align="center" valign="middle" class="botao"
            onClick="window.location.href='frmreg3.php';">
            <font color="#FFFF99" style="font-size:80px;" face="Arial, Helvetica, sans-serif"><strong>Ver
                    fotos</strong></font></td>
    </tr>
</table>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="71%" height="150" align="center" valign="middle" class="botao2"
            onClick="window.location.href='frmreg4.php';">
            <font color="#FFFF99" style="font-size:80px;" face="Arial, Helvetica, sans-serif"><strong>Ver
                    Registros</strong></font></td>
    </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="71%" height="150" align="center" valign="middle" class="botao4" onClick="fncconfirma();">
            <font color="#FFFF99" style="font-size:80px;" face="Arial, Helvetica, sans-serif"><strong>Excluir
                    Registros</strong></font></td>
    </tr>
</table>
<p>&nbsp;</p>
<iframe name="janela" style="display:none"></iframe>