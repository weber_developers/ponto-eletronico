﻿<?php session_start();
$bt = $_GET["bt"]; ?>
<head><title>Registro de Ponto</title>

    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <link rel="stylesheet" type="text/css" media="screen" href="./css/estilos.css">
    <script language="JavaScript" src="./funcoes/funcao.js"></script>
    <script language="javascript" type="text/javascript">
        function fncEnter() {
            if (window.event.keyCode == 13) {
                if (document.registro_ponto.matricula.value == '') {
                    alert("Informe sua matrícula!");
                    document.registro_ponto.matricula.focus();
                    return false;
                }
                document.registro_ponto.submit();
            }
        }

        function setFocus() {
            document.registro_ponto.matricula.focus();

        }

        function fncValida() {

            if (document.registro_ponto.matricula.value == '') {
                alert("Informe sua matrícula!");
                document.registro_ponto.matricula.focus();
                return false;
            }
            document.registro_ponto.submit();
        }
    </script>
</head>
<body onload="javascript:setFocus()">


<BR> <BR> <BR>

<form action="frm_confirma_ponto_monica.php" method="post" name="registro_ponto" id="loginForm">
    <table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="680" height="80" background="./images/header.jpg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="93%"><strong><font color="#333333" size="4">&nbsp;&nbsp;&nbsp;&nbsp;Registro
                                                de Ponto - SED</font></strong></td>
                                    <td width="7%" rowspan="2" valign="bottom">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="bottom">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><font
                                                color="#666666" size="1">&nbsp;
                                                <?php

                                                $hoje = getdate();

                                                switch ($hoje['wday']) {
                                                    case 0:
                                                        echo "Domingo, ";
                                                        break;
                                                    case 1:
                                                        echo "Segunda-Feira, ";
                                                        break;
                                                    case 2:
                                                        echo "Terça-Feira, ";
                                                        break;
                                                    case 3:
                                                        echo "Quarta-Feira, ";
                                                        break;
                                                    case 4:
                                                        echo "Quinta-Feira, ";
                                                        break;
                                                    case 5:
                                                        echo "Sexta-Feira, ";
                                                        break;
                                                    case 6:
                                                        echo "Sábado, ";
                                                        break;
                                                }

                                                echo $hoje['mday'];
                                                switch ($hoje['mon']) {
                                                    case 1:
                                                        echo " de Janeiro de ";
                                                        break;
                                                    case 2:
                                                        echo " de Fevereiro de ";
                                                        break;
                                                    case 3:
                                                        echo " de Março de ";
                                                        break;
                                                    case 4:
                                                        echo " de Abril de ";
                                                        break;
                                                    case 5:
                                                        echo " de Maio de ";
                                                        break;
                                                    case 6:
                                                        echo " de Junho de ";
                                                        break;
                                                    case 7:
                                                        echo " de Julho de ";
                                                        break;
                                                    case 8:
                                                        echo " de Agosto de ";
                                                        break;
                                                    case 9:
                                                        echo " de Setembro de ";
                                                        break;
                                                    case 10:
                                                        echo " de Outubro de ";
                                                        break;
                                                    case 11:
                                                        echo " de Novembro de ";
                                                        break;
                                                    case 12:
                                                        echo " de Dezembro de ";
                                                        break;
                                                }

                                                echo $hoje['year'] . ".";
                                                ?>
                                                <input name="bt" type="text" style="display:none"
                                                       value="<?php echo $bt; ?>">
                                            </font></strong></td>
                                </tr>
                            </table>

                        </td>
                        <td width="20" background="./images/header_rightcap.jpg">&nbsp;</td>
                    </tr>
                </table>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="880" background="./images/contentshadow.gif" height="2"></td>
                    </tr>
                </table>
                <table width="699" height="13" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="3" height="5"></td>
                        <td width="12" height="4" background="./images/rightside.gif"></td>
                        <td width="890" valign="top" bgcolor="#FFFFFF"><BR>

                            <p>&nbsp;</p>

                            <p>&nbsp; </p>

                            <table width="536" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="185" align="center"><img src="./images/user.png" width="150"
                                                                        height="150">
                                        <br>

                                        <iframe scrolling="no" src="horario.php" width="150" height="50"
                                                frameborder="0"></iframe>
                                    </td>
                                    <td width="369" valign="bottom">
                                        <table width="369" border="0" align="center" cellpadding="0" cellspacing="0"
                                               class="Tabela_rel">
                                            <tr valign="bottom">
                                                <td height="25" class="Sub_titulo_rel">&nbsp;&nbsp;Autenticação
                                                </td>
                                            </tr>
                                            <tr class="Itens_rel_s_traco">
                                                <td height="150">
                                                    <p>&nbsp;</p>
                                                    <table width="89%" border="0" align="center" cellpadding="0"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td height="50" colspan="2"><strong><font color="#FF3300"
                                                                                                      size="5">Informe
                                                                        sua matrícula:</font></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="71%" valign="top">
                                                                <input name="matricula" type="text" id="matricula"
                                                                       style="font-size:34px; width:200px"
                                                                       onKeyPress="return fncEnter();" size="15"
                                                                       maxlength="15">
                                                            </td>
                                                            <td width="29%" valign="bottom">
                                                                <div id="resultado1" class="tryit"
                                                                     onClick="fncValida();">
                                                                    <div align="center"><font size="3"><strong><font
                                                                                    size="4">Confirma</font></strong></font>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br>
                                                </td>
                                            </tr>
                                            <tr class="Sub_titulo_rel">
                                                <td height="15">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <p>&nbsp;</p>

                            <p>&nbsp;</p>

                            <p>&nbsp;</p></td>
                        <td width="4" valign="top" background="./images/sidebar.gif"><br>
                        </td>
                    </tr>
                </table>
                <table width="700" height="59" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="1" colspan="2"></td>
                    </tr>
                    <tr>
                        <td width="596" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                            de Ponto Eletrônico 2012 - SED&reg;</td>
                        <td width="2" class="Fundo_caixa_canto_jpg"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">&nbsp;</td>
        </tr>
    </table>
</form>
<BR>
<iframe width="500" height="500" name="Verifica" style="display:none"></iframe>
</body>

</html>