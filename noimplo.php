﻿<?php session_start(); ?>
<?php ob_start(); ?>
<?php
if ($_REQUEST['erro_ponto']) {
    $titulo = 'SEU PONTO NÃO FOI REGISTRADO!';
    $texto = 'Por favor tente registrá-lo novamente.<br/>Se o problema persistir, entre em contato com a equipe de T.I.';
} else {
    $titulo = 'CPF NÃO ENCONTRADO!';
    $texto = 'Por favor digite seu CPF novamente.<br/>Se o problema persistir, entre em contato com a equipe de T.I.';
}

?>
    <script>
        function fncAtualiza() {
            setTimeout("fncChama()", 5000);
        }
        function fncChama() {
            window.location.href = 'index.php';
        }
        function fncEnter() {
            window.location.href = 'index.php';
        }
        $(document).ready(function () {
            $(document).keydown(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    window.location.href = 'index.php';
                }
            });
            fncAtualiza();
        });
    </script>
    <div class="alert alert-danger">
        <h1 class="text-center"><?php echo $titulo; ?></h1>
    </div>
    <div class="well">
        <h3><?php echo $texto; ?></h3>
    </div>
    <div>
        <a href="javascript:fncChama();" class="btn btn-primary btn-lg btn-block">Voltar e tentar novamente</a>
    </div>
<?php
$html = ob_get_clean();
include 'index.php';