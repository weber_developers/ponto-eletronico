<meta charset="utf-8"/>
<?php
require_once("funcoes/conexao.php");

$numr_ip = ($_SERVER[HTTP_X_FORWARDED_FOR] ? $_SERVER[HTTP_X_FORWARDED_FOR] . ' - ' : '') . $_SERVER[REMOTE_ADDR];
$data_tentativa = date('Y') . "/" . date('m') . "/" . date('d') . " " . date("H") . ":" . date("i") . ":" . date("s");
$usuario = getUsuario($_REQUEST['matricula']);
$matricula = $usuario->matricula;
$nome_arquivo = $_POST["nome_arquivo"];
$dia = date("d");
$mes = date("m");
$ano = date("Y");

$sql_consulta = "select id_usr,nome,usuarios.ativo,foto,id_grade,sigla,gerente from usuarios inner join depto on usuarios.id_depto = depto.id_depto where matricula = '" . $matricula . "'";
$dados_consulta = mysqli_query( $conexao, $sql_consulta);
$resultado_consulta = mysqli_fetch_array($dados_consulta);
$bt = $_POST["bt"];

$gerente = $resultado_consulta[gerente];
$id_usr = $resultado_consulta[id_usr];
$foto = $resultado_consulta[foto];
$ativo = $resultado_consulta[ativo];

$nome = explode(' ', $resultado_consulta[nome]);

if ($id_usr == '') {
    ?>
    <script language="JavaScript" type="text/javascript">
        window.parent.location.href = 'noimplo.php';
    </script>
    <?php
    die;
}


$id_grade = $resultado_consulta[id_grade];
$etapa = 1;
$local = 0;

$numr_ip = $_SERVER[HTTP_X_FORWARDED_FOR] . "-" . $_SERVER[REMOTE_ADDR];
$numr_ip2 = "i" . $_SERVER[HTTP_X_FORWARDED_FOR] . "-" . $_SERVER[REMOTE_ADDR];


$data_registro = date('Y') . "/" . date('m') . "/" . date('d') . " " . date("H") . ":" . date("i") . ":" . date("s");
$dia = date("d");
$mes = date("m");
$ano = date("Y");

$hora = date("H");
$minutos = date("i");

if ($id_usr != '' || $id_usr != 0) {
    $sql_consulta_grade = "select * from p_grade where id_grade = " . $id_grade;

    $dados_consulta_grade = mysqli_query( $conexao, $sql_consulta_grade);
    $resultado_consulta_grade = mysqli_fetch_array($dados_consulta_grade);

    if ($resultado_consulta_grade[id_grade] == "") {
        ?>
        <script language="JavaScript" type="text/javascript">
            window.parent.location.href = 'nograde.php';
        </script>
        <?php
        die;
    } else {
        $tipo_grade = $resultado_consulta_grade[tipo_grade];
        $entrada_1 = $resultado_consulta_grade[entrada_1];
        $saida_1 = $resultado_consulta_grade[saida_1];
        $entrada_2 = $resultado_consulta_grade[entrada_2];
        $saida_2 = $resultado_consulta_grade[saida_2];


        $time_agora = mktime(date("H"), date("i"), date("s"));


        $sql_etapa = "select etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and nao_registrou = 0 order by id_registro desc ";

        $dados_etapa = mysqli_query( $conexao, $sql_etapa);
        $resultado_etapa = mysqli_fetch_array($dados_etapa);
        $etapa_consulta = $resultado_etapa[etapa] + 1;


        if ($etapa_consulta == 1) {
            $sub_horario_grade = explode(':', $entrada_1);
        }
        if ($etapa_consulta == 2) {
            $sub_horario_grade = explode(':', $saida_1);
        }
        if ($etapa_consulta == 3) {
            $sub_horario_grade = explode(':', $entrada_2);
        }
        if ($etapa_consulta == 4) {
            $sub_horario_grade = explode(':', $saida_2);
        }

        $time_hora_grade = mktime($sub_horario_grade[0], $sub_horario_grade[1], '00');

        $diferenca_hora_grade = ($time_hora_grade - $time_agora) / 60;
        $diferenca_envia = 0;
        if (abs($diferenca_hora_grade) > 30) {
            $diferenca_envia = 1;
        }
        if ($gerente == 1) {
            $diferenca_envia = 0;
        }
        //echo $etapa_consulta."<BR>";
        //echo abs($diferenca_hora_grade)."<BR>";
        //echo date("H").":".date("i")."<BR>";
        //echo $sub_horario_grade[0].":".$sub_horario_grade[1];


        $horario_1 = explode(':', $horario_1);
        $hora_1 = (60 * 60 * $horario_1[0]) + (60 * $horario_1[1]);

        $diferenca_registro_1 = abs(($hora_1_banco - $hora_1) / 60);
    }

}

if ($tipo_grade == 0) {
    $total_registros_grade = 4;
} else {
    $total_registros_grade = 2;
}
$existe_evento = 0;
$sql_consulta_evento = "select id_evento,titulo from p_eventos inner join p_tipo_justificativa on p_eventos.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa where (day(data_evento) = " . $dia . " and month(data_evento) = " . $mes . " and year(data_evento) = " . $ano . ") and (id_usr = " . $id_usr . " or id_depto = 0)";
$dados_consulta_evento = mysqli_query( $conexao, $sql_consulta_evento);
$resultado_consulta_evento = mysqli_fetch_array($dados_consulta_evento);
if ($resultado_consulta_evento[id_evento] != '') {
    $existe_evento = 1;
}
//echo $sql_consulta_evento;

$sql_consulta_ponto = "select count(id_registro) as total from p_registro where id_usr = " . $id_usr . " and day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and nao_registrou = 0";;
$dados_consulta_ponto = mysqli_query( $conexao, $sql_consulta_ponto);
$resultado_consulta_ponto = mysqli_fetch_array($dados_consulta_ponto);
$total_registro_diario = $resultado_consulta_ponto[total];

$ipad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
$iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
$palmpre = strpos($_SERVER['HTTP_USER_AGENT'], "webOS");
$berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");
$windows = strpos($_SERVER['HTTP_USER_AGENT'], "Windows");
$linux = strpos($_SERVER['HTTP_USER_AGENT'], "linux");
$permissao = 0;

$texto = "";
$texto = $_SERVER['HTTP_USER_AGENT'];

if ($ipad == true) {
    $texto = "iPad";
    $permissao = 1;
}
if ($iphone == true) {
    $texto = "iPhone";
    $permissao = 1;
}
if ($android == true) {
    $texto = "Android";
    $permissao = 1;
}
if ($palmpre == true) {
    $texto = "Palmpre";
    $permissao = 1;
}
if ($berry == true) {
    $texto = "Berry";
    $permissao = 1;
}
if ($ipod == true) {
    $texto = "iPod";
    $permissao = 1;
}
if ($windows == true) {
    $texto = "Computador com Windows";
}
if ($linux == true) {
    $texto = "Computador com Linux";
}
$sed = strpos($numr_ip2, "10.5.22");
if ($sed) {
    $texto = "SED - Computador com Windows";
    $permissao = 0;
}

if (($SED + $palacio_1 + $palacio_2 + $Segplan_1 + $SED_anapolis_1 + $SED_anapolis_2 + $agri_1 + $agri_2 + $agri_3 + $seg_1 + $seg_2 + $edu_1 + $agecom_1 + $agecom_2 + $agecom_3 + $saude_1 + $ipasgo_1 + $ipasgo_2 + $ipasgo_3 + $agetop_1 + $agetop_2 + $agetop_3 + $agecom_4) == 0 && $permissao == 0) {
    //$permissao = 3;
    //$local = 1;
}


$ipa = strpos($_SERVER['HTTP_USER_AGENT'], "iPa");
$ispa = strpos($_SERVER['HTTP_USER_AGENT'], "iPa");


if ($bt == 2 && $id_usr == 4) {
    $numr_ip = '10.5.6.202, 177.135.248.165-177.135.248.165';
    $texto .= "Computador com Windows";
    $permissao = 0;
}
//if ($gerente != 1) {
//    if (abs($diferenca_hora_grade) > 30) {
//        $podePular = false;
//    }
//    if (abs($diferenca_hora_grade) > 15 && $tipo_grade == 0 && ($etapa_consulta == 2 || $etapa_consulta == 3)) {
//        $podePular = false;
//    }
//} else {
//    $podePular = true;
//}


$usuario = getUsuario($_REQUEST['matricula']);
$grade = getGradeUsuario($usuario->id_usr);
$registros = getRegistrosUsuario($usuario->id_usr, date('d/m/Y'));
//so pode registrar se for
$ocorrencia = getPossivelOcorrencia($usuario->id_usr, date('d/m/Y H:i:s'));
gravarTentativa($id_usr, date('d/m/Y H:i:s'));
$podePular = false;
if (empty($ocorrencia->mensagem)) {
    $podePular = true;
}
?>
<?php ((is_null($___mysqli_res = mysqli_close($conexao))) ? false : $___mysqli_res); ?>
<?php session_start(); ?>
<?php ob_start(); ?>
<script language="JavaScript" type="text/javascript">
    function fncAtualiza() {
        document.registro_ponto.tecla.focus();
        setTimeout("fncChama()", 5000);
    }
    //setTimeout("fncChama()", 10000);
    function fncChama() {
        window.location.href = 'index.php';
    }
    function fncEnter() {
        window.location.href = 'index.php';
    }
    $(document).ready(function () {
        $(document).keydown(function (e) {
            if (e.which == 13 || e.keyCode == 13) {
                atualiza();
            }
        });
        $(document).keyup(function (e) {
            if (e.which == 27 || e.keyCode == 27) {
                fncChama();
            }
        });
        $('#confirma').click(function () {
            atualiza();
        });
        <?php if($podePular){?>
        atualiza();
        <?php }?>
    });
    function atualiza() {
        $('#registra_ponto').submit();
    }
</script>
<?php if (!$podePular) { ?>
    <div class="alert alert-warning">
        <h1 class="text-center"><b><?php echo date('H:i'); ?></b> <?php echo $ocorrencia->titulo; ?>!</h1>

        <h3 class="text-center"><?php echo $usuario->nome; ?></h3>
    </div>
    <div class="well">
        <?php if (!$ocorrencia->trava) { ?>
            <div class="row-fluid hide">
                <div class="col-lg-12">
                    <h3 class="text-center"><?php echo $usuario->nome; ?></h3>
                </div>
            </div>
            <div class="row-fluid">
                <div class="col-lg-12">
                    <h1 class="text-center">REGISTRA O SEU PONTO?</h1>
                </div>
                <div class="col-lg-12">
                    <div class="row-fluid">
                        <div class="col-lg-6">
                            <a href="index.php" class="btn btn-danger btn-block white"
                               style="font-size: 1.6em;color:#ffffff;">
                                CANCELAR
                            </a>
                        </div>
                        <div class="col-lg-6">
                            <a href="javascript:void(0);" id="confirma" class="btn btn-success btn-block"
                               style="font-size: 1.6em;color:#ffffff;">
                                REGISTRAR PONTO
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <h1 class="text-justify">
                        <small>
                            <?php echo $ocorrencia->mensagem; ?>
                        </small>
                    </h1>
                </div>
                <div class="col-lg-3"></div>
            </div>
        <?php } else { ?>
            <div class="alert alert-info"><h3 class="text-center"><?php echo $ocorrencia->mensagem; ?></h3></div>
			<script>$(document).ready(function(){setTimeout("fncChama()", 10000);});</script>
        <?php } ?>
        <div class="clearfix"></div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-striped">
                <tr>
                    <th class="text-center"><h3>Sua Grade</h3></th>
                    <th><h3 class="text-center">Pontos Registrados Hoje</h3></th>
                </tr>
                <tr>
                    <td>
                        <h4 class="text-center"><?php echo $grade->entrada_1; ?></h4>
                        <h4 class="text-center"><?php echo $grade->saida_1; ?></h4>
                        <?php if ($grade->entrada_2) {
                            $_qtdeGrade = 2; ?>
                            <h4 class="text-center"><?php echo $grade->entrada_2; ?></h4>
                            <h4 class="text-center"><?php echo $grade->saida_2; ?></h4>
                        <?php }
                        $_qtdeGrade += 2;
                        ?>
                    </td>
                    <td>
                        <div class="row-fluid">
                            <?php foreach ($registros as $registro) {
                                $_qtdeGrade--;
                                ?>
                                <div class="col-lg-3 text-center">
                                    <img src="images/timthumb.png" width="64" height="64"/>
                                    <h4 class="text-center"><?php echo $registro->hora . ':' . $registro->minutos; ?></h4>
                                </div>
                            <?php } ?>
                            <?php while ($_qtdeGrade) { ?>
                                <div class="col-lg-3 text-center">
                                    <img src="images/notime.png" width="64" height="64">
                                </div>
                                <?php $_qtdeGrade--;
                            } ?>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php } ?>
<?php if ($ocorrencia->trava == false) { ?>
    <form action="frm_registra_ponto.php" method="post" name="registro_ponto" id="registra_ponto" target="Verifica">
    <textarea name="dadosConexao" id="dadosConexao"
              style="display:none;visibility: hidden;"><?php echo $_REQUEST['dadosConexao']; ?></textarea>
        <input name="bt" type="hidden" value="<?php echo($bt ? $bt : 0); ?>">
        <input type="hidden" name="diferenca_envia" value="<?php echo $diferenca_envia; ?>">
        <input type="hidden" name="op" value="0">
        <input name="matricula" type="hidden" id="matricula" value="<?php echo $matricula; ?>">
        <input name="nome_arquivo" type="hidden" id="nome_arquivo" value="<?php echo $nome_arquivo; ?>">
    </form>
<?php } ?>
<br/>
<iframe width="500" height="500" name="Verifica" style="display:none"></iframe>
</div>
<?php
$html = ob_get_clean();
include 'index.php';