<?php
/**
 * Created by PhpStorm.
 * User: marcelo-tm
 * Date: 21/08/2015
 * Time: 13:55
 */
require_once '../funcoes/conexao.php';
require_once '../lib/BaseObject.php';
require_once '../funcoes/funcoes.php';
$hoje = new DateTime(date('Y-10-01'));
$fim = new DateTime(date('Y-10-23'));
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
</head>
<body>

<h1>Número de Ausências de Outubro/2015</h1>

<p>Quantidade de dias úteis entre 01/10/2015 e 23/10/2015</p>

<table>
    <tr>
        <td>Departamento</td>
        <td>Servidor</td>
        <td>CPF</td>
        <td>Quantidade de Ausências</td>
    </tr>
    <?php foreach (R::findAll('usuarios', 'inner join depto on depto.id_depto = usuarios.id_depto order by depto.depto') as $usuario) {?>
        <tr>
            <td><?= $usuario->getDepartamento()->depto; ?></td>
            <td><?= $usuario->nome; ?></td>
            <td><?= $usuario->matricula; ?></td>
            <td><?= getDescricaoDia(getTotalMensal($usuario->id_usr, $hoje->format('d/m/Y'), $fim->format('d/m/Y'))['totalFaltas'],$usuario->id_usr) ?></td>
        </tr>
    <?php } ?>
</table>
</body>
</html>