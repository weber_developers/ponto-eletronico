<?php
/**
 * Created by PhpStorm.
 * User: TMP-mmendonca
 * Date: 17/03/2015
 * Time: 15:08
 */
require_once 'funcoes/conexao.php';
require_once 'funcoes/funcoes.php';

$matricula = preg_replace('/[\D]?/', '', $_REQUEST['cpf']);

$usuario = getUsuario($matricula);
echo json_encode(['existe' => file_exists($_SESSION['config']->pastaFotosUsuarios . $usuario->foto), 'usuario' => ['nome' => $usuario->nome]]);