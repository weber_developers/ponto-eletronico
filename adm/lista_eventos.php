﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 && $_SESSION["sessao_id_usr"] == 0) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.location.href = 'index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}

require_once("../funcoes/conexao.php");


$id_depto = $_POST["id_depto"];

$id_usr = $_POST["id_usr"];
if ($id_usr == '') {
    $id_usr = 0;
}
$sql = "select * from p_eventos inner join p_tipo_justificativa on p_eventos.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa where true ";
if ($id_depto && !$id_usr) {
    $sql .= " and id_depto = " . $id_depto;
}

if ($id_usr != 0) {
    $sql = $sql . " and id_usr = " . $id_usr;
}
$sql = $sql . " group by id_pai order by data_evento desc";
$dados = mysqli_query( $conexao, $sql);

?>
<html>
<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_reloadPage(init) {  //reloads the window if Nav4 resized
        if (init == true) with (navigator) {
            if ((appName == "Netscape") && (parseInt(appVersion) == 4)) {
                document.MM_pgW = innerWidth;
                document.MM_pgH = innerHeight;
                onresize = MM_reloadPage;
            }
        }
        else if (innerWidth != document.MM_pgW || innerHeight != document.MM_pgH) location.reload();
    }
    MM_reloadPage(true);
    //-->
</script>


<script language="JavaScript" type="text/JavaScript">
    <!--
    function fncExclui(id_evento) {
        if (window.confirm('Excluir Evento?')) {
            grava.location.href = 'exclui_evento.php?id_evento=' + id_evento;
        }
    }
    function fncMontacombo(id_depto) {
        grava.location.href = 'frmMontafuncionarios.php?id_depto=' + id_depto;
    }
    function fncValida() {

        document.formulario.submit();
    }
    function fncInicio() {
        setTimeout("document.formulario.id_depto.value=<?php echo $id_depto;?>;", 500);
        setTimeout("grava.location.href='frmMontafuncionarios.php?id_depto='+<?php echo $id_depto;?>;;", 800);
        if (<?php echo $id_usr;?>!=
        0
    )
        {


            setTimeout("document.formulario.id_usr.value=<?php echo $id_usr;?>;", 1300);
        }
    }

    function fncLista() {
        window.open("ListaTamanhos.php", "", "");
    }
    function fncConsulta(id_pai) {
        window.close();
        window.opener.location.href = 'cad_eventos.php?id_pai=' + id_pai;

    }


    //-->
</script>
<head>
    <title>Eventos</title>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="fncInicio();">
<table width="950" border="0" align="center" cellpadding="0" cellspacing="0" class="tablefill">
    <tr>
        <td valign="top"
            style='border-left:solid #F7F7F7 .5pt;border-top:solid #F7F7F7 .5pt; border-right:solid #F7F7F7 .5pt;'>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="30" colspan="4"><font color="#FF9900" size="3"><strong><font color="#666666">
                                </font></strong></font>

                        <form action="lista_eventos.php" method="post" name="formulario">
                            <table width="950" border="0" align="center" cellpadding="0" cellspacing="2">
                                <tr>
                                    <td width="179" valign="middle"><font color="#FF9900" size="3"><strong>Lista
                                                de Eventos de: </strong></font></td>
                                    <td width="271" valign="top"><font color="#FF9900" size="3"><strong><font
                                                    color="#666666">
                                                    <select name="id_depto" id="select2" style="width:250px"
                                                            onChange="fncMontacombo(this.value);">
                                                        <option value="0" selected>Todas Unidades</option>
                                                        <?php

                                                        $sql_und = "select id_depto,depto,sigla from depto where ativo = 1 order by sigla;";
                                                        $dados_und = mysqli_query( $conexao, $sql_und);
                                                        while ($resultado_und = mysqli_fetch_array($dados_und)) {
                                                            ?>
                                                            <option
                                                                value="<?php echo $resultado_und[id_depto]; ?>"><?php echo $resultado_und[depto]; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </font></strong></font></td>
                                    <td width="270" valign="top">
                                        <div id="div_id_usr">
                                            <select name="id_usr" id="id_usr" style="width:250px">
                                                <option value="0">Todos funcionários</option>
                                                <?php while ($resultado_usuarios = mysqli_fetch_array($dados_usuarios)) { ?>
                                                    <option
                                                        value="<?php echo $resultado_usuarios[id_usr]; ?>"><?php echo $resultado_usuarios[nome]; ?></option>
                                                <?php } ?>
                                            </select>
                                            <strong><font color="#666666"></font><font size="2"><font
                                                        color="#FF3300"></font></font></strong></div>

                                    </td>
                                    <td width="220" valign="middle"><a href="#" onClick="fncValida();"><img
                                                src="../images/ok_bt.gif" width="27" height="15" border="0"
                                                title="Realiza o filtro"></a></td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td width="57%" height="20" bgcolor="#F7F7F7"><strong><font color="#333333" size="2">&nbsp;&nbsp;Evento</font></strong>
                    </td>
                    <td width="25%" bgcolor="#F7F7F7"><strong><font color="#333333" size="2">Tipo</font></strong></td>
                    <td width="12%" bgcolor="#F7F7F7"><strong><font color="#333333" size="2">Data</font></strong></td>
                    <td width="6%" bgcolor="#F7F7F7"><font color="#666666">&nbsp;</font></td>
                </tr>
                <?php
                $cont = 0;
                $par = 0;
                while ($resultado = mysqli_fetch_array($dados)) {
                    $cont++;
                    $par = ($cont % 2);
                    ?>
                    <tr <?php if ($par == 0) {
                        echo "class='Itens_sel_rel'";
                    } else {
                        echo "class='Itens_normal'";
                    }
                    ?>>
                        <td height="20" valign="top"><font size="1">&nbsp; 
						<a href="#" onClick="fncConsulta(<?php echo $resultado[id_pai]; ?>)">
						<?php echo $resultado[descricao]; ?>
						&nbsp;
						<?php if($resultado['id_usr']){echo getUsuario($resultado['id_usr'])->nome;}?>
						</a>
                            </font></td>
                        <td align="left" valign="top"><font size="1"><?php echo $resultado[titulo]; ?>
                            </font></td>
                        <td valign="top"><font
                                size="1"> <?php echo date('d/m/Y', strtotime($resultado[data_evento])); ?>
                            </font></td>
                        <td valign="top"><font size="1"><a href="#"
                                                           onClick="fncExclui(<?php echo $resultado[id_pai]; ?>)">Excluir</a>
                            </font></td>
                    </tr>
                <?php } ?>
                <tr bgcolor="#F7F7F7">
                    <td height="20" colspan="4">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</form>
<BR>
<BR>
<iframe width="801" height="201" name="grava" frameborder="1" style="display:none" id="grava"></iframe>
</body>
</html>