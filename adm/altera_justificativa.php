﻿<?php
session_start();
require_once("../funcoes/conexao.php");


$qual = $_POST["qual"];
$tipo_usr = $_POST["tipo_usr"];
if ($tipo_usr == '') {
    $tipo_usr = 0;
}
$id_justificativa = $_POST["id_justificativa"];
$id_registro = $_POST["id_registro"];
$abonado = $_POST["abonado"];

$id_usr = $_POST["id_usr"];
$dia = $_POST["dia"];
$mes = $_POST["mes"];
$id_depto = $_POST["id_depto"];
$ano = $_POST["ano"];

if ($qual == 1) {
    $id_tipo_justificativa = $_POST["id_tipo_justificativa_1"];
    $qtd_horas = $_POST["qtd_horas_1"];
    $observacoes = $_POST["observacoes_1"];
    $observacoes_chefe = $_POST["observacoes_chefe_1"];
}
if ($qual == 2) {
    $id_tipo_justificativa = $_POST["id_tipo_justificativa_2"];
    $qtd_horas = $_POST["qtd_horas_2"];
    $observacoes = $_POST["observacoes_2"];
    $observacoes_chefe = $_POST["observacoes_chefe_2"];
}
if ($qual == 3) {
    $id_tipo_justificativa = $_POST["id_tipo_justificativa_3"];
    $qtd_horas = $_POST["qtd_horas_3"];
    $observacoes = $_POST["observacoes_3"];
    $observacoes_chefe = $_POST["observacoes_chefe_3"];
}
if ($qual == 4) {
    $id_tipo_justificativa = $_POST["id_tipo_justificativa_4"];
    $qtd_horas = $_POST["qtd_horas_4"];
    $observacoes = $_POST["observacoes_4"];
    $observacoes_chefe = $_POST["observacoes_chefe_4"];
}

if ($qtd_horas < 10 && strlen($qtd_horas) < 2) {
    $qtd_horas = "0" . $qtd_horas;
}
if (strlen($qtd_horas) == 2) {
    $qtd_horas = $qtd_horas . ":00";
}


$observacoes = str_replace("\n", " ", $observacoes);
$observacoes = str_replace(chr(13), "<br>", $observacoes);
$observacoes = str_replace(chr(10), " ", $observacoes);
$observacoes = str_replace("\'", "", $observacoes);
$observacoes = str_replace('\"', "", $observacoes);
$observacoes = str_replace("'", "", $observacoes);
$observacoes = str_replace('"', "", $observacoes);


$observacoes_chefe = str_replace("\n", " ", $observacoes_chefe);
$observacoes_chefe = str_replace(chr(13), "<br>", $observacoes_chefe);
$observacoes_chefe = str_replace(chr(10), " ", $observacoes_chefe);
$observacoes_chefe = str_replace("\'", "", $observacoes_chefe);
$observacoes_chefe = str_replace('\"', "", $observacoes_chefe);
$observacoes_chefe = str_replace("'", "", $observacoes_chefe);
$observacoes_chefe = str_replace('"', "", $observacoes_chefe);

$data_grade = date('Y') . "/" . date('m') . "/" . date('d') . " " . date("H") . ":" . date("i") . ":" . date("s");
$diap = date('d');
$mesp = date('m');
$anop = date('Y');
$hora = date("H");
$minuto = date("i");
$segundo = date("s");
$digito = rand(0, 100);
$protocolo = $ano . "." . $mesp . $diap . "." . $hora . $minuto . $segundo . "-" . $digito;

$sql_consulta = "select entrada_1,saida_1,entrada_2,saida_2,tipo_grade from usuarios inner join p_grade on usuarios.id_grade = p_grade.id_grade where id_usr = " . $id_usr;
$dados_consulta = mysqli_query( $conexao, $sql_consulta);
$resultado_consulta_grade = mysqli_fetch_array($dados_consulta);

$periodo_banco_1 = $resultado_consulta_grade[entrada_1];
$periodo_banco_1 = explode(':', $periodo_banco_1);
$periodo_banco_1_x = (60 * 60 * $periodo_banco_1[0]) + (60 * $periodo_banco_1[1]);

$periodo_banco_2 = $resultado_consulta_grade[saida_1];
$periodo_banco_2 = explode(':', $periodo_banco_2);
$periodo_banco_2_x = (60 * 60 * $periodo_banco_2[0]) + (60 * $periodo_banco_2[1]);

$periodo_banco_3 = $resultado_consulta_grade[entrada_2];
$periodo_banco_3 = explode(':', $periodo_banco_3);
$periodo_banco_3_x = (60 * 60 * $periodo_banco_3[0]) + (60 * $periodo_banco_3[1]);

$periodo_banco_4 = $resultado_consulta_grade[saida_2];
$periodo_banco_4 = explode(':', $periodo_banco_4);
$periodo_banco_4_x = (60 * 60 * $periodo_banco_4[0]) + (60 * $periodo_banco_4[1]);

$total_horas_periodo_1 = abs(($periodo_banco_2_x - $periodo_banco_1_x) / 60);
$total_horas_periodo_2 = abs(($periodo_banco_4_x - $periodo_banco_3_x) / 60);

if ($resultado_consulta_grade[tipo_grade] == 2) {
    $total_horas = $total_horas_periodo_1;
} else {
    $total_horas = $total_horas_periodo_1 + $total_horas_periodo_2;
}
$total_horas = ($total_horas * 3) / 60;

$sql_consulta_tipos = "select id_tipo_justificativa from p_tipo_justificativa where cota = 3";
//echo $sql_consulta_tipos;
$dados_consulta_tipos = mysqli_query( $conexao, $sql_consulta_tipos);
//$resultado_consulta_tipos	= mysql_fetch_array($dados_consulta_tipos);
while ($resultado_consulta_tipos = mysqli_fetch_array($dados_consulta_tipos)) {
    $lista_tipos = $lista_tipos . "," . $resultado_consulta_tipos[id_tipo_justificativa];
}
//echo $lista_tipos."<br>";


if (substr($lista_tipos, 0, 1) == ",") {
    $lista_tipos = substr($lista_tipos, 1, strlen($lista_tipos));
}
$total_horas_mes_total1 = 0;
$sql_consulta1 = "select qtd_horas from p_justificativa where id_usr = " . $id_usr . " and month(data_justificativa) = " . $mes . " and year(data_justificativa) = " . $ano . " and id_tipo_justificativa in(" . $lista_tipos . ")";
$dados_consulta1 = mysqli_query( $conexao, $sql_consulta1);
$total_horas_mes_total = 0;
while ($resultado_consulta1 = mysqli_fetch_array($dados_consulta1)) {
    //$resultado_consulta2	= mysql_fetch_array($dados_consulta2);
    $total_horas_banco1 = $resultado_consulta1[qtd_horas];
    $total_horas_mes1 = explode(':', $total_horas_banco1);
    $total_horas_mes_p1 = ((60 * 60 * $total_horas_mes1[0]) + (60 * $total_horas_mes1[1]));
    $total_horas_mes1 = $total_horas_mes_p1 / 60 / 60;

    $total_horas_mes_total1 = $total_horas_mes_total1 + $total_horas_mes1;
}


$sql_consulta2 = "select qtd_horas from p_justificativa where id_usr = " . $id_usr . " and month(data_justificativa) = " . $mes . " and year(data_justificativa) = " . $ano . " and id_tipo_justificativa in(" . $lista_tipos . ") and id_justificativa <> " . $id_justificativa;
//echo $sql_consulta2;
$dados_consulta2 = mysqli_query( $conexao, $sql_consulta2);
$total_horas_mes_total = 0;
while ($resultado_consulta2 = mysqli_fetch_array($dados_consulta2)) {
    //$resultado_consulta2	= mysql_fetch_array($dados_consulta2);
    $total_horas_banco = $resultado_consulta2[qtd_horas];
    $total_horas_mes = explode(':', $total_horas_banco);
    $total_horas_mes_p = ((60 * 60 * $total_horas_mes[0]) + (60 * $total_horas_mes[1]));
    $total_horas_mes = $total_horas_mes_p / 60 / 60;

    $total_horas_mes_total = $total_horas_mes_total + $total_horas_mes;
}


$total_horas_comparap = 0;
if ($id_tipo_justificativa == 12 || $id_tipo_justificativa == 12 || $id_tipo_justificativa == 29 || $id_tipo_justificativa == 30) {
    $total_horas_compara = explode(':', $qtd_horas);
    $total_horas_comparap = ((60 * 60 * $total_horas_compara[0]) + (60 * $total_horas_compara[1]));
    $total_horas_comparap = $total_horas_comparap / 60 / 60;
}


$sql_consulta_1 = "select cota from p_tipo_justificativa where id_tipo_justificativa = " . $id_tipo_justificativa;
$dados_consulta_1 = mysqli_query( $conexao, $sql_consulta_1);
$resultado_consulta_grade_1 = mysqli_fetch_array($dados_consulta_1);
$cota_1 = $resultado_consulta_grade_1[cota];


$tempox = explode('.', $total_horas_mes_total1); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
$horax = $tempox[0];
@$minutosx = (float)(0) . '.' . $tempox[1]; // Aqui forçamos a conversão para float, para não ter erro.
$minutosx = $minutosx * 60; // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.

$minutosx = explode('.', $minutosx); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
$minutosx = $minutosx[0];
if ($minutosx < 10) {
    $minutosx = "0" . $minutosx;
}

$total_acum_mostrax = $horax . ':' . $minutosx;


//echo $total_horas_mes."<BR>";
//echo $total_horas_mes_total."<BR>".$total_horas_comparap."<br>total horas: ".$total_horas;
if ($abonado == 1) {
    if ($cota_1 == 3) {
        if (($total_horas_mes_total + $total_horas_comparap) > $total_horas) {
            ?>
            <script language="JavaScript">
                alert("Quantidade de horas excedidas neste este mês para este tipo de justificativa!\n\nSeu limite é de <?php echo $total_horas?>hs.\nJá foram utilizados <?php echo $total_acum_mostrax;?>hs.");
            </script>
            <?php
            die;
        }
    }

}


if ($id_justificativa != 0) {
    if ($tipo_usr == 0) {
        $sql = "update p_justificativa set abonado = " . $abonado . ",id_tipo_justificativa = " . $id_tipo_justificativa . ",observacoes_chefe = '" . $observacoes_chefe . "',qtd_horas = '" . $qtd_horas . "',id_chefe = " . $_SESSION["sessao_id_usr"] . " where id_justificativa = " . $id_justificativa;
    } else {
        $sql = "update p_justificativa set observacoes='{$observacoes}', id_tipo_justificativa = " . $id_tipo_justificativa . ",qtd_horas = '" . $qtd_horas . "' where id_justificativa = " . $id_justificativa;
    }
    $dados = mysqli_query( $conexao, $sql);
} else {
    $sql = "insert into p_justificativa (id_tipo_justificativa,data_justificativa,id_usr,observacoes,observacoes_chefe,data_cadastro_just,qtd_horas,protocolo,who_r) values (" . $id_tipo_justificativa . ",'" . $data_grade . "'," . $id_usr . ",'" . $observacoes . "','" . $observacoes_chefe . "','" . $data_grade . "','" . $qtd_horas . "','" . $protocolo . "'," . $_SESSION["sessao_id_usr"] . ")";
    $dados = mysqli_query( $conexao, $sql);

    $sql_ultimo = "select id_justificativa from p_justificativa order by id_justificativa desc limit 0,1";
    $dados_ultimo = mysqli_query( $conexao, $sql_ultimo);
    $resultado_ultimo = mysqli_fetch_array($dados_ultimo);
    $id_justificativa = $resultado_ultimo[id_justificativa];
    $sql_atualiza = "update p_registro set id_justificativa = " . $resultado_ultimo[id_justificativa] . " where id_registro = " . $id_registro;
    $dados_atualiza = mysqli_query( $conexao, $sql_atualiza);
}


if ($_FILES['arquivo_1']['size'] != 0) {

    $tamanho_arquivo = $_FILES['arquivo_1']['size'];
    $extensao = strtolower(end(explode('.', $_FILES['arquivo_1']['name'])));

    $caminho = $_SESSION['config']->pastaAnexosJustificativas . $id_justificativa . "." . $extensao;
    if ($tamanho_arquivo <= ((int)ini_get('upload_max_filesize') * 1024 * 1024) && $extensao != "com" && $extensao != "exe" && $extensao != "php" && $extensao != "inc") {
        if (move_uploaded_file($_FILES['arquivo_1']['tmp_name'], $caminho)) {
            $sql_registra = "insert into p_anexos (id_usr,id_justificativa,data_anexo,nivel_just) values(" . $id_usr . "," . $id_justificativa . ",'" . $data_cadastro_just . "',1)";
            $dados_registra = mysqli_query( $conexao, $sql_registra);
            $sql_atualiza = "update p_justificativa set anexo = 1,ordem_justificativa = 1, nome_arquivo = '" . $id_justificativa . "." . $extensao . "',extensao = '" . $extensao . "' where id_justificativa = " . $id_justificativa;
            $dados_atualiza = mysqli_query( $conexao, $sql_atualiza);
        }
    } else {
        echo("<script>alert ('Evento cadastrado com sucesso, porém o arquivo não foi aceito!\n\nTamanho ou extensão errada!') </script>");
    }
}

if ($_FILES['arquivo_2']['size'] != 0) {

    $tamanho_arquivo = $_FILES['arquivo_2']['size'];
    $extensao = strtolower(end(explode('.', $_FILES['arquivo_2']['name'])));

    $caminho = $_SESSION['config']->pastaAnexosJustificativas . $id_justificativa . "." . $extensao;
    if ($tamanho_arquivo <= ((int)ini_get('upload_max_filesize') * 1024 * 1024) && $extensao != "com" && $extensao != "exe" && $extensao != "php" && $extensao != "inc") {
        if (move_uploaded_file($_FILES['arquivo_2']['tmp_name'], $caminho)) {
            $sql_registra = "insert into p_anexos (id_usr,id_justificativa,data_anexo,nivel_just) values(" . $id_usr . "," . $id_justificativa . ",'" . $data_cadastro_just . "',2)";
            $dados_registra = mysqli_query( $conexao, $sql_registra);
            $sql_atualiza = "update p_justificativa set anexo = 1,ordem_justificativa = 2, nome_arquivo = '" . $id_justificativa . "." . $extensao . "',extensao = '" . $extensao . "' where id_justificativa = " . $id_justificativa;
            $dados_atualiza = mysqli_query( $conexao, $sql_atualiza);
        }
    } else {
        echo("<script>alert ('Evento cadastrado com sucesso, porém o arquivo não foi aceito!\n\nTamanho ou extensão errada!') </script>");
    }
}

if ($_FILES['arquivo_3']['size'] != 0) {

    $tamanho_arquivo = $_FILES['arquivo_3']['size'];
    $extensao = strtolower(end(explode('.', $_FILES['arquivo_3']['name'])));

    $caminho = $_SESSION['config']->pastaAnexosJustificativas . $id_justificativa . "." . $extensao;
    if ($tamanho_arquivo <= ((int)ini_get('upload_max_filesize') * 1024 * 1024) && $extensao != "com" && $extensao != "exe" && $extensao != "php" && $extensao != "inc") {
        if (move_uploaded_file($_FILES['arquivo_3']['tmp_name'], $caminho)) {
            $sql_registra = "insert into p_anexos (id_usr,id_justificativa,data_anexo,nivel_just) values(" . $id_usr . "," . $id_justificativa . ",'" . $data_cadastro_just . "',3)";
            $dados_registra = mysqli_query( $conexao, $sql_registra);
            $sql_atualiza = "update p_justificativa set anexo = 1,ordem_justificativa = 3, nome_arquivo = '" . $id_justificativa . "." . $extensao . "',extensao = '" . $extensao . "' where id_justificativa = " . $id_justificativa;
            $dados_atualiza = mysqli_query( $conexao, $sql_atualiza);
        }
    } else {
        echo("<script>alert ('Evento cadastrado com sucesso, porém o arquivo não foi aceito!\n\nTamanho ou extensão errada!') </script>");
    }
}

if ($_FILES['arquivo_4']['size'] != 0) {

    $tamanho_arquivo = $_FILES['arquivo_4']['size'];
    $extensao = strtolower(end(explode('.', $_FILES['arquivo_4']['name'])));

    $caminho = $_SESSION['config']->pastaAnexosJustificativas . $id_justificativa . "." . $extensao;
    if ($tamanho_arquivo <= ((int)ini_get('upload_max_filesize') * 1024 * 1024) && $extensao != "com" && $extensao != "exe" && $extensao != "php" && $extensao != "inc") {
        if (move_uploaded_file($_FILES['arquivo_4']['tmp_name'], $caminho)) {
            $sql_registra = "insert into p_anexos (id_usr,id_justificativa,data_anexo,nivel_just) values(" . $id_usr . "," . $id_justificativa . ",'" . $data_cadastro_just . "',4)";
            $dados_registra = mysqli_query( $conexao, $sql_registra);
            $sql_atualiza = "update p_justificativa set anexo = 1,ordem_justificativa = 1, nome_arquivo = '" . $id_justificativa . "." . $extensao . "',extensao = '" . $extensao . "' where id_justificativa = " . $id_justificativa;
            $dados_atualiza = mysqli_query( $conexao, $sql_atualiza);
        }
    } else {
        echo("<script>alert ('Evento cadastrado com sucesso, porém o arquivo não foi aceito!\n\nTamanho ou extensão errada!') </script>");
    }
}

?>

<script language="JavaScript" type="text/javascript">

    alert("Dados cadastrados com sucesso!");
    window.parent.envia_atualiza_dados(<?php echo $id_depto;?>, <?php echo $id_usr;?>, <?php echo $dia;?>, <?php echo $mes;?>, <?php echo $ano;?>);
    //window.parent.opener.location.href='frm_justificativa.php';
</script>
