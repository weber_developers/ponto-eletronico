<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0) {
    header("Location: index.php");
    die;
}
//comeco o output buffer aqui
ob_start();

?><!doctype html>
<?php
require_once("../funcoes/conexao.php");
$cont = 0;

$id_depto = $_POST["id_depto"];
$mes = $_POST["mes"];
$ano = $_POST["ano"];

$dataInicio = new DateTime("{$ano}-{$mes}-01");
$dataFim = new DateTime("{$ano}-{$mes}-01");
$dataFim->modify("+1 month");
$dataFim->modify("-1 day");

$sql_depto = "select sigla,depto from depto where id_depto = " . $id_depto;
$dados_depto = mysqli_query( $conexao, $sql_depto);
$resultado_depto = mysqli_fetch_array($dados_depto);
$sql_func = "select u.*,d.depto from usuarios u inner join depto d on u.id_depto = d.id_depto where u.id_depto = '" . $id_depto . "' and u.registra = 1 and u.ativo = 1 order by u.nome";
$dados_func = mysqli_query( $conexao, $sql_func);
//$resultado_func = mysql_fetch_array($dados_func);
?>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Ficha de Departamento</title>
    <style type="text/css">
        .table td {
            font-size: 7pt !important;
            padding: 5pt;
        }

        .table th {
            font-size: 8pt !important;
            padding: 5pt;
        }

        <?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/css/print.css'); ?>
    </style>
</head>
<body>
<div style="font-size:10pt;font-weight: bold;margin:0pt;padding:0pt;" class="text-center">Boletim de Frequência
    - <?php echo getMes($mes); ?> de <?php echo $ano; ?></div>
<br/>
<table class="table border1">
    <tr>
        <th colspan="2" class="text-right">UNIDADE ADMINISTRATIVA</th>
        <th colspan="6"><?php echo $resultado_depto['depto']; ?></th>
    </tr>
    <tr class="text-center">
        <th class="col-1">N&deg;</th>
        <th class="col-5">Nome do Servidor</th>
        <th class="col-1">CPF</th>
        <th class="col-4">Cargo</th>
        <th class="col-1">Faltas</th>
        <?php if (false) { ?>
            <th class="col-3">Observações</th>
        <?php } ?>
    </tr>
    <?php while ($resultado_func = mysqli_fetch_assoc($dados_func)) {
        $totalMensal = getTotalMensal($resultado_func['id_usr'], $dataInicio->format('d/m/Y'), $dataFim->format('d/m/Y'));
        ?>
        <tr class="text-left <?php echo($_c++ % 2 == 0 ? 'bg-dark' : ''); ?>">
            <td><?php echo $_c ?></td>
            <td><?php echo $resultado_func['nome']; ?></td>
            <td><?php echo $resultado_func['id_usr']; ?></td>
            <td><?php echo $resultado_func['cargo']; ?></td>
            <td style="text-align:center;"><?php
                if ($totalMensal['totalFaltas'] != '00:00') {
                    echo getDescricaoDia($totalMensal['totalFaltas'],$resultado_func['id_usr']);
                }
                ?></td>
            <?php if (false) { ?>
                <td><?php
                    $justificativas = getJustificativas($resultado_func['id_usr'], $dataInicio->format("d/m/Y"), $dataFim->format("d/m/Y"));
                    foreach ($justificativas as $justificativa) {
                        ?>
                        <div style="width:100%;padding:0pt;margin:0pt;line-height:7pt;"
                             class="text-left"><?php echo mudaData($justificativa->data_justificativa); ?><br/>
                            <?php
                            echo $justificativa->observacoes; ?><br/>
                            <?php echo mudaData($justificativa->data_cadastro_just); ?>
                            <br/>
                        </div>
                    <?php }
                    ?></td>
            <?php } ?>
        </tr>
    <?php } ?>
    <tr>
        <td colspan="5">
            <table>
                <tr>
                    <td>
                        <br/>

                        <div>
                            <div class="text-center">MANIFESTAÇÃO CHEFIA IMEDIATA</div>
                            <p>§ 5&deg;, Art. 55, da lei 10.460 "O funcionário poderá ter abonadas até o limite de
                                3(três) faltas ao
                                serviço em cada
                                mês civil, desde que devidamente justificadas" - § 4&deg;, Art. 4 do Decreto 7.204/2011
                                "O ocupante
                                do cargo de chefia
                                ou direção que permitir ou for leniente com o descumprimento da carga horária ou da
                                assiduidade do
                                pessoal a ele imadiatamente
                                subordinado será punido com perda do cargo em comissão ou da função comissionada de que
                                for titular,
                                sem prejuízo de outras
                                sanções cabíveis, em face do regime jurídico a que estiver sujeito".</p>
                        </div>
                    </td>
                    <td>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>

                        <div class="text-center">
                            <small>Assinatura/ carimbo da Chefia Imediata</small>
                            <br/>
                            <?php echo $resultado_depto['depto']; ?><br/>
                            Data: <?php echo date('d/m/Y'); ?>
                        </div>
                    </td>
                    <td>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>

                        <div class="text-center">
                            Ciência do Chefe/Superintendente<br/>
                            Data:____/____/________
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
<?php
$html = ob_get_clean();
gerarPdf($html, ['orientation' => 'L']);
?>


<?php if (false) { ?>
    <script language="JavaScript" type="text/javascript">

        function fncJustifica(id_usr, mes, ano, id_depto) {
            document.justifica.id_usr.value = id_usr;
            document.justifica.mes.value = mes;
            document.justifica.ano.value = ano;
            document.justifica.id_depto.value = id_depto;
            document.justifica.submit();

        }
        function Imprime() {
            window.print();
        }
        function AbreJustifivativa(id_usr, dia, mes, ano) {

            //document.abre_janela2.id_justificativa.value=id_justificativa;
            document.abre_janela2.id_usr.value = id_usr;
            document.abre_janela2.dia.value = dia;
            document.abre_janela2.mes.value = mes;
            document.abre_janela2.ano.value = ano;
//    window.open("","myNewWin","width=500,height=300,toolbar=0"); 
            window.open("", "myNewWin", "");
            var a = window.setTimeout("document.abre_janela2.submit();", 500);
        }


        function fncRelatorio2(sql, sql_total) {
            document.relatorio.sql.value = sql;
            document.relatorio.sql_total.value = sql_total;
            document.relatorio.submit();
        }
        function fncEnter() {
            if (window.event.keyCode == 13) {
                fncBusca();
            }
        }
        function fncDeleta_favorito(id_favorito) {
            janela.location.href = 'exclui_favorito.php?id_favorito=' + id_favorito + '&pagina=frm_relatorios.php';
        }
        function fncBusca() {
            if (document.form_busca.texto_busca.value == '') {
                alert("Informe um texto para busca!");
                document.form_busca.texto_busca.focus();
                return false;
            }
            document.form_busca.submit();
        }

        function Abre(id_processo) {
            window.open("historico.php?id_processo=" + id_processo + '&tp=1', "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=1070,height=700,top=10,left=20");
//window.open("processo_auditor.php?id_processo="+id_processo, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=1070,height=700,top=10,left=20");
        }

        function Abre2(id_processo) {
//window.open("processo.php?id_processo="+id_processo, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=1070,height=800,top=10,left=20");
            window.showModalDialog("processo.php?id_processo=" + id_processo, "resizable: no", "dialogWidth:1070px; dialogHeight:800px; center:yes");
        }
        function fncFavoritos() {
            div_favorito.style.display = '';
            iframe_favoritos.location.href = 'favorito.php';
        }
    </script>
    <html>


    <head>
        <title>Ficha individual</title>

    </head>

    <meta name="title" content="SED - SED"/>
    <meta name="url" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>"/>
    <meta name="description"
          content="SED-GO - A SED de Goiás implementou em um projeto altamente inovador de acompanhamento de Processos totalmente digital."/>
    <meta name="keywords"
          content="sistema,ouvidoria,corregedoria,controladoria,processos,protocolo,jedi,workflow,processo eletronico,gerencial,manifestação,acompanhamento,encaminhamento,controle,pesquisa,histórico,governo de goiás,sistema de acompanhamento de processos,sistema gerencial,marcelo roncato"/>

    <meta name="autor" content="Marcelo Roncato"/>
    <meta name="company" content="SED"/>
    <meta name="revisit-after" content="5"/>
    <body bgcolor="#FFFFFF">
    <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100" colspan="2">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="2">
                            <div style="width:30%;display:inline-block;float:left;">
                                <img src="../images/brasao_p.jpg" width="60" height="80" align="absmiddle">
                            </div>
                            <div style="width:70%;display:inline-block;float:left;">
                                <h3 style="text-align: justify;">
                                    Secretaria de Desenvolvimento Econômico, Científico e Tecnológico ede Agricultura,
                                    Pecuária e Irrigação
                                </h3>
                            </div>
                        </td>
                        <td align="right"><font style='font-size:14px; font-family:Verdana, Arial'><strong>Ficha
                                    de Frequ&ecirc;ncia</strong></font></td>
                    </tr>
                    <tr>
                        <td width="58">&nbsp;</td>
                        <td colspan="2" align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="20"><strong></strong></td>
                        <td width="520" align="center">&nbsp;</td>
                        <td width="222" rowspan="3" align="right" valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="20"><strong><font
                                    style='font-size:12px; font-family: Verdana, Arial; color=#333333'>Depto:</font></strong>
                        </td>
                        <td><font color="#0066FF"
                                  style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'><strong><?php echo $resultado_depto[sigla] . " - " . $resultado_depto[depto]; ?></strong></font>
                        </td>
                    </tr>
                    <tr>
                        <td height="20"><strong><font
                                    style='font-size:12px; font-family: Verdana, Arial; color=#333333'>Per&iacute;odo</font><font
                                    style="font-size:12px" color="#333333">:&nbsp;</font></strong></td>
                        <td><strong><font color="#0066FF"> <font
                                        style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'>
                                        <?php switch ($mes) {
                                            case 1:
                                                echo "Janeiro";
                                                break;
                                            case 2:
                                                echo "Fevereiro";
                                                break;
                                            case 3:
                                                echo "Março";
                                                break;
                                            case 4:
                                                echo "Abril";
                                                break;
                                            case 5:
                                                echo "Maio";
                                                break;
                                            case 6:
                                                echo "Junho";
                                                break;
                                            case 7:
                                                echo "Julho";
                                                break;
                                            case 8:
                                                echo "Agosto";
                                                break;
                                            case 9:
                                                echo "Setembro";
                                                break;
                                            case 10:
                                                echo "Outubro";
                                                break;
                                            case 11:
                                                echo "Novembro";
                                                break;
                                            case 12:
                                                echo "Dezembro";
                                                break;
                                        }
                                        echo "/" . $ano; ?>
                                    </font></font></strong></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel"
           style='border-bottom: 1px solid #cccccc; border-top: 1px solid #cccccc;border-left: 1px solid #cccccc;border-right: 1px solid #cccccc'>
        <tr valign="bottom">
            <td height="25" bgcolor="#CCCCCC" class="Titulo_rel" style='border-bottom: 1px solid #cccccc'>
                <table width="780" border="0" cellpadding="2" cellspacing="0">
                    <tr>
                        <td width="50" height="25">
                            <div align="center"><strong><font
                                        style="font-size:10px; font-family: Verdana, Arial; color=#333333">
                                        N&ordm;</font></strong></div>
                        </td>
                        <td width="80"><strong><font style="font-size:10px; font-family: Verdana, Arial; color=#333333">Matr&iacute;cula&nbsp;</font></strong>
                        </td>
                        <td width="200"><strong><font
                                    style="font-size:10px; font-family: Verdana, Arial; color=#333333">Nome</font></strong>
                        </td>
                        <td width="150"><strong><font
                                    style="font-size:10px; font-family: Verdana, Arial; color=#333333">Cargo</font></strong>
                        </td>
                        <td width="50"><strong><font style="font-size:10px; font-family: Verdana, Arial; color=#333333">Frequ&ecirc;ncia</font></strong>
                        </td>
                        <td width="250">
                            <div align="center"><strong><font
                                        style="font-size:10px; font-family: Verdana, Arial; color=#333333">Qtd.
                                        N&atilde;o justificada</font></strong></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="Itens_rel_s_traco">
            <td height="15" valign="top">
                <table width="800" border="0" cellpadding="2" cellspacing="0">
                    <?php
                    $cont = 0;
                    while ($resultado_func = mysqli_fetch_array($dados_func)) {
                        $cont++;
                        ?>
                        <tr valign="middle">
                            <td width="50" height="22" align="center" bgcolor="#CCCCCC"
                                style='border-right: 1px solid #cccccc'>
                                <strong> <font style="font-size:10px; font-family: Verdana, Arial; color=#666666">
                                        <?php echo $cont; ?> </font></strong></td>


                            <td width="80" style="border-bottom: 1px solid #cccccc">
                                <strong><?php echo $resultado_func[matricula]; ?>
                                </strong></td>

                            <td width="200"
                                style="border-bottom: 1px solid #cccccc"><?php echo $resultado_func[nome]; ?>
                            </td>
                            <td width="150"
                                style="border-bottom: 1px solid #cccccc"><?php echo $resultado_func[cargo]; ?>
                            </td>

                            <td width="50" align="center" style="border-bottom: 1px solid #cccccc">&nbsp;</td>

                            <td width="270"
                                style="border-left: 1px solid #cccccc; padding: 5px;border-bottom: 1px solid #cccccc">
                                &nbsp;</td>

                        </tr>
                    <?php } ?>
                </table>

            </td>
        </tr>
        <tr class="Sub_titulo_rel">
            <td height="25">&nbsp;</td>
        </tr>
    </table>
    <?php ((is_null($___mysqli_res = mysqli_close($conexao))) ? false : $___mysqli_res); ?>
    <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr align="center">
            <td height="50">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr align="center" valign="top">
            <td width="800" height="30" style="border-top: 1px solid #000000"><strong><font
                        style='font-size:10px; font-family: Verdana, Arial; color=#333333'>Visto
                        do Chefe Imediato</font></strong></td>
            <td width="800">&nbsp;</td>
            <td width="800" style="border-top: 1px solid #000000"><font
                    style='font-size:10px; font-family: Verdana, Arial; color=#333333'><strong>Data</strong></font></td>
        </tr>
    </table>
    </body>
    </html>
<?php } ?>