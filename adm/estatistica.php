﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 && $_SESSION["sessao_id_usr"] == 0) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.close();
    </script>
    <?php

    die;
}

require_once("../funcoes/conexao.php");


$dia = $_POST["dia"];
if ($dia < 10) {
    $dia = "0" . $dia;
}
$mes = $_POST["mes"];
if ($mes < 10) {
    $mes = "0" . $mes;
}

$ano = $_POST["ano"];
$id_usr = $_POST["id_usr"];


$id_justificativa = $_POST["id_justificativa"];
if ($id_justificativa == '') {
    $id_justificativa = 0;
}


$sql_10 = "select count(id_justificativa) as qtd_total from p_justificativa where month(data_justificativa) = 10 and id_usr = " . $id_usr;
$dados_10 = mysqli_query( $conexao, $sql_10);
$resultado_10 = mysqli_fetch_array($dados_10);

$sql_11 = "select count(id_justificativa) as qtd_total from p_justificativa where month(data_justificativa) = 11 and id_usr = " . $id_usr;
$dados_11 = mysqli_query( $conexao, $sql_11);
$resultado_11 = mysqli_fetch_array($dados_11);

$sql_12 = "select count(id_justificativa) as qtd_total from p_justificativa where month(data_justificativa) = 12 and id_usr = " . $id_usr;
$dados_12 = mysqli_query( $conexao, $sql_12);
$resultado_12 = mysqli_fetch_array($dados_12);


$sql_10a = "select count(id_justificativa) as qtd_total from p_justificativa where month(data_justificativa) = 10 and abonado = 1 and id_usr = " . $id_usr;
$dados_10a = mysqli_query( $conexao, $sql_10a);
$resultado_10a = mysqli_fetch_array($dados_10a);

$sql_11a = "select count(id_justificativa) as qtd_total from p_justificativa where month(data_justificativa) = 11 and abonado = 1 and id_usr = " . $id_usr;
$dados_11a = mysqli_query( $conexao, $sql_11a);
$resultado_11a = mysqli_fetch_array($dados_11a);

$sql_12a = "select count(id_justificativa) as qtd_total from p_justificativa where month(data_justificativa) = 12 and abonado = 1 and id_usr = " . $id_usr;
$dados_12a = mysqli_query( $conexao, $sql_12a);
$resultado_12a = mysqli_fetch_array($dados_12a);


$sql_contador0 = "select count(id_justificativa) as qtd_total from p_justificativa where id_usr = " . $id_usr;
$dados_contador0 = mysqli_query( $conexao, $sql_contador0);
$resultado_contador0 = mysqli_fetch_array($dados_contador0);

$sql_contador1 = "select count(id_justificativa) as qtd_total from p_justificativa where abonado = 1 and id_usr = " . $id_usr;
$dados_contador1 = mysqli_query( $conexao, $sql_contador1);
$resultado_contador1 = mysqli_fetch_array($dados_contador1);

$sql_contador2 = "select count(p_justificativa.id_tipo_justificativa) as qtd_total, titulo from p_justificativa inner join p_tipo_justificativa on p_justificativa.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa  where id_usr = " . $id_usr . " group by p_justificativa.id_tipo_justificativa order by qtd_total desc limit 0,1";
$dados_contador2 = mysqli_query( $conexao, $sql_contador2);
$resultado_contador2 = mysqli_fetch_array($dados_contador2);


$sql_tipo_grade = "select tipo_grade from p_grade where id_grade = " . $_SESSION["sessao_id_grade"];
$dados_tipo_grade = mysqli_query( $conexao, $sql_tipo_grade);
$resultado_tipo_grade = mysqli_fetch_array($dados_tipo_grade);
$data_registro = $ano . "/" . $mes . "/" . $dia;

$sql_consulta_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = 1 order by etapa";
$dados_consulta_registro = mysqli_query( $conexao, $sql_consulta_registro);
$resultado_consulta_registro = mysqli_fetch_array($dados_consulta_registro);
//echo $sql_consulta_registro;
if ($resultado_consulta_registro[id_registro] == "") {
    gravarRegistro($oUsuario, [
        'data_registro' => $data_registro,
        'etapa' => 1,
        'numr_ip' => $numr_ip,
        'hora' => $hora,
        'minutos' => $minutos,
        'nao_registrou' => 1
    ]);
}

$sql_consulta_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = 2 order by etapa";
$dados_consulta_registro = mysqli_query( $conexao, $sql_consulta_registro);
$resultado_consulta_registro = mysqli_fetch_array($dados_consulta_registro);
if ($resultado_consulta_registro[id_registro] == "") {
    gravarRegistro($oUsuario, [
        'data_registro' => $data_registro,
        'etapa' => 2,
        'numr_ip' => $numr_ip,
        'hora' => $hora,
        'minutos' => $minutos,
        'nao_registrou' => 1
    ]);
}

if ($resultado_tipo_grade[tipo_grade] == 0) {
    $sql_consulta_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = 3 order by etapa";
    $dados_consulta_registro = mysqli_query( $conexao, $sql_consulta_registro);
    $resultado_consulta_registro = mysqli_fetch_array($dados_consulta_registro);
    if ($resultado_consulta_registro[id_registro] == "") {
        gravarRegistro($oUsuario, [
            'data_registro' => $data_registro,
            'etapa' => 3,
            'numr_ip' => $numr_ip,
            'hora' => $hora,
            'minutos' => $minutos,
            'nao_registrou' => 1
        ]);
    }

    $sql_consulta_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = 4 order by etapa";
    $dados_consulta_registro = mysqli_query( $conexao, $sql_consulta_registro);
    $resultado_consulta_registro = mysqli_fetch_array($dados_consulta_registro);
    if ($resultado_consulta_registro[id_registro] == "") {
        gravarRegistro($oUsuario, [
            'data_registro' => $data_registro,
            'etapa' => 4,
            'numr_ip' => $numr_ip,
            'hora' => $hora,
            'minutos' => $minutos,
            'nao_registrou' => 1
        ]);
    }
}


$sql_consulta_registro4 = "select id_justificativa from p_justificativa where day(data_justificativa) = " . $dia . " and month(data_justificativa) = " . $mes . " and year(data_justificativa) = " . $ano . " and id_usr = " . $id_usr;
$dados_consulta_registro4 = mysqli_query( $conexao, $sql_consulta_registro4);
$resultado_consulta_registro4 = mysqli_fetch_array($dados_consulta_registro4);
$id_justificativa = $resultado_consulta_registro4[id_justificativa];

if ($id_justificativa != 0) {
    $sql_tipo = "select * from  p_justificativa inner join p_tipo_justificativa on p_justificativa.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa where id_justificativa = " . $id_justificativa;

    $dados_tipo = mysqli_query( $conexao, $sql_tipo);
    $resultado_tipo = mysqli_fetch_array($dados_tipo);
    $data_justificativa = $resultado_tipo[data_justificativa];
    $observacoes = $resultado_tipo[observacoes];
    $diap = substr($data_justificativa, 8, 2);
    if ($diap != '') {
        $dia = $diap;
    }
    $mesp = substr($data_justificativa, 5, 2);
    if ($mesp != '') {
        $mes = $mesp;
    }
    $anop = substr($data_justificativa, 0, 4);
    if ($anop != '') {
        $ano = $anop;
    }

//$id_usr 		= $resultado_tipo[id_usr];
}
$sql_consulta = "select * from usuarios inner join depto on usuarios.id_depto = depto.id_depto where usuarios.id_usr = " . $id_usr;
$dados_consulta = mysqli_query( $conexao, $sql_consulta);
$resultado_consulta = mysqli_fetch_array($dados_consulta);
$nome = explode(' ', $resultado_consulta[nome]);
$foto = $resultado_consulta[foto];
$id_depto = $resultado_consulta[id_depto];


$sql_consulta_grade = "select * from p_grade where id_grade = " . $resultado_consulta[id_grade];
$dados_consulta_grade = mysqli_query( $conexao, $sql_consulta_grade);
$resultado_consulta_grade = mysqli_fetch_array($dados_consulta_grade);
$entrada_1 = $resultado_consulta_grade[entrada_1];
$saida_1 = $resultado_consulta_grade[saida_1];
$entrada_2 = $resultado_consulta_grade[entrada_2];
$saida_2 = $resultado_consulta_grade[saida_2];


?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">

<html>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Mes', 'Justificativas', 'Abonos'],
            ['Outubro', 10, 8],
            ['Novembro', 15, 14],
            ['Dezembro', 13, 13]

//		   ['Outubro',  <?php echo $resultado_10[qtd_total]?>, <?php echo $resultado_10a[qtd_total]?>],
//          ['Novembro',  <?php echo $resultado_11[qtd_total]?>, <?php echo $resultado_11a[qtd_total]?>],
//          ['Dezembro',  <?php echo $resultado_11[qtd_total]?>, <?php echo $resultado_12a[qtd_total]?>]

        ]);

        var options = {
            title: 'Gráfico de acompanhamento do funcionário (Dados Fictícios)',
            hAxis: {title: '', titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
</script>
<script language="JavaScript" type="text/javascript">

    function fncLista() {
        window.open("lista_observacoess.php", "", "");
    }
    function fncValida2(abonado) {
        document.formulario.abonado.value = abonado;
        if (document.formulario.observacoes.value == 0) {
            alert("Informe uma justificativa para este abono!");
            document.formulario.observacoes.focus();
            return false;
        }
        document.formulario.submit();
    }
    function fncValida(abonado) {
        document.formulario.abonado.value = abonado;

        if (document.formulario.horario_1.value == '') {
            alert("Informe o horário da entrada pela manhã!");
            document.formulario.horario_1.focus();
            return false;
        }
        if (document.formulario.etapa_1.value == 0) {
            alert("Selecione a etapa para o primeiro horário!");
            document.formulario.etapa_1.focus();
            return false;
        }
        if (document.formulario.horario_2.value == '') {
            alert("Informe o horário da saída pela manhã!");
            document.formulario.horario_2.focus();
            return false;
        }
        if (document.formulario.etapa_2.value == 0) {
            alert("Selecione a etapa para o segundo horário!");
            document.formulario.etapa_2.focus();
            return false;
        }
        if (document.formulario.horario_3.value == '') {
            alert("Informe o horário da entrada pela tarde!");
            document.formulario.horario_3.focus();
            return false;
        }
        if (document.formulario.etapa_3.value == 0) {
            alert("Selecione a etapa para o terceiro horário!");
            document.formulario.etapa_3.focus();
            return false;
        }
        if (document.formulario.horario_4.value == '') {
            alert("Informe o horário da saída pela tarde!");
            document.formulario.horario_4.focus();
            return false;
        }


        if (document.formulario.etapa_4.value == 0) {
            alert("Selecione a etapa para o quarto horário!");
            document.formulario.etapa_4.focus();
            return false;
        }
        if (document.formulario.observacoes.value == 0) {
            alert("Informe uma justificativa para este abono!");
            document.formulario.observacoes.focus();
            return false;
        }
        document.formulario.submit();
    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
    }

    limite = 5000;
    function soma() {
        var mais_um = eval(document.formulario.observacoes.value.length - 1);
        mais_um++;
        if (document.formulario.observacoes.value.length > limite) {
            document.formulario.observacoes.value = '';
            document.formulario.observacoes.value = valor_limite;
            alert("Limite de " + limite + " caracteres excedido!");
        } else {
//document.frmCOF.exibe.value=''; 
//document.frmCOF.exibe.value=eval(mais_um); 
            valor_limite = document.formulario.observacoes.value;
            document.formulario.exibe.value = '';
            document.formulario.exibe.value = (limite - mais_um);
        }
        document.formulario.observacoes.focus();
    }
    function txtBoxFormat(objeto, sMask, evtKeyPress) {
        var i, nCount, sValue, fldLen, mskLen, bolMask, sCod, nTecla;


        if (document.all) { // Internet Explorer
            nTecla = evtKeyPress.keyCode;
        } else if (document.layers) { // Nestcape
            nTecla = evtKeyPress.which;
        } else {
            nTecla = evtKeyPress.which;
            if (nTecla == 8) {
                return true;
            }
        }

        sValue = objeto.value;

        // Limpa todos os caracteres de formatação que
        // já estiverem no campo.
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(" ", "");
        sValue = sValue.toString().replace(" ", "");
        fldLen = sValue.length;
        mskLen = sMask.length;

        i = 0;
        nCount = 0;
        sCod = "";
        mskLen = fldLen;

        while (i <= mskLen) {
            bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ":"))
            bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

            if (bolMask) {
                sCod += sMask.charAt(i);
                mskLen++;
            }
            else {
                sCod += sValue.charAt(nCount);
                nCount++;
            }

            i++;
        }

        objeto.value = sCod;

        if (nTecla != 8) { // backspace
            if (sMask.charAt(i - 1) == "9") { // apenas números...
                return ((nTecla > 47) && (nTecla < 58));
            }
            else { // qualquer caracter...
                return true;
            }
        }


        else {
            return true;
        }
    }
</script>

<head>
    <title>Estat&iacute;stica de desempenho do funcion&aacute;rio</title>

</head>

<body>
<table width="790" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
    <tr valign="bottom">
        <td height="25" class="Titulo_rel">
            <table width="790" border="0" cellpadding="0" cellspacing="2">
                <tr>
                    <td width="194"><strong><font style="font-size:10px" color="#333333">
                                &nbsp;&nbsp;&nbsp;&nbsp;<font size="3">Estat&iacute;stica</font></font></strong></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="Itens_rel_s_traco">
        <td height="312">
            <table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="174" align="center">
                        <div align="center"><strong><font color="#333333">
                                    <?php if ($foto == 1){ ?>
                                </font></strong>

                            <div align="center"><img src="../fotos/<?php echo $id_usr . "_usr.jpg" ?>" height="160">
                                <?php } ?>
                                <?php if ($foto == 0) { ?>
                                    <img src="../images/nouser.jpg" width="150" height="150"><br>
                                    <strong>Sem Foto</strong>
                                <?php } ?>
                            </div>
                            <strong><font color="#333333"> </font></strong></div>
                    </td>
                    <td width="152" valign="top"><strong><br>
                        </strong>
                        <table width="89%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="2" align="center"><strong><font color="#0066FF" size="2">
                                            <?php echo $nome[0] . " " . $nome[count($nome) - 1]; ?></font><font
                                            color="#0066FF"><br>
                                            <font color="#FF6600"><?php echo $resultado_consulta[sigla]; ?>
                                            </font></font></strong></td>
                            </tr>
                            <tr>
                                <td width="100%" colspan="2" align="center"><font
                                        color="#666666">___________________</font></td>
                            </tr>
                            <tr>
                                <td height="25" colspan="2" align="center"><strong>Sua Grade:</strong></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center"><strong><font color="#333333">
                                            <?php
                                            echo $entrada_1 . "<br>";
                                            echo $saida_1 . "<br>";
                                            if ($resultado_consulta_grade[tipo_grade] == 0) {
                                                echo $entrada_2 . "<br>";
                                                echo $saida_2 . "<br>";
                                            }; ?>
                                        </font></strong></td>
                            </tr>
                        </table>
                        <strong><br>
                            <font color="#333333"></font></strong></td>
                    <td width="374" valign="top">
                        <div align="center">
                            <table width='314' border='0' align='right' cellpadding='0' cellspacing='0'>
                                <tr>
                                    <td>
                                        <DIV class="commit commit-tease js-details-container"><font
                                                color="#000033"><strong><font size="2">Resumo
                                                        anual deste funcion&aacute;rio</font></strong> <br>
                                                <br>
                                            </font>

                                            <DIV class="commit-meta">
                                                <table width='100%' border='0' cellspacing='0' cellpadding='2'>
                                                    <tr>
                                                        <td width="4%" align="center"><strong><font color="#666666">Justificativas:</font></strong>
                                                        </td>
                                                        <td width="96%"><strong><font
                                                                    color="#FF0000"><?php echo $resultado_contador0[qtd_total] ?></font>
                                                            </strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong><font color="#666666">Abonos:</font></strong></td>
                                                        <td><strong><font
                                                                    color="#FF0000"><?php echo $resultado_contador1[qtd_total] ?></font></strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="border-bottom: 1px solid #cccccc">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"><font color="#666666"><strong>Maior
                                                                    incid&ecirc;ncia: </strong></font></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"><strong><font
                                                                    color="#FF0000"><?php echo $resultado_contador2[titulo] ?></font></strong>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>


                    </td>
                </tr>
            </table>
            <div id="chart_div" style="width: 780px; height: 400px;"></div>

            <table width='100%' border='0' cellspacing='0' cellpadding='2'>
                <tr>
                    <td width="58%" align="center"><?php if ($_SESSION["usr_gerente"] > 0) { ?><img
                            src="../images/g3.jpg" width="50" height="46" align="absmiddle">
                            <a href="#" onClick="document.frm_depto.submit();">Visualizar estat&iacute;stica do
                                departamento</a><?php } ?></td>
                    <td width="42%">
                        <table width="124" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center">
                                    <div id="resultado1" class="tryit" onClick="document.volta.submit();">
                                        Voltar
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="Sub_titulo_rel">
        <td height="15">&nbsp;</td>
    </tr>
</table>


<form name="volta" method="post" action="mostra_justificativa.php">
    <input type="hidden" name="id_usr" value="<?php echo $id_usr; ?>">
    <input type="hidden" name="dia" value="<?php echo $dia; ?>">
    <input type="hidden" name="mes" value="<?php echo $mes; ?>">
    <input type="hidden" name="ano" value="<?php echo $ano; ?>">
    <input type="hidden" name="id_depto" value="<?php echo $id_depto; ?>">
    <input type="hidden" name="id_justificativa" value="<?php echo $id_justificativa; ?>">
</form>

<form name="frm_depto" method="post" action="estatistica_depto.php">
    <input type="hidden" name="id_depto" value="<?php echo $id_depto; ?>">

</form>


<iframe name="janela" width="800" height="200" style="display:none"></iframe>
</body>
</html>
