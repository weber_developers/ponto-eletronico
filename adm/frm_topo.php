<?php
session_start();
if ($_SESSION["sessao_id_usr"] == '') {
    ?>
    <script language="JavaScript">
        alert("Sua sessão expirou!\n\nLogue no sistema novamente!");
        window.location.href = 'http://<?php echo $_SERVER['HTTP_HOST'];?>/index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}

?>
<?php

$ipad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
$iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
$palmpre = strpos($_SERVER['HTTP_USER_AGENT'], "webOS");
$berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");

$texto = "";
if ($ipad == true) {
    $texto = " para iPad";
}
if ($iphone == true) {
    $texto = " para iPhone";
}
if ($android == true) {
    $texto = " para Android";
}
if ($palmpre == true) {
    $texto = " para Palmpre";
}
if ($berry == true) {
    $texto = " para Berry";
}
if ($ipod == true) {
    $texto = " para iPod";
}
?>
<html>
<head>
    <style type="text/css">
        <!--
        .style2 {
            color: #009966;
            font-weight: bold;
        }

        -->

    </style>

    <link rel="shortcut icon" href="../images/goias.ico" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
    <!--<link rel="shortcut icon" type="../images/x-icon" href="../images/favicon.ico">-->

    <title>Sistema de Ponto Eletr&ocirc;nico</title>
    <script language="JavaScript" type="text/JavaScript">
        <!--
        function MM_reloadPage(init) {  //reloads the window if Nav4 resized
            if (init == true) with (navigator) {
                if ((appName == "Netscape") && (parseInt(appVersion) == 4)) {
                    document.MM_pgW = innerWidth;
                    document.MM_pgH = innerHeight;
                    onresize = MM_reloadPage;
                }
            }
            else if (innerWidth != document.MM_pgW || innerHeight != document.MM_pgH) location.reload();
        }
        MM_reloadPage(true);

        function fncMural() {
            window.location.href = 'frm_mural.php';
        }

        function fncAgenda() {
            div_calendario.style.display = '';
            iframe_calendario.location.href = 'calendario.php';

        }

        //-->
    </script>
</head>


<meta name="title" content="SED - SED"/>
<meta name="url" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>"/>
<meta name="description"
      content="SED-GO - A SED de Goiás implementou em um projeto altamente inovador de acompanhamento de Processos totalmente digital."/>
<meta name="keywords"
      content="sistema,ouvidoria,corregedoria,controladoria,processos,protocolo,jedi,workflow,processo eletronico,gerencial,manifestação,acompanhamento,encaminhamento,controle,pesquisa,histórico,governo de goiás,sistema de acompanhamento de processos,sistema gerencial,marcelo roncato"/>

<meta name="autor" content="Marcelo Roncato"/>
<meta name="company" content="SED"/>
<meta name="revisit-after" content="5"/>
<body>
<div id="Layer1" style="position:absolute; left:5px; top:13px; width:1034px; height:195px; z-index:1;">
    <table width="1031" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="197" rowspan="3"><p><font size="2" face="Arial, Helvetica, sans-serif"><strong><font size="4">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="frm_Principal.php"
                                                                                               title="Principal"><img
                                        src="../images/2.png" width="100" height="100" border="0"></a></font><br>
                            &nbsp; &nbsp;Sistema de Ponto Eletr&ocirc;nico</strong></font></p></td>
            <td align="right" valign="top" height="8"></td>
            <td width="806" rowspan="3" align="right" valign="top"><font color="#006666" size="2"
                                                                         face="Arial, Helvetica, sans-serif"><strong><?php echo $_SESSION["sessao_unidade"]; ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;</strong></font></td>
        </tr>
        <tr>
            <td width="28" align="right" valign="top">&nbsp; </td>
        </tr>
        <tr>
            <td align="right">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="1030">
                    <tbody>
                    <tr>
                        <td align="left" width="439"><span class="copyright"><font color="#999999">
                                    &nbsp;&nbsp;
                                    <script language="JavaScript">
                                        <!--
                                        /* mydate      = new Date();
                                         myday       = mydate.getDay();
                                         mymonth     = mydate.getMonth();
                                         myweekday   = mydate.getDate();

                                         var hrs     = mydate.getHours();
                                         var min     = mydate.getMinutes();

                                         weekday= myweekday;

                                         if(myday == 0)
                                         day = " Domingo, "

                                         else if(myday == 1)
                                         day = " Segunda-Feira, "

                                         else if(myday == 2)
                                         day = " Ter&ccedil;a-Feira, "

                                         else if(myday == 3)
                                         day = " Quarta-Feira, "

                                         else if(myday == 4)
                                         day = " Quinta-Feira, "

                                         else if(myday == 5)
                                         day = " Sexta-Feira, "

                                         else if(myday == 6)
                                         day = " S&aacute;bado, "

                                         if(mymonth == 0)
                                         month = "Janeiro "

                                         else if(mymonth ==1)
                                         month = "Fevereiro "

                                         else if(mymonth ==2)
                                         month = "Mar&ccedil;o "

                                         else if(mymonth ==3)
                                         month = "Abril "

                                         else if(mymonth ==4)
                                         month = "Maio "

                                         else if(mymonth ==5)
                                         month = "Junho "

                                         else if(mymonth ==6)
                                         month = "Julho "

                                         else if(mymonth ==7)
                                         month = "Agosto "

                                         else if(mymonth ==8)
                                         month = "Setembro "

                                         else if(mymonth ==9)
                                         month = "Outubro "

                                         else if(mymonth ==10)
                                         month = "Novembro "

                                         else if(mymonth ==11)
                                         month = "Dezembro "

                                         document.write(day);
                                         document.write(myweekday+" de "+month+ " - "+hrs+":"+min);*/
                                        // -->
                                    </script>
                                    <?php
                                    // leitura das datas
                                    $dia = date('d');
                                    $mes = date('m');
                                    $ano = date('Y');
                                    $semana = date('w');

                                    $hora = date('H:i:s');


                                    // configuraÃ§Ã£o mes

                                    switch ($mes) {

                                        case 1:
                                            $mes = "Janeiro";
                                            break;
                                        case 2:
                                            $mes = "Fevereiro";
                                            break;
                                        case 3:
                                            $mes = "Mar&ccedil;o";
                                            break;
                                        case 4:
                                            $mes = "Abril";
                                            break;
                                        case 5:
                                            $mes = "Maio";
                                            break;
                                        case 6:
                                            $mes = "Junho";
                                            break;
                                        case 7:
                                            $mes = "Julho";
                                            break;
                                        case 8:
                                            $mes = "Agosto";
                                            break;
                                        case 9:
                                            $mes = "Setembro";
                                            break;
                                        case 10:
                                            $mes = "Outubro";
                                            break;
                                        case 11:
                                            $mes = "Novembro";
                                            break;
                                        case 12:
                                            $mes = "Dezembro";
                                            break;

                                    }


                                    // configuraÃ§Ã£o semana

                                    switch ($semana) {

                                        case 0:
                                            $semana = "Domingo";
                                            break;
                                        case 1:
                                            $semana = "Segunda-Feira";
                                            break;
                                        case 2:
                                            $semana = "Ter&ccedil;a-Feira";
                                            break;
                                        case 3:
                                            $semana = "Quarta-Feira";
                                            break;
                                        case 4:
                                            $semana = "Quinta-Feira";
                                            break;
                                        case 5:
                                            $semana = "Sexta-Feira";
                                            break;
                                        case 6:
                                            $semana = "S&aacute;bado";
                                            break;

                                    }
                                    //Agora basta imprimir na tela...
                                    print ("$semana, $dia de $mes de $ano.");
                                    ?>
                                </font> </span></td>
                        <td width="579" align="right"><font color="#999999">&nbsp;</font>&nbsp;&nbsp;<strong><font
                                    color="#999999">Bem
                                    vindo(a)<a href="frm_justificativa_teste.php">:</a></font></strong>
                            <a href="frm_Acessos.php" title="Meus acessos!"> <font color="#FF6600">
                                    <?php
                                    echo $_SESSION["sessao_usuario"];
                                    ?>
                                    &nbsp; </font> </a></span></td>
                    </tr>
                    </tbody>
                </table>
                <table width="1028" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="2"></td>
                        <td width="1028" height="65" background="../images/howtobrowser-nav-bg-hover-20090608.png">
                            <table width="100%" height="70" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="11%" height="52" align="center"><a href="frm_Principal.php"><img
                                                src="../images/1.png" title="Meus Registros"
                                                width="40" height="40" border="0"></a></td>
                                    <td width="11%" height="52" align="center"><a href="frm_justificativa.php"><img
                                                src="../images/cadastramento.png" title="Cadastro de Justificativa"
                                                width="40" height="40" border="0"></a></td>
                                    <?php if ($_SESSION["usr_gerente"] >= 1 || $_SESSION["sessao_rh"] == 1) { ?>
                                        <td width="11%" align="center"><a href="frm_Lista_ocorrencias.php"><img
                                                    src="../images/ico_processos2.png" title="Lista de Ocorrências"
                                                    width="40" height="40" border="0"></a></td>
                                        <td width="11%" align="center"><a href="frm_menu_relatorios.php"><img
                                                    src="../images/playlist.png" title="Relatórios de Ocorrências"
                                                    width="40" height="40" border="0"></a></td>
                                    <?php } ?>
                                    <td width="11%" align="center"><a href="frm_ficha_individual.php"><img
                                                src="../images/Agenda_p.jpg" title="Ficha de frequência" width="45"
                                                height="45" border="0"></a></td>
                                    <?php if ($_SESSION["usr_gerente"] >= 1 || $_SESSION["sessao_rh"] == 1) { ?>
                                        <td width="11%" align="center"><a href="frm_menu_estatistica.php"><img
                                                    src="../images/estatistica.png"
                                                    title="Relatórios de Ocorrências" width="57" height="40" border="0"></a>
                                        </td>
                                    <?php } ?>
                                    <td width="11%">
                                        <div align="center"><a href="frm_Config.php"><img src="../images/setup2.png"
                                                                                          title="Minhas configurações"
                                                                                          width="40" height="40"
                                                                                          border="0"></a></div>
                                    </td>
                                    <td width="11%">
                                        <div align="center"><a href="sair.php"><img src="../images/Exit.png"
                                                                                    title="Sair do Sistema de Ponto Eletrônico"
                                                                                    width="40" height="40"
                                                                                    border="0"></a></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="18" align="center"><font color="#333333"><strong><a
                                                    href="frm_Principal.php">Meus Registros</a></strong></font></td>
                                    <td height="18" align="center"><font color="#333333"><strong><a
                                                    href="frm_justificativa.php">Justificativas</a></strong></font></td>
                                    <?php if ($_SESSION["usr_gerente"] >= 1 || $_SESSION["sessao_rh"] == 1) { ?>
                                        <td align="center"><font color="#333333"><strong><a
                                                        href="frm_Lista_ocorrencias.php">Ocorr&ecirc;ncias</a></strong></font>
                                        </td>
                                        <td align="center"><font color="#333333"><strong><a
                                                        href="frm_menu_relatorios.php">Relat&oacute;rios</a></strong></font>
                                        </td>
                                    <?php } ?>
                                    <td align="center"><font color="#333333"><strong><a href="frm_ficha_individual.php">Ficha
                                                    de frequ&ecirc;ncia</a></strong></font></td>
                                    <?php if ($_SESSION["usr_gerente"] >= 1 || $_SESSION["sessao_rh"] == 1) { ?>
                                        <td align="center"><font color="#333333"><strong><a
                                                        href="frm_menu_estatistica.php">Estat&iacute;stica</a></strong></font>
                                        </td>
                                    <?php } ?>
                                    <td>
                                        <div align="center"><font color="#333333"><strong><a href="frm_Config.php">Configura&ccedil;&otilde;es</a></strong></font>
                                        </div>
                                    </td>
                                    <td>
                                        <div align="center"><font color="#333333"><strong><a
                                                        href="sair.php">Sair</a></strong></font></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="../images/content_top20080422.png" width="1030" height="8"></td>
    </tr>
    <tr>
        <td valign="top"><img src="../images/content_bg20080422.png" width="1030" height="208">
        </td>
    </tr>
    <tr>
        <td><img src="../images/contentfooter_bgbottom.gif" width="1030" height="10"></td>
    </tr>
</table>


</body>

</html>