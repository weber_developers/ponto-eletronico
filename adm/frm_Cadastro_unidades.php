﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 && $_SESSION["sessao_id_usr"] == 0) {
    ?>
    <script language="JavaScript">
        alert("Sessão expirada ou você não tem permissão para acessar este módulo!");
        window.location.href = 'index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}

require_once("../funcoes/conexao.php");
mysqli_select_db( $conexao, intranet) or print(mysqli_error($GLOBALS["___mysqli_ston"]));;

$id_unidade = $_GET["id_unidade"];

if ($id_unidade == '') {
    $id_unidade = 0;
    $sigla = '';
    $nome = '';
    $titular = '';
    $ddd = '';
    $fone = '';
    $email = '';

} else {

    $sql = "select * from unidades where id_unidade = " . $id_unidade;
    $dados = mysqli_query( $conexao, $sql);
    $resultado = mysqli_fetch_array($dados);
    $sigla = $resultado[sigla];
    $nome = $resultado[nome];
    $titular = $resultado[titular];
    $ddd = $resultado[ddd];
    $fone = $resultado[fone];
    $email = $resultado[email];
}

?>
<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
<html>
<head>

    <title>Cadastro de Unidades</title>
    <style type="text/css">
        <!--

        -->
    </style>
</head>

<body>
<script language="JavaScript" type="text/javascript">
    function fncMontaMunicipios(uf, ComboName, nome_div) {
        janela.location.href = 'frm_MontaMunicipios.php?uf=' + uf + '&ComboName=' + ComboName + '&nome_div=' + nome_div;
    }
    function txtBoxFormat(objeto, sMask, evtKeyPress) {
        var i, nCount, sValue, fldLen, mskLen, bolMask, sCod, nTecla;


        if (document.all) { // Internet Explorer
            nTecla = evtKeyPress.keyCode;
        } else if (document.layers) { // Nestcape
            nTecla = evtKeyPress.which;
        } else {
            nTecla = evtKeyPress.which;
            if (nTecla == 8) {
                return true;
            }
        }

        sValue = objeto.value;

        // Limpa todos os caracteres de formatação que
        // já estiverem no campo.
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(" ", "");
        sValue = sValue.toString().replace(" ", "");
        fldLen = sValue.length;
        mskLen = sMask.length;

        i = 0;
        nCount = 0;
        sCod = "";
        mskLen = fldLen;

        while (i <= mskLen) {
            bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ":"))
            bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

            if (bolMask) {
                sCod += sMask.charAt(i);
                mskLen++;
            }
            else {
                sCod += sValue.charAt(nCount);
                nCount++;
            }

            i++;
        }

        objeto.value = sCod;

        if (nTecla != 8) { // backspace
            if (sMask.charAt(i - 1) == "9") { // apenas números...
                return ((nTecla > 47) && (nTecla < 58));
            }
            else { // qualquer caracter...
                return true;
            }
        }


        else {
            return true;
        }
    }

    function fncLista() {
        window.open("lista_unidades.php", "", "");
    }


    function fncValida() {
        if (document.frm_unidades.nome.value == '') {
            alert("Informe o nome do Orgão!");
            document.frm_unidades.nome.focus();
            return false;
        }
        if (document.frm_unidades.titular.value == '') {
            alert("Informe o nome do titular deste Orgão!");
            document.frm_unidades.titular.focus();
            return false;
        }

        if (document.frm_unidades.ddd.value == '') {
            alert("Informe o ddd deste Orgão!");
            document.frm_unidades.ddd.focus();
            return false;
        }
        if (document.frm_unidades.fone.value == '') {
            alert("Informe o fone deste Orgão!");
            document.frm_unidades.fone.focus();
            return false;
        }


        if (fncValidaEmail(document.frm_unidades.email.value) == false) {
            alert("É necessário informar um endereço de e-mail válido!");
            document.frm_unidades.email.focus();
            return false;
        }

        if (fncValidaEmail(document.frm_unidades.email2.value) == false) {
            alert("É necessário informar um endereço de e-mail válido!");
            document.frm_unidades.email2.focus();
            return false;
        }

        document.frm_unidades.submit();

    }
    function fncValidaEmail(vEmail) {
        var vEmailIndex = vEmail.indexOf("@");
        var vDiv = vEmail.split("@");
        var vValAft = vEmail.substring(vEmailIndex + 1, vEmail.length);
        var vDiv2 = vValAft.split(".");
        if ((vEmailIndex == -1) || (vDiv.length < 2) || (vDiv2.length < 2)) {
            //fncEmailInvalido();
            alert("Formato do E-mail inválido!");
            return false;

        }
        for (i = 0; i < vDiv.length; i++)
            if (vDiv[i] == "") {
                alert("Formato do E-mail inválido!");
                return false;
            }

        for (i = 0; i < vDiv2.length; i++)
            if (vDiv2[i] == "") {
                alert("Formato do E-mail inválido!");
                return false;
            }
    }


</script>
<?php require_once("frm_topo.php"); ?>
<p>&nbsp;</p>
<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><strong><font color="#333333"
                                                                                                    size="4">&nbsp;
                                Cadastro de Org&atilde;os / Entidades</font></strong></td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" valign="top" bgcolor="#FFFFFF"><BR>

                        <form action="grava_unidade.php" method="post" name="frm_unidades" id="frm_unidades"
                              target="janela">
                            <table width="780" border="0" align="center" cellpadding="0" cellspacing="0"
                                   class="Tabela_rel">
                                <tr valign="bottom">
                                    <td height="40" class="Titulo_rel"><font color="#000000"
                                                                             size="2"><strong>Dados</strong></font></td>
                                </tr>
                                <tr class="Itens_rel_s_traco">
                                    <td height="15" valign="top">
                                        <table width="780" border="0" cellpadding="2">
                                            <tr>
                                                <td width="62">&nbsp;</td>
                                                <td width="704">&nbsp;</td>
                                            </tr>
                                            <tr valign="top">
                                                <td colspan="2">

                                                    <table width="750" border="0" cellspacing="0" cellpadding="2">
                                                        <tr>
                                                            <td>Sigla do Org&atilde;o:</td>
                                                            <td><input name="sigla" type="text" id="sigla"
                                                                       style="font-family: Arial; font-size: 8 pt; border: 1 solid #000000"
                                                                       size="30" maxlength="30"
                                                                       value="<?php echo $sigla ?>">
                                                                <input name="id_unidade" type="text" id="id_unidade"
                                                                       value="<?php echo $id_unidade ?>"
                                                                       style="display:none"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="115">Nome do Org&atilde;o:</td>
                                                            <td width="627"><input name="nome" type="text" id="nome3"
                                                                                   style="font-family: Arial; font-size: 8 pt; border: 1 solid #000000"
                                                                                   size="80" maxlength="100"
                                                                                   value="<?php echo $nome ?>"></td>
                                                        </tr>
                                                        <tr>

                                                            <td>Titular:</td>
                                                            <td><input name="titular" type="text" id="titular"
                                                                       style="font-family: Arial; font-size: 8 pt; border: 1 solid #000000"
                                                                       size="80" maxlength="100"
                                                                       value="<?php echo $titular ?>"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fone contato:</td>
                                                            <td><input name="ddd" type="text" id="ddd2"
                                                                       style="font-family: Arial; font-size: 8 pt; border: 1 solid #000000"
                                                                       size="5" maxlength="5"
                                                                       onKeyPress="return txtBoxFormat(this, '(999)', event);"
                                                                       value="<?php echo $ddd ?>">
                                                                <input name="fone" type="text" id="fone2"
                                                                       style="font-family: Arial; font-size: 8 pt; border: 1 solid #000000"
                                                                       size="9" maxlength="9"
                                                                       onKeyPress="return txtBoxFormat(this, '9999-9999', event);"
                                                                       value="<?php echo $fone ?>"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email:</td>
                                                            <td><input name="email" type="text" id="email2"
                                                                       style="font-family: Arial; font-size: 8 pt; border: 1 solid #000000"
                                                                       size="50" value="<?php echo $email ?>"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="Sub_titulo_rel">
                                    <td height="15">&nbsp;</td>
                                </tr>
                            </table>
                            <br>
                            <table width="371" border="0" align="left" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="56" height="20" align="right" valign="middle"><img
                                            src="../images/bg_list.gif" width="3" height="5">&nbsp;</td>
                                    <td width="315"><strong><a href="#" onClick="fncLista();">Listar
                                                todas Unidades</a></strong></td>
                                </tr>
                            </table>
                            <p>&nbsp;</p>
                            <br>

                            <p align="center">&nbsp;</p></form>
                        <table width="383" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr align="center">
                                <td>
                                    <table width="87" height="25" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="center" background="../images/button-azul.png"><strong><a
                                                        href="#" onClick="fncValida();"><font color="#333333" size="1">Enviar</font></a></strong>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table width="87" height="25" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="center" background="../images/button-azul.png"><strong><a
                                                        href="#"
                                                        onClick="window.location.href='frm_Cadastro_unidades.php';"><font
                                                            color="#333333" size="1">Limpar</font></a></strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <p>&nbsp;</p></td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Op&ccedil;&otilde;es</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <table width='100%' border='0' cellspacing='0' cellpadding='3'>
                                        <tr>
                                            <td width="4%"><img src="../images/morearrow_08c.gif" width="4"
                                                                height="7" border="0"></td>
                                            <td width="96%"><a href="frm_Acessos.php">Meus Acessos</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="frm_Config.php">Menu</a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br>
                    </td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Controle
                        Interno - SED&reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<iframe width="1" height="1" name="janela" frameborder="1" style="display:none" id="janela"></iframe>
</body>
</html>
