// JavaScript Document
/*
 Autor: Marcelo Augusto de Carvalho
 Data : 22/01/2003
 Fun��o: Criar e remover options dentro de objetos Select
 */

//---------- Este Bloco � para uso da propriedade insertAdjacentElement no MOZILA ----
if (typeof HTMLElement != "undefined" && !
        HTMLElement.prototype.insertAdjacentElement) {
    HTMLElement.prototype.insertAdjacentElement = function
        (where, parsedNode) {
        switch (where) {
            case 'beforeBegin':
                this.parentNode.insertBefore(parsedNode, this)
                break;
            case 'afterBegin':
                this.insertBefore(parsedNode, this.firstChild);
                break;
            case 'beforeEnd':
                this.appendChild(parsedNode);
                break;
            case 'afterEnd':
                if (this.nextSibling)
                    this.parentNode.insertBefore(parsedNode, this.nextSibling);
                else this.parentNode.appendChild(parsedNode);
                break;
        }
    }

    HTMLElement.prototype.insertAdjacentHTML = function
        (where, htmlStr) {
        var r = this.ownerDocument.createRange();
        r.setStartBefore(this);
        var parsedHTML = r.createContextualFragment(htmlStr);
        this.insertAdjacentElement(where, parsedHTML)
    }


    HTMLElement.prototype.insertAdjacentText = function
        (where, txtStr) {
        var parsedText = document.createTextNode(txtStr)
        this.insertAdjacentElement(where, parsedText)
    }
}
//-------------------------------------------------------------------------------


function fncSelectInsertOptions(vObject, vValue, vText, vBeforeAfter) {

    if (typeof(vObject) != "object") return false;
    var IE = (navigator.appName.toUpperCase() == "MICROSOFT INTERNET EXPLORER");
    var re = / /g;
    vBeforeAfter = vBeforeAfter + "";
    vBeforeAfter = vBeforeAfter.toUpperCase();
    vBeforeAfter = vBeforeAfter.replace(re, "");

    var vOption = document.createElement("OPTION");
    vOption.value = vValue;
    vOption.text = vText;

    if (vBeforeAfter == "BEFORE") {
        if (IE) {
            //-- Item a ser Inserido
            var vOption = new Array(vObject.options.length + 1);
            vOption[0] = document.createElement("OPTION");
            vOption[0].value = vValue;
            vOption[0].text = vText;
            //-- Guarda os Valores Existentes no Combo
            for (i = 0; i < vObject.options.length; i++) {
                vOption[i + 1] = document.createElement("OPTION");
                vOption[i + 1].value = vObject.options[i].value;
                vOption[i + 1].text = vObject.options[i].text;
            }
            //-- Remove todos os Valores do Combo
            fncSelectRemoveOptions(vObject);
            //-- Adiciona todos os Valores
            for (i = 0; i < vOption.length; i++) vObject.add(vOption[i]);
        }
        //-- MOZILA
        else vObject.insertAdjacentElement("afterBegin", vOption);
    }
    else {
        if (IE) vObject.add(vOption);
        //-- Mozila
        else vObject.insertAdjacentElement("beforeEnd", vOption);
    }

}

function fncSelectRemoveOptions(vObject, OptionIndex) {
    if (typeof(vObject) != "object") return false;
    OptionIndex += "";
    //-- Deleta Todos
    if (OptionIndex == "" || OptionIndex == "undefined") {
        while (vObject.options.length > 0)
            vObject.remove(0);
    } else if (isNaN(OptionIndex) == false) vObject.remove(OptionIndex);
}

function fncSelectValue(vObject, vValue) {
    var i;
    if (typeof(vObject) != "object") return false;
    vValue = vValue.toUpperCase();
    for (i = 0; i < vObject.options.length; i++) {
        var vCurrentValue = vObject.options[i].value.toUpperCase();
        //alert(vCurrentValue+"=="+ vValue);
        if (vCurrentValue == vValue) {
            vObject.selectedIndex = i;
            break;
        }
    }
}

function fncSelectText(vObject, vText) {
    var i;
    if (typeof(vObject) != "object") return false;
    vText = vText.toUpperCase();
    for (i = 0; i < vObject.options.length; i++) {
        var vCurrentText = vObject.options[i].text.toUpperCase();
        if (vCurrentText == vText) {
            vObject.selectedIndex = i;
            break;
        }
    }
}
