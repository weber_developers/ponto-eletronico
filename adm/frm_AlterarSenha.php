﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == '') {
    ?>
    <script language="JavaScript">
        alert("Você precisa estar logado para acessar esta página!");
        window.location.href = 'index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}





?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
<html>

<script language="JavaScript" type="text/JavaScript">
    <!--


    documentall = document.all;

    function formatamoney(c) {
        var t = this;
        if (c == undefined) c = 2;
        var p, d = (t = t.split("."))[1].substr(0, c);
        for (p = (t = t[0]).length; (p -= 3) >= 1;) {
            t = t.substr(0, p) + "." + t.substr(p);
        }
        return t + "," + d + Array(c + 1 - d.length).join(0);
    }

    String.prototype.formatCurrency = formatamoney

    function demaskvalue(valor, currency) {

// Se currency é false, retorna o valor sem apenas com os números. Se é true, os dois últimos caracteres são considerados as
// casas decimais
        var val2 = '';
        var strCheck = '0123456789';
        var len = valor.length;
        if (len == 0) {
            return 0.00;
        }

        if (currency == true) {
// Elimina os zeros à esquerda
// a variável <i> passa a ser a localização do primeiro caractere após os zeros e
// val2 contém os caracteres (descontando os zeros à esquerda)


            for (var i = 0; i < len; i++)
                if ((valor.charAt(i) != '0') && (valor.charAt(i) != ',')) break;

            for (; i < len; i++) {
                if (strCheck.indexOf(valor.charAt(i)) != -1) val2 += valor.charAt(i);
            }

            if (val2.length == 0) return "0.00";
            if (val2.length == 1)return "0.0" + val2;
            if (val2.length == 2)return "0." + val2;

            var parte1 = val2.substring(0, val2.length - 2);
            var parte2 = val2.substring(val2.length - 2);
            var returnvalue = parte1 + "." + parte2;
            return returnvalue;

        }
        else {
// currency é false: retornamos os valores COM os zeros à esquerda,
// sem considerar os últimos 2 algarismos como casas decimais

            val3 = "";
            for (var k = 0; k < len; k++) {
                if (strCheck.indexOf(valor.charAt(k)) != -1) val3 += valor.charAt(k);
            }
            return val3;
        }
    }

    function reais(obj, event) {

        var whichCode = (window.Event) ? event.which : event.keyCode;

//Executa a formatação após o backspace nos navegadores !document.all

        if (whichCode == 8 && !documentall) {

//Previne a ação padrão nos navegadores

            if (event.preventDefault) { //standart browsers
                event.preventDefault();
            } else { // internet explorer
                event.returnValue = false;
            }
            var valor = obj.value;
            var x = valor.substring(0, valor.length - 1);
            obj.value = demaskvalue(x, true).formatCurrency();
            return false;
        }

//Executa o Formata Reais e faz o format currency novamente após o backspace

        FormataReais(obj, '.', ',', event);
    } // end reais


    function backspace(obj, event) {

//Essa função basicamente altera o backspace nos input com máscara reais para os navegadores IE e opera.
//O IE não detecta o keycode 8 no evento keypress, por isso, tratamos no keydown.
//Como o opera suporta o infame document.all, tratamos dele na mesma parte do código.


        var whichCode = (window.Event) ? event.which : event.keyCode;
        if (whichCode == 8 && documentall) {
            var valor = obj.value;
            var x = valor.substring(0, valor.length - 1);
            var y = demaskvalue(x, true).formatCurrency();

            obj.value = ""; //necessário para o opera
            obj.value += y;

            if (event.preventDefault) { //standart browsers
                event.preventDefault();
            } else { // internet explorer
                event.returnValue = false;
            }
            return false;

        }// end if
    }// end backspace

    function FormataReais(fld, milSep, decSep, e) {
        var sep = 0;
        var key = '';
        var i = j = 0;
        var len = len2 = 0;
        var strCheck = '0123456789';
        var aux = aux2 = '';
        var whichCode = (window.Event) ? e.which : e.keyCode;

//if (whichCode == 8 ) return true;
//backspace - estamos tratando disso em outra função no keydown
        if (whichCode == 0) return true;
        if (whichCode == 9) return true; //tecla tab
        if (whichCode == 13) return true; //tecla enter
        if (whichCode == 16) return true; //shift internet explorer
        if (whichCode == 17) return true; //control no internet explorer
        if (whichCode == 27) return true; //tecla esc
        if (whichCode == 34) return true; //tecla end
        if (whichCode == 35) return true;//tecla end
        if (whichCode == 36) return true; //tecla home


//O trecho abaixo previne a ação padrão nos navegadores. Não estamos inserindo o caractere normalmente, mas via script


        if (e.preventDefault) { //standart browsers
            e.preventDefault()
        } else { // internet explorer
            e.returnValue = false
        }

        var key = String.fromCharCode(whichCode); // Valor para o código da Chave
        if (strCheck.indexOf(key) == -1) return false; // Chave inválida


//Concatenamos ao value o keycode de key, se esse for um número

        fld.value += key;

        var len = fld.value.length;
        var bodeaux = demaskvalue(fld.value, true).formatCurrency();
        fld.value = bodeaux;


//Essa parte da função tão somente move o cursor para o final no opera. Atualmente não existe como movê-lo no konqueror.

        if (fld.createTextRange) {
            var range = fld.createTextRange();
            range.collapse(false);
            range.select();
        }
        else if (fld.setSelectionRange) {
            fld.focus();
            var length = fld.value.length;
            fld.setSelectionRange(length, length);
        }
        return false;

    }


    //-->
</script>


<head>
    <titlePlanejamento estratégico
    </title>

</head>

<body>
<?php require_once("frm_topo.php"); ?>

<script language="JavaScript" type="text/javascript">

    function fncValida_senha() {

        if (document.Senha.txtSENHA.value == '' || document.Senha.txtSENHA_NOVA_1.value == '' || document.Senha.txtSENHA_NOVA_2.value == '') {
            alert("Você deve preencher todos os campos!");
            return false;
        }

        if (document.Senha.txtSENHA_NOVA_1.value != document.Senha.txtSENHA_NOVA_2.value) {
            alert("As senhas informadas não conferem!");
            document.Senha.txtSENHA_NOVA_1.focus();
            return false;
        }
        document.Senha.submit();
    }
</script>


<br>
<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><p><strong><font color="#333333"
                                                                                                       size="4">
                                    &nbsp; Alterar Senha<br>
                                    &nbsp;</font><font color="#333333">&nbsp; <font color="#666666">Perfil
                                        de:</font> <font color="#FF6600">
                                        <?php
                                        echo $_SESSION["usuario"]; ?>
                                    </font></span></font></strong></p></td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" valign="top" bgcolor="#FFFFFF"><p><strong><font color="#666666">&nbsp;
                                    <br>
                                    &nbsp;&nbsp;&nbsp;<img src="../images/morearrow_08c.gif" width="4" height="7"
                                                           border="0">
                                    Configura&ccedil;&otilde;es \ Alterar Senha<br>
                                    <br>
                                </font></strong></p>

                        <form action="frm_Grava_senha.php" method="post" name="Senha" target="grava_senha" id="Diaria">
                            <table width="453" border="0" cellspacing="0" cellpadding="5">
                                <tr>
                                    <td width="3">&nbsp;</td>
                                    <td width="109">Senha atual:</td>
                                    <td width="95"><input name="txtSENHA" type="password" id="txtSENHA" value=""
                                                          size="10"></td>
                                    <td width="222">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Senha Nova:</td>
                                    <td><input name="txtSENHA_NOVA_1" type="password" id="txtSENHA_NOVA_1" size="10"
                                               maxlength="15"></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Senha Nova (<em>novamente</em>):</td>
                                    <td><input name="txtSENHA_NOVA_2" type="password" id="txtSENHA_NOVA_2" size="10"
                                               maxlength="15"></td>
                                    <td>
                                        <table width="87" height="25" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="center">
                                                    <div id="resultado1" class="tryit" onClick="fncValida_senha();">
                                                        <div align="center">Confirma</div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <p>&nbsp;</p></form>
                        <table width="762" border="0" cellspacing="0" cellpadding="2">
                            <tr>
                                <td width="1">&nbsp;</td>
                                <td width="10"><img src="../images/read-16x16.gif"></td>
                                <td width="717"><font color="#FF0000"><strong>Ao efetuar a altera&ccedil;&atilde;o
                                            de sua senha, ser&aacute; alterado para todos os sistemas que
                                            utilizam a mesma.</strong></font></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><img src="../images/read-16x16.gif"></td>
                                <td>Verifique o teclado se o <strong>Caps Lock </strong>est&aacute;
                                    ligado, pois o sistema diferencia mai&uacute;sculas de min&uacute;sculas.
                                </td>
                            </tr>
                        </table>
                        <p>&nbsp; </p>
                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td width="96%">&nbsp;</td>
                                <td width="4%">
                                    <div align="center"><a href="javascript:history.go(-1)"><img
                                                src="../images/reply-16x16.gif" title="Voltar" width="16"
                                                height="16" border="0"></a></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Op&ccedil;&otilde;es</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <table width='100%' border='0' cellspacing='0' cellpadding='3'>
                                        <tr>
                                            <td width="4%"><img src="../images/morearrow_08c.gif" width="4"
                                                                height="7" border="0"></td>
                                            <td width="96%"><a href="frm_Config.php">Menu</a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br>
                    </td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                        de Ponto Eletrônico <?=date('Y')?> - SED&reg;<br>
                        <strong></strong></td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p>&nbsp; </p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<iframe width="801" height="201" name="grava_senha" frameborder="1" style="display:none"></iframe>

</body>
</html>
