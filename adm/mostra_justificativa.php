﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 && $_SESSION["sessao_id_usr"] == 0) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.close();
    </script>
    <?php

    die;
}

require_once("../funcoes/conexao.php");


$id_usr = $_POST["id_usr"];
$dia = $_POST["dia"];
$id_depto = $_POST["id_depto"];

if ($dia < 10) {
    $dia = "0" . $dia;
}
$mes = $_POST["mes"];
if ($mes < 10) {
    $mes = "0" . $mes;
}

$ano = $_POST["ano"];

$sql_tipo_grade = "select tipo_grade from p_grade where id_grade = " . $_SESSION["sessao_id_grade"];
$dados_tipo_grade = mysqli_query( $conexao, $sql_tipo_grade);
$resultado_tipo_grade = mysqli_fetch_array($dados_tipo_grade);
$data_registro = $ano . "/" . $mes . "/" . $dia;
$tipo_grade = $resultado_tipo_grade[tipo_grade];

$sql_consulta_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = 1 order by etapa";
$dados_consulta_registro = mysqli_query( $conexao, $sql_consulta_registro);
$resultado_consulta_registro = mysqli_fetch_array($dados_consulta_registro);
if ($resultado_consulta_registro[id_registro] == "") {
    $sql_insere_registro = "insert into p_registro (id_usr,data_registro,etapa,numr_ip,hora,minutos,nao_registrou) values (" . $id_usr . ",'" . $data_registro . "',1,'" . $numr_ip . "','','',1)";
    $dados_insere_registro = mysqli_query( $conexao, $sql_insere_registro);
}

$sql_consulta_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = 2 order by etapa";
$dados_consulta_registro = mysqli_query( $conexao, $sql_consulta_registro);
$resultado_consulta_registro = mysqli_fetch_array($dados_consulta_registro);
if ($resultado_consulta_registro[id_registro] == "") {
    $sql_insere_registro = "insert into p_registro (id_usr,data_registro,etapa,numr_ip,hora,minutos,nao_registrou) values (" . $id_usr . ",'" . $data_registro . "',2,'" . $numr_ip . "','','',1)";
    $dados_insere_registro = mysqli_query( $conexao, $sql_insere_registro);
    //echo $sql_insere_registro;
}

if ($resultado_tipo_grade[tipo_grade] == 0) {
    $sql_consulta_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = 3 order by etapa";
    $dados_consulta_registro = mysqli_query( $conexao, $sql_consulta_registro);
    $resultado_consulta_registro = mysqli_fetch_array($dados_consulta_registro);
    if ($resultado_consulta_registro[id_registro] == "") {
        $sql_insere_registro = "insert into p_registro (id_usr,data_registro,etapa,numr_ip,hora,minutos,nao_registrou) values (" . $id_usr . ",'" . $data_registro . "',3,'" . $numr_ip . "','','',1)";
        $dados_insere_registro = mysqli_query( $conexao, $sql_insere_registro);
    }

    $sql_consulta_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = 4 order by etapa";
    $dados_consulta_registro = mysqli_query( $conexao, $sql_consulta_registro);
    $resultado_consulta_registro = mysqli_fetch_array($dados_consulta_registro);
    if ($resultado_consulta_registro[id_registro] == "") {
        $sql_insere_registro = "insert into p_registro (id_usr,data_registro,etapa,numr_ip,hora,minutos,nao_registrou) values (" . $id_usr . ",'" . $data_registro . "',4,'" . $numr_ip . "','','',1)";
        $dados_insere_registro = mysqli_query( $conexao, $sql_insere_registro);
    }
}


$sql_consulta = "select * from usuarios inner join depto on usuarios.id_depto = depto.id_depto where usuarios.id_usr = " . $id_usr;
$dados_consulta = mysqli_query( $conexao, $sql_consulta);
$resultado_consulta = mysqli_fetch_array($dados_consulta);
$nome = explode(' ', $resultado_consulta[nome]);
$foto = $resultado_consulta[foto];
$id_depto = $resultado_consulta[id_depto];

$sql_consulta_grade = "select * from p_grade where id_grade = " . $resultado_consulta[id_grade];
$dados_consulta_grade = mysqli_query( $conexao, $sql_consulta_grade);
$resultado_consulta_grade = mysqli_fetch_array($dados_consulta_grade);
$entrada_1 = $resultado_consulta_grade[entrada_1];
$saida_1 = $resultado_consulta_grade[saida_1];
$entrada_2 = $resultado_consulta_grade[entrada_2];
$saida_2 = $resultado_consulta_grade[saida_2];

$sql_tipo_1 = "select * from  p_tipo_justificativa where lancamento = 1 and exibe = 1 order by titulo";
$dados_tipo_1 = mysqli_query( $conexao, $sql_tipo_1);

$sql_tipo_2 = "select * from  p_tipo_justificativa where lancamento = 1 and exibe = 1 order by titulo";
$dados_tipo_2 = mysqli_query( $conexao, $sql_tipo_2);

$sql_tipo_3 = "select * from  p_tipo_justificativa where lancamento = 1 and exibe = 1 order by titulo";
$dados_tipo_3 = mysqli_query( $conexao, $sql_tipo_3);

$sql_tipo_4 = "select * from  p_tipo_justificativa where lancamento = 1 and exibe = 1 order by titulo";
$dados_tipo_4 = mysqli_query( $conexao, $sql_tipo_4);



?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">

<html>
<script type="text/javascript" src="../js/jquery-2.1.3.min.js"></script>
<script language="JavaScript" type="text/javascript">
    function fecha1() {
        window.opener.envia_dados(<?php echo $id_depto;?>, <?php echo $id_usr;?>, <?php echo $mes;?>, <?php echo $ano;?>, 1);
        window.close();
    }
    function envia_atualiza_dados(id_depto, id_usr, dia, mes, ano) {
        document.atualiza_form.id_depto.value = id_depto;
        document.atualiza_form.id_usr.value = id_usr;
        document.atualiza_form.dia.value = dia;
        document.atualiza_form.mes.value = mes;
        document.atualiza_form.ano.value = ano;
        document.atualiza_form.submit();
    }
    function fncExcluiDecisao(id_usr, id_depto, id_justificativa) {
        document.formularioAltera.id_usr.value = id_usr;
        document.formularioAltera.id_depto.value = id_depto;
        document.formularioAltera.id_justificativa.value = id_justificativa;
        window.open("", "myNewWinFoto", "");
        var a = window.setTimeout("document.formularioAltera.submit();", 500);

    }
    function fncAlteraJustificativa(qual, id_justificativa) {
        if (temExcesso()) {
            alert('Horas justificadas excedem a quantidade na grade!');
            return false;
        }
        if (qual == 1) {
            if (document.formulario.id_tipo_justificativa_1.value == 0) {
                alert("Selecione o tipo de justificativa para este registro!");
                document.formulario.id_tipo_justificativa_1.focus();
                return false;
            }
            if (document.formulario.qtd_horas_1.value == '') {
                alert("Informa a quantidade de horas para este registro!");
                document.formulario.qtd_horas_1.focus();
                return false;
            }
            <?php if($podeEditar){?>
            if (document.formulario.observacoes_1.value == '') {
                alert("Informe uma justificativa do funcionário para este registro!");
                document.formulario.observacoes_1.focus();
                return false;
            }
            <?php } ?>
        }

        if (qual == 2) {
            if (document.formulario.id_tipo_justificativa_2.value == 0) {
                alert("Selecione o tipo de justificativa para este registro!");
                document.formulario.id_tipo_justificativa_2.focus();
                return false;
            }
            if (document.formulario.qtd_horas_2.value == '') {
                alert("Informa a quantidade de horas para este registro!");
                document.formulario.qtd_horas_2.focus();
                return false;
            }
            <?php if($podeEditar){?>
            if (document.formulario.observacoes_2.value == '') {
                alert("Informe uma justificativa do funcionário para este registro!");
                document.formulario.observacoes_2.focus();
                return false;
            }
            <?php } ?>
        }

        if (qual == 3) {
            if (document.formulario.id_tipo_justificativa_3.value == 0) {
                alert("Selecione o tipo de justificativa para este registro!");
                document.formulario.id_tipo_justificativa_3.focus();
                return false;
            }
            if (document.formulario.qtd_horas_3.value == '') {
                alert("Informa a quantidade de horas para este registro!");
                document.formulario.qtd_horas_3.focus();
                return false;
            }
            <?php if($podeEditar){?>
            if (document.formulario.observacoes_3.value == '') {
                alert("Informe uma justificativa do funcionário para este registro!");
                document.formulario.observacoes_3.focus();
                return false;
            }
            <?php } ?>
        }


        if (qual == 4) {
            if (document.formulario.id_tipo_justificativa_4.value == 0) {
                alert("Selecione o tipo de justificativa para este registro!");
                document.formulario.id_tipo_justificativa_4.focus();
                return false;
            }
            if (document.formulario.qtd_horas_4.value == '') {
                alert("Informa a quantidade de horas para este registro!");
                document.formulario.qtd_horas_4.focus();
                return false;
            }
            <?php if($podeEditar){?>
            if (document.formulario.observacoes_4.value == '') {
                alert("Informe uma justificativa do funcionário para este registro!");
                document.formulario.observacoes_4.focus();
                return false;
            }
            <?php } ?>
        }
        document.formulario.tipo_usr.value = 1;
        document.formulario.qual.value = qual;
        document.formulario.id_justificativa.value = id_justificativa;
        document.formulario.submit();

    }

    function temExcesso() {
        var minutosTrabalhados = parseInt('<?php echo getMinutos(getHorasTrabalhadas(getUsuario()->id_usr,"{$dia}/{$mes}/{$ano}"))?>');
        var minutos = 0;
        var minutosGrade = parseInt('<?php echo getMinutos(getHorasGrade(getUsuario()->id_grade));?>');
        $('input[id^=qtd_horas]').each(function () {
            arr = $(this).val().split(/:/);
            minutos += parseInt(arr[0]) * 60 + parseInt(arr[1]);
        });
        if (minutosTrabalhados > minutosGrade) {
            return false
        } else {
            return minutos > minutosGrade;
        }
    }

    function fncValida_chefe(qual, id_justificativa, abonado, id_registro) {

        if (temExcesso()) {
            alert('Horas justificadas excedem a quantidade de <?php echo getHorasGrade(getUsuario()->id_grade);?> horas definidas na grade!');
            return false;
        }

        if (qual == 1) {
            if (document.formulario.id_tipo_justificativa_1.value == 0) {
                alert("Selecione o tipo de justificativa para este registro!");
                document.formulario.id_tipo_justificativa_1.focus();
                return false;
            }
            if (document.formulario.qtd_horas_1.value == '') {
                alert("Informa a quantidade de horas para este registro!");
                document.formulario.qtd_horas_1.focus();
                return false;
            }
            <?php if($podeEditar){?>
            if (document.formulario.observacoes_1.value == '') {
                alert("Informe uma justificativa do funcionário para este registro!");
                document.formulario.observacoes_1.focus();
                return false;
            }
            <?php } ?>
            if (document.formulario.observacoes_chefe_1.value == '') {
                alert("Informe uma justificativa para este registro!");
                document.formulario.observacoes_chefe_1.focus();
                return false;
            }

        }
        if (qual == 2) {
            if (document.formulario.id_tipo_justificativa_2.value == 0) {
                alert("Selecione o tipo de justificativa para este registro!");
                document.formulario.id_tipo_justificativa_2.focus();
                return false;
            }
            if (document.formulario.qtd_horas_2.value == '') {
                alert("Informa a quantidade de horas para este registro!");
                document.formulario.qtd_horas_2.focus();
                return false;
            }
            <?php if($podeEditar){?>
            if (document.formulario.observacoes_2.value == '') {
                alert("Informe uma justificativa do funcionário para este registro!");
                document.formulario.observacoes_2.focus();
                return false;
            }
            <?php } ?>
            if (document.formulario.observacoes_chefe_2.value == '') {
                alert("Informe uma justificativa para este registro!");
                document.formulario.observacoes_chefe_2.focus();
                return false;
            }

        }
        if (qual == 3) {
            if (document.formulario.id_tipo_justificativa_3.value == 0) {
                alert("Selecione o tipo de justificativa para este registro!");
                document.formulario.id_tipo_justificativa_3.focus();
                return false;
            }
            if (document.formulario.qtd_horas_3.value == '') {
                alert("Informa a quantidade de horas para este registro!");
                document.formulario.qtd_horas_3.focus();
                return false;
            }
            <?php if($podeEditar){?>
            if (document.formulario.observacoes_3.value == '') {
                alert("Informe uma justificativa do funcionário para este registro!");
                document.formulario.observacoes_3.focus();
                return false;
            }
            <?php } ?>
            if (document.formulario.observacoes_chefe_3.value == '') {
                alert("Informe uma justificativa para este registro!");
                document.formulario.observacoes_chefe_3.focus();
                return false;
            }

        }

        if (qual == 4) {
            if (document.formulario.id_tipo_justificativa_4.value == 0) {
                alert("Selecione o tipo de justificativa para este registro!");
                document.formulario.id_tipo_justificativa_4.focus();
                return false;
            }
            if (document.formulario.qtd_horas_4.value == '') {
                alert("Informa a quantidade de horas para este registro!");
                document.formulario.qtd_horas_4.focus();
                return false;
            }
            <?php if($podeEditar){?>
            if (document.formulario.observacoes_4.value == '') {
                alert("Informe uma justificativa do funcionário para este registro!");
                document.formulario.observacoes_4.focus();
                return false;
            }
            <?php } ?>
            if (document.formulario.observacoes_chefe_4.value == '') {
                alert("Informe uma justificativa para este registro!");
                document.formulario.observacoes_chefe_4.focus();
                return false;
            }

        }


        document.formulario.id_registro.value = id_registro;
        document.formulario.qual.value = qual;
        document.formulario.abonado.value = abonado;
        document.formulario.id_justificativa.value = id_justificativa;

        document.formulario.submit();
    }

    function AbreFoto(id_registro) {

        document.abre_foto.id_registro.value = id_registro;
        window.open("", "myNewWinFoto", "");
        var a = window.setTimeout("document.abre_foto.submit();", 500);
    }
    function fncLista() {
        window.open("lista_observacoess.php", "", "");
    }
    function fncValida() {

        if (document.formulario.id_tipo_justificativa_1.value == 0) {
            alert("Selecione o tipo desta justificativa!");
            document.formulario.id_tipo_justificativa_1.focus();
            return false;
        }
        if (document.formulario.observacoes_1.value == '') {
            alert("Informe a justificativa!");
            document.formulario.observacoes_1.focus();
            return false;
        }

        if (document.formulario.id_tipo_justificativa_2.value == 0 && document.formulario.observacoes_2.value != '') {
            alert("Selecione o tipo desta justificativa!");
            document.formulario.id_tipo_justificativa_2.focus();
            return false;
        }
        if (document.formulario.id_tipo_justificativa_2.value != 0 && document.formulario.observacoes_2.value == '') {
            alert("Selecione a justificativa!");
            document.formulario.observacoes_2.focus();
            return false;
        }

        if (document.formulario.id_tipo_justificativa_3.value == 0 && document.formulario.observacoes_3.value != '') {
            alert("Selecione o tipo desta justificativa!");
            document.formulario.id_tipo_justificativa_3.focus();
            return false;
        }
        if (document.formulario.id_tipo_justificativa_3.value != 0 && document.formulario.observacoes_3.value == '') {
            alert("Selecione a justificativa!");
            document.formulario.observacoes_3.focus();
            return false;
        }

        if (document.formulario.id_tipo_justificativa_4.value == 0 && document.formulario.observacoes_4.value != '') {
            alert("Selecione o tipo desta justificativa!");
            document.formulario.id_tipo_justificativa_4.focus();
            return false;
        }
        if (document.formulario.id_tipo_justificativa_4.value != 0 && document.formulario.observacoes_4.value == '') {
            alert("Selecione a justificativa!");
            document.formulario.observacoes_4.focus();
            return false;
        }

        if (document.formulario.id_tipo_justificativa_1.value != 0 || document.formulario.observacoes_1.value != '') {
            if (document.formulario.qtd_horas_1.value == '') {
                alert("Informe a quantidade de horas que deseja justificar!");
                document.formulario.qtd_horas_1.focus();
                return false;
            }
        }
        if (document.formulario.id_tipo_justificativa_2.value != 0 || document.formulario.observacoes_2.value != '') {
            if (document.formulario.qtd_horas_2.value == '') {
                alert("Informe a quantidade de horas que deseja justificar!");
                document.formulario.qtd_horas_2.focus();
                return false;
            }
        }
        if (document.formulario.id_tipo_justificativa_3.value != 0 || document.formulario.observacoes_3.value != '') {
            if (document.formulario.qtd_horas_3.value == '') {
                alert("Informe a quantidade de horas que deseja justificar!");
                document.formulario.qtd_horas_3.focus();
                return false;
            }
        }

        if (document.formulario.id_tipo_justificativa_4.value != 0 || document.formulario.observacoes_4.value != '') {
            if (document.formulario.qtd_horas_4.value == '') {
                alert("Informe a quantidade de horas que deseja justificar!");
                document.formulario.qtd_horas_4.focus();
                return false;
            }
        }


        document.formulario.submit();
    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
    }

    limite_1 = 5000;
    function soma_1() {
        var mais_um_1 = eval(document.formulario.observacoes_1.value.length - 1);
        mais_um_1++;
        if (document.formulario.observacoes_1.value.length > limite_1) {
            document.formulario.observacoes_1.value = '';
            document.formulario.observacoes_1.value = valor_limite_1;
            alert("Limite de " + limite + " caracteres excedido!");
        } else {
//document.frmCOF.exibe.value=''; 
//document.frmCOF.exibe.value=eval(mais_um); 
            valor_limite_1 = document.formulario.observacoes_1.value;
            document.formulario.exibe_1.value = '';
            document.formulario.exibe_1.value = (limite_1 - mais_um_1);
        }
        document.formulario.observacoes_1.focus();
    }

    limite_2 = 5000;
    function soma_2() {
        var mais_um_2 = eval(document.formulario.observacoes_2.value.length - 1);
        mais_um_2++;
        if (document.formulario.observacoes_2.value.length > limite_2) {
            document.formulario.observacoes_2.value = '';
            document.formulario.observacoes_2.value = valor_limite_2;
            alert("Limite de " + limite + " caracteres excedido!");
        } else {
//document.frmCOF.exibe.value=''; 
//document.frmCOF.exibe.value=eval(mais_um); 
            valor_limite_2 = document.formulario.observacoes_2.value;
            document.formulario.exibe_2.value = '';
            document.formulario.exibe_2.value = (limite_2 - mais_um_2);
        }
        document.formulario.observacoes_2.focus();
    }


    limite_3 = 5000;
    function soma_3() {
        var mais_um_3 = eval(document.formulario.observacoes_3.value.length - 1);
        mais_um_3++;
        if (document.formulario.observacoes_3.value.length > limite_3) {
            document.formulario.observacoes_3.value = '';
            document.formulario.observacoes_3.value = valor_limite_3;
            alert("Limite de " + limite + " caracteres excedido!");
        } else {
//document.frmCOF.exibe.value=''; 
//document.frmCOF.exibe.value=eval(mais_um); 
            valor_limite_3 = document.formulario.observacoes_3.value;
            document.formulario.exibe_3.value = '';
            document.formulario.exibe_3.value = (limite_3 - mais_um_3);
        }
        document.formulario.observacoes_3.focus();
    }


    limite_4 = 5000;
    function soma_4() {
        var mais_um_4 = eval(document.formulario.observacoes_4.value.length - 1);
        mais_um_4++;
        if (document.formulario.observacoes_4.value.length > limite_4) {
            document.formulario.observacoes_4.value = '';
            document.formulario.observacoes_4.value = valor_limite_4;
            alert("Limite de " + limite + " caracteres excedido!");
        } else {
//document.frmCOF.exibe.value=''; 
//document.frmCOF.exibe.value=eval(mais_um); 
            valor_limite_4 = document.formulario.observacoes_4.value;
            document.formulario.exibe_4.value = '';
            document.formulario.exibe_4.value = (limite_4 - mais_um_4);
        }
        document.formulario.observacoes_4.focus();
    }


    function txtBoxFormat(objeto, sMask, evtKeyPress) {
        var i, nCount, sValue, fldLen, mskLen, bolMask, sCod, nTecla;


        if (document.all) { // Internet Explorer
            nTecla = evtKeyPress.keyCode;
        } else if (document.layers) { // Nestcape
            nTecla = evtKeyPress.which;
        } else {
            nTecla = evtKeyPress.which;
            if (nTecla == 8) {
                return true;
            }
        }

        sValue = objeto.value;

        // Limpa todos os caracteres de formatação que
        // já estiverem no campo.
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(" ", "");
        sValue = sValue.toString().replace(" ", "");
        fldLen = sValue.length;
        mskLen = sMask.length;

        i = 0;
        nCount = 0;
        sCod = "";
        mskLen = fldLen;

        while (i <= mskLen) {
            bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ":"))
            bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

            if (bolMask) {
                sCod += sMask.charAt(i);
                mskLen++;
            }
            else {
                sCod += sValue.charAt(nCount);
                nCount++;
            }

            i++;
        }

        objeto.value = sCod;

        if (nTecla != 8) { // backspace
            if (sMask.charAt(i - 1) == "9") { // apenas números...
                return ((nTecla > 47) && (nTecla < 58));
            }
            else { // qualquer caracter...
                return true;
            }
        }


        else {
            return true;
        }
    }
</script>

<head>
    <title>Justificativa de Ocorr&ecirc;ncia</title>

</head>

<body>
<table width="898" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
    <tr valign="bottom">
        <td height="25" class="Titulo_rel">
            <table width="790" border="0" cellpadding="0" cellspacing="2">
                <tr>
                    <td width="194"><strong><font style="font-size:10px" color="#333333">
                                &nbsp;&nbsp;&nbsp;&nbsp;<font size="3">Justificativa</font></font></strong></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="Itens_rel_s_traco">
        <td height="15">
            <form action="altera_justificativa.php" method="post" name="formulario" id="formulario"
                  enctype="multipart/form-data" target="janela">
                <table border="0" align="center" cellpadding="2" cellspacing="0">
                    <tr>
                        <td colspan="2" style="border-bottom: 1px solid #cccccc">
                            <table width="890" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="185" align="center">
                                        <div align="center">
                                            <?php if (file_exists($_SESSION['config']->pastaFotosUsuarios . $id_usr . '.jpg')) { ?>
                                                <img style="height:150px"
                                                     src="<?php echo data_uri($_SESSION['config']->pastaFotosUsuarios . $id_usr . '.jpg'); ?>"/>
                                            <?php } ?>
                                        </div>
                                    </td>
                                    <td width="360" valign="top"><strong><br>
                                        </strong>
                                        <table width="89%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td colspan="2" align="center"><strong><font color="#0066FF" size="2">
                                                            <?php echo $nome[0] . " " . $nome[count($nome) - 1]; ?></font><font
                                                            color="#0066FF"><br>
                                                            <font
                                                                color="#FF6600"><?php echo $resultado_consulta[sigla]; ?>
                                                            </font></font></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" colspan="2" align="center"><font color="#666666">___________________</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="25" colspan="2" align="center"><strong>Sua
                                                        Grade:</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center"><strong><font color="#333333">
                                                            <?php
                                                            echo $entrada_1 . "<br>";
                                                            echo $saida_1 . "<br>";
                                                            if ($resultado_consulta_grade[tipo_grade] == 0) {
                                                                echo $entrada_2 . "<br>";
                                                                echo $saida_2 . "<br>";
                                                            }; ?>
                                                        </font></strong></td>
                                            </tr>
                                        </table>
                                        <strong><br>
                                            <font color="#333333"></font></strong></td>
                                    <td width="360" valign="top">
                                        <table width="340" height="57" border="0" align="center" cellpadding="5"
                                               cellspacing="0" class="Tabela_rel">
                                            <tr>
                                                <td colspan="4" align="center"><strong>Hor&aacute;rios
                                                        registrados em <font
                                                            color="#FF0000"><?php echo $dia . "/" . $mes . "/" . $ano; ?></font></strong>
                                                </td>
                                            </tr>
                                            <tr><br>
                                                <?php
                                                for ($i = 1; $i < 5; $i++) {
                                                    $sql_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = " . $i . " order by id_registro";


                                                    $dados_registro = mysqli_query( $conexao, $sql_registro);
                                                    $resultado_registro = mysqli_fetch_array($dados_registro);
                                                    ?>
                                                    <td width="64" align="center" valign="top">
                                                        <?php if ($resultado_registro[hora] != "") { ?>
                                                            <img src="../images/timthumb.png" width="64"
                                                                 height="64"><br>
                                                        <?php } ?>
                                                        <?php if ($resultado_registro[hora] == "") { ?>
                                                            <img src="../images/notime.png" width="64" height="64">
                                                            <br>
                                                        <?php } ?>
                                                        <br> <font color="#FF3300"><strong><font size="3">
                                                                    <?php
                                                                    if ($resultado_registro[hora] != "") {
                                                                        echo $resultado_registro[hora] . ":" . $resultado_registro[minutos];
                                                                    }
                                                                    ?>
                                                                </font></strong></font></td>
                                                <?php } ?>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td width="830"><input name="id_usr" type="text" id="id_usr" value="<?php echo $id_usr ?>"
                                               style="display:none">
                            <input name="dia" type="text" id="dia" value="<?php echo $dia ?>" style="display:none">
                            <input name="mes" type="text" id="mes" value="<?php echo $mes ?>" style="display:none">
                            <input name="ano" type="text" id="ano" value="<?php echo $ano ?>" style="display:none">
                            <input name="qual" type="text" style="display:none" size="13">
                            <input name="abonado" type="text" style="display:none" size="13">
                            <input name="id_justificativa" type="text" style="display:none" size="13">
                            <input name="tipo_usr" type="text" style="display:none" size="13">

                            <input name="id_registro" type="text" style="display:none" size="13">
                            <input name="id_depto" type="text" id="id_depto" value="<?php echo $id_depto ?>"
                                   style="display:none"></td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top">
                            <?php
                            //Gilsa pediu para retirar se horas forem positivas
                            $detalhes = getDetalhesDoDia($id_usr, "{$dia}/{$mes}/{$ano}");
                            if (getMinutos($detalhes['saldo']) < 0) {
                                ?>
                                <table
                                    style="border:1px solid #ccc;border-collapse: collapse;width:100%;padding:10px;font-size:1.5em;">
                                    <tr style="background-color: #eeeeee;">
                                        <th>Horas Previstas</th>
                                        <th>Horas Trabalhadas</th>
                                        <th>Saldo do Dia</th>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><?php echo $detalhes['horasGrade'] ?></td>
                                        <td><?php echo $detalhes['horasTrabalhadas']; ?></td>
                                        <td style="<?php if (strpos($detalhes['saldo'], '-') === 0) {
                                            echo 'color:#FF0000;';
                                        } ?>"><?php echo $detalhes['saldo']; ?></td>
                                    </tr>
                                </table>
                            <?php
                            }
                            ?>
                            <?php

                            if ($tipo_grade == 2) {
                                $quantidade = 3;
                            } else {
                                $quantidade = 5;
                            }
                            //for($i=1;$i<$quantidade;$i++){
                            //$sql_agenda			= "select p_registro.id_justificativa,id_registro,hora,minutos,etapa from p_registro inner join p_justificativa on p_registro.id_justificativa = p_justificativa.id_justificativa where day(data_registro) = ".$dia." and month(data_registro) = ".$mes." and year(data_registro) = ".$ano." and p_registro.id_usr = ".$id_usr." and etapa = ".$i." order by etapa";
                            $sql_agenda = "select p_registro.id_justificativa,id_registro,hora,minutos,etapa from p_registro inner join p_justificativa on p_registro.id_justificativa = p_justificativa.id_justificativa where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and p_registro.id_usr = " . $id_usr . " order by etapa";
                            //echo $sql_agenda;
                            $dados_agenda = mysqli_query( $conexao, $sql_agenda);
                            while ($resultado_agenda = mysqli_fetch_array($dados_agenda)) {
                                //                    $resultado_agenda = mysql_fetch_array($dados_agenda);


                                if ($resultado_agenda[hora] != '') {
                                    $horario = $horario = str_pad($resultado_agenda[hora], 2, 0, STR_PAD_LEFT) . ":" . str_pad($resultado_agenda[minutos], 2, 0, STR_PAD_LEFT);
                                } else {
                                    $horario = "";
                                }


                                $sql_dados = "select * from  p_justificativa where id_justificativa = " . $resultado_agenda[id_justificativa];
                                $dados_preenchimento = mysqli_query( $conexao, $sql_dados);
                                $resultado_preenchimento = mysqli_fetch_array($dados_preenchimento);
                                ?>
                                <table width="476" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="314">
                                            <DIV class="commit commit-tease js-details-container"><font color="#000033"><strong><font
                                                            size="3"><?php echo $resultado_agenda[etapa]; ?>º
                                                            registro de ponto</font></strong> <br>
                                                    <br>
                                                </font>

                                                <DIV class="commit-meta">
                                                    <table width="750" border="0" align="center" cellpadding="4"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td width="100" rowspan="4" valign="top">
                                                                <div style='cursor:pointer;'
                                                                     onClick='AbreFoto(<?php echo $resultado_agenda[id_registro]; ?>)'
                                                                     ;>
                                                                    <?php
                                                                    $nome_arq = "../registro/" . $resultado_agenda[id_registro] . ".jpg";
                                                                    if (is_file($nome_arq) == 1) {
                                                                        ?>
                                                                        <div style='cursor:pointer;'
                                                                             onClick='AbreFoto(<?php echo $resultado_agenda[id_registro]; ?>)'
                                                                             ;><img
                                                                                src="../registro/<?php echo $resultado_agenda[id_registro]; ?>.jpg"
                                                                                width="100" border="0"></div>
                                                                    <?php } else { ?>
                                                                        <img src="../images/no_photo.jpg"
                                                                             width="100" height="100" border="0">
                                                                    <?php } ?>
                                                                </div>
                                                            </td>
                                                            <td width="85" valign="middle">Hor&aacute;rio:</td>
                                                            <td width="188" valign="top"><strong><font size="3"
                                                                                                       color="#FF0000"><?php echo $horario; ?></font></strong>
                                                                <input
                                                                    name="id_registro_<?php echo $resultado_agenda[etapa]; ?>"
                                                                    type="text" size="3"
                                                                    value="<?php echo $resultado_agenda[id_registro]; ?>"
                                                                    style="display:none"></strong>
                                                            </td>
                                                            <td width="163">&nbsp;</td>
                                                            <td width="381"><strong> </strong></td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td valign="top">Tipo:</td>
                                                            <td>
                                                                <?php

                                                                if ($resultado_agenda[etapa] == 1) { ?>
                                                                    <select name="id_tipo_justificativa_1"
                                                                            id="id_tipo_justificativa_1"
                                                                        <?php if ($resultado_preenchimento[abonado] == 1 || $resultado_preenchimento[abonado] == 2 || $resultado_preenchimento[abonado] == 3) {
                                                                            echo " disabled ";
                                                                        }
                                                                        ?>
                                                                        >
                                                                        <option value="0">Selecione</option>
                                                                        <?php
                                                                        while ($resultado_tipo_1 = mysqli_fetch_assoc($dados_tipo_1)) {
                                                                            ?>
                                                                            <option
                                                                                value="<?php echo $resultado_tipo_1[id_tipo_justificativa]; ?>"
                                                                                <?php if ($resultado_preenchimento[id_tipo_justificativa] == $resultado_tipo_1[id_tipo_justificativa]) {
                                                                                    echo " selected ";
                                                                                } ?>
                                                                                ><?php echo $resultado_tipo_1[titulo]; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                <?php } ?>
                                                                <?php if ($resultado_agenda[etapa] == 2) { ?>
                                                                    <select name="id_tipo_justificativa_2"
                                                                            id="id_tipo_justificativa_2"
                                                                        <?php if ($resultado_preenchimento[abonado] == 1 || $resultado_preenchimento[abonado] == 2 || $resultado_preenchimento[abonado] == 3) {
                                                                            echo " disabled ";
                                                                        }
                                                                        ?>
                                                                        >
                                                                        <option value="0">Selecione</option>
                                                                        <?php
                                                                        while ($resultado_tipo_2 = mysqli_fetch_assoc($dados_tipo_2)) {
                                                                            ?>
                                                                            <option
                                                                                value="<?php echo $resultado_tipo_2[id_tipo_justificativa]; ?>"<?php if ($resultado_preenchimento[id_tipo_justificativa] == $resultado_tipo_2[id_tipo_justificativa]) {
                                                                                echo " selected ";
                                                                            } ?>><?php echo $resultado_tipo_2[titulo]; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                <?php } ?>
                                                                <?php if ($resultado_agenda[etapa] == 3) { ?>
                                                                    <select name="id_tipo_justificativa_3"
                                                                            id="id_tipo_justificativa_3"
                                                                        <?php if ($resultado_preenchimento[abonado] == 1 || $resultado_preenchimento[abonado] == 2 || $resultado_preenchimento[abonado] == 3) {
                                                                            echo " disabled ";
                                                                        }
                                                                        ?>
                                                                        >
                                                                        <option value="0">Selecione</option>
                                                                        <?php
                                                                        while ($resultado_tipo_3 = mysqli_fetch_assoc($dados_tipo_3)) {
                                                                            ?>
                                                                            <option
                                                                                value="<?php echo $resultado_tipo_3[id_tipo_justificativa]; ?>"<?php if ($resultado_preenchimento[id_tipo_justificativa] == $resultado_tipo_3[id_tipo_justificativa]) {
                                                                                echo " selected ";
                                                                            } ?>><?php echo $resultado_tipo_3[titulo]; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                <?php } ?>
                                                                <?php if ($resultado_agenda[etapa] == 4) { ?>
                                                                    <select name="id_tipo_justificativa_4"
                                                                            id="id_tipo_justificativa_4"
                                                                        <?php if ($resultado_preenchimento[abonado] == 1 || $resultado_preenchimento[abonado] == 2 || $resultado_preenchimento[abonado] == 3) {
                                                                            echo " disabled ";
                                                                        }
                                                                        ?>
                                                                        >
                                                                        <option value="0">Selecione</option>
                                                                        <?php
                                                                        while ($resultado_tipo_4 = mysqli_fetch_assoc($dados_tipo_4)) {
                                                                            ?>
                                                                            <option
                                                                                value="<?php echo $resultado_tipo_4[id_tipo_justificativa]; ?>"<?php if ($resultado_preenchimento[id_tipo_justificativa] == $resultado_tipo_4[id_tipo_justificativa]) {
                                                                                echo " selected ";
                                                                            } ?>><?php echo $resultado_tipo_4[titulo]; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                <?php } ?>
                                                            </td>
                                                            <td width="163">&nbsp;</td>
                                                            <td rowspan="3">
                                                                <?php if ($resultado_preenchimento[abonado] == 1) { ?>
                                                                    <img src="../images/abonado.jpg" width="140"
                                                                         height="80">
                                                                <?php } ?>
                                                                <?php if ($resultado_preenchimento[abonado] == 2) { ?>
                                                                    <img src="../images/nao_abonado.jpg" width="140"
                                                                         height="80">
                                                                <?php } ?>
                                                                <?php if ($resultado_preenchimento[abonado] == 3) { ?>
                                                                    <img src="../images/ponto_cortado.jpg"
                                                                         width="140" height="80">
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="middle">Qtd. Horas:<strong> </strong></td>
                                                            <td valign="top"><strong>
                                                                    <input
                                                                        name="qtd_horas_<?php echo $resultado_agenda[etapa]; ?>"
                                                                        id="qtd_horas_<?php echo $resultado_agenda[etapa]; ?>"
                                                                        type="text" size="5" maxlength="5"
                                                                        onKeyPress="return txtBoxFormat(this, '99:99', event);"
                                                                        value="<?php echo $resultado_preenchimento[qtd_horas]; ?>"
                                                                        <?php if ($resultado_preenchimento[abonado] == 1 || $resultado_preenchimento[abonado] == 2 || $resultado_preenchimento[abonado] == 3) {
                                                                            echo " readonly='yes' ";
                                                                        }
                                                                        ?>
                                                                        >
                                                                    <strong><font color="#FF0000">Formato: 00:00</font></strong></strong>
                                                            </td>
                                                            <td valign="top">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="middle">&nbsp;</td>
                                                            <td valign="top">&nbsp; </td>
                                                            <td width="163" valign="top">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5" valign="middle">
                                                                <table width="100%" border="0" cellpadding="1"
                                                                       cellspacing="0">
                                                                    <tr>
                                                                        <td><strong>Justificativa do
                                                                                funcion&aacute;rio:</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <?php
                                                                            $registro = getRegistro($resultado_agenda['id_registro']);
                                                                            if ($registro->id_usr == $_SESSION['sessao_id_usr']) {
                                                                                $podeEditar = true;
                                                                                ?>
                                                                                <textarea
                                                                                    name="observacoes_<?php echo $resultado_agenda[etapa]; ?>"
                                                                                    cols="145" rows="4"
                                                                                    id="observacoes_<?php echo $resultado_agenda[etapa]; ?>"
                                                                                    style="font-family: Arial; font-size: 8 pt; "
                                                                                    onKeyPress="soma_<?php echo $resultado_agenda[etapa]; ?>(this.value)"
                                                                                    onKeyUp="soma_<?php echo $resultado_agenda[etapa]; ?>(this.value)"
                                                                                    <?php if ($resultado_preenchimento[abonado] == 1 || $resultado_preenchimento[abonado] == 2 || $resultado_preenchimento[abonado] == 3) {
                                                                                        echo " readonly='yes' ";
                                                                                    }
                                                                                    ?>><?php echo $resultado_preenchimento[observacoes]; ?></textarea>
                                                                            <?php } else { ?>
                                                                                <div
                                                                                    style="width:100%;font-size:1.3em;padding:5px;text-align: justify;"><?php echo $resultado_preenchimento[observacoes]; ?></div>
                                                                            <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                    $exclusoes = getHistoricoExclusao($resultado_agenda['id_justificativa']);
                                                                    if ($exclusoes && $_SESSION['sessao_rh']) { ?>
                                                                        <tr>
                                                                            <td><br/>

                                                                                <h3 style="margin:0;">Histórico de
                                                                                    Exclusões</h3>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div
                                                                                    style="width:100%;font-size:1em;text-align: justify;">
                                                                                    <?php foreach ($exclusoes as $exclusao) { ?>
                                                                                        <p><b>Decisão do
                                                                                                Chefe:</b> <?php echo $exclusao['decisao']; ?><?php echo $exclusao['data_decisao']; ?>
                                                                                        </p>
                                                                                        <p><b>Decisão
                                                                                                Excluida: </b><?php echo $exclusao['observacoes_chefe']; ?>
                                                                                        </p>
                                                                                    <?php } ?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                    <?php if (false) { ?>
                                                                        <tr>
                                                                            <td align="right"><font size="1">Caracteres
                                                                                    restantes</font> <input type="text"
                                                                                                            name="exibe_<?php echo $resultado_agenda[etapa]; ?>"
                                                                                                            size="3"
                                                                                                            maxlength="3"
                                                                                                            readonly="yes">
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </table>
                                                                <br>
                                                                <?php if ($resultado_preenchimento[anexo] == 1) { ?>
                                                                    <table width="65" border="0" cellspacing="0"
                                                                           cellpadding="2">
                                                                        <tr>
                                                                            <td><strong>Anexo:</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="80"><a
                                                                                    href="abrirAnexo.php?anexo=<?php echo $resultado_preenchimento[nome_arquivo]; ?>"
                                                                                    target="_blank">
                                                                                    <img src='../images/anexo.jpg'
                                                                                         alt='Arquivo Anexo'
                                                                                         title="Abrir Anexo"
                                                                                         height='60' border='0'>

                                                                                </a></td>
                                                                        </tr>
                                                                    </table>
                                                                <?php } ?>
                                                                <br> <br>
                                                                <?php

                                                                if ($resultado_preenchimento[abonado] != 1 && $resultado_preenchimento[abonado] != 2 && $resultado_preenchimento[abonado] != 3) {
                                                                    ?>
                                                                    <img src="../images/attachment.png"
                                                                         alt="Permite adicionar atestados, documentos ou comprovantes da sua justificativa."
                                                                         title="Permite adicionar atestados, documentos ou comprovantes da sua justificativa."
                                                                         width="25" height="25" align="absmiddle">
                                                                    Substituir Anexo / Anexar (opcional):
                                                                    <input
                                                                        name="arquivo_<?php echo $resultado_agenda[etapa]; ?>"
                                                                        type="file"
                                                                        id="arquivo_<?php echo $resultado_agenda[etapa]; ?>6"
                                                                        size="50">
                                                                <?php } ?>
                                                                <?php if ($_SESSION["usr_gerente"] >= 1 && $id_usr != $_SESSION["sessao_id_usr"]){ ?>
                                                                <br>
                                                                <table width="455" border="0" cellpadding="1"
                                                                       cellspacing="0">
                                                                    <tr>
                                                                        <td><strong>Justificativa do Chefe
                                                                                Imediato:</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="455" align="right"> <textarea
                                                                                name="observacoes_chefe_<?php echo $resultado_agenda[etapa]; ?>"
                                                                                cols="145" rows="4"
                                                                                id="observacoes_chefe_<?php echo $resultado_agenda[etapa]; ?>"
                                                                                style="font-family: Arial; font-size: 8 pt;"
                                                                                <?php if ($resultado_preenchimento[abonado] == 1 || $resultado_preenchimento[abonado] == 2 || $resultado_preenchimento[abonado] == 3) {
                                                                                    echo " readonly='yes' ";
                                                                                }
                                                                                ?>
                                                                                ><?php
                                                                                echo $resultado_preenchimento[observacoes_chefe];
                                                                                ?></textarea></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                                <?php

                                                                if ($resultado_preenchimento[abonado] != 1 && $resultado_preenchimento[abonado] != 2 && $resultado_preenchimento[abonado] != 3) { ?>
                                                                    <table width="577" border="0" align="center"
                                                                           cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td width="182" align="center">
                                                                                <table width="150" height="25"
                                                                                       border="0" align="center"
                                                                                       cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <div id="resultado1"
                                                                                                 class="tryit"
                                                                                                 onClick="fncValida_chefe(<?php echo $resultado_agenda[etapa]; ?>,<?php echo $resultado_agenda[id_justificativa]; ?>,1,<?php echo $resultado_agenda[id_registro]; ?>);">
                                                                                                Abonar Ponto
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td width="16" align="center">&nbsp;</td>
                                                                            <td width="168" align="center">
                                                                                <table width="150" height="25"
                                                                                       border="0" align="center"
                                                                                       cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td width="160" align="center">
                                                                                            <div id="resultado1"
                                                                                                 class="tryit_red"
                                                                                                 onClick="fncValida_chefe(<?php echo $resultado_agenda[etapa]; ?>,<?php echo $resultado_agenda[id_justificativa]; ?>,3,<?php echo $resultado_agenda[id_registro]; ?>);">
                                                                                                Cortar
                                                                                                Ponto
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td width="13" align="center">&nbsp;</td>
                                                                            <td width="186" align="center">
                                                                                <table width="150" height="25"
                                                                                       border="0" align="center"
                                                                                       cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <div id="resultado1"
                                                                                                 class="tryit_red"
                                                                                                 onClick="fncValida_chefe(<?php echo $resultado_agenda[etapa]; ?>,<?php echo $resultado_agenda[id_justificativa]; ?>,2,<?php echo $resultado_agenda[id_registro]; ?>);">
                                                                                                N&atilde;o
                                                                                                abonar
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                <?php } ?>
                                                                <div align="center">
                                                                    <?php }
                                                                    else {
                                                                        ?>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <?php if ($resultado_preenchimento[abonado] != 1 && $resultado_preenchimento[abonado] != 2 && $resultado_preenchimento[abonado] != 3) { ?>
                                                                            <table width="150" height="25" border="0"
                                                                                   align="center" cellpadding="0"
                                                                                   cellspacing="0">
                                                                                <tr>
                                                                                    <td width="160" align="center">
                                                                                        <div id="resultado1"
                                                                                             class="tryit"
                                                                                             onClick="fncAlteraJustificativa(<?php echo $resultado_agenda[etapa]; ?>,<?php echo $resultado_agenda[id_justificativa]; ?>);">
                                                                                            Alterar
                                                                                            Justificativa
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        <?php }
                                                                    }
                                                                    ?>
                                                                    <font color="#000000" size="2"><strong> <br>
                                                                            <br>
                                                                            <?php if ($resultado_preenchimento[abonado] == 1) {
                                                                                echo "Ponto abonado em: " . date('d/m/Y H:i:s', strtotime($resultado_preenchimento[data_cadastro_just]));
                                                                            }
                                                                            if ($resultado_preenchimento[abonado] == 3) {
                                                                                echo "Ponto cortado em: " . date('d/m/Y H:i:s', strtotime($resultado_preenchimento[data_cadastro_just]));
                                                                            }
                                                                            if ($resultado_preenchimento[abonado] == 2) {
                                                                                echo "Ponto não abonado em: " . date('d/m/Y H:i:s', strtotime($resultado_preenchimento[data_cadastro_just]));
                                                                            }
                                                                            ?>
                                                                            <br>

                                                                            <?php
                                                                            if ($_SESSION["sessao_rh"] == 1 || $_SESSION["usr_gerente"] == 1 || $_SESSION["usr_gerente"] == 2){
                                                                            if ($resultado_preenchimento[abonado] == 1 || $resultado_preenchimento[abonado] == 2 || $resultado_preenchimento[abonado] == 3){ ?>
                                                                            <br>
                                                                            <br>
                                                                        </strong></font>
                                                                    <table width="217" height="25" border="0"
                                                                           align="center" cellpadding="0"
                                                                           cellspacing="0">
                                                                        <tr>
                                                                            <td width="160" align="center">
                                                                                <div id="resultado1" class="tryit_red"
                                                                                     onClick="fncExcluiDecisao(<?php echo $id_usr;?>,<?php echo $id_depto;?>,<?php echo $resultado_agenda[id_justificativa];?>);">
                                                                                    <img src="../images/lock_16.png"
                                                                                         width="16" height="16"
                                                                                         align="absmiddle">
                                                                                    Excluir decis&atilde;o do Gerente
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <font color="#000000" size="2"><strong>
                                                                            <?php }
                                                                            } ?>
                                                                        </strong></font></div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5" valign="middle">&nbsp;</td>
                                                        </tr>
                                                    </table>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2" valign="top"><font color="#000000" size="2"> <strong>
                                </strong></font></td>
                    </tr>
                </table>
                <p>&nbsp;</p>

                <p>&nbsp;</p>
            </form>
            <br>
        </td>
    </tr>
    <tr class="Sub_titulo_rel">
        <td height="15">&nbsp;</td>
    </tr>
</table>


<form name="abre_foto" method="post" action="mostra_foto.php" target="myNewWinFoto">
    <input type="hidden" name="id_registro">
</form>
<form name="formularioAltera" method="post" action="alteradecisao.php" target="myNewWinFoto">
    <input type="hidden" name="id_justificativa">
    <input type="hidden" name="id_usr">
    <input type="hidden" name="id_depto">
    <input type="hidden" name="dia" value="<?php echo $dia; ?>">
    <input type="hidden" name="mes" value="<?php echo $mes; ?>">
    <input type="hidden" name="ano" value="<?php echo $ano; ?>">
</form>
<form name="atualiza_form" method="post" action="mostra_justificativa.php">
    <input type="hidden" name="id_depto">
    <input type="hidden" name="id_usr">
    <input type="hidden" name="dia">
    <input type="hidden" name="mes">
    <input type="hidden" name="ano">
</form>
<iframe name="janela" width="800" height="200" style="display:none"></iframe>
</body>
</html>
