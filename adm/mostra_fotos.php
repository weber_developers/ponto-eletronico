﻿<?php
session_start();
$dia = $_POST["dia"];
$mes = $_POST["mes"];
$ano = $_POST["ano"];
$id_usr = $_POST["id_usr"];

$dia_mostra = $dia;
if ($dia < 10) {
    $dia_mostra = "0" . $dia;
}
$dia_mostra = $dia;
if ($mes < 10) {
    $mes_mostra = "0" . $mes;
} else {
    $mes_mostra = $mes;
}

$resto_pasta = $_SESSION['config']->pastaFotos . "fotos_" . $mes_mostra . $ano . "/";

//echo getcwd()."<BR>";
//echo $mes_mostra;
require_once("../funcoes/conexao.php");


$sql = "select id_registro,hora,minutos from p_registro where id_usr = " . $id_usr . " and day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_registro is not null order by etapa,id_registro";
//echo $sql; 
$dados = mysqli_query( $conexao, $sql);

?>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css"/>
    <title>Foto de Funcionário</title>
</head>
<table width="184" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td height="25">&nbsp;&nbsp;&nbsp;Registros de:
            <strong><?php echo $dia_mostra . "/" . $mes_mostra . "/" . $ano; ?></strong></td>
    </tr>
    <tr>
        <?php
        $contador = 0;
        unset($_SESSION['img']);
        while ($resultado = mysqli_fetch_array($dados)) {
            $contador++;
            if ($contador == 3) {
                echo "<tr>";
            }
            ?>
            <td height="25" align="center"><strong>
                    <font color="#FF3300" size="3">
                        <?php echo $resultado[hora] . ":" . $resultado[minutos]; ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                    <?php
                    $nome_arq = $resto_pasta . $resultado[id_registro] . ".jpg";
                    if (is_file($nome_arq) == 1){
                    $_SESSION['img'][$resultado['id_registro']] = $nome_arq;
                    ?>
                </strong><img src="./view_image.php?id=<?php echo $resultado['id_registro'];?>" border="0">
                <?php }
                else { ?>
                    <img src="../images/no_photo_grande.jpg" border="0">
                <?php } ?>
            </td>
        <?php }
        ?>
    </tr>
</table>
</body>
</html>
