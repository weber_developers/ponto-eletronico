﻿<?php
$nome = $_GET["nome"];
?>
<head><title>Sistema de Ponto Eletrônico</title>
    <link rel="shortcut icon" href="../images/goias.ico" type="image/x-icon"/>


    <meta http-equiv="Content-Script-Type" content="text/javascript">

    <link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">


<body>

<BR> <BR> <BR>
</p>
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td valign="top">
            <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="680" height="80" background="../images/header.jpg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="93%"><strong><font color="#333333" size="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Login
                                            de Acesso</font></strong></td>
                                <td width="7%" rowspan="2" valign="bottom">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="bottom">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><font color="#666666"
                                                                                                      size="1">&nbsp;
                                            <?php

                                            $hoje = getdate();

                                            switch ($hoje['wday']) {
                                                case 0:
                                                    echo "Domingo, ";
                                                    break;
                                                case 1:
                                                    echo "Segunda-Feira, ";
                                                    break;
                                                case 2:
                                                    echo "Terça-Feira, ";
                                                    break;
                                                case 3:
                                                    echo "Quarta-Feira, ";
                                                    break;
                                                case 4:
                                                    echo "Quinta-Feira, ";
                                                    break;
                                                case 5:
                                                    echo "Sexta-Feira, ";
                                                    break;
                                                case 6:
                                                    echo "Sábado, ";
                                                    break;
                                            }

                                            echo $hoje['mday'];
                                            switch ($hoje['mon']) {
                                                case 1:
                                                    echo " de Janeiro de ";
                                                    break;
                                                case 2:
                                                    echo " de Fevereiro de ";
                                                    break;
                                                case 3:
                                                    echo " de Março de ";
                                                    break;
                                                case 4:
                                                    echo " de Abril de ";
                                                    break;
                                                case 5:
                                                    echo " de Maio de ";
                                                    break;
                                                case 6:
                                                    echo " de Junho de ";
                                                    break;
                                                case 7:
                                                    echo " de Julho de ";
                                                    break;
                                                case 8:
                                                    echo " de Agosto de ";
                                                    break;
                                                case 9:
                                                    echo " de Setembro de ";
                                                    break;
                                                case 10:
                                                    echo " de Outubro de ";
                                                    break;
                                                case 11:
                                                    echo " de Novembro de ";
                                                    break;
                                                case 12:
                                                    echo " de Dezembro de ";
                                                    break;
                                            }

                                            echo $hoje['year'] . ".";
                                            ?>
                                        </font></strong></td>
                            </tr>
                        </table>

                    </td>
                    <td width="20" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="700" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="3" height="5"></td>
                    <td width="4" height="4" background="../images/rightside.gif"></td>
                    <td width="696" valign="top" bgcolor="#FFFFFF"><BR>

                        <p>&nbsp; </p>
                        <table width="587" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
                            <tr valign="bottom">
                                <td height="25" class="Titulo_rel">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                        <tr>
                                            <td width="90" align="center"><img src="../images/2.png" width="128"
                                                                               height="128" border="0">
                                            </td>
                                            <td width="531" valign="middle"><strong><font color="#666666" size="4">
                                                        Sistema de Ponto Eletrônico
                                                        <?php echo $texto; ?>
                                                    </font></strong> <br>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="1">
                                                    <tr>
                                                        <td align="right">-</td>
                                                        <td><font color="#999999"><strong>Gestão de
                                                                    Ponto</strong></font></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">-</td>
                                                        <td><font color="#999999"><strong>Relatórios</strong></font>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">-</td>
                                                        <td><font color="#999999"><strong>Configurações</strong></font>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <strong></strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr valign="bottom">
                                <td height="25" class="Sub_titulo_rel">&nbsp;&nbsp;<font color="#FF3300">Envio de
                                        Senha</font></td>
                            </tr>
                            <tr>
                                <td height="2" style='padding:10pt 3.5pt 0cm 3.5pt'>

                                    <br>
                                    <br>
                                    Prezado(a):<strong>
                                        <?php echo $nome; ?></strong>,<br>
                                    <br>
                                    Conforme solicitado uma nova senha foi enviada para seu email
                                    cadastrado.</p>
                                    <p>&nbsp;</p>

                                    <p>* Alguns emails podem cair em Spam ou Lixo Eletrônico. Certifique-se de checar
                                        todas
                                        suas caixas.</p>

                                    <p>&nbsp;</p>

                                    <p align="center">&nbsp;</p>

                                    <p align="center">Clique <a
                                            href="http://<?php echo $_SERVER['HTTP_HOST']; ?>./adm/"><font
                                                color="#FF3300">aqui</font></a>
                                        para se logar no sistema.</p>

                                    <p><br>
                                        <br>
                                    </p>

                                </td>
                            </tr>
                            <tr class="Sub_titulo_rel">
                                <td height="15">
                                    <form name="formulario_envia" method="post" action="reenvia_senha.php"
                                          target="Verifica">
                                        <input type="text" name="Usuario" style="display:none">
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <p>&nbsp;</p>

                        <p>&nbsp;</p></td>
                    <td width="2" valign="top" background="../images/sidebar.gif"><br>
                    </td>
                </tr>
            </table>
            <table width="700" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="596" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                        de Ponto Eletrônico <?=date('Y')?> - SED&reg;</td>
                    <td width="2" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top">&nbsp;</td>
    </tr>
</table>

<BR>
<iframe width="1500" height="1500" name="Verifica" style="display:none"></iframe>
</body>

</html>