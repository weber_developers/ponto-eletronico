﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 && $_SESSION["sessao_id_usr"] == 0) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.location.href = 'index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}

require_once("../funcoes/conexao.php");


$sql = "select * from depto order by depto";
$dados = mysqli_query( $conexao, $sql);

?>
<html>
<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_reloadPage(init) {  //reloads the window if Nav4 resized
        if (init == true) with (navigator) {
            if ((appName == "Netscape") && (parseInt(appVersion) == 4)) {
                document.MM_pgW = innerWidth;
                document.MM_pgH = innerHeight;
                onresize = MM_reloadPage;
            }
        }
        else if (innerWidth != document.MM_pgW || innerHeight != document.MM_pgH) location.reload();
    }
    MM_reloadPage(true);
    //-->
</script>


<script language="JavaScript" type="text/JavaScript">
    <!--

    function fncExclui(id_depto) {
        grava.location.href = 'exclui_depto.php?id_depto=' + id_depto;

    }

    function fncLista() {
        window.open("ListaTamanhos.php", "", "");
    }
    function fncConsulta(id_depto) {
        window.close();
        window.opener.location.href = 'cad_deptos.php?id_depto=' + id_depto;

    }


    //-->
</script>
<head>
    <title>Departamentos</title>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="950" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td valign="top"
            style='border-left:solid #F7F7F7 .5pt;border-top:solid #F7F7F7 .5pt; border-right:solid #F7F7F7 .5pt;'>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="30" colspan="4"><font color="#FF9900" size="3"><strong>Lista
                                de Departamentos</strong></font></td>
                </tr>
                <tr>
                    <td width="62%" height="20" bgcolor="#F7F7F7"><strong><font color="#333333" size="1">&nbsp;&nbsp;Depto</font></strong>
                    </td>
                    <td width="24%" bgcolor="#F7F7F7"><strong><font color="#333333" size="1">Sigla</font></strong></td>
                    <td width="9%" bgcolor="#F7F7F7"><strong><font color="#333333" size="1">Ramal</font></strong></td>
                    <td width="5%" bgcolor="#F7F7F7"><font color="#666666">&nbsp;</font></td>
                </tr>
                <?php while ($resultado = mysqli_fetch_array($dados)) { ?>
                    <tr>
                        <td height="20" valign="top"><font size="1">&nbsp; <a href="#"
                                                                              onClick="fncConsulta(<?php echo $resultado[id_depto]; ?>);"><?php echo $resultado[depto]; ?> </a>
                            </font></td>
                        <td align="left" valign="top"><font size="1"> <?php echo $resultado[sigla]; ?> </font></td>
                        <td valign="top"><font size="1">&nbsp;</font> <font size="1"> <?php echo $resultado[ramal]; ?>
                            </font></td>
                        <td valign="top"><font size="1"><a href="#"
                                                           onClick="fncExclui(<?php echo $resultado[id_depto]; ?>)">Excluir</a>
                            </font></td>
                    </tr>
                <?php }
                ?>
                <tr bgcolor="#F7F7F7">
                    <td height="20" colspan="4">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</form>
<BR>
<BR>
<iframe width="801" height="201" name="grava" frameborder="1" style="display:none" id="grava"></iframe>
</body>
</html>