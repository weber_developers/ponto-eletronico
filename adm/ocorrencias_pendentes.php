﻿<?php
session_start();
require_once("../funcoes/conexao.php");
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
    <title>Cadastro de tipos de ocorr&ecirc;ncias</title>
</head>

<body>
<?php require_once("frm_topo.php"); ?>
<br>
<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><p><BR><strong><font
                                    color="#333333" size="4">
                                </font></strong>
                    </td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" align="center" valign="top" bgcolor="#FFFFFF"><br>

                        <h1>Você ainda tem ocorrências pendentes!</h1>

                        <h3>Por favor aguarde a resolução delas e tente novamente.</h3>

                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif">
                        <br>
                    </td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Planejamento
                        estratégico - SED &reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
