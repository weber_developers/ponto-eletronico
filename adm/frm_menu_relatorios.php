﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 && $_SESSION["sessao_id_usr"] == 0) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.location.href = 'index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}
require_once("../funcoes/conexao.php");

$sql_favorito = "select * from p_favoritos where id_gerencia_dono = " . $_SESSION["sessao_id_usr"];
$dados_favorito = mysqli_query( $conexao, $sql_favorito);
?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
<html>
<script language="JavaScript" type="text/javascript">
    function fncRelatorio() {
        document.formulario.submit();
    }
    function fncRelatorio2(sql, sql_total) {
        document.formulario.sql.value = sql;
        document.formulario.sql_total.value = sql_total;
        document.formulario.existe_favorito.value = 1;
        document.formulario.submit();
    }

    function fncDeleta_favorito(id_favorito) {
        janela.location.href = 'exclui_favorito.php?id_favorito=' + id_favorito + '&pagina=frm_relatorios.php';
    }
    function fncMontacombo(id_depto) {
        janela.location.href = 'frmMontafuncionarios.php?id_depto=' + id_depto;
    }

    function fncEnter() {
        if (window.event.keyCode == 13) {
            fncBusca();
        }
    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
    }
    function Abre2(id_processo) {
//window.open("processo.php?id_processo="+id_processo, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=1070,height=800,top=10,left=20");
        window.showModalDialog("processo.php?id_processo=" + id_processo, "resizable: no", "dialogWidth:1070px; dialogHeight:800px; center:yes");
    }

</script>

<head>
    <title>Menu de relat&oacute;rios</title>

</head>

<body>
<?php require_once("frm_topo.php"); ?>

<br>
<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><p><strong><font color="#333333"
                                                                                                       size="4">
                                    &nbsp; Relat&oacute;rios<br>
                                </font>&nbsp;&nbsp;&nbsp;Selecione as op&ccedil;&otilde;es de busca
                                do seu relat&oacute;rio:<font color="#333333" size="4"> </font></strong></p></td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" valign="top" bgcolor="#FFFFFF"><p>&nbsp;</p>

                        <form name="formulario" method="post" action="relatorio.php">
                            <table width="779" border="0" cellspacing="0" cellpadding="3">
                                <tr>
                                    <td width="48">&nbsp;</td>
                                    <td colspan="3"><strong><font color="#FF3300">Listar todas justificativas
                                                que:
                                                <input name="sql" type="text" id="sql" style="display:none">
                                                <input name="sql_total" type="text" id="sql_total" style="display:none">
                                                <input name="existe_favorito" type="text" id="existe_favorito"
                                                       style="display:none">
                                            </font></strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">Sejam do tipo:
                                        <select name="id_tipo_justificativa" id="id_tipo_justificativa">
                                            <option value="0" selected>Indiferente</option>
                                            <?php

                                            $sql = "select id_tipo_justificativa,titulo from p_tipo_justificativa order by titulo";
                                            $dados = mysqli_query( $conexao, $sql);
                                            while ($resultado = mysqli_fetch_array($dados)) {
                                                ?>
                                                <option
                                                    value="<?php echo $resultado[id_tipo_justificativa]; ?>"><?php echo $resultado[titulo]; ?>
                                                </option>
                                            <?php } ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td><select name="condicao_1" id="condicao_1">
                                            <option value="and" selected>e</option>
                                            <option value="or">ou</option>
                                        </select></td>
                                    <td colspan="3">que contenham o texto na justificativa:
                                        <input name="observacoes" type="text" id="observacoes"
                                               style="font-family: Arial; font-size: 8 pt; " size="50" maxlength="50">
                                    </td>
                                </tr>
                                <tr>
                                    <td><select name="condicao_2" id="condicao_2">
                                            <option value="and" selected>e</option>
                                            <option value="or">ou</option>
                                        </select></td>
                                    <td colspan="3">que contenham o n&uacute;mero do protocolo:
                                        <input name="protocolo" type="text" id="protocolo"
                                               style="font-family: Arial; font-size: 8 pt; " size="30" maxlength="30">
                                    </td>
                                </tr>
                                <tr>
                                    <?php if ($_SESSION["sessao_tipo"] != 4) { ?>
                                        <td><select name="condicao_3" id="condicao_3">
                                                <option value="and" selected>e</option>
                                                <option value="or">ou</option>
                                            </select></td>
                                        <td colspan="3">do depto : <strong><font color="#666666">
                                                    <select name="id_depto" id="id_depto" style="width:250px"
                                                        <?php if ($_SESSION["usr_gerente"] == 1 && $_SESSION["sessao_rh"] == 0) {
                                                            echo " disabled ";
                                                        } ?>

                                                            onChange='fncMontacombo(this.value)'>
                                                        <option value="0" selected>Todas Unidades</option>
                                                        <?php

                                                        if ($_SESSION["usr_gerente"] == 2) {
                                                            $sql_und = "select id_depto,depto,sigla from depto where ativo = 1 and id_superintendencia  like concat(trim(TRAILING '0' from '{$_SESSION["sessao_id_superintendencia"]}'),'%') order by sigla;";
                                                        }
                                                        if ($_SESSION["usr_gerente"] == 1 && $_SESSION["sessao_rh"] == 0) {
                                                            $sql_und = "select id_depto,depto,sigla from depto where ativo = 1 and id_depto = " . $_SESSION["sessao_id_depto"] . " order by sigla;";
                                                        }
                                                        if ($_SESSION["sessao_rh"] == 1) {
                                                            $sql_und = "select id_depto,depto,sigla from depto where ativo = 1 order by sigla;";
                                                        }


                                                        $dados_und = mysqli_query( $conexao, $sql_und);
                                                        while ($resultado_und = mysqli_fetch_array($dados_und)) {
                                                            ?>
                                                            <option value="<?php echo $resultado_und[id_depto]; ?>"
                                                                <?php if ($_SESSION["usr_gerente"] == 1 && $_SESSION["sessao_rh"] == 0) {
                                                                    echo " selected ";
                                                                } ?>
                                                                ><?php echo $resultado_und[depto]; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </font></strong></td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <td><select name="condicao_4" id="condicao_4">
                                            <option value="and" selected>e</option>
                                            <option value="or">ou</option>
                                        </select></td>
                                    <td colspan="3">estejam
                                        <input type="radio" name="abonado" value="0">
                                        Em aberto
                                        <input type="radio" name="abonado" value="1">
                                        Abonados
                                        <input type="radio" name="abonado" value="2">
                                        N&atilde;o abonados
                                        <input name="abonado" type="radio" value="" checked>
                                        Todos
                                    </td>
                                </tr>
                                <tr>
                                    <td><select name="condicao_5" id="condicao_5">
                                            <option value="and" selected>e</option>
                                            <option value="or">ou</option>
                                        </select></td>
                                    <td colspan="3">sejam de <strong>
                                            <select name="selMES" id="selMES">
                                                <option value="0" selected>Todos os meses</option>
                                                <option value="01">Janeiro</option>
                                                <option value="02">Fevereiro</option>
                                                <option value="03">Mar&ccedil;o</option>
                                                <option value="04">Abril</option>
                                                <option value="05">Maio</option>
                                                <option value="06">Junho</option>
                                                <option value="07">Julho</option>
                                                <option value="08">Agosto</option>
                                                <option value="09">Setembro</option>
                                                <option value="10">Outubro</option>
                                                <option value="11">Novembro</option>
                                                <option value="12">Dezembro</option>
                                            </select>
                                        </strong></td>
                                </tr>
                                <tr>
                                    <td><select name="condicao_6" id="condicao_6">
                                            <option value="and" selected>e</option>
                                            <option value="or">ou</option>
                                        </select></td>
                                    <td colspan="3">sejam de
                                        <select name="selANO" id="selANO">
                                            <option value="0" selected>Todos</option>
                                            <?php echo montarComboAnos(); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td><select name="condicao_7" id="condicao_7">
                                            <option value="and" selected>e</option>
                                            <option value="or">ou</option>
                                        </select></td>
                                    <td width="130">sejam do funcion&aacute;rio</td>
                                    <td width="583" colspan="2">
                                        <div id="div_id_usr">
                                            <select name="id_usr" id="id_usr">
                                                <option value="0" selected>Indiferente</option>
                                                <?php
                                                if ($_SESSION["usr_gerente"] == 1 && $_SESSION["sessao_rh"] == 0) {
                                                    $sql = "select id_usr,nome from usuarios where id_depto = " . $_SESSION["sessao_id_depto"] . " order by nome;";
                                                } else {
                                                    $sql = "select id_usr,nome from usuarios order by nome;";
                                                }
                                                $dados = mysqli_query( $conexao, $sql);
                                                while ($resultado = mysqli_fetch_array($dados)) {
                                                    ?>
                                                    <option
                                                        value="<?php echo $resultado[id_usr]; ?>"><?php echo $resultado[nome]; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">
                                        <table width="186" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr align="center">
                                                <td>
                                                    <div align="center" class="tryit" id="resultado1"
                                                         onClick="fncRelatorio();">Gerar
                                                        Relatório
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <div align="right"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">
                                        <div align="right"></div>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>
                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td width="96%">&nbsp;</td>
                                <td width="4%">
                                    <div align="center"><a href="javascript:history.go(-1)"><img
                                                src="../images/reply-16x16.gif" title="Voltar" width="16"
                                                height="16" border="0"></a></div>
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><BR>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'><img src="../images/favorito_1.png" width="20" height="20"
                                                              align="absmiddle">
                                    Relat&oacute;rios Favoritos
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <table width='100%' border='0' cellpadding='1' cellspacing='0'>

                                        <?php $qtd_rel = 0;
                                        while ($resultado_favorito = mysqli_fetch_array($dados_favorito)) {
                                            $qtd_rel++;
                                            ?>
                    <td width="95%" valign="middle" style="padding: 3px 3px 0;"><strong><font color="#666666" size="1"> 
                      <a href="#" onClick="fncRelatorio2('<?php echo $resultado_favorito[instrucao_sql]; ?>','<?php echo $resultado_favorito[instrucao_sql_total]; ?>');"><?php echo $resultado_favorito[titulo_favorito]; ?></a></font></strong></td>
                    <td width="5%" valign="middle" class="Itens_normal"> <a href="#" onClick="fncDeleta_favorito(<?php echo $resultado_favorito[id_favorito] ?>);"><img src="../images/delete2.png" width="16" height="16" border="0"></a></td>
                    </tr>
                    <?php } ?>
                                        <?php if ($qtd_rel == 0) { ?>
                                            <tr>
                                                <td colspan="2">
                                                    <div align="center"><font
                                                            color="#666666">______________________</font><font
                                                            color="#FF0000"><br>
                                                            Crie um relat&oacute;rio e adione como seu favorito!
                                                        </font></div>
                                                </td>
                                            </tr><?php } ?>
                                    </table>
                                </td>
                            </tr>

                        </table>
                        <br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Fichas</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <table width='100%' border='0' cellspacing='0' cellpadding='3'>
                                        <tr>
                                            <td width="4%"><img src="../images/morearrow_08c.gif" width="4"
                                                                height="7" border="0"></td>
                                            <td width="96%"><a href="frm_ficha_individual.php">Individual</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="frm_ficha_depto.php">Departamento</a></td>
                                        </tr>
                                    </table>
                                    <br>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Busca</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <form name="form_busca" method="post" action="frm_Lista_ocorrencias.php">
                                        <font color="#FF6600" size="1"><strong>Informe texto de
                                                busca:</strong></font><br>
                                        <font color="#FF6600" size="1"><strong>
                                                <input name="status_atual2" type="text" id="status_atual" size="3"
                                                       style="display:none" value="<?php echo $status_atual; ?>">
                                                <input name="selMES2" type="text" id="selMES2" size="3"
                                                       value="<?php echo $MES; ?>" style="display:none">
                                                <input name="selANO2" type="text" id="selANO2" size="3"
                                                       value="<?php echo $ANO; ?>" style="display:none">
                                                <input name="local_atual2" type="text" id="local_atual2" size="3"
                                                       value="<?php echo $local_atual; ?>" style="display:none">
                                                <input name="enviados" type="text" id="enviados" size="3"
                                                       value="<?php echo $enviados; ?>" style="display:none">
                                                <input name="texto" type="text" id="texto" size="3"
                                                       value="<?php echo $texto; ?>" style="display:none">
                                            </strong></font>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td><input type="text" name="texto_busca" style="width:130px"
                                                           value="<?php echo $texto_busca; ?>" onKeyPress="fncEnter();">
                                                </td>
                                                <td><a href="#" onClick="fncBusca();"><img src="../images/ok_bt.gif"
                                                                                           width="27" height="15"
                                                                                           border="0"></a></td>
                                            </tr>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <br>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p align="right">&nbsp; </p>
                    </td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                        de Ponto Eletrônico 2013 - SED&reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p>&nbsp; </p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<iframe width="801" height="201" name="janela" frameborder="1" style="display:none"></iframe>

</body>
</html>
