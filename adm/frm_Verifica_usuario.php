﻿<?php
echo $_SERVER[HTTP_X_FORWARDED_FOR] . "<BR>";
session_cache_limiter("private");
$cache_limiter = session_cache_limiter();
session_cache_expire(180); // Tempo da sessão: 60 minutos
$cache_expire = session_cache_expire();


session_start();
ob_start();
require_once("../funcoes/conexao.php");


$_SESSION["sessao_tipo"] = $tipo;
$numr_ip = $_POST["numr_ip"];
$login = strtolower($_POST["Usuario"]);
$senha = $_POST["Senha"];
$resultado = autenticar();
$id_usr = $resultado[id_usr];
$_SESSION['usuario'] = getUsuario($id_usr);
$hd_user_passwd_banco = $resultado[hd_user_passwd];
$tipo = $resultado[tipo];
$ativo = $resultado[ativo];
$sigla = $resultado[sigla];
$depto = $resultado[depto];
$ativo = $resultado[ativo];


if ($ativo == 0) {
    ?>
    <script language="JavaScript" type="text/javascript">
        alert("Usuario inativo ou Problemas com conexão com o banco de dados!");
    </script>
    <?php
    die;
}

if ($id_usr == '') {
    ?>
    <script language="JavaScript" type="text/javascript">
        alert("Usuario inexistente ou Problemas com conexão com o banco de dados!");
    </script>
    <?php
    die;
}

if (($hd_user_passwd != $hd_user_passwd_banco) && ($hd_user_passwd_adm != $hd_user_passwd_adm_banco)) {
    ?>
    <script language="JavaScript" type="text/javascript">
        alert("Senha incorreta!");
    </script>
    <?php
    die;
}
if ($ativo == 0) {
    ?>
    <script language="JavaScript" type="text/javascript">
        alert("Usuário inativo!");
    </script>
    <?php
    die;
}

if ($hd_user_passwd_adm_banco == $hd_user_passwd_adm) {
    $_SESSION["sessao_senhamaster"] = 1;
    $sessao_senhamaster = 1;
} else {
    $_SESSION["sessao_senhamaster"] = 0;
    $sessao_senhamaster = 0;
}


$_SESSION["sessao_id_usr"] = $resultado[id_usr];
$_SESSION["sessao_usuario"] = $resultado[nome];
$_SESSION["usr_gerente"] = $resultado[gerente];
$_SESSION["sessao_id_depto"] = $resultado[id_depto];
$_SESSION["sessao_id_grade"] = $resultado[id_grade];
$_SESSION["sessao_rh"] = $resultado[rh];
$_SESSION["sessao_sigla"] = $resultado[sigla];
//$_SESSION["sessao_id_superintendencia"] = $resultado[id_superintendencia];
$_SESSION["sessao_id_superintendencia"] = getUsuario()->getDepartamento()->id_depto;

if ($resultado[tipo] == 1) {
    $unidade = "Administrador";
}


$_SESSION["nome_unidade"] = $unidade;
$numr_ip = $numr_ip . "-" . $_SERVER[REMOTE_ADDR];


$sql = "insert into acessos (id_usr,data_acesso,entrada,numr_ip,pma) values (" . $resultado[id_usr] . ",'" . date("Y/m/d") . "','" . date("H:i:s") . "','" . $numr_ip . "'," . $sessao_senhamaster . ")";
$dados = mysqli_query( $conexao, $sql);
$sql_ac = "select id_acesso from acessos order by id_acesso desc limit 0,1";
$dados_ac = mysqli_query( $conexao, $sql_ac);
$resultado_ac = mysqli_fetch_array($dados_ac);
$_SESSION["id_acesso"] = $resultado_ac[id_acesso];

$sql = "select id_acesso,data_acesso,entrada from acessos where id_usr = " . $resultado[id_usr] . " order by id_acesso desc limit 0,2";
$dados = mysqli_query( $conexao, $sql);
$resultado = mysqli_fetch_array($dados);
$id_acesso = $resultado[id_acesso];
$data_acesso = $resultado[data_acesso];
$entrada = $resultado[entrada];
while ($resultado = mysqli_fetch_array($dados)) {
    $data_acesso = $resultado[data_acesso];
    $entrada = $resultado[entrada];
}

$_SESSION["sessao_id_acesso"] = $id_acesso;
$_SESSION["ultimo"] = substr($data_acesso, 8, 2) . "/" . substr($data_acesso, 5, 2) . "/" . substr($data_acesso, 0, 4) . " às " . $entrada . "hs.";

((mysqli_free_result($dados) || (is_object($dados) && (get_class($dados) == "mysqli_result"))) ? true : false);
((is_null($___mysqli_res = mysqli_close($conexao))) ? false : $___mysqli_res);

?>
<script language="JavaScript" type="text/javascript">
    window.parent.location.href = 'frm_Principal.php';
</script>
