﻿<script language="JavaScript" type="text/javascript">
    window.parent.botao_registra.style.display = 'none';
</script>
<form action="envia_mensagem.php" method="post" name="envia_msg">
    <input type="text" name="id_justificativa" style="display:none">
    <input type="text" name="total_justi_mes" style="display:none">
    <input type="text" name="cota" style="display:none">
    <input type="text" name="id_tipo_justificativa" style="display:none">
    <input type="text" name="id_usr" style="display:none">
    <input type="text" name="id_depto" style="display:none">
    <input type="text" name="mes_p" style="display:none">
    <input type="text" name="ano_p" style="display:none">
    <input type="text" name="dia_p" style="display:none">
    <input type="text" name="id_grade" style="display:none">
</form>
<?php
session_start();
require_once("../funcoes/conexao.php");


$id_grade = $_POST["id_grade"];
if ($id_grade == '') {
    $id_grade = 0;
}
$total_justi_mes = 0;
$id_usr = $_POST["id_usr"];
$id_depto = $_POST["id_depto"];
$dia = $_POST["dia"];
$mes = $_POST["mes"];
$ano = $_POST["ano"];

$id_registro_1 = $_POST["id_registro_1"];
$id_registro_2 = $_POST["id_registro_2"];
$id_registro_3 = $_POST["id_registro_3"];
$id_registro_4 = $_POST["id_registro_4"];

$id_tipo_justificativa_1 = $_POST["id_tipo_justificativa_1"];
$observacoes_1 = $_POST["observacoes_1"];
$observacoes_1 = $_POST["observacoes_1"];
$observacoes_1 = str_replace("\n", " ", $observacoes_1);
$observacoes_1 = str_replace(chr(13), "<br>", $observacoes_1);
$observacoes_1 = str_replace(chr(10), " ", $observacoes_1);
$observacoes_1 = str_replace("\'", "", $observacoes_1);
$observacoes_1 = str_replace('\"', "", $observacoes_1);
$observacoes_1 = str_replace("'", "", $observacoes_1);
$observacoes_1 = str_replace('"', "", $observacoes_1);

$id_tipo_justificativa_2 = $_POST["id_tipo_justificativa_2"];
$observacoes_2 = $_POST["observacoes_2"];
$observacoes_2 = str_replace("\n", " ", $observacoes_2);
$observacoes_2 = str_replace(chr(13), "<br>", $observacoes_2);
$observacoes_2 = str_replace(chr(10), " ", $observacoes_2);
$observacoes_2 = str_replace("\'", "", $observacoes_2);
$observacoes_2 = str_replace('\"', "", $observacoes_2);
$observacoes_2 = str_replace("'", "", $observacoes_2);
$observacoes_2 = str_replace('"', "", $observacoes_2);

$id_tipo_justificativa_3 = $_POST["id_tipo_justificativa_3"];
$observacoes_3 = $_POST["observacoes_3"];
$observacoes_3 = str_replace("\n", " ", $observacoes_3);
$observacoes_3 = str_replace(chr(13), "<br>", $observacoes_3);
$observacoes_3 = str_replace(chr(10), " ", $observacoes_3);
$observacoes_3 = str_replace("\'", "", $observacoes_3);
$observacoes_3 = str_replace('\"', "", $observacoes_3);
$observacoes_3 = str_replace("'", "", $observacoes_3);
$observacoes_3 = str_replace('"', "", $observacoes_3);


$id_tipo_justificativa_4 = $_POST["id_tipo_justificativa_4"];
$observacoes_4 = $_POST["observacoes_4"];
$observacoes_4 = str_replace("\n", " ", $observacoes_4);
$observacoes_4 = str_replace(chr(13), "<br>", $observacoes_4);
$observacoes_4 = str_replace(chr(10), " ", $observacoes_4);
$observacoes_4 = str_replace("\'", "", $observacoes_4);
$observacoes_4 = str_replace('\"', "", $observacoes_4);
$observacoes_4 = str_replace("'", "", $observacoes_4);
$observacoes_4 = str_replace('"', "", $observacoes_4);


$qtd_horas_1 = $_POST["qtd_horas_1"];
$qtd_horas_2 = $_POST["qtd_horas_2"];
$qtd_horas_3 = $_POST["qtd_horas_3"];
$qtd_horas_4 = $_POST["qtd_horas_4"];

//retirei para nao fazer a verificacao e emitir alerta
if (false) {
    if ($id_tipo_justificativa_1 == 0 && $id_tipo_justificativa_2 == 0 && $id_tipo_justificativa_3 == 0 && $id_tipo_justificativa_4 == 0) {
        ?>
        <script language="JavaScript">
            window.parent.botao_registra.style.display = '';
            alert("Informe ao menos uma justificativa!");
        </script>
        <?php
        die;
    }

    if ($id_tipo_justificativa_1 == 0 && $id_tipo_justificativa_2 == 0 && $id_tipo_justificativa_3 == 0 && $id_tipo_justificativa_4 == 0) {
        ?>
        <script language="JavaScript">
            window.parent.botao_registra.style.display = '';
            alert("Informe ao menos uma justificativa!");
        </script>
        <?php
        die;
    }


    if ($observacoes_1 != '') {
        $qtd_horas_1 = $_POST["qtd_horas_1"];
        if ($qtd_horas_1 == '' || $qtd_horas_1 == '00:00') {
            ?>
            <script language="JavaScript">
                window.parent.botao_registra.style.display = '';
                alert("Quantidade de horas inválida!");
            </script>
            <?php
            die;
        }
    }

    if ($observacoes_2 != '') {
        $qtd_horas_2 = $_POST["qtd_horas_2"];
        if ($qtd_horas_2 == '' || $qtd_horas_2 == '00:00') {
            ?>
            <script language="JavaScript">
                window.parent.botao_registra.style.display = '';
                alert("Quantidade de horas inválida!");
            </script>
            <?php
            die;
        }
    }

    if ($observacoes_3 != '') {
        $qtd_horas_3 = $_POST["qtd_horas_3"];
        if ($qtd_horas_3 == '' || $qtd_horas_3 == '00:00') {
            ?>
            <script language="JavaScript">
                window.parent.botao_registra.style.display = '';
                alert("Quantidade de horas inválida!");
            </script>
            <?php
            die;
        }
    }

    if ($observacoes_4 != '') {
        $qtd_horas_4 = $_POST["qtd_horas_4"];
        if ($qtd_horas_4 == '' || $qtd_horas_4 == '00:00') {
            ?>
            <script language="JavaScript">
                window.parent.botao_registra.style.display = '';
                alert("Quantidade de horas inválida!");
            </script>
            <?php
            die;
        }
    }
}
if (strlen($qtd_horas_1) < 5) {
    if ($qtd_horas_1 < 10 && strlen($qtd_horas_1) < 2) {
        $qtd_horas_1 = "0" . $qtd_horas_1;
    }
    $qtd_horas_1 = $qtd_horas_1 . ":00";
}

if (strlen($qtd_horas_2) < 5) {
    if ($qtd_horas_2 < 10 && strlen($qtd_horas_2) < 2) {
        $qtd_horas_2 = "0" . $qtd_horas_2;
    }
    $qtd_horas_2 = $qtd_horas_2 . ":00";
}

if (strlen($qtd_horas_3) < 5) {
    if ($qtd_horas_3 < 10 && strlen($qtd_horas_3) < 2) {
        $qtd_horas_3 = "0" . $qtd_horas_3;
    }
    $qtd_horas_3 = $qtd_horas_3 . ":00";
}

if (strlen($qtd_horas_4) < 5) {
    if ($qtd_horas_4 < 10 && strlen($qtd_horas_4) < 2) {
        $qtd_horas_4 = "0" . $qtd_horas_4;
    }
    $qtd_horas_4 = $qtd_horas_4 . ":00";
}
if ($qtd_horas_1 == '' && $qtd_horas_2 == '' && $qtd_horas_3 == '' && $qtd_horas_4 == '') {
    ?>
    <script language="JavaScript">
        window.parent.botao_registra.style.display = '';
        alert("Preencha com a quantidade de horas a justificar!");
    </script>
    <?php
    die;
}
if ($observacoes_1 == '' && $observacoes_2 == '' && $observacoes_3 == '' && $observacoes_4 == '') {
    ?>
    <script language="JavaScript">
        window.parent.botao_registra.style.display = '';
        alert("Informe ao menos uma justificativas!");
    </script>
    <?php
    die;
}
//$dt_final_periodo		= $_POST["dt_final_periodo"];

$data_justificativa = $ano . "/" . $mes . "/" . $dia;

$diap = date('d');
$mesp = date('m');
$anop = date('Y');
$hora = date("H");
$minuto = date("i");
$segundo = date("s");
$digito = rand(0, 100);
$protocolo = $ano . "." . $mesp . $diap . "." . $hora . $minuto . $segundo . "-" . $digito;

//$dt_final_periodo				= substr($dt_final_periodo,6,4)."/".substr($dt_final_periodo,3,2)."/".substr($dt_final_periodo,0,2);
//$vetor_dt_final_periodo			= explode("/",$dt_final_periodo);
//$data_checa_dt_final_periodo	= mktime(0,0,0,$vetor_dt_final_periodo[1],$vetor_dt_final_periodo[2],$vetor_dt_final_periodo[0]);

//$vetor_data_justificativa		= explode("/",$data_justificativa);
//$data_checa_vetor_data_justificativa	= mktime(0,0,0,$vetor_data_justificativa[1],$vetor_data_justificativa[2],$vetor_data_justificativa[0]);


//$dias = ceil(($data_checa_dt_final_periodo-$data_checa_vetor_data_justificativa)/86400);
//if($dias<0){
?>
<script language="JavaScript">
    //alert("Data final da ocorrência não pode ser inferior a data da justificativa!");
    window.parent.botao_registra.style.display = 'none';
</script>
<?php
//die;
//}
$data_cadastro_just = date('Y') . "/" . date('m') . "/" . date('d') . " " . date("H") . ":" . date("i") . ":" . date("s");

$sql_consulta = "select entrada_1,saida_1,entrada_2,saida_2,tipo_grade from usuarios inner join p_grade on usuarios.id_grade = p_grade.id_grade where id_usr = " . $id_usr;
$dados_consulta = mysqli_query( $conexao, $sql_consulta);
$resultado_consulta_grade = mysqli_fetch_array($dados_consulta);

$periodo_banco_1 = $resultado_consulta_grade[entrada_1];
$periodo_banco_1 = explode(':', $periodo_banco_1);
$periodo_banco_1_x = (60 * 60 * $periodo_banco_1[0]) + (60 * $periodo_banco_1[1]);

$periodo_banco_2 = $resultado_consulta_grade[saida_1];
$periodo_banco_2 = explode(':', $periodo_banco_2);
$periodo_banco_2_x = (60 * 60 * $periodo_banco_2[0]) + (60 * $periodo_banco_2[1]);

$periodo_banco_3 = $resultado_consulta_grade[entrada_2];
$periodo_banco_3 = explode(':', $periodo_banco_3);
$periodo_banco_3_x = (60 * 60 * $periodo_banco_3[0]) + (60 * $periodo_banco_3[1]);

$periodo_banco_4 = $resultado_consulta_grade[saida_2];
$periodo_banco_4 = explode(':', $periodo_banco_4);
$periodo_banco_4_x = (60 * 60 * $periodo_banco_4[0]) + (60 * $periodo_banco_4[1]);

$total_horas_periodo_1 = abs(($periodo_banco_2_x - $periodo_banco_1_x) / 60);
$total_horas_periodo_2 = abs(($periodo_banco_4_x - $periodo_banco_3_x) / 60);

if ($resultado_consulta_grade[tipo_grade] == 2) {
    $total_horas = $total_horas_periodo_1;
} else {
    $total_horas = $total_horas_periodo_1 + $total_horas_periodo_2;
}
$total_horas = ($total_horas * 3) / 60;

$sql_consulta_tipos = "select id_tipo_justificativa from p_tipo_justificativa where cota = 3";
//echo $sql_consulta_tipos;
$dados_consulta_tipos = mysqli_query( $conexao, $sql_consulta_tipos);
//$resultado_consulta_tipos	= mysql_fetch_array($dados_consulta_tipos);
while ($resultado_consulta_tipos = mysqli_fetch_array($dados_consulta_tipos)) {
    $lista_tipos = $lista_tipos . "," . $resultado_consulta_tipos[id_tipo_justificativa];
}
//echo $lista_tipos."<br>";


if (substr($lista_tipos, 0, 1) == ",") {
    $lista_tipos = substr($lista_tipos, 1, strlen($lista_tipos));
}

$sql_consulta2 = "select qtd_horas from p_justificativa where id_usr = " . $id_usr . " and month(data_justificativa) = " . $mes . " and year(data_justificativa) = " . $ano . " and id_tipo_justificativa in(" . $lista_tipos . ")";
$dados_consulta2 = mysqli_query( $conexao, $sql_consulta2);
$total_horas_mes_total = 0;
while ($resultado_consulta2 = mysqli_fetch_array($dados_consulta2)) {
    //$resultado_consulta2	= mysql_fetch_array($dados_consulta2);
    $total_horas_banco = $resultado_consulta2[qtd_horas];
    $total_horas_mes = explode(':', $total_horas_banco);
    $total_horas_mes_p = ((60 * 60 * $total_horas_mes[0]) + (60 * $total_horas_mes[1]));
    $total_horas_mes = $total_horas_mes_p / 60 / 60;

    $total_horas_mes_total = $total_horas_mes_total + $total_horas_mes;
}


$tempox = explode('.', $total_horas_mes_total); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
$horax = $tempox[0];
@$minutosx = (float)(0) . '.' . $tempox[1]; // Aqui forçamos a conversão para float, para não ter erro.
$minutosx = $minutosx * 60; // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.

$minutosx = explode('.', $minutosx); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
$minutosx = $minutosx[0];
if ($minutosx < 10) {
    $minutosx = "0" . $minutosx;
}

$total_acum_mostrax = $horax . ':' . $minutosx;


$total_horas_comparap_1 = 0;
if ($id_tipo_justificativa_1 == 12 || $id_tipo_justificativa_1 == 12 || $id_tipo_justificativa_1 == 29 || $id_tipo_justificativa_1 == 30) {
    $total_horas_compara_1 = explode(':', $qtd_horas_1);
    $total_horas_comparap_1 = ((60 * 60 * $total_horas_compara_1[0]) + (60 * $total_horas_compara_1[1]));
    $total_horas_comparap_1 = $total_horas_comparap_1 / 60 / 60;
}

$total_horas_comparap_2 = 0;
if ($id_tipo_justificativa_2 == 12 || $id_tipo_justificativa_2 == 12 || $id_tipo_justificativa_2 == 29 || $id_tipo_justificativa_2 == 30) {
    $total_horas_compara_2 = explode(':', $qtd_horas_2);
    $total_horas_comparap_2 = ((60 * 60 * $total_horas_compara_2[0]) + (60 * $total_horas_compara_2[1]));
    $total_horas_comparap_2 = $total_horas_comparap_2 / 60 / 60;
}

$total_horas_comparap_3 = 0;
if ($id_tipo_justificativa_3 == 12 || $id_tipo_justificativa_3 == 12 || $id_tipo_justificativa_3 == 29 || $id_tipo_justificativa_3 == 30) {
    $total_horas_compara_3 = explode(':', $qtd_horas_3);
    $total_horas_comparap_3 = ((60 * 60 * $total_horas_compara_3[0]) + (60 * $total_horas_compara_3[1]));
    $total_horas_comparap_3 = $total_horas_comparap_3 / 60 / 60;
}

$total_horas_comparap_4 = 0;
if ($id_tipo_justificativa_4 == 12 || $id_tipo_justificativa_4 == 12 || $id_tipo_justificativa_4 == 29 || $id_tipo_justificativa_4 == 30) {
    $total_horas_compara_4 = explode(':', $qtd_horas_4);
    $total_horas_comparap_4 = ((60 * 60 * $total_horas_compara_4[0]) + (60 * $total_horas_compara_4[1]));
    $total_horas_comparap_4 = $total_horas_comparap_4 / 60 / 60;
}

$total_horas_comparap = $total_horas_comparap_1 + $total_horas_comparap_2 + $total_horas_comparap_3 + $total_horas_comparap_4;


//############################################# JUSTIFICATIVA 1 ##############################################################
$sql_consulta_1 = "select cota from p_tipo_justificativa where id_tipo_justificativa = " . $id_tipo_justificativa_1;
$dados_consulta_1 = mysqli_query( $conexao, $sql_consulta_1);
$resultado_consulta_grade_1 = mysqli_fetch_array($dados_consulta_1);
$cota_1 = $resultado_consulta_grade_1[cota];

//echo $total_horas_mes."<BR>";
//echo $total_horas_mes_total . "<BR>" . $total_horas_comparap . "<br>" . $total_horas;
if ($cota_1 == 3) {
    if (($total_horas_mes_total + $total_horas_comparap) > $total_horas) {
        ?>
        <script language="JavaScript">
            //		alert("Quantidade de horas excedidas neste este mês para este tipo de justificativa!");
            window.parent.botao_registra.style.display = '';
            alert("Quantidade de horas excedidas neste este mês para este tipo de justificativa!\n\nSeu limite é de <?php echo $total_horas?>hs.\nJá foram utilizados <?php echo $total_acum_mostrax;?>hs.");
        </script>
        <?php
        die;
    }
}

//############################################# FINAL JUSTIFICATIVA 1 ########################################################
/**
 * //############################################# JUSTIFICATIVA 2 ##############################################################
 * $sql_consulta_2                = "select cota from p_tipo_justificativa where id_tipo_justificativa = ".$id_tipo_justificativa_2;
 * $dados_consulta_2            = mysql_query($sql_consulta_2, $conexao);
 * $resultado_consulta_grade_2    = mysql_fetch_array($dados_consulta_2);
 * $cota_2                        = $resultado_consulta_grade_2[cota];
 *
 *
 * if($cota_2==3){
 * if(($total_horas_mes_total+$total_horas_comparap)>$total_horas){
 * ?>
 * <script language="JavaScript">
 * alert("Quantidade de horas excedidas neste este mês para este tipo de justificativa!");
 * </script>
 * <?php
 * die;
 * }
 * }
 * //############################################# FINAL JUSTIFICATIVA 2 ########################################################
 *
 * //############################################# JUSTIFICATIVA 3 ##############################################################
 * $sql_consulta_3                = "select cota from p_tipo_justificativa where id_tipo_justificativa = ".$id_tipo_justificativa_3;
 * $dados_consulta_3            = mysql_query($sql_consulta_3, $conexao);
 * $resultado_consulta_grade_3    = mysql_fetch_array($dados_consulta_3);
 * $cota_3                        = $resultado_consulta_grade_3[cota];
 *
 *
 * if($cota_3==3){
 * if(($total_horas_mes_total+$total_horas_comparap)>$total_horas){
 * ?>
 * <script language="JavaScript">
 * alert("Quantidade de horas excedidas neste este mês para este tipo de justificativa!");
 * </script>
 * <?php
 * die;
 * }
 * }
 * //############################################# FINAL JUSTIFICATIVA 3 ########################################################
 *
 * //############################################# JUSTIFICATIVA 4 ##############################################################
 * $sql_consulta_4                = "select cota from p_tipo_justificativa where id_tipo_justificativa = ".$id_tipo_justificativa_4;
 * $dados_consulta_4            = mysql_query($sql_consulta_4, $conexao);
 * $resultado_consulta_grade_4    = mysql_fetch_array($dados_consulta_4);
 * $cota_4                        = $resultado_consulta_grade_4[cota];
 *
 *
 * if($cota_4==3){
 * if(($total_horas_mes_total+$total_horas_comparap)>$total_horas){
 * ?>
 * <script language="JavaScript">
 * alert("Quantidade de horas excedidas neste este mês para este tipo de justificativa!");
 * </script>
 * <?php
 * die;
 * }
 * }**/
//############################################# FINAL JUSTIFICATIVA 2 ########################################################


if (trim($observacoes_1) != '') {
    $nextdate = date('d/m/Y', mktime(0, 0, 0, $mes, $dia, $ano));
    $novadata_grava = substr($nextdate, 6, 4) . "/" . substr($nextdate, 3, 2) . "/" . substr($nextdate, 0, 2);
    R::begin();
    $sql_1 = "insert into p_justificativa (id_tipo_justificativa,data_justificativa,id_usr,observacoes,data_cadastro_just,qtd_horas,protocolo,who_r) values (" . $id_tipo_justificativa_1 . ",'" . $novadata_grava . "'," . $id_usr . ",'" . $observacoes_1 . "','" . $data_cadastro_just . "','" . $qtd_horas_1 . "','" . $protocolo . "'," . $_SESSION["sessao_id_usr"] . ")";
    $dados_1 = mysqli_query( $conexao, $sql_1);

    $sql_ultimo_1 = "select id_justificativa from p_justificativa order by id_justificativa desc limit 0,1";
    $dados_ultimo_1 = mysqli_query( $conexao, $sql_ultimo_1);
    $resultado_ultimo_1 = mysqli_fetch_array($dados_ultimo_1);
    $id_justificativa_1 = $resultado_ultimo_1[id_justificativa];
    $sql_atualiza_1 = "update p_registro set id_justificativa = " . $resultado_ultimo_1[id_justificativa] . ",nao_registrou = 0 where id_registro = " . $id_registro_1;
    $dados_atualiza_1 = mysqli_query( $conexao, $sql_atualiza_1);
    R::commit();
    if ($_FILES['arquivo_1']['size'] != 0) {

        $tamanho_arquivo = $_FILES['arquivo_1']['size'];
        $extensao = strtolower(end(explode('.', $_FILES['arquivo_1']['name'])));

        $caminho = $_SESSION['config']->pastaAnexosJustificativas . $id_justificativa_1 . "." . $extensao;
        if ($tamanho_arquivo < 93148000 && $extensao != "com" && $extensao != "exe" && $extensao != "php" && $extensao != "inc") {
            if (move_uploaded_file($_FILES['arquivo_1']['tmp_name'], $caminho)) {
                $sql_registra_1 = "insert into p_anexos (id_usr,id_justificativa,data_anexo,nivel_just) values(" . $id_usr . "," . $id_justificativa_1 . ",'" . $data_cadastro_just . "',1)";
                $dados_registra_1 = mysqli_query( $conexao, $sql_registra_1);
                $sql_atualiza_11 = "update p_justificativa set anexo = 1,ordem_justificativa = 1, nome_arquivo = '" . $id_justificativa_1 . "." . $extensao . "',extensao = '" . $extensao . "' where id_justificativa = " . $id_justificativa_1;
                $dados_atualiza_11 = mysqli_query( $conexao, $sql_atualiza_11);
            }
        } else {
            echo("<script>alert ('Evento cadastrado com sucesso, porém o arquivo não foi aceito!\n\nTamanho ou extensão errada!') </script>");
        }
    }
}


if (trim($observacoes_2) != '') {
    $nextdate = date('d/m/Y', mktime(0, 0, 0, $mes, $dia, $ano));
    $novadata_grava = substr($nextdate, 6, 4) . "/" . substr($nextdate, 3, 2) . "/" . substr($nextdate, 0, 2);
    R::begin();
    $sql_2 = "insert into p_justificativa (id_tipo_justificativa,data_justificativa,id_usr,observacoes,data_cadastro_just,qtd_horas,protocolo,who_r) values (" . $id_tipo_justificativa_2 . ",'" . $novadata_grava . "'," . $id_usr . ",'" . $observacoes_2 . "','" . $data_cadastro_just . "','" . $qtd_horas_2 . "','" . $protocolo . "'," . $_SESSION["sessao_id_usr"] . ")";
    $dados_2 = mysqli_query( $conexao, $sql_2);

    $sql_ultimo_2 = "select id_justificativa from p_justificativa order by id_justificativa desc limit 0,1";
    $dados_ultimo_2 = mysqli_query( $conexao, $sql_ultimo_2);
    $resultado_ultimo_2 = mysqli_fetch_array($dados_ultimo_2);
    $id_justificativa_2 = $resultado_ultimo[id_justificativa];
    $sql_atualiza_2 = "update p_registro set id_justificativa = " . $resultado_ultimo_2[id_justificativa] . ",nao_registrou = 0 where id_registro = " . $id_registro_2;
    $dados_atualiza_2 = mysqli_query( $conexao, $sql_atualiza_2);
    R::commit();
//	echo $sql_atualiza."<br>"; 
    if ($_FILES['arquivo_2']['size'] != 0) {

        $tamanho_arquivo = $_FILES['arquivo_2']['size'];
        $extensao = strtolower(end(explode('.', $_FILES['arquivo_2']['name'])));

        $caminho = $_SESSION['config']->pastaAnexosJustificativas . $id_justificativa_2 . "." . $extensao;
        if ($tamanho_arquivo < 93148000 && $extensao != "com" && $extensao != "exe" && $extensao != "php" && $extensao != "inc") {
            if (move_uploaded_file($_FILES['arquivo_2']['tmp_name'], $caminho)) {
                $sql_registra_2 = "insert into p_anexos (id_usr,id_justificativa,data_anexo,nivel_just) values(" . $id_usr . "," . $id_justificativa_2 . ",'" . $data_cadastro_just . "',2)";
                $dados_registra = mysqli_query( $conexao, $sql_registra_2);
                $sql_atualiza_22 = "update p_justificativa set anexo = 1, ordem_justificativa = 2, nome_arquivo = '" . $id_justificativa_2 . "." . $extensao . "',extensao = '" . $extensao . "' where id_justificativa = " . $id_justificativa_2;
                $dados_atualiza_22 = mysqli_query( $conexao, $sql_atualiza_22);
            }
        } else {
            echo("<script>alert ('Evento cadastrado com sucesso, porém o arquivo não foi aceito!\n\nTamanho ou extensão errada!') </script>");
        }
    }
}


if (trim($observacoes_3) != '') {
    $nextdate = date('d/m/Y', mktime(0, 0, 0, $mes, $dia, $ano));
    $novadata_grava = substr($nextdate, 6, 4) . "/" . substr($nextdate, 3, 2) . "/" . substr($nextdate, 0, 2);
    R::begin();
    $sql_3 = "insert into p_justificativa (id_tipo_justificativa,data_justificativa,id_usr,observacoes,data_cadastro_just,qtd_horas,protocolo,who_r) values (" . $id_tipo_justificativa_3 . ",'" . $novadata_grava . "'," . $id_usr . ",'" . $observacoes_3 . "','" . $data_cadastro_just . "','" . $qtd_horas_3 . "','" . $protocolo . "'," . $_SESSION["sessao_id_usr"] . ")";
    $dados_3 = mysqli_query( $conexao, $sql_3);

    $sql_ultimo_3 = "select id_justificativa from p_justificativa order by id_justificativa desc limit 0,1";
    $dados_ultimo_3 = mysqli_query( $conexao, $sql_ultimo_3);
    $resultado_ultimo_3 = mysqli_fetch_array($dados_ultimo_3);
    $id_justificativa_3 = $resultado_ultimo_3[id_justificativa];
    $sql_atualiza_3 = "update p_registro set id_justificativa = " . $resultado_ultimo_3[id_justificativa] . ",nao_registrou = 0 where id_registro = " . $id_registro_3;
    $dados_atualiza_3 = mysqli_query( $conexao, $sql_atualiza_3);
    R::commit();
//	echo $sql_atualiza."<br>"; 
    if ($_FILES['arquivo_3']['size'] != 0) {

        $tamanho_arquivo = $_FILES['arquivo_3']['size'];
        $extensao = strtolower(end(explode('.', $_FILES['arquivo_3']['name'])));

        $caminho = $_SESSION['config']->pastaAnexosJustificativas . $id_justificativa_3 . "." . $extensao;
        if ($tamanho_arquivo < 93148000 && $extensao != "com" && $extensao != "exe" && $extensao != "php" && $extensao != "inc") {
            if (move_uploaded_file($_FILES['arquivo_3']['tmp_name'], $caminho)) {
                $sql_registra_3 = "insert into p_anexos (id_usr,id_justificativa,data_anexo,nivel_just) values(" . $id_usr . "," . $id_justificativa_3 . ",'" . $data_cadastro_just . "',3)";
                $dados_registra_3 = mysqli_query( $conexao, $sql_registra_3);
                $sql_atualiza_33 = "update p_justificativa set anexo = 1, ordem_justificativa = 3, nome_arquivo = '" . $id_justificativa_3 . "." . $extensao . "',extensao = '" . $extensao . "' where id_justificativa = " . $id_justificativa_3;
                $dados_atualiza_33 = mysqli_query( $conexao, $sql_atualiza_33);
            }
        } else {
            echo("<script>alert ('Evento cadastrado com sucesso, porém o arquivo não foi aceito!\n\nTamanho ou extensão errada!') </script>");
        }
    }
}


if (trim($observacoes_4) != '') {
    $nextdate = date('d/m/Y', mktime(0, 0, 0, $mes, $dia, $ano));
    $novadata_grava = substr($nextdate, 6, 4) . "/" . substr($nextdate, 3, 2) . "/" . substr($nextdate, 0, 2);
    R::begin();
    $sql_4 = "insert into p_justificativa (id_tipo_justificativa,data_justificativa,id_usr,observacoes,data_cadastro_just,qtd_horas,protocolo,who_r) values (" . $id_tipo_justificativa_4 . ",'" . $novadata_grava . "'," . $id_usr . ",'" . $observacoes_4 . "','" . $data_cadastro_just . "','" . $qtd_horas_4 . "','" . $protocolo . "'," . $_SESSION["sessao_id_usr"] . ")";
    $dados_4 = mysqli_query( $conexao, $sql_4);

    $sql_ultimo_4 = "select id_justificativa from p_justificativa order by id_justificativa desc limit 0,1";
    $dados_ultimo_4 = mysqli_query( $conexao, $sql_ultimo_4);
    $resultado_ultimo_4 = mysqli_fetch_array($dados_ultimo_4);
    $id_justificativa_4 = $resultado_ultimo_4[id_justificativa];
    $sql_atualiza_4 = "update p_registro set id_justificativa = " . $resultado_ultimo_4[id_justificativa] . ",nao_registrou = 0 where id_registro = " . $id_registro_4;
    $dados_atualiza_4 = mysqli_query( $conexao, $sql_atualiza_4);
    R::commit();
//	echo $sql_atualiza."<br>"; 
    if ($_FILES['arquivo_4']['size'] != 0) {

        $tamanho_arquivo = $_FILES['arquivo_4']['size'];
        $extensao = strtolower(end(explode('.', $_FILES['arquivo_4']['name'])));

        $caminho = $_SESSION['config']->pastaAnexosJustificativas . $id_justificativa_4 . "." . $extensao;
        if ($tamanho_arquivo < 93148000 && $extensao != "com" && $extensao != "exe" && $extensao != "php" && $extensao != "inc") {
            if (move_uploaded_file($_FILES['arquivo_4']['tmp_name'], $caminho)) {
                $sql_registra_4 = "insert into p_anexos (id_usr,id_justificativa,data_anexo,nivel_just) values(" . $id_usr . "," . $id_justificativa_4 . ",'" . $data_cadastro_just . "',4)";
                $dados_registra_4 = mysqli_query( $conexao, $sql_registra_4);
                $sql_atualiza_44 = "update p_justificativa set anexo = 1,ordem_justificativa = 4, nome_arquivo = '" . $id_justificativa_4 . "." . $extensao . "',extensao = '" . $extensao . "' where id_justificativa = " . $id_justificativa_4;
                $dados_atualiza_44 = mysqli_query( $conexao, $sql_atualiza_44);
            }
        } else {
            echo("<script>alert ('Evento cadastrado com sucesso, porém o arquivo não foi aceito!\n\nTamanho ou extensão errada!') </script>");
        }
    }
}
//echo $sql_1."<BR>".$sql_2."<BR>".$sql_3."<BR>".$sql_4."<BR>";
//
//echo $sql_atualiza_1."<BR>".$sql_atualiza_4."<BR>";

?>
<script language="JavaScript" type="text/javascript">
    window.parent.opener.envia_dados(<?php echo $id_depto;?>, <?php echo $id_usr;?>, <?php echo $mes;?>, <?php echo $ano;?>, 1);
    //document.envia_msg.id_justificativa.value=<?php echo $id_justificativa; ?>;
    //document.envia_msg.total_justi_mes.value=<?php echo $total_justi_mes; ?>;
    //document.envia_msg.cota.value=<?php echo $cota; ?>;
    //document.envia_msg.id_tipo_justificativa.value=<?php echo $id_tipo_justificativa; ?>;
    document.envia_msg.id_usr.value =<?php echo $id_usr; ?>;
    document.envia_msg.id_depto.value =<?php echo $id_depto; ?>;
    document.envia_msg.mes_p.value =<?php echo $mes; ?>;
    document.envia_msg.ano_p.value =<?php echo $ano; ?>;
    document.envia_msg.dia_p.value =<?php echo $dia; ?>;
    document.envia_msg.id_grade.value =<?php echo $id_grade; ?>;

    //alert("Justificativa cadastrada com sucesso!");
    document.envia_msg.submit();
    //window.parent.close();

    //window.parent.opener.location.href='frm_justificativa.php';
</script>
