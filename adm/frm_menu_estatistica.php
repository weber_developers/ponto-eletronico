﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == '') {
    ?>
    <script language="JavaScript">
        alert("Sua sessão expirou!\n\nLogue no sistema novamente!");
        window.location.href = 'http://<?php echo $_SERVER['HTTP_HOST'];?>/index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}
require_once("../funcoes/conexao.php");

$mes = date('m');
$ano = date('Y');

$sql_lista = "select count(id_justificativa) as qtd_total_justificativa from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto where month(data_justificativa) = " . $mes . " and year(data_justificativa) = " . $ano;
if ($_SESSION["sessao_rh"] == 0) {
    $sql_lista = $sql_lista . " and usuarios.id_depto = " . $_SESSION["sessao_id_depto"];
}
$dados_lista = mysqli_query( $conexao, $sql_lista);
$resultado_lista = mysqli_fetch_array($dados_lista);
$qtd_total_justificativa = $resultado_lista[qtd_total_justificativa];

$sql_lista1 = "select count(id_justificativa) as qtd_total_justificativa1 from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto where month(data_justificativa) = " . $mes . " and year(data_justificativa) = " . $ano . " and abonado = 1 ";
if ($_SESSION["sessao_rh"] == 0) {
    $sql_lista1 = $sql_lista1 . " and usuarios.id_depto = " . $_SESSION["sessao_id_depto"];
}

$dados_lista1 = mysqli_query( $conexao, $sql_lista1);
$resultado_lista1 = mysqli_fetch_array($dados_lista1);
$qtd_total_justificativa1 = $resultado_lista1[qtd_total_justificativa1];

$sql_lista2 = "select count(id_justificativa) as qtd_total_justificativa2 from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto where month(data_justificativa) = " . $mes . " and year(data_justificativa) = " . $ano . " and abonado = 2 ";
if ($_SESSION["sessao_rh"] == 0) {
    $sql_lista2 = $sql_lista2 . " and usuarios.id_depto = " . $_SESSION["sessao_id_depto"];
}

$dados_lista2 = mysqli_query( $conexao, $sql_lista2);
$resultado_lista2 = mysqli_fetch_array($dados_lista2);
$qtd_total_justificativa2 = $resultado_lista2[qtd_total_justificativa2];

$sql_processos4 = "select depto.id_depto,depto.sigla,(count(id_justificativa)*100)/(select count(id_usr) from usuarios where id_depto = depto.id_depto) as percentual from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on depto.id_depto = usuarios.id_depto group by usuarios.id_depto order by percentual desc limit 0,10";
$sql_processos4 = "select depto.id_depto,depto.sigla,count(id_justificativa) as total_just,(select count(id_usr) from usuarios where id_depto = depto.id_depto) as percentual from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on depto.id_depto = usuarios.id_depto group by usuarios.id_depto order by percentual desc";
$dados_processos4 = mysqli_query( $conexao, $sql_processos4);


?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
<html>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart"]});
    google.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Ocorrências', 'Quantitativo'],
            <?php while($resultado_processos4 = mysqli_fetch_array($dados_processos4)){
            echo "['".$resultado_processos4[sigla]."',".number_format($resultado_processos4[total_just],0,",",".")."],";
            } ?>
        ]);

        var options = {
            title: 'Gráfico de Ocorrências (% do quantitativo em relação ao total)',
            is3D: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }

</script>

<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_preloadImages() { //v3.0
        var d = document;
        if (d.images) {
            if (!d.MM_p) d.MM_p = new Array();
            var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
            for (i = 0; i < a.length; i++)
                if (a[i].indexOf("#") != 0) {
                    d.MM_p[j] = new Image;
                    d.MM_p[j++].src = a[i];
                }
        }
    }
    function abre_deptos(id_depto) {
        document.abre_lista.id_depto.value = id_depto;
        document.abre_lista.submit();

    }
    function fncPainel(tipo) {
        document.abre_janela.tipo_ocorrencia.value = tipo;
        document.abre_janela.enviados.value = tipo;
        document.abre_janela.submit();
    }
    //-->
</script>
<head>

    <title>Principal</title>
</head>


<body>
<meta name="title" content="SED - SED"/>
<meta name="url" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>"/>
<meta name="description"
      content="SED-GO - A SED de Goiás implementou em um projeto altamente inovador de acompanhamento de Processos totalmente digital."/>
<meta name="keywords"
      content="Sistema de Ponto Eletrônico,sistema,ouvidoria,corregedoria,controladoria,processos,protocolo,jedi,workflow,processo eletronico,gerencial,manifestação,acompanhamento,encaminhamento,controle,pesquisa,histórico,governo de goiás,sistema de acompanhamento de processos,sistema gerencial,marcelo roncato"/>

<meta name="autor" content="Marcelo Roncato"/>
<meta name="company" content="SED"/>
<meta name="revisit-after" content="5"/>
<script language="JavaScript" type="text/javascript">

    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
//window.location.href="frm_Lista_processos.php?texto_busca="+document.form_busca.texto_busca.value;
    }


    function fncFechaMensagem() {
        mensagem.style.display = 'none';
    }

    function fncOpcoes2() {
        status_opcoes.style.display = '';
    }
    function Fechar() {
        status_opcoes.style.display = 'none';
    }
    function Abre2(id_processo) {
        window.open("processo.php?id_processo=" + id_processo, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=1070,height=700,top=10,left=20");
    }
    function fncEnter() {
        if (window.event.keyCode == 13) {
            fncBusca();
        }
    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
//document.form_busca.id_gerencia.value = document.lista.id_gerencia.value;
//document.form_busca.selMES.value = document.lista.selMES.value;
//document.form_busca.selANO.value = document.lista.selANO.value;


        document.form_busca.submit();
//window.location.href="frm_Lista_processos.php?texto_busca="+document.form_busca.texto_busca.value;

    }

</script>
<?php require_once("frm_topo.php"); ?>

<br>
<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><strong><font color="#333333"
                                                                                                    size="4">&nbsp;
                                Estat&iacute;stica<br>
                            </font>&nbsp;&nbsp;&nbsp;Selecione as op&ccedil;&otilde;es para sua
                            estat&iacute;stica:<font color="#333333" size="4"> </font></strong></td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" valign="top" bgcolor="#FFFFFF"><BR>
                        <table width='100%' border='0' cellspacing='0' cellpadding='2'>
                            <tr>
                                <td width="4%" align="center"><img src="../images/g1p.jpg" width="175" height="160">
                                </td>
                                <td width="96%"><a href="grafico_funcionario.php"><font size="2">Ocorr&ecirc;ncias por
                                            Servidores</font></a></td>
                            </tr>
                            <tr>
                                <td align="center"><img src="../images/gpp.jpg" width="180" height="180"></td>
                                <td><a href="grafico_depto.php"><font
                                            size="2"> <?php if ($_SESSION["usr_gerente"] > 1 || $_SESSION["sessao_rh"] == 1) { ?>Ocorr&ecirc;ncias por Departamentos
                                            <?php } else {
                                                ?>
                                                Ocorrência do meu departamento
                                            <?php } ?>
                                        </font></a></td>
                            </tr>
                        </table>  <?php if ($_SESSION["usr_gerente"] > 1 || $_SESSION["sessao_rh"] == 1){ ?>
                        <div align="center">
                            _________________________________________________________________________________________________________
                        </div>

                        <div id="chart_div" style="width: 820px; height: 650px;">
                            <?php } ?>


                        </div>
                        <p>&nbsp;</p>

                        <p>&nbsp;</p>
                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Busca</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <form name="form_busca" method="post" action="frm_Lista_ocorrencias.php">
                                        <font color="#FF6600" size="1"><strong>Informe texto de
                                                busca:</strong></font><br>
                                        <font color="#FF6600" size="1"><strong>
                                                <input name="status_atual" type="text" id="status_atual" size="3"
                                                       style="display:none" value="<?php echo $status_atual; ?>">
                                                <input name="selMES" type="text" id="selMES" size="3"
                                                       value="<?php echo $MES; ?>" style="display:none">
                                                <input name="selANO" type="text" id="selANO" size="3"
                                                       value="<?php echo $ANO; ?>" style="display:none">
                                                <input name="local_atual" type="text" id="local_atual" size="3"
                                                       value="<?php echo $local_atual; ?>" style="display:none">
                                                <input name="enviados" type="text" id="enviados" size="3"
                                                       value="<?php echo $enviados; ?>" style="display:none">
                                                <input name="texto" type="text" id="texto" size="3"
                                                       value="<?php echo $texto; ?>" style="display:none">
                                            </strong></font>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td><input type="text" name="texto_busca" style="width:130px"
                                                           value="<?php echo $texto_busca; ?>" onKeyPress="fncEnter();">
                                                </td>
                                                <td><a href="#" onClick="fncBusca();"><img src="../images/ok_bt.gif"
                                                                                           width="27" height="15"
                                                                                           border="0"></a></td>
                                            </tr>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                            <br>
                            <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                                <tr>
                                    <td class='Titulo_caixa'> Ranking por propo&ccedil;&atilde;o (%)</td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" class='Corpo_caixa'>
                                        <table width='100%' border='0' cellspacing='0' cellpadding='2'>
                                            <tr>
                                                <td colspan="2"><p><font color="#FF6600"><strong>10
                                                                primeiros</strong></font></p>
                                                </td>
                                            </tr>
                                            <?php

                                            //$sql_processos2 = "select count(id_usr) as total,sigla,depto.id_depto from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto group by depto.id_depto order by total desc limit 0,10";
                                            //$dados_processos2 = mysql_query($sql_processos2, $conexao);

                                            //$sql_processos2 = "select count(id_justificativa) as total,sigla,depto.id_depto from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto group by depto.id_depto order by total desc limit 0,10";
                                            $sql_processos2 = "select depto.id_depto,depto.sigla,depto.depto,(count(id_justificativa)*100)/(select count(id_usr) from usuarios where id_depto = depto.id_depto) as percentual from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on depto.id_depto = usuarios.id_depto group by usuarios.id_depto order by percentual desc limit 0,10";
                                            $dados_processos2 = mysqli_query( $conexao, $sql_processos2);
                                            while ($resultado_processos2 = mysqli_fetch_array($dados_processos2)) {
                                                ?>
                                                <tr>
                                                    <td width="4%" align="center"><img src="../images/check.jpg"
                                                                                       width="15" height="17"></td>
                                                    <td width="96%" class="Itens_normal">
                                                        <div style="cursor:pointer;"
                                                             onClick="abre_deptos(<?php echo $resultado_processos2[id_depto] ?>);"
                                                             id="<?php echo $resultado_processos2[id_depto] ?>"
                                                             title="<?php echo $resultado_processos2[depto] ?>">
                                                            <p><strong><font
                                                                        color="#009933"> <?php echo $resultado_processos2[sigla] . " - " . number_format($resultado_processos2[percentual], 0, ",", ".") . "%"; ?></font></strong>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <tr align="right">
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        <?php } ?>
                        <br>

                        <p align="right"> &nbsp; &nbsp;</p></td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                        de Ponto Eletrônico <?=date('Y')?> - SED&reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<form name="abre_janela" method="post" action="frm_Lista_ocorrencias.php">
    <input type="hidden" name="tipo_ocorrencia">
    <input type="hidden" name="enviados">

</form>
<form name="abre_lista" method="post" action="frm_Lista_ocorrencias.php">
    <input type="hidden" name="id_depto">
    <input type="hidden" name="enviados" value="0">
    <input type="hidden" name="tipo_ocorrencia" value="0">


</form>

<?php
((is_null($___mysqli_res = mysqli_close($conexao))) ? false : $___mysqli_res);
?>
</body>
</html>
