﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 || $_SESSION["sessao_rh"] != 1) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.location.href = 'index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}
require_once("../funcoes/conexao.php");

$id_tipo_justificativa = $_GET["id_tipo_justificativa"];

if ($id_tipo_justificativa == '') {
    $id_tipo_justificativa = 0;
    $titulo = '';
    $cota = 0;
    $lancamento = 3;
    $homologa = 3;
    $exibe = 1;
    $mostraRelatorio = 0;
} else {

    $sql = "select * from p_tipo_justificativa where id_tipo_justificativa = " . $id_tipo_justificativa;
    $dados = mysqli_query( $conexao, $sql);
    $resultado = mysqli_fetch_array($dados);
    $titulo = $resultado[titulo];
    $cota = $resultado[cota];
    $lancamento = $resultado[lancamento];
    $homologa = $resultado[homologa];
    $exibe = $resultado[exibe];
    $mostraRelatorio = $resultado[mostraRelatorio];
    $exigeAnexo = $resultado[exigeAnexo];
}

?>
<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">

<html>

<script language="JavaScript" type="text/javascript">
    function fncLista() {
        window.open("lista_tipos.php", "", "");
    }
    function fncValida() {

        if (document.formulario.titulo.value == '') {
            alert("Informe o título desta tipo de justificativa!");
            document.formulario.titulo.focus();
            return false;
        }
        if (document.formulario.cota.value == '') {
            alert("Informe a quantidade de dias!");
            document.formulario.cota.focus();
            return false;
        }

        document.formulario.submit();
    }

    function fncAtualiza() {
        if ('<?php echo $id_tipo_justificativa;?>' != 0) {
            if ('<?php echo $lancamento;?>' == 3) {
                document.formulario.lancamento[1].checked = true;
                document.formulario.lancamento[0].checked = true;
            }
            if ('<?php echo $lancamento;?>' != 3) {
                document.formulario.lancamento[0].checked = true;
                document.formulario.lancamento[1].checked = true;
            }

            if ('<?php echo $cota;?>' == 3) {
                document.formulario.cota[1].checked = true;
                document.formulario.cota[0].checked = true;
            }
            if ('<?php echo $cota;?>' != 3) {
                document.formulario.cota[0].checked = true;
                document.formulario.cota[1].checked = true;
            }

            if ('<?php echo $homologa;?>' == 3) {
                document.formulario.homologa[1].checked = true;
                document.formulario.homologa[0].checked = true;
            }
            if ('<?php echo $homologa;?>' != 3) {
                document.formulario.homologa[0].checked = true;
                document.formulario.homologa[1].checked = true;
            }


            if ('<?php echo $exibe;?>' != 1) {
                document.formulario.exibe[1].checked = true;
                document.formulario.exibe[0].checked = false;
            }
            if ('<?php echo $homologa;?>' == 1) {
                document.formulario.exibe[0].checked = false;
                document.formulario.exibe[1].checked = true;
            }
            if ('<?php echo $mostraRelatorio;?>' == 1) {
                document.formulario.mostraRelatorio[1].checked = false;
                document.formulario.mostraRelatorio[0].checked = true;
            }


        }
    }
    function txtBoxFormat(objeto, sMask, evtKeyPress) {
        var i, nCount, sValue, fldLen, mskLen, bolMask, sCod, nTecla;


        if (document.all) { // Internet Explorer
            nTecla = evtKeyPress.keyCode;
        } else if (document.layers) { // Nestcape
            nTecla = evtKeyPress.which;
        } else {
            nTecla = evtKeyPress.which;
            if (nTecla == 8) {
                return true;
            }
        }

        sValue = objeto.value;

        // Limpa todos os caracteres de formatação que
        // já estiverem no campo.
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(" ", "");
        sValue = sValue.toString().replace(" ", "");
        fldLen = sValue.length;
        mskLen = sMask.length;

        i = 0;
        nCount = 0;
        sCod = "";
        mskLen = fldLen;

        while (i <= mskLen) {
            bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ":"))
            bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

            if (bolMask) {
                sCod += sMask.charAt(i);
                mskLen++;
            }
            else {
                sCod += sValue.charAt(nCount);
                nCount++;
            }

            i++;
        }

        objeto.value = sCod;

        if (nTecla != 8) { // backspace
            if (sMask.charAt(i - 1) == "9") { // apenas números...
                return ((nTecla > 47) && (nTecla < 58));
            }
            else { // qualquer caracter...
                return true;
            }
        }


        else {
            return true;
        }
    }
</script>

<head>
    <title>Cadastro de tipos de ocorr&ecirc;ncias</title>

</head>

<body onLoad="fncAtualiza();">
<?php require_once("frm_topo.php"); ?>
<br>

<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><p><BR><strong><font
                                    color="#333333" size="4">
                                </font></strong>

                        <p><strong><font color="#333333" size="4">&nbsp; Cadastro de Tipos
                                    de Ocorr&ecirc;ncias</font></strong></p>
                    </td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" align="center" valign="top" bgcolor="#FFFFFF"><br>
                        <table width="790" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
                            <tr valign="bottom">
                                <td height="25" class="Titulo_rel">
                                    <table width="790" border="0" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td width="194"><strong><font style="font-size:10px" color="#333333">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>Dados da Ocorr&ecirc;ncia:</strong>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="Itens_rel_s_traco">
                                <td height="15">
                                    <form action="grava_tipos_ocorrencias.php" method="post" name="formulario"
                                          id="formulario" target="janela">
                                        <table width="800" border="0" align="center" cellpadding="4" cellspacing="0">
                                            <tr>
                                                <td width="3" align="right">&nbsp;</td>
                                                <td width="36">&nbsp;</td>
                                                <td width="737"><input name="id_tipo_justificativa" type="text"
                                                                       id="id_tipo_justificativa"
                                                                       value="<?php echo $id_tipo_justificativa ?>"
                                                                       style="display:none"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td>T&iacute;tulo:</td>
                                                <td><input name="titulo" type="text" id="titulo"
                                                           style="font-family: Arial; font-size: 8 pt; " size="65"
                                                           maxlength="100" value="<?php echo $titulo ?>"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Cota:</td>
                                                <td><input name="cota" type="radio" id="cota_1" value="3" checked>
                                                    <label for="cota_1"><strong>Sim</strong></label> <input name="cota"
                                                                                                            type="radio"
                                                                                                            value="0"
                                                                                                            id="cota_2">
                                                    <label for="cota_2"><strong>N&atilde;o</strong></label>
                                                    <label for="op2"></label></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Lan&ccedil;amento:</td>
                                                <td valign="top"><input name="lancamento" type="radio" id="lanca_1"
                                                                        value="3" checked>
                                                    <label for="lanca_1"><strong>RH</strong></label>
                                                    <input name="lancamento" type="radio" value="1" id="lanca_2">
                                                    <label for="lanca_2"><strong>Funcion&aacute;rio</strong></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Homologa:</td>
                                                <td valign="top"><input name="homologa" type="radio" id="homo_1"
                                                                        value="3" checked>
                                                    <label for="homo_1"><strong>RH</strong></label>
                                                    <input name="homologa" type="radio" value="2" id="homo_2">
                                                    <label for="homo_2"><strong>Gestor</strong></label></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Ativo:</td>
                                                <td valign="top"><input name="exibe" type="radio" id="exibe_1" value="1"
                                                                        checked>
                                                    <label for="exibe_1"><strong>Sim</strong></label>
                                                    <input name="exibe" type="radio" value="0" id="exibe_2">
                                                    <label for="exibe_2"><strong>N&atilde;o</strong></label></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Mostra nos Relatórios:</td>
                                                <td valign="top">
                                                    <input name="mostraRelatorio" type="radio" id="mostraRelatorio_1"
                                                           value="1">
                                                    <label for="mostraRelatorio_1"><strong>Sim</strong></label>
                                                    <input name="mostraRelatorio" type="radio" value="0"
                                                           id="mostraRelatorio_2" checked>
                                                    <label for="mostraRelatorio_2"><strong>N&atilde;o</strong></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Exige Anexo:</td>
                                                <td valign="top">
                                                    <input name="exigeAnexo" type="radio" id="exigeAnexo_1" value="1">
                                                    <label for="exigeAnexo_1"><strong>Sim</strong></label>
                                                    <input name="exigeAnexo" type="radio" value="0"
                                                           id="exigeAnexo_2" checked>
                                                    <label for="exigeAnexo_2"><strong>N&atilde;o</strong></label>
                                                </td>
                                            </tr>
                                        </table>
                                        <p>&nbsp;</p>
                                        <table width="291" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr align="center">
                                                <td>
                                                    <table width="87" height="25" border="0" cellpadding="0"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <div id="resultado1" class="tryit"
                                                                     onClick="fncValida();">
                                                                    <div align="center">Enviar</div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table width="87" height="25" border="0" cellpadding="0"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <div id="resultado1" class="tryit"
                                                                     onClick="document.location.href='frm_cadastro_grades.php';">
                                                                    <div align="center">Limpar</div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <p>&nbsp;</p></form>

                                    <table width="371" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="56" height="20" align="right" valign="middle"><img
                                                    src="../images/bg_list.gif" width="3" height="5">&nbsp;</td>
                                            <td width="315"><strong><a href="#" onClick="fncLista();">Listar
                                                        todos os tipos de Ocorr&ecirc;ncias</a></strong></td>
                                        </tr>
                                    </table>
                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p></td>
                            </tr>
                            <tr class="Sub_titulo_rel">
                                <td height="15">&nbsp;</td>
                            </tr>
                        </table>
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br>


                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td width="96%">&nbsp;</td>
                                <td width="4%">
                                    <div align="center"><a href="javascript:history.go(-1)"><img
                                                src="../images/reply-16x16.gif" title="Voltar" width="16"
                                                height="16" border="0"></a></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Op&ccedil;&otilde;es</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <table width='100%' border='0' cellspacing='0' cellpadding='3'>
                                        <tr>
                                            <td width="4%"><img src="../images/morearrow_08c.gif" width="4"
                                                                height="7" border="0"></td>
                                            <td width="96%"><a href="frm_Config.php">Menu
                                                    Configura&ccedil;&otilde;es</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="cad_usuarios.php">Cadastro de usu&aacute;rios</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="cad_deptos.php">Cadastro de Unidades</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="frm_Principal.php">In&iacute;cio</a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <br>
                    </td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Planejamento
                        estratégico - SED &reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p>&nbsp; </p>

<p>&nbsp;</p>


<iframe name="janela" width="800" height="200" style="display:none"></iframe>
</body>
</html>
