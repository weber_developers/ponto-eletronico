﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == '') {
    ?>
    <script language="JavaScript">
        alert("Sua sessão expirou!\n\nLogue no sistema novamente!");
        window.location.href = 'http://<?php echo $_SERVER['HTTP_HOST'];?>../adm/index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}

$cont = 0;

?>
<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
<script language="JavaScript" type="text/javascript">
    function fncUsuarios() {
        window.open("mostra_usuarios.php?tipo=0", "", "");
    }

    function fncValida_envia() {

        if (document.formulario.titulo_favorito.value == '') {
            apprise("Informe o nome deste relatório!");
            document.formulario.titulo_favorito.focus();
            return false;
        }
        document.formulario.submit();
    }
    function fncColoca() {
        document.formulario.instrucao_sql.value = window.parent.relatorio.sql.value;
        document.formulario.instrucao_sql_total.value = window.parent.relatorio.sql_total.value;
    }
</script>
<html>
<head>
    <title>Favoritos</title>

</head>

<body bgcolor="#FFFFFF" onLoad="fncColoca();">
<table width="785" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
    <tr valign="bottom">
        <td valign="middle" bgcolor="#E8F0FF">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="95%" height="110" valign="middle"><font color="#333333" size="4"><strong><font
                                    color="#666666">&nbsp;<img src="../images/favoritos3.png" width="92" height="92"
                                                               align="absmiddle">&nbsp;Favoritos</font></strong></font>
                    </td>
                    <td width="5%" align="center">
                        <div align="center"><a href="#" onClick="window.parent.div_favorito.style.display='none';"
                                               title="Fechar esta janela"><img src="../images/close.gif" width="32"
                                                                               height="16" border="0"></a></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="Itens_rel_s_traco">
        <td valign="top">
            <form name="formulario" method="post" action="grava_favoritos.php" target="janela_envia">
                <br>
                <br>
                <table width="740" border="0" align="center" cellpadding="2" cellspacing="0" class="Tabela_rel">
                    <tr>
                        <td height="30" colspan="2" bgcolor="#F4F4F4"><strong>&nbsp;Gerenciador
                                de Relat&oacute;rios:</strong></td>
                    </tr>
                    <tr>
                        <td width="107">&nbsp;</td>
                        <td width="500">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong><font color="#666666">
                                    <input type="text" name="id_usr"
                                           value="<?php echo $_SESSION["sessao_id_usuario"]; ?>" style="display:none">
                                    <input type="text" name="id_manifestacao" value="<?php echo $id_manifestacao; ?>"
                                           style="display:none">
                                    <input type="text" name="id_unidade_origem"
                                           value="<?php echo $_SESSION["sessao_id_unidade"]; ?>" style="display:none">
                                </font></strong></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;Nome do relat&oacute;rio favorito:
                            <input name="titulo_favorito" type="text" id="titulo_favorito"
                                   style="font-family: Arial; font-size: 8 pt; " size="50" maxlength="50">
                            <input name="instrucao_sql" type="text" id="instrucao_sql" size="50" maxlength="50"
                                   style="display:none">
                            <input name="instrucao_sql_total" type="text" id="instrucao_sql_total" size="50"
                                   maxlength="50" style="display:none">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp; </td>
                    </tr>
                </table>
            </form>
            <br>
            <table width="162" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="30" valign="middle">
                        <div align="center" class="tryit" id="resultado1" onClick="fncValida_envia();">Gravar
                            favorito
                        </div>

                    </td>
                </tr>
            </table>
            <br>
        </td>
    </tr>
    <tr class="Sub_titulo_rel">
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="65%" height="30"><strong><font color="#009933">&nbsp; </font><font color="#666666">&nbsp;Atualiza&ccedil;&atilde;o
                                efetuada <strong> por:</strong></font><strong> <a href="frm_Acessos.php"
                                                                                  title="Meus acessos!"><font
                                        color="#FF6600">
                                        <?php
                                        echo $_SESSION["sessao_usuario"];
                                        ?>
                                    </font></a></strong></strong></td>
                    <td width="35%" align="right"><font size="1"><a href="#"
                                                                    onClick="window.parent.div_favorito.style.display='none';"
                                                                    title="Fechar esta janela">Fechar</a>&nbsp;&nbsp;&nbsp;
                        </font></td>
                </tr>
            </table>
        </td>
    </tr>

</table>
<iframe width="800" height="800" name="janela_envia" frameborder="1" style="display:none" id="janela_envia"></iframe>
</body>
</html>
