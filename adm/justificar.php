﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 && $_SESSION["sessao_id_usr"] == 0) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.close();
    </script>
    <?php

    die;
}

require_once("../funcoes/conexao.php");


$id_usr = $_POST["id_usr"];
$dia = $_POST["dia"];
$id_depto = $_POST["id_depto"];
if ($dia < 10) {
    $dia = "0" . $dia;
}
$mes = $_POST["mes"];
if ($mes < 10) {
    $mes = "0" . $mes;
}

$ano = $_POST["ano"];
$sql_tipo_grade = "select tipo_grade from p_grade where id_grade = " . $_SESSION["sessao_id_grade"];
$dados_tipo_grade = mysqli_query( $conexao, $sql_tipo_grade);
$resultado_tipo_grade = mysqli_fetch_assoc($dados_tipo_grade);
$data_registro = $ano . "/" . $mes . "/" . $dia;
$tipo_grade = $resultado_tipo_grade[tipo_grade];


$sql_consulta_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = 1 order by etapa";
$dados_consulta_registro = mysqli_query( $conexao, $sql_consulta_registro);
$resultado_consulta_registro = mysqli_fetch_array($dados_consulta_registro);
if ($resultado_consulta_registro[id_registro] == "") {
    gravarRegistro($oUsuario, [
        'data_registro' => $data_registro,
        'etapa' => 1,
        'numr_ip' => $numr_ip,
        'hora' => $hora,
        'minutos' => $minutos,
        'nao_registrou' => 1
    ]);
}

$sql_consulta_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = 2 order by etapa";
$dados_consulta_registro = mysqli_query( $conexao, $sql_consulta_registro);
$resultado_consulta_registro = mysqli_fetch_array($dados_consulta_registro);
if ($resultado_consulta_registro[id_registro] == "") {
    gravarRegistro($oUsuario, [
        'data_registro' => $data_registro,
        'etapa' => 2,
        'numr_ip' => $numr_ip,
        'hora' => $hora,
        'minutos' => $minutos,
        'nao_registrou' => 1
    ]);
    //echo $sql_insere_registro;
}

if ($resultado_tipo_grade[tipo_grade] == 0) {
    $sql_consulta_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = 3 order by etapa";
    $dados_consulta_registro = mysqli_query( $conexao, $sql_consulta_registro);
    $resultado_consulta_registro = mysqli_fetch_array($dados_consulta_registro);
    if ($resultado_consulta_registro[id_registro] == "") {
        gravarRegistro($oUsuario, [
            'data_registro' => $data_registro,
            'etapa' => 3,
            'numr_ip' => $numr_ip,
            'hora' => $hora,
            'minutos' => $minutos,
            'nao_registrou' => 1
        ]);
    }

    $sql_consulta_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = 4 order by etapa";
    $dados_consulta_registro = mysqli_query( $conexao, $sql_consulta_registro);
    $resultado_consulta_registro = mysqli_fetch_array($dados_consulta_registro);
    if ($resultado_consulta_registro[id_registro] == "") {
        gravarRegistro($oUsuario, [
            'data_registro' => $data_registro,
            'etapa' => 4,
            'numr_ip' => $numr_ip,
            'hora' => $hora,
            'minutos' => $minutos,
            'nao_registrou' => 1
        ]);
    }
}


$sql_consulta = "select * from usuarios inner join depto on usuarios.id_depto = depto.id_depto where usuarios.id_usr = " . $id_usr;
$dados_consulta = mysqli_query( $conexao, $sql_consulta);
$resultado_consulta = mysqli_fetch_array($dados_consulta);
$nome = explode(' ', $resultado_consulta[nome]);
$foto = $resultado_consulta[foto];


$sql_consulta_grade = "select * from p_grade where id_grade = " . $resultado_consulta[id_grade];
$dados_consulta_grade = mysqli_query( $conexao, $sql_consulta_grade);
$resultado_consulta_grade = mysqli_fetch_array($dados_consulta_grade);
$entrada_1 = $resultado_consulta_grade[entrada_1];
$saida_1 = $resultado_consulta_grade[saida_1];
$entrada_2 = $resultado_consulta_grade[entrada_2];
$saida_2 = $resultado_consulta_grade[saida_2];

$sql_tipo_1 = "select * from  p_tipo_justificativa where lancamento = 1 and exibe = 1 order by titulo";
$dados_tipo_1 = mysqli_query( $conexao, $sql_tipo_1);

$sql_tipo_2 = "select * from  p_tipo_justificativa where lancamento = 1 and exibe = 1 order by titulo";
$dados_tipo_2 = mysqli_query( $conexao, $sql_tipo_2);

$sql_tipo_3 = "select * from  p_tipo_justificativa where lancamento = 1 and exibe = 1 order by titulo";
$dados_tipo_3 = mysqli_query( $conexao, $sql_tipo_3);

$sql_tipo_4 = "select * from  p_tipo_justificativa where lancamento = 1 and exibe = 1 order by titulo";
$dados_tipo_4 = mysqli_query( $conexao, $sql_tipo_4);


$minutosCota = getMinutos($oUsuario->getHorasAbonosComCota("{$dia}/{$mes}/{$ano}"));
?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
<script language="JavaScript" src="../js/jquery-2.1.3.min.js"></script>
<html>
<script language="JavaScript" src="../funcoes/funcao.js"></script>
<script language="JavaScript" type="text/javascript">

    function AbreFoto(id_registro) {

        document.abre_foto.id_registro.value = id_registro;
        window.open("", "myNewWinFoto", "");
        var a = window.setTimeout("document.abre_foto.submit();", 500);
    }
    function fncLista() {
        window.open("lista_observacoess.php", "", "");
    }

    $(document).ready(function () {
        qtdeCheck = $("[id^='observacoes_'],[id^='qtd_horas_'],[id^='id_tipo_justificativa_']").length;

        $("[id^='observacoes_'],[id^='qtd_horas_'],[id^='id_tipo_justificativa_']").each(function () {
            if ($(this).val() != "" || $(this).not(':empty')) {
                $(this).attr('data-ok', 1);
            }
        });

        //$("[id^='observacoes_'],[id^='qtd_horas_'],[id^='id_tipo_justificativa_']").blur(function () {
        $("[id^='observacoes_'],[id^='qtd_horas_'],[id^='id_tipo_justificativa_']").blur(function () {
            if ($(this).val() != "" || $(this).not(':empty')) {
                $(this).attr('data-ok', 1);
            } else {
                $(this).attr('data-ok', 0);
            }
        });
    });

    function getMinutos(horas) {
        v = horas.split(':');
        return parseInt(v[0] * 60) + parseInt(v[1]);
    }

    function temExcesso() {
        var minutosTrabalhados = parseInt('<?php echo getMinutos(getHorasTrabalhadas(getUsuario()->id_usr,"{$dia}/{$mes}/{$ano}"))?>');
        var minutos = 0;
        var minutosGrade = parseInt('<?php echo getMinutos(getHorasGrade(getUsuario()->id_grade));?>');
        $('input[id^=qtd_horas]').each(function () {
            arr = $(this).val().split(/:/);
            minutos += parseInt(arr[0]) * 60 + parseInt(arr[1]);
        });
        if (minutosTrabalhados > minutosGrade) {
            return false
        } else {
            return minutos > minutosGrade;
        }
    }

    function fncValida() {
        var error = ( qtdeCheck < ( $('[data-ok="1"]').length ) );
        //console.log('error',qtdeCheck,error);
        var totalMinutos = 0;
        if (temExcesso()) {
            alert('Horas justificadas excedem a quantidade de <?php echo getHorasGrade(getUsuario()->id_grade);?> horas definidas na grade!');
            return false;
        }
        for (var x = 1; x <= <?php echo getQtdePontosEsperados($id_usr);?>; x++) {
            if ($("[id^='id_tipo_justificativa_" + x + "']").val() == 6 ||
                $("[id^='id_tipo_justificativa_" + x + "']").val() == 65) {
                totalMinutos += getMinutos($("[id^='qtd_horas_" + x + "']").val());
            }

            if ($("[id^='observacoes_" + x + "']").val() == "") {
                alert('Por favor informe a justificativa!');
                error = true;
                return;
            }
            if ($("[id^='qtd_horas_" + x + "']").val() == "") {
                alert('Por favor informe as horas!');
                error = true;
                return;
            }
            if ($("[id^='id_tipo_justificativa_" + x + "']").val() == 0) {
                alert('Por favor informe o tipo!');
                error = true;
                return;
            }
        }
        if (totalMinutos + <?php echo $minutosCota;?> > (24 * 60)) {
            alert('Horas excedem o limite permitido para abono! ');
            error = true;
            return;
        }

        if (error == false) {
            document.getElementById('botao_registra').style.display = 'none';
            document.formulario.submit();
            setTimeout("$('#registro-ok').css('display','');", 1000);
        }
    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
    }

    limite_1 = 5000;
    function soma_1() {
        var mais_um_1 = eval(document.formulario.observacoes_1.value.length - 1);
        mais_um_1++;
        if (document.formulario.observacoes_1.value.length > limite_1) {
            document.formulario.observacoes_1.value = '';
            document.formulario.observacoes_1.value = valor_limite_1;
            alert("Limite de " + limite + " caracteres excedido!");
        } else {
//document.frmCOF.exibe.value=''; 
//document.frmCOF.exibe.value=eval(mais_um); 
            valor_limite_1 = document.formulario.observacoes_1.value;
            document.formulario.exibe_1.value = '';
            document.formulario.exibe_1.value = (limite_1 - mais_um_1);
        }
        $(document.formulario.observacoes_1).attr('data-fill', mais_um_1);
        document.formulario.observacoes_1.focus();
    }

    limite_2 = 5000;
    function soma_2() {
        var mais_um_2 = eval(document.formulario.observacoes_2.value.length - 1);
        mais_um_2++;
        if (document.formulario.observacoes_2.value.length > limite_2) {
            document.formulario.observacoes_2.value = '';
            document.formulario.observacoes_2.value = valor_limite_2;
            alert("Limite de " + limite + " caracteres excedido!");
        } else {
//document.frmCOF.exibe.value=''; 
//document.frmCOF.exibe.value=eval(mais_um); 
            valor_limite_2 = document.formulario.observacoes_2.value;
            document.formulario.exibe_2.value = '';
            document.formulario.exibe_2.value = (limite_2 - mais_um_2);
        }
        $(document.formulario.observacoes_2).attr('data-fill', mais_um_2);
        document.formulario.observacoes_2.focus();
    }


    limite_3 = 5000;
    function soma_3() {
        var mais_um_3 = eval(document.formulario.observacoes_3.value.length - 1);
        mais_um_3++;
        if (document.formulario.observacoes_3.value.length > limite_3) {
            document.formulario.observacoes_3.value = '';
            document.formulario.observacoes_3.value = valor_limite_3;
            alert("Limite de " + limite + " caracteres excedido!");
        } else {
//document.frmCOF.exibe.value=''; 
//document.frmCOF.exibe.value=eval(mais_um); 
            valor_limite_3 = document.formulario.observacoes_3.value;
            document.formulario.exibe_3.value = '';
            document.formulario.exibe_3.value = (limite_3 - mais_um_3);
        }
        $(document.formulario.observacoes_3).attr('data-fill', mais_um_3);
        document.formulario.observacoes_3.focus();
    }


    limite_4 = 5000;
    function soma_4() {
        var mais_um_4 = eval(document.formulario.observacoes_4.value.length - 1);
        mais_um_4++;
        if (document.formulario.observacoes_4.value.length > limite_4) {
            document.formulario.observacoes_4.value = '';
            document.formulario.observacoes_4.value = valor_limite_4;
            alert("Limite de " + limite + " caracteres excedido!");
        } else {
//document.frmCOF.exibe.value=''; 
//document.frmCOF.exibe.value=eval(mais_um); 
            valor_limite_4 = document.formulario.observacoes_4.value;
            document.formulario.exibe_4.value = '';
            document.formulario.exibe_4.value = (limite_4 - mais_um_4);
        }
        $(document.formulario.observacoes_4).attr('data-fill', mais_um_4);
        document.formulario.observacoes_4.focus();
    }


    function txtBoxFormat(objeto, sMask, evtKeyPress) {
        var i, nCount, sValue, fldLen, mskLen, bolMask, sCod, nTecla;


        if (document.all) { // Internet Explorer
            nTecla = evtKeyPress.keyCode;
        } else if (document.layers) { // Nestcape
            nTecla = evtKeyPress.which;
        } else {
            nTecla = evtKeyPress.which;
            if (nTecla == 8) {
                return true;
            }
        }

        sValue = objeto.value;

        // Limpa todos os caracteres de formatação que
        // já estiverem no campo.
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(" ", "");
        sValue = sValue.toString().replace(" ", "");
        fldLen = sValue.length;
        mskLen = sMask.length;

        i = 0;
        nCount = 0;
        sCod = "";
        mskLen = fldLen;

        while (i <= mskLen) {
            bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ":"))
            bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

            if (bolMask) {
                sCod += sMask.charAt(i);
                mskLen++;
            }
            else {
                sCod += sValue.charAt(nCount);
                nCount++;
            }

            i++;
        }

        objeto.value = sCod;

        if (nTecla != 8) { // backspace
            if (sMask.charAt(i - 1) == "9") { // apenas números...
                return ((nTecla > 47) && (nTecla < 58));
            }
            else { // qualquer caracter...
                return true;
            }
        }


        else {
            return true;
        }
    }
</script>

<head>
    <title>Justificativa de Ocorr&ecirc;ncia</title>

</head>

<body>
<table width="898" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
    <tr valign="bottom">
        <td height="25" class="Titulo_rel">
            <table width="100%" border="0" cellpadding="0" cellspacing="2">
                <tr>
                    <td width="115"><strong><font style="font-size:10px" color="#333333">
                                &nbsp;&nbsp;&nbsp;&nbsp;<font size="3">Justificativa </font></font></strong></td>
                    <td width="820"><strong><font style="font-size:10px" color="#333333"><font size="3">
                                    <script>//exibeFash('tolerancia2.swf', 760, 50)</script>
                                </font></font></strong></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="Itens_rel_s_traco">
        <td height="15">
            <form action="grava_justificativa.php" method="post" name="formulario" id="formulario"
                  enctype="multipart/form-data" target="janela">
                <input type="hidden" name="atualizado" value="1"/>
                <table border="0" align="center" cellpadding="2" cellspacing="0">
                    <tr>
                        <td colspan="2" style="border-bottom: 1px solid #cccccc">
                            <table width="890" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="185" align="center">
                                        <div align="center">
                                            <?php if (file_exists($_SESSION['config']->pastaFotosUsuarios . $id_usr . '.jpg')) { ?>
                                                <img style="height:150px"
                                                     src="<?php echo data_uri($_SESSION['config']->pastaFotosUsuarios . $id_usr . '.jpg'); ?>"/>
                                            <?php } ?>
                                        </div>
                                    </td>
                                    <td width="360" valign="top"><strong><br>
                                        </strong>
                                        <table width="89%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td colspan="2" align="center"><strong><font color="#0066FF" size="2">
                                                            <?php echo $nome[0] . " " . $nome[count($nome) - 1]; ?></font><font
                                                            color="#0066FF"><br>
                                                            <font
                                                                color="#FF6600"><?php echo $resultado_consulta[sigla]; ?>
                                                            </font></font></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" colspan="2" align="center"><font color="#666666">___________________</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="25" colspan="2" align="center"><strong>Sua
                                                        Grade:</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center"><strong><font color="#333333">
                                                            <?php
                                                            echo $entrada_1 . "<br>";
                                                            echo $saida_1 . "<br>";
                                                            if ($resultado_consulta_grade[tipo_grade] == 0) {
                                                                echo $entrada_2 . "<br>";
                                                                echo $saida_2 . "<br>";
                                                            }; ?>
                                                        </font></strong></td>
                                            </tr>
                                        </table>
                                        <strong><br>
                                            <font color="#333333"></font></strong></td>
                                    <td width="360" valign="top">
                                        <table width="340" height="57" border="0" align="center" cellpadding="5"
                                               cellspacing="0" class="Tabela_rel">
                                            <tr>
                                                <td colspan="4" align="center"><strong>Hor&aacute;rios
                                                        registrados em <font
                                                            color="#FF0000"><?php echo $dia . "/" . $mes . "/" . $ano; ?></font></strong>
                                                </td>
                                            </tr>
                                            <tr><br>
                                                <?php
                                                for ($i = 1; $i < 5; $i++) {
                                                    $sql_registro = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = " . $i . " order by id_registro";
                                                    $dados_registro = mysqli_query( $conexao, $sql_registro);
                                                    $resultado_registro = mysqli_fetch_array($dados_registro);
                                                    ?>
                                                    <td width="64" align="center" valign="top">
                                                        <?php if ($resultado_registro[hora] != "") { ?>
                                                            <img src="../images/timthumb.png" width="64"
                                                                 height="64"><br>
                                                        <?php } ?>
                                                        <?php if ($resultado_registro[hora] == "") { ?>
                                                            <img src="../images/notime.png" width="64" height="64">
                                                            <br>
                                                        <?php } ?>
                                                        <br> <font color="#FF3300"><strong><font size="3">
                                                                    <?php
                                                                    if ($resultado_registro[hora] != "") {
                                                                        echo str_pad($resultado_registro[hora], 2, 0, STR_PAD_LEFT) . ":" . str_pad($resultado_registro[minutos], 2, 0, STR_PAD_LEFT);
                                                                    }
                                                                    ?>
                                                                </font></strong></font></td>
                                                <?php } ?>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td width="830"><input name="id_usr" type="text" id="id_usr" value="<?php echo $id_usr ?>"
                                               style="display:none">
                            <input name="dia" type="text" id="dia" value="<?php echo $dia ?>" style="display:none">
                            <input name="mes" type="text" id="mes" value="<?php echo $mes ?>" style="display:none">
                            <input name="ano" type="text" id="ano" value="<?php echo $ano ?>" style="display:none">
                            <input name="id_depto" type="text" id="id_depto" value="<?php echo $id_depto ?>"
                                   style="display:none"></td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top"><strong><font style="font-size:14px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Preencha
                                    somente os pontos que deseja justificar e clique em Registrar Justificativa
                                    no final desta p&aacute;gina: <br>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#FF0000">* Se for
                                        falta pelo dia inteiro, preencha somente a primeira justificativa!</font><br>
                                    <br>
                                </font></strong>
                            <?php
                            //Gilsa pediu para retirar se horas forem positivas
                            $detalhes = getDetalhesDoDia($id_usr, "{$dia}/{$mes}/{$ano}");
                            if (getMinutos($detalhes['saldo']) < 0) {
                                ?>
                                <table
                                    style="border:1px solid #ccc;border-collapse: collapse;width:100%;padding:10px;font-size:1.5em;">
                                    <tr style="background-color: #eeeeee;">
                                        <th>Horas Previstas</th>
                                        <th>Horas Trabalhadas</th>
                                        <th>Saldo do Dia</th>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><?php echo $detalhes['horasGrade'] ?></td>
                                        <td><?php echo $detalhes['horasTrabalhadas']; ?></td>
                                        <td style="<?php if (strpos($detalhes['saldo'], '-') === 0) {
                                            echo 'color:#FF0000;';
                                        } ?>"><?php echo $detalhes['saldo']; ?></td>
                                    </tr>
                                </table>
                            <?php
                            }

                            if ($tipo_grade == 2) {
                                $quantidade = 3;
                            } else {
                                $quantidade = 5;
                            }

                            $descumpriuJornada = getUsuario($id_usr)->descumpriuJornada("{$dia}/{$mes}/{$ano}");
                            $temHoranaoAutorizada = getUsuario($id_usr)->temHoraNaoAutorizada("{$dia}/{$mes}/{$ano}");

                            for ($i = 1; $i < $quantidade; $i++) {
                                $sql_agenda = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and id_usr = " . $id_usr . " and etapa = " . $i . " order by etapa";
                                $dados_agenda = mysqli_query( $conexao, $sql_agenda);
                                $resultado_agenda = mysqli_fetch_array($dados_agenda);
                                if ($resultado_agenda[hora] != '') {
                                    $horario = str_pad($resultado_agenda[hora], 2, 0, STR_PAD_LEFT) . ":" . str_pad($resultado_agenda[minutos], 2, 0, STR_PAD_LEFT);
                                } else {
                                    $horario = "";
                                }
								if(getUsuario()->id_usr == "07089849910"){
								
								}
                                if (geraOcorrencia($resultado_agenda['id_registro'])) {
                                    ?>
                                    <table width="476" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="314">
                                                <DIV class="commit commit-tease js-details-container">
                                                    <font color="#000033" style="width:100%;">
                                                        <strong>
                                                            <font size="3">
                                                                <?php echo $i; ?>º registro de ponto
                                                            </font>
                                                        </strong>
                                                    </font>
                                                    <br/><br/>

                                                    <DIV class="commit-meta">
                                                        <table width="809" border="0" align="center" cellpadding="4"
                                                               cellspacing="0">
                                                            <tr>
                                                                <td width="50" rowspan="4" valign="top">
                                                                    <?php
                                                                    $nome_arq = "../registro/" . $resultado_agenda[id_registro] . ".jpg";
                                                                    if (is_file($nome_arq) == 1) {
                                                                        ?>
                                                                        <div style='cursor:pointer;'
                                                                             onClick='AbreFoto(<?php echo $resultado_agenda[id_registro]; ?>)'
                                                                             ;><img
                                                                                src="../registro/<?php echo $resultado_agenda[id_registro]; ?>.jpg"
                                                                                width="100" border="0"></div>
                                                                    <?php } else { ?>
                                                                        <img src="../images/no_photo.jpg" width="100"
                                                                             height="100" border="0">
                                                                    <?php } ?>
                                                                </td>
                                                                <td width="120" valign="middle">Hor&aacute;rio:</td>
                                                                <td width="150" valign="top"><strong><font size="3"
                                                                                                           color="#FF0000"><?php echo $horario; ?></font></strong>
                                                                    <input name="id_registro_<?php echo $i; ?>"
                                                                           type="text"
                                                                           size="3"
                                                                           value="<?php echo $resultado_agenda[id_registro]; ?>"
                                                                           style="display:none"> </strong>
                                                                </td>
                                                                <td width="576" rowspan="3" valign="top">&nbsp; </td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td>Tipo:</td>
                                                                <td>
                                                                    <select
                                                                        name="id_tipo_justificativa_<?php echo $i; ?>"
                                                                        id="id_tipo_justificativa_<?php echo $i; ?>"
                                                                        <?php if (($temHoranaoAutorizada || !$descumpriuJornada) && $i == 1) {
                                                                            echo "readonly";
                                                                        } ?>
                                                                        >
                                                                        <?php if (!$temHoranaoAutorizada && $descumpriuJornada) { ?>
                                                                            <option value="0">Selecione</option>
                                                                        <?php } ?>
                                                                        <?php
                                                                        while (${"resultado_tipo_$i"} = mysqli_fetch_assoc(${"dados_tipo_$i"})) {
                                                                            if (
                                                                                (
																					($temHoranaoAutorizada && ${"resultado_tipo_$i"}[id_tipo_justificativa] != BaseObject::ID_HORAS_NAO_AUTORIZADAS )||
                                                                                    (!$descumpriuJornada && ${"resultado_tipo_$i"}[id_tipo_justificativa] == BaseObject::ID_DESCUMPRIMENTO_DE_GRADE)
                                                                                )
                                                                                && $i == 1
                                                                            ) {
                                                                                continue;
                                                                            }
                                                                            ?>
                                                                            <option
                                                                                value="<?php echo ${"resultado_tipo_$i"}[id_tipo_justificativa]; ?>"><?php echo ${"resultado_tipo_$i"}[titulo]; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="middle">Qtd horas:<strong> </strong></td>
                                                                <td valign="top"><strong>
                                                                        <input name="qtd_horas_<?php echo $i; ?>"
                                                                               id="qtd_horas_<?php echo $i; ?>"
                                                                               type="text"
                                                                            <?php if ($temHoranaoAutorizada && $i == 1) { ?>
                                                                                value="<?php echo $detalhes['saldo']; ?>"
                                                                                readonly
                                                                            <?php } ?>
                                                                               size="5" maxlength="5"
                                                                               onKeyPress="return txtBoxFormat(this, '99:99', event);">
                                                                        <strong><font color="#FF0000">Formato:
                                                                                00:00</font></strong></strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="middle">&nbsp;</td>
                                                                <td valign="top">&nbsp;</td>
                                                                <td valign="top">&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        <table width="455" border="0" cellspacing="0">
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Justificativa do
                                                                        funcion&aacute;rio:</strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="455" align="right"><textarea
                                                                        name="observacoes_<?php echo $i; ?>" cols="150"
                                                                        rows="4" id="observacoes_<?php echo $i; ?>"
                                                                        style="font-family: Arial; font-size: 8 pt; "
                                                                        onKeyPress="soma_<?php echo $i; ?>(this.value)"
                                                                        onKeyUp="soma_<?php echo $i; ?>(this.value)"
                                                                        data-fill="0"
                                                                        ><?php
                                                                        if ($i == 1) {
                                                                            echo $observacoes_1;
                                                                        }
                                                                        if ($i == 2) {
                                                                            echo $observacoes2;
                                                                        }
                                                                        if ($i == 3) {
                                                                            echo $observacoes_3;
                                                                        }
                                                                        if ($i == 4) {
                                                                            echo $observacoes_4;
                                                                        }
                                                                        ?></textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right"><font size="1">Caracteres
                                                                        restantes</font>
                                                                    <input type="text" name="exibe_<?php echo $i; ?>"
                                                                           size="3" maxlength="3" readonly="yes">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br>
                                                        &nbsp;&nbsp;<img src="../images/attachment.png"
                                                                         alt="Permite adicionar atestados, documentos ou comprovantes da sua justificativa."
                                                                         title="Permite adicionar atestados, documentos ou comprovantes da sua justificativa."
                                                                         width="25" height="25" align="absmiddle">
                                                        Anexo (opcional):
                                                        <input name="arquivo_<?php echo $i; ?>" type="file"
                                                               id="arquivo_<?php echo $i; ?>2" size="50">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        </tr>
                                    </table>
                                <?php } ?>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr id="registro-ok" style="display:none;">
                        <td colspan="2" valign="top">
                            <table width="740" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr align="center">
                                    <td>&nbsp;</td>
                                </tr>
                                <tr align="center">
                                    <td>&nbsp;</td>
                                </tr>
                                <tr align="center">
                                    <td width="314">
                                        <table border="0" align="center"
                                               cellpadding="4" cellspacing="0">
                                            <tr>
                                                <td><h1>Justificativa registrada! Você já pode fechar esta aba.</h1>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <p>&nbsp;</p>

                <div id="botao_registra">
                    <table width="379" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr align="center">
                            <td width="314">
                                <table width="170" height="25" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="center">
                                            <div id="resultado1" class="tryit" onClick="fncValida();">Registrar
                                                Justificativa
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="162">
                                <table width="87" height="25" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="center">
                                            <div id="resultado1" class="tryit" onClick="document.formulario.reset();">
                                                Limpar
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <p>&nbsp;</p>
            </form>
            <br>
        </td>
    </tr>
    <tr class="Sub_titulo_rel">
        <td height="15">&nbsp;</td>
    </tr>
</table>


<form name="abre_foto" method="post" action="mostra_foto.php" target="myNewWinFoto">
    <input type="hidden" name="id_registro">
</form>

<iframe name="janela" width="800" height="200" style="display:none"></iframe>
</body>
</html>
