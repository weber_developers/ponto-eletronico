﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 && $_SESSION["sessao_id_usr"] == 0) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.location.href = 'index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}

require_once("../funcoes/conexao.php");


if ($_SESSION["usr_gerente"] >= 1) {
    if ($_SESSION["sessao_id_usr"] == 55 || $_SESSION["sessao_id_usr"] == 4) {
        $sql_usuarios = "select id_usr,nome,id_grade from usuarios where id_depto = " . $_SESSION["sessao_id_depto"] . " and ativo = 1 and registra = 1 order by nome";
    } else {
        $sql_usuarios = "select id_usr,nome,id_grade from usuarios where id_depto = " . $_SESSION["sessao_id_depto"] . " and ativo = 1 and registra = 1 and id_usr <> 4 order by nome";
    }

//	$sql_usuarios = "select id_usr,nome,id_grade from usuarios where id_depto = ".$_SESSION["sessao_id_depto"]." and ativo = 1 order by nome";
    $dados_usuarios = mysqli_query( $conexao, $sql_usuarios);
}
$id_usr = $_REQUEST["id_usr"];
if ($id_usr == "") {
    // echo $_SESSION["usr_gerente"];
    if ($_SESSION["sessao_rh"] != 1 && $_SESSION["usr_gerente"] != 1 && $_SESSION["usr_gerente"] != 2) {
        $id_usr = $_SESSION["sessao_id_usr"];
    } else {
        $id_usr = 0;
    }
}

$sql_usuarios2 = "select id_grade from usuarios where id_usr = " . $id_usr;

$dados_usuarios2 = mysqli_query( $conexao, $sql_usuarios2);
$resultado_usuarios2 = mysqli_fetch_array($dados_usuarios2);

$sql_consulta_grade = "select * from p_grade where id_grade = " . $resultado_usuarios2[id_grade];
$dados_consulta_grade = mysqli_query( $conexao, $sql_consulta_grade);
$resultado_consulta_grade = mysqli_fetch_array($dados_consulta_grade);
$entrada_1 = $resultado_consulta_grade[entrada_1];
$saida_1 = $resultado_consulta_grade[saida_1];
$entrada_2 = $resultado_consulta_grade[entrada_2];
$saida_2 = $resultado_consulta_grade[saida_2];

//	$sql_mostra 	= "select foto,nome,gerente from usuarios where id_usr = ".$id_usr." and ativo = 1 and registra = 1";
$sql_mostra = "select foto,nome,gerente from usuarios where id_usr = " . $id_usr;
$dados_mostra = mysqli_query( $conexao, $sql_mostra);
$resultado_mostra = mysqli_fetch_array($dados_mostra);
$foto = $resultado_mostra[foto];

$gerente_status = $resultado_mostra[gerente];
$nome = explode(' ', $resultado_mostra[nome]);
$id_depto = $_REQUEST["id_depto"];
if ($id_depto == "") {
    $id_depto = 0;
}

$domingo = "style=color:#C30; bgcolor='#FFDDDD'";
$evento_classe = "style=color:#C30; bgcolor='#666666'";
$hoje = "style=color:#0066ff;";
$mes_p = $_POST["mes_p"];
if ($mes_p == "") {
    if (date('d') < 10) {
        $mes_p = date("m") - 1;
    } else {
        $mes_p = date("m");
    }
}

if (strlen($mes_p) < 2) {
    $mes_p = "0" . $mes_p;
}
$dia = date("d");
$ano_p = $_POST["ano_p"];

if ($ano_p == "") {
    $ano_p = date("Y");
}


$ano_ = substr($ano_p, -2);


function meses($a)
{
    switch ($a) {
        case 1:
            $mes = "Janeiro";
            break;
        case 2:
            $mes = "Fevereiro";
            break;
        case 3:
            $mes = "Março";
            break;
        case 4:
            $mes = "Abril";
            break;
        case 5:
            $mes = "Maio";
            break;
        case 6:
            $mes = "Junho";
            break;
        case 7:
            $mes = "Julho";
            break;
        case 8:
            $mes = "Agosto";
            break;
        case 9:
            $mes = "Setembro";
            break;
        case 10:
            $mes = "Outubro";
            break;
        case 11:
            $mes = "Novembro";
            break;
        case 12:
            $mes = "Dezembro";
            break;
    }
    return $mes;
}

$mes_ex = meses($mes_p);

$sql_horas_aguardando = "select qtd_horas from p_justificativa where id_tipo_justificativa <> 65 and id_usr = " . $id_usr . " and month(data_justificativa) = " . $mes_p . " and year(data_justificativa) = " . $ano_p . " and abonado = 0";

$dados_horas_aguardando = mysqli_query( $conexao, $sql_horas_aguardando);
while ($resultado_horas_aguardando = mysqli_fetch_array($dados_horas_aguardando)) {
    //echo $resultado_horas_aguardando[qtd_horas]."<BR>";
    $horario_1_banco = $resultado_horas_aguardando[qtd_horas];
    $horario_1_banco = explode(':', $horario_1_banco);
    $total_horas = (60 * 60 * $horario_1_banco[0]) + (60 * $horario_1_banco[1]) + $total_horas;
}
$total_horas = $total_horas / (60 * 60);
$tempo1 = explode('.', $total_horas); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
$hora1 = $tempo1[0];
@$minutos1 = (float)(0) . '.' . $tempo1[1]; // Aqui forçamos a conversão para float, para não ter erro.
$minutos1 = round($minutos1 * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.
$minutos1 = explode('.', $minutos1); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
$minutos1 = $minutos1[0];
if ($minutos1 < 10) {
    $minutos1 = "0" . $minutos1;
}
if ($hora1 < 10) {
    $hora1 = "0" . $hora1;
}
$total_1 = $hora1 . ':' . $minutos1;
//echo $total_horas."<BR>";
//echo $total_1."<BR>";
//die;
$sql_horas_nao_abonadas = "select qtd_horas from p_justificativa where id_usr = " . $id_usr . " and month(data_justificativa) = " . $mes_p . " and year(data_justificativa) = " . $ano_p . " and (abonado = 2 or abonado = 3)";
$dados_horas_nao_abonadas = mysqli_query( $conexao, $sql_horas_nao_abonadas);
while ($resultado_horas_nao_abonadas = mysqli_fetch_array($dados_horas_nao_abonadas)) {
    //echo $resultado_horas_aguardando[qtd_horas]."<BR>";
    $horario_2_banco = $resultado_horas_nao_abonadas[qtd_horas];
    $horario_2_banco = explode(':', $horario_2_banco);
    $total_horas2 = (60 * 60 * $horario_2_banco[0]) + (60 * $horario_2_banco[1]) + $total_horas2;
}
$total_horas2 = $total_horas2 / (60 * 60);
$tempo2 = explode('.', $total_horas2); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
$hora2 = $tempo2[0];
@$minutos2 = (float)(0) . '.' . $tempo2[1]; // Aqui forçamos a conversão para float, para não ter erro.
$minutos2 = round($minutos2 * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.
$minutos2 = explode('.', $minutos2); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
$minutos2 = $minutos2[0];
if ($minutos2 < 10) {
    $minutos2 = "0" . $minutos2;
}
if ($hora2 < 10) {
    $hora2 = "0" . $hora2;
}
$total_2 = $hora2 . ':' . $minutos2;


$sql_horas_abonadas = "select cota,qtd_horas from p_justificativa inner join p_tipo_justificativa on p_justificativa.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa where p_justificativa.id_usr = " . $id_usr . " and month(data_justificativa) = " . $mes_p . " and year(data_justificativa) = " . $ano_p . " and abonado = 1";

$dados_horas_abonadas = mysqli_query( $conexao, $sql_horas_abonadas);
while ($resultado_horas_abonadas = mysqli_fetch_array($dados_horas_abonadas)) {
    //echo $resultado_horas_aguardando[qtd_horas]."<BR>";
    $horario_3_banco = $resultado_horas_abonadas[qtd_horas];
    $horario_3_banco = explode(':', $horario_3_banco);

    if ($resultado_horas_abonadas[cota] == 3) {
        $total_horas4 = (60 * 60 * $horario_3_banco[0]) + (60 * $horario_3_banco[1]) + $total_horas4;
    } else {
        $total_horas3 = (60 * 60 * $horario_3_banco[0]) + (60 * $horario_3_banco[1]) + $total_horas3;
    }
}
$total_horas3 = $total_horas3 / (60 * 60);
$tempo3 = explode('.', $total_horas3); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
$hora3 = $tempo3[0];
@$minutos3 = (float)(0) . '.' . $tempo3[1]; // Aqui forçamos a conversão para float, para não ter erro.
$minutos3 = round($minutos3 * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.
$minutos3 = explode('.', $minutos3); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
$minutos3 = $minutos3[0];
if ($minutos3 < 10) {
    $minutos3 = "0" . $minutos3;
}
if ($hora3 < 10) {
    $hora3 = "0" . $hora3;
}
$total_3 = $hora3 . ':' . $minutos3;


$total_horas4 = $total_horas4 / (60 * 60);
$tempo4 = explode('.', $total_horas4); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
$hora4 = $tempo4[0];
@$minutos4 = (float)(0) . '.' . $tempo4[1]; // Aqui forçamos a conversão para float, para não ter erro.
$minutos4 = round($minutos4 * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.
$minutos4 = explode('.', $minutos4); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
$minutos4 = $minutos4[0];
if ($minutos4 < 10) {
    $minutos4 = "0" . $minutos4;
}
if ($hora4 < 10) {
    $hora4 = "0" . $hora4;
}
$total_4 = $hora4 . ':' . $minutos4;

//	echo $id_usr;
$numr_ip = $_SERVER[HTTP_X_FORWARDED_FOR] . "-" . $_SERVER[REMOTE_ADDR];
if ($_SESSION["sessao_id_usr"] != $id_usr) {
    //$sql3 = "insert into acessos (id_usr,data_acesso,entrada,numr_ip,visao,ho) values (".$_SESSION["sessao_id_usr"].",'".date("Y/m/d H:i:s")."','".date("H:i:s")."','".$numr_ip."',1,".$id_usr.")";
    $sql3 = "update acessos set visao = 1, ho = " . $id_usr . " where id_acesso = " . $_SESSION["id_acesso"];
    $dados3 = mysqli_query( $conexao, $sql3);
}


?>


<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">

<html>
<script language="JavaScript" type="text/javascript">
    function fncValida() {
        if (<?php echo $_SESSION["usr_gerente"];?>!=
        0 || <?php echo $_SESSION["sessao_rh"];?> != 0
    )
        {
            if (document.funcionario.id_usr.value == 0) {
                alert("Selecione um funcionário!");
                document.funcionario.id_usr.focus();
                return false;
            }
        }
        document.funcionario.submit()
    }
    function fncMostratodos() {
        document.abre_janela4.mes.value = document.funcionario.mes_p.value;
        document.abre_janela4.ano.value = document.funcionario.ano_p.value;
        document.abre_janela4.id_usr.value = document.justificativa.id_usr.value;
        document.abre_janela4.submit();

    }
    function AbreFotos(dia, mes, ano, id_usr) {
        document.mostra_fotos.dia.value = dia;
        document.mostra_fotos.mes.value = mes;
        document.mostra_fotos.ano.value = ano;
        window.open("", "myNewWin7", "");
        var a = window.setTimeout("document.mostra_fotos.submit();", 500);
    }
    function envia_dados(id_depto, id_usr, mes, ano, onde) {
        document.ocorrencias.id_depto.value = id_depto;
        document.ocorrencias.id_usr.value = id_usr;
        document.ocorrencias.mes_p.value = mes;
        document.ocorrencias.ano_p.value = ano;
        document.ocorrencias.submit();
    }
    function AbreJanela(id_usr, dia, mes, ano) {

        document.abre_janela.dia.value = dia;
        document.abre_janela.mes.value = mes;
        document.abre_janela.ano.value = ano;
        document.abre_janela.id_depto.value =<?php echo $id_depto;?>;
        document.abre_janela.id_usr.value =<?php echo $id_usr;?>;
//    window.open("","myNewWin","width=500,height=300,toolbar=0");
        window.open("", "myNewWin", "");
        var a = window.setTimeout("document.abre_janela.submit();", 500);
    }
    function AbreJustifivativa(id_usr, dia, mes, ano) {

        //document.abre_janela2.id_justificativa.value=id_justificativa;
        document.abre_janela2.id_usr.value = id_usr;
        document.abre_janela2.dia.value = dia;
        document.abre_janela2.mes.value = mes;
        document.abre_janela2.ano.value = ano;
        document.abre_janela.id_depto.value =<?php echo $id_depto;?>;
        document.abre_janela.id_usr.value =<?php echo $id_usr;?>;
//    window.open("","myNewWin","width=500,height=300,toolbar=0");
        window.open("", "myNewWin", "");
        var a = window.setTimeout("document.abre_janela2.submit();", 500);
    }

    function fncColoca() {
        if (<?php echo $_SESSION["sessao_rh"];?>==1 || <?php echo $_SESSION["usr_gerente"]?> == 2
    )
        {

            setTimeout("document.funcionario.id_depto.value='<?php echo $id_depto?>'", 300);
            setTimeout("fncMontacombo('<?php echo $id_depto?>')", 600);

            if (<?php echo $id_usr;?>!=
            0
        )
            {

                setTimeout("fncColoca2()", 1000);

            }
        }

        if (<?php echo $_SESSION["usr_gerente"];?>>=
        1
    )
        {
            setTimeout("document.funcionario.id_usr.value='<?php echo $id_usr?>'", 1400);
        }
        setTimeout("fncColoca3()", 2000);
        setTimeout("fncColoca4()", 2300);
    }
    function fncColoca2() {
        document.funcionario.id_usr.value = '<?php echo $id_usr?>';
        setTimeout("document.funcionario.mes_p.value='<?php echo $mes_p?>';", 1800);
    }
    function fncColoca3() {

        document.funcionario.mes_p.value = '<?php echo $mes_p?>';
    }
    function fncColoca4() {
        document.funcionario.ano_p.value = '<?php echo $ano_p?>';
    }

    function fncMontacombo(id_depto) {
        janela.location.href = 'frmMontafuncionarios.php?id_depto=' + id_depto;
    }
    //window.open("justificar.php?", "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=500,height=600,top=30,left=50");

    function fnc_data(qtd) {
        mes = new Number(document.justificativa.mes_p.value);
        ano = new Number(document.justificativa.ano_p.value);
        valor = new Number(qtd);

        document.justificativa.mes_p.value = mes + valor;
        if (document.justificativa.mes_p.value == 0) {
            document.justificativa.mes_p.value = 12;
            document.justificativa.ano_p.value = ano - 1;
        }
        if (document.justificativa.mes_p.value == 13) {
            document.justificativa.mes_p.value = 1;
            document.justificativa.ano_p.value = ano + 1;
        }

        document.justificativa.submit();
    }

    function fncLista() {
        window.open("lista_usuarios.php", "",
            "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,top=200,left=20");
    }


    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
    }


    function txtBoxFormat(objeto, sMask, evtKeyPress) {
        var i, nCount, sValue, fldLen, mskLen, bolMask, sCod, nTecla;


        if (document.all) { // Internet Explorer
            nTecla = evtKeyPress.keyCode;
        } else if (document.layers) { // Nestcape
            nTecla = evtKeyPress.which;
        } else {
            nTecla = evtKeyPress.which;
            if (nTecla == 8) {
                return true;
            }
        }

        sValue = objeto.value;

        // Limpa todos os caracteres de formatação que
        // já estiverem no campo.
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(" ", "");
        sValue = sValue.toString().replace(" ", "");
        fldLen = sValue.length;
        mskLen = sMask.length;

        i = 0;
        nCount = 0;
        sCod = "";
        mskLen = fldLen;

        while (i <= mskLen) {
            bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ":"))
            bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

            if (bolMask) {
                sCod += sMask.charAt(i);
                mskLen++;
            }
            else {
                sCod += sValue.charAt(nCount);
                nCount++;
            }

            i++;
        }

        objeto.value = sCod;

        if (nTecla != 8) { // backspace
            if (sMask.charAt(i - 1) == "9") { // apenas números...
                return ((nTecla > 47) && (nTecla < 58));
            }
            else { // qualquer caracter...
                return true;
            }
        }


        else {
            return true;
        }
    }
</script>

<head>
    <title>Justificativa de Ponto</title>

</head>

<body onLoad="fncColoca();">
<?php require_once("frm_topo.php"); ?>

<br>
<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg">&nbsp;
                        <form name="funcionario" method="post" action="frm_justificativa.php">
                            <strong>&nbsp;&nbsp; <font size="3">Justificativa de Ponto</font><br>
                                &nbsp;&nbsp;</strong>
                            <table border="0" cellpadding="0" cellspacing="3">
                                <tr>
                                    <td height="12" align="right"><strong><font color="#666666" size="2">&nbsp;&nbsp;&nbsp;Ocorr&ecirc;ncia(s)
                                                de<a href="#" onClick="fncMostratodos();">:</a></font><font
                                                size="2"> </font><font color="#666666">
                                                <?php if ($_SESSION["sessao_rh"] == 1 || $_SESSION["usr_gerente"] == 2) { ?>
                                                    <select name="id_depto" id="id_depto" style="width:250px"
                                                            onChange="fncMontacombo(this.value);">
                                                        <option value="0" selected>Todas Unidades</option>
                                                        <?php
                                                        foreach ($oUsuario->getMeusDepartamentosArray() as $id => $nome) {
                                                            ?>
                                                            <option value="<?php echo $id; ?>">
                                                                <?php echo $nome; ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                <?php } ?>
                                            </font></strong></td>
                                    <td><strong></strong> <strong><font size="2"><font color="#FF3300">
                                                    <?php
                                                    if ($_SESSION["usr_gerente"] == 0 && $_SESSION["sessao_rh"] == 0) {
                                                        echo $_SESSION["sessao_usuario"];
                                                    } else {
                                                        ?>
                                                        <div id="div_id_usr"> &nbsp;&nbsp;
                                                            <select name="id_usr" id="id_usr" style="width:250px">
                                                                <option value="0">Selecione</option>
                                                                <?php foreach (getUsuario($_SESSION['sessao_id_usr'])->getMeusServidoresArray() as $id => $nome) { ?>
                                                                    <option value="<?php echo $id; ?>">
                                                                        <?php echo $nome; ?>
                                                                    </option>
                                                                <?php } ?>
                                                            </select>
                                                            <strong><font color="#666666"></font></strong></div>
                                                    <?php }
                                                    ?>
                                                </font></font> </strong></td>
                                    <td align="center"><strong><font color="#666666" size="2">&nbsp;em:</font></strong>
                                        <font size="2"><strong>
                                                <select name="mes_p" id="mes_p">
                                                    <option value="01">Janeiro</option>
                                                    <option value="02">Fevereiro</option>
                                                    <option value="03">Mar&ccedil;o</option>
                                                    <option value="04">Abril</option>
                                                    <option value="05">Maio</option>
                                                    <option value="06">Junho</option>
                                                    <option value="07">Julho</option>
                                                    <option value="08">Agosto</option>
                                                    <option value="09">Setembro</option>
                                                    <option value="10">Outubro</option>
                                                    <option value="11">Novembro</option>
                                                    <option value="12">Dezembro</option>
                                                </select>
                                            </strong> <strong><font color="#666666">de:</font></strong>
                                        </font>
                                        <select name="ano_p" id="ano_p">
                                            <?php echo montarComboAnos(); ?>
                                        </select>
                                        <?php // if($_SESSION["usr_gerente"]!=0||$_SESSION["sessao_rh"]!=0){?>
                                        <strong><font size="2"><font color="#FF3300"><a href="#" onClick="fncValida();"><img
                                                            src="../images/ok_bt.gif" width="27" height="15"
                                                            border="0"
                                                            title="Realiza o filtro"></a></font></font></strong>
                                        <?php // } ?>
                                    </td>
                                </tr>
                            </table>
                            <strong> </strong><strong><font size="2"><font color="#FF3300">
                                    </font></font></strong>
                        </form>
                    </td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" valign="top" bgcolor="#FFFFFF">
                        <?php if ($id_usr != 0) { ?>
                            <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0"
                                   bordercolor="#666666" bgcolor="#FFF2C2"
                                   style='border-left:solid #DFDFDF .2pt; border-right:solid #DFDFDF .2pt; border-top:solid #DFDFDF .2pt; border-bottom:solid #DFDFDF .2pt;'
                                   summary="Calendário">
                                <tr align="center">
                                    <td height="40" colspan="7" bgcolor="#bdd7e7">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="1%"><font color="#333333" size="4"><strong><font
                                                                color="#666666">
                                                            </font></strong></font></td>
                                                <td width="98%" height="60" align="center">
                                                    <form name="justificativa" method="post"
                                                          action="frm_justificativa.php">
                                                        <font color="#333333" size="4"><strong><font color="#0066FF"
                                                                                                     size="5">
                                                                </font><font color="#333333" size="4"><strong><font
                                                                            color="#333333"
                                                                            size="4"></font></strong></font><font
                                                                    color="#0066FF" size="5">
                                                                </font></strong></font>
                                                        <table width='48%' border='0' cellpadding='3' cellspacing='0'>
                                                            <tr>
                                                                <td width="1%">
                                                                    <div style='cursor:pointer;'
                                                                         onClick="fnc_data(-1);"><img
                                                                            src="../images/seta_esq.jpg" width="15"
                                                                            height="15" border="0"></div>
                                                                </td>
                                                                <td width="92%" align="center"><font color="#0066FF"
                                                                                                     size="5"><strong><?php echo $mes_ex . " de " . $ano_p . ""; ?></strong></font>
                                                                </td>
                                                                <td width="7%">
                                                                    <div style='cursor:pointer;' onClick="fnc_data(1);">
                                                                        <img src="../images/seta_dir.jpg" width="15"
                                                                             height="15" border="0"></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <input type="text" name="id_usr" value="<?php echo $id_usr; ?>"
                                                               style="display:none">
                                                        <input type="text" name="mes_p" value="<?php echo $mes_p; ?>"
                                                               style="display:none">
                                                        <input type="text" name="ano_p" value="<?php echo $ano_p; ?>"
                                                               style="display:none">
                                                        <input type="text" name="id_depto"
                                                               value="<?php echo $id_depto; ?>" style="display:none">
                                                    </form>
                                                    <font color="#333333" size="4"><strong><font color="#0066FF"
                                                                                                 size="5">
                                                            </font><font color="#333333"
                                                                         size="4"><strong></strong></font>
                                                            <font color="#0066FF" size="5"> </font></strong></font></td>
                                                <td width="1%" valign="top"><a href="#"
                                                                               onClick="window.parent.div_calendario.style.display='none';"
                                                                               title="Fechar esta janela"><br>
                                                    </a></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="icon_sous_nav">
                                    <th width="110" height="25" title="Domingo" abbr="Domingo">
                                        <b <?php echo("$domingo"); ?>>Dom</b></th>
                                    <th width="110" title="Segunda" abbr="Segunda"><b>Seg</b></th>
                                    <th width="110" title="Terça" abbr="Terça"><b>Ter</b></th>
                                    <th width="110" title="Quarta" abbr="Quarta"><b>Qua</b></th>
                                    <th width="110" title="Quinta" abbr="Quinta"><b>Qui</b></th>
                                    <th width="110" title="Sexta" abbr="Sexta"><b>Sex</b></th>
                                    <th width="110" title="Sábado" abbr="Sábado"><b>Sab</b></th>
                                </tr>
                                <tbody>
                                <?php
                                $Data = strtotime($mes_p . "/" . $dia . "/" . $ano_p);
                                $Dia = date('w', strtotime(date('n/\0\1\/Y', $Data)));
                                $Dias = date('t', $Data);
                                $dias_ok = 0;

                                for ($i = 1, $d = 1; $d <= $Dias;) {

                                    echo("<tr>");
                                    for ($x = 1; $x <= 7 && $d <= $Dias; $x++, $i++) {
                                        $mostra_botao = 0;

                                        if ($x == 1 || $x == 7) {
                                            //$total_acum=0;
                                        }
                                        if ($i > $Dia) {
                                            $destaque = '';


                                            if ($x == 1) {
                                                $destaque = $domingo;
                                            }
                                            if ($d == $dia && $mes_p == date("m") && $ano_p = date("Y")) {
                                                $destaque = $hoje;
                                            }
                                            if (($x == 1) && ($d == $dia)) {
                                                $destaque = $hoje;
                                            }

                                            $nao_calcula = 0;
                                            $sql_evento = "select id_evento,titulo,p_eventos.descricao from p_eventos inner join p_tipo_justificativa on p_eventos.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa where (day(data_evento) = " . $d . " and month(data_evento) = " . $mes_p . " and year(data_evento) = " . $ano_p . ") and (id_usr = " . $id_usr . " or id_depto = 0)";
                                            $dados_evento = mysqli_query( $conexao, $sql_evento);
                                            $resultado_evento = mysqli_fetch_array($dados_evento);
                                            if ($resultado_evento[id_evento] != "") {
                                                $nao_calcula = 1;

                                            }

                                            echo "<td " . $destaque . " height=95px width=110px valign=top";

                                            if ($x != 1 && $x != 7) {
                                                if ($nao_calcula == 1) {
                                                    echo " class='tab_evento' ";
                                                } else {
                                                    echo " class='tab_resa' ";
                                                    $dias_ok++;
                                                }
                                            }


                                            echo " >" . $d;


                                            $d_busca = $d;
                                            $d++;


                                            //echo " ".$d."/".$mes."/".$ano;

                                            $data_calendario = $ano_p . "/" . $mes_p . "/" . $d;
                                            $vetor_registro = explode("/", $data_calendario);
                                            $data = mktime(0, 0, 0, $vetor_registro[1], $vetor_registro[2], $vetor_registro[0]);
                                            $data_atual = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
                                            $tdias = ($data - $data_atual) / 86400;
                                            $tdias = ceil($tdias);
                                            if ($x != 1 && $x != 7) {
                                                echo "<br><table width='100%' border='0' cellspacing='1'> ";

                                                //for($i=1;$i<5;$i++){


                                                $sql_agenda = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $d_busca . " and month(data_registro) = " . $mes_p . " and year(data_registro) = " . $ano_p . " and id_usr = " . $id_usr . " and nao_registrou = 0 order by etapa";
                                                //echo $sql_agenda;
                                                $dados_agenda = mysqli_query( $conexao, $sql_agenda);

                                                //$resultado_agenda = mysql_fetch_array($dados_agenda);
                                                $qtd_registro = 0;
                                                $diferenca_1 = 0;
                                                $diferenca_2 = 0;
                                                $achou_registro = 0;


                                                $periodo_banco_1 = $resultado_consulta_grade[entrada_1];
                                                $periodo_banco_1 = explode(':', $periodo_banco_1);
                                                $periodo_banco_1_x = (60 * 60 * $periodo_banco_1[0]) + (60 * $periodo_banco_1[1]);

                                                $periodo_banco_2 = $resultado_consulta_grade[saida_1];
                                                $periodo_banco_2 = explode(':', $periodo_banco_2);
                                                $periodo_banco_2_x = (60 * 60 * $periodo_banco_2[0]) + (60 * $periodo_banco_2[1]);

                                                $periodo_banco_3 = $resultado_consulta_grade[entrada_2];
                                                $periodo_banco_3 = explode(':', $periodo_banco_3);
                                                $periodo_banco_3_x = (60 * 60 * $periodo_banco_3[0]) + (60 * $periodo_banco_3[1]);

                                                $periodo_banco_4 = $resultado_consulta_grade[saida_2];
                                                $periodo_banco_4 = explode(':', $periodo_banco_4);
                                                $periodo_banco_4_x = (60 * 60 * $periodo_banco_4[0]) + (60 * $periodo_banco_4[1]);

                                                $total_horas_periodo_1 = abs(($periodo_banco_2_x - $periodo_banco_1_x) / 60);
                                                $total_horas_periodo_2 = abs(($periodo_banco_4_x - $periodo_banco_3_x) / 60);


                                                //echo $resultado_consulta_grade[tipo_grade];
                                                if ($resultado_consulta_grade[tipo_grade] == 0) {
                                                    $total_horas_grade = abs(($total_horas_periodo_2 + $total_horas_periodo_1));
                                                    $qtd_total_registros = 4;
                                                } else {
                                                    $total_horas_grade = abs($total_horas_periodo_1);
                                                    $qtd_total_registros = 2;
                                                }
                                                //echo $total_horas_periodo_1." - ".$total_horas_periodo_2." yy".$total_horas_grade;
                                                $zera_periodo_1 = 0;
                                                $zera_periodo_2 = 0;
                                                $preenchimento_1 = 0;
                                                $preenchimento_2 = 0;
                                                $preenchimento_3 = 0;
                                                $preenchimento_4 = 0;
                                                $abonado_mostra = 0;
                                                $status_abonado = 0;
                                                $status_nao_abonado = 0;
                                                $status_cortado = 0;
                                                $qtd_justificativas = 0;
                                                $existe_abonado_0 = 0;
                                                while ($resultado_agenda = mysqli_fetch_array($dados_agenda)) {
                                                    $resultado_agenda['minutos'] = $resultado_agenda['minutos'] > 0 || $resultado_agenda['hora'] ? str_pad($resultado_agenda['minutos'], 2, '0', STR_PAD_LEFT) : '';
                                                    //$sql_consulta_registro			= "select p_justificativa.id_justificativa,abonado from p_justificativa inner join p_registro on p_registro.id_justificativa = p_justificativa.id_justificativa where day(data_justificativa) = ".$d_busca." and month(data_justificativa) = ".$mes_p." and year(data_justificativa) = ".$ano_p." and p_justificativa.id_usr = ".$id_usr." and etapa = ".($qtd_registro+1)." limit 0,1";
                                                    $sql_consulta_registro = "select p_justificativa.id_justificativa,abonado from p_justificativa inner join p_registro on p_registro.id_justificativa = p_justificativa.id_justificativa where day(data_justificativa) = " . $d_busca . " and month(data_justificativa) = " . $mes_p . " and year(data_justificativa) = " . $ano_p . " and p_justificativa.id_usr = " . $id_usr . " and etapa = " . $resultado_agenda[etapa] . " limit 0,1";
                                                    //	echo "<br>".$sql_consulta_registro;
                                                    $dados_consulta_registro = mysqli_query( $conexao, $sql_consulta_registro);
                                                    $resultado_consulta_registro = mysqli_fetch_array($dados_consulta_registro);
                                                    $qtd_justificativas++;
                                                    //$achou_registro = ;
                                                    if ($resultado_consulta_registro[id_justificativa] != '') {
                                                        $achou_registro++;
                                                        //echo "* ".$achou_registro." *";
                                                    }
                                                    $abonado = $resultado_consulta_registro[abonado];
                                                    $abonado_mostra = $abonado;
                                                    if ($abonado != '' && $abonado == 0) {
                                                        $existe_abonado_0++;
                                                    }
                                                    if ($abonado == 1) {
                                                        $status_abonado++;
                                                    }
                                                    if ($abonado == 3) {
                                                        $status_cortado++;
                                                    }
                                                    if ($abonado == 2) {
                                                        $status_nao_abonado++;
                                                    }

                                                    $justifica = 0;
                                                    $qtd_registro++;
                                                    if ($nao_calcula == 0) {
                                                        if ($qtd_registro == 1) {
                                                            $horario_1_banco = $entrada_1;
                                                            $horario_1_banco = explode(':', $horario_1_banco);
                                                            $hora_1_banco = (60 * 60 * $horario_1_banco[0]) + (60 * $horario_1_banco[1]);

                                                            $horario_1 = $resultado_agenda[hora] . ":" . $resultado_agenda[minutos];
                                                            if ($resultado_agenda[hora] != '') {
                                                                $preenchimento_1 = 1;
                                                            }
                                                            $horario_1 = explode(':', $horario_1);
                                                            $hora_1 = (60 * 60 * $horario_1[0]) + (60 * $horario_1[1]);

                                                            $diferenca_registro_1 = abs(($hora_1_banco - $hora_1) / 60);

                                                            if ($diferenca_registro_1 > 30 && $gerente_status == 0) {
                                                                $mostra_botao = 1;
                                                                $cor = "#FF6600";
                                                                $justifica = 1;
                                                            } else {
                                                                $cor = "#666666";
                                                            }

                                                            if ($resultado_consulta_registro[abonado] == 1) {
                                                                $cor = "#009933";
                                                            }
                                                            if ($resultado_consulta_registro[abonado] == 3) {
                                                                $cor = "#FF0000";
                                                            }

                                                            echo "<center><font color = '" . $cor . "'; size = 2> <strong>" . $resultado_agenda[hora] . ":" . $resultado_agenda[minutos] . "</strong></font></center>";
                                                        }
                                                        if ($qtd_registro == 2) {

                                                            $horario_2_banco = $saida_1;
                                                            $horario_2_banco = explode(':', $horario_2_banco);
                                                            $hora_2_banco = (60 * 60 * $horario_2_banco[0]) + (60 * $horario_2_banco[1]);


                                                            $horario_2 = $resultado_agenda[hora] . ":" . $resultado_agenda[minutos];
                                                            $horario_2 = explode(':', $horario_2);
                                                            $hora_2 = (60 * 60 * $horario_2[0]) + (60 * $horario_2[1]);
                                                            //echo $hora_1." - ".$hora_2."<BR>";
                                                            $diferenca_registro_2 = abs(($hora_2_banco - $hora_2) / 60);
                                                            $diferenca_periodo_1 = abs(($hora_1 - $hora_2) / 60);
                                                            $total_horas_periodo = $diferenca_periodo_1;

                                                            //if($resultado_agenda[hora]==''&&$preenchimento_1==1){
                                                            //$preenchimento_2=0;
                                                            //$zera_periodo_1=1;
                                                            //}
                                                            $diferenca_registro_2 = abs(($hora_2_banco - $hora_2) / 60);
                                                            //echo $diferenca_registro_2;
                                                            if ($diferenca_registro_2 > 30 && $gerente_status == 0) {
                                                                $mostra_botao = 1;
                                                                $cor = "#FF6600";
                                                                $justifica = 1;
                                                            } else {
                                                                $cor = "#666666";
                                                            }

                                                            if ($resultado_agenda[hora] != '') {
                                                                $preenchimento_2 = 1;
                                                            }

                                                            //echo $diferenca_1;


                                                            if ($resultado_consulta_registro[abonado] == 1) {
                                                                $cor = "#009933";
                                                            }
                                                            if ($resultado_consulta_registro[abonado] == 3) {
                                                                $cor = "#FF0000";
                                                            }
                                                            //#########################################
                                                            if ($d_busca >= 25 && $mes_p >= 3 && $ano_p >= 2013 && $gerente_status == 0) {
                                                                if ($diferenca_registro_2 > 15 && $resultado_consulta_grade[tipo_grade] == 0) {
                                                                    $mostra_botao = 1;
                                                                    $cor = "#FF6600";
                                                                    $justifica = 1;
                                                                }
                                                            }
                                                            //#########################################
                                                            echo "<center><font color = '" . $cor . "'; size = 2> <strong>" . $resultado_agenda[hora] . ":" . $resultado_agenda[minutos] . "</strong></font><BR></center>";
                                                        }
                                                        if ($qtd_registro == 3 && $resultado_consulta_grade[tipo_grade] == 0) {

                                                            $horario_3_banco = $entrada_2;
                                                            $horario_3_banco = explode(':', $horario_3_banco);
                                                            $hora_3_banco = (60 * 60 * $horario_3_banco[0]) + (60 * $horario_3_banco[1]);


                                                            $horario_3 = $resultado_agenda[hora] . ":" . $resultado_agenda[minutos];
                                                            $horario_3 = explode(':', $horario_3);
                                                            $hora_3 = (60 * 60 * $horario_3[0]) + (60 * $horario_3[1]);

                                                            $diferenca_registro_3 = abs(($hora_3_banco - $hora_3) / 60);
                                                            if ($resultado_agenda[hora] != '') {
                                                                $preenchimento_3 = 1;
                                                            }
                                                            //echo $gerente_status;
                                                            if ($diferenca_registro_3 > 30 && $gerente_status == 0) {

                                                                $cor = "#FF6600";
                                                                $mostra_botao = 1;
                                                                $justifica = 1;
                                                            } else {
                                                                $cor = "#666666";
                                                            }
                                                            if ($resultado_consulta_registro[abonado] == 1) {
                                                                $cor = "#009933";
                                                            }
                                                            if ($resultado_consulta_registro[abonado] == 3) {
                                                                $cor = "#FF0000";
                                                            }
                                                            //#########################################
                                                            //echo $d_busca."-".$mes_p."-".$ano_p."-".$resultado_consulta_grade[tipo_grade]."-";
                                                            if ($d_busca >= 25 && $mes_p >= 3 && $ano_p >= 2013 && $gerente_status == 0) {
                                                                if ($diferenca_registro_3 > 15 && $resultado_consulta_grade[tipo_grade] == 0) {
                                                                    //echo "aqui";
                                                                    $mostra_botao = 1;
                                                                    $cor = "#FF6600";
                                                                    $justifica = 1;
                                                                }
                                                            }
                                                            //#########################################
                                                            echo "<center><font color = '" . $cor . "'; size = 2> <strong>" . $resultado_agenda[hora] . ":" . $resultado_agenda[minutos] . "</strong></font><BR></center>";

                                                        }
                                                        if ($qtd_registro == 4 && $resultado_consulta_grade[tipo_grade] == 0) {

                                                            $horario_4_banco = $saida_2;
                                                            $horario_4_banco = explode(':', $horario_4_banco);
                                                            $hora_4_banco = (60 * 60 * $horario_4_banco[0]) + (60 * $horario_4_banco[1]);

                                                            $horario_4 = $resultado_agenda[hora] . ":" . $resultado_agenda[minutos];

                                                            $horario_4 = explode(':', $horario_4);
                                                            $hora_4 = (60 * 60 * $horario_4[0]) + (60 * $horario_4[1]);
                                                            $diferenca_periodo_2 = abs(($hora_3 - $hora_4) / 60);
                                                            //echo $diferenca_2;

                                                            $total_horas_periodo = $diferenca_periodo_1 + $diferenca_periodo_2;

                                                            $diferenca_registro_4 = abs(($hora_4_banco - $hora_4) / 60);
                                                            if ($diferenca_registro_4 > 30 && $gerente_status == 0) {
                                                                $cor = "#FF6600";
                                                                $mostra_botao = 1;
                                                                $justifica = 1;
                                                            } else {
                                                                $cor = "#666666";
                                                            }


                                                            if ($resultado_consulta_registro[abonado] == 1) {
                                                                $cor = "#009933";
                                                            }
                                                            if ($resultado_consulta_registro[abonado] == 3) {
                                                                $cor = "#FF0000";
                                                            }
                                                            //if($resultado_agenda[hora]==''&&$preenchimento_1==1){
                                                            //	$preenchimento_4=0;
                                                            //	$zera_periodo_2=1;
                                                            //}
                                                            //echo "(".$resultado_agenda[hora].")";
                                                            if ($resultado_agenda[hora] != '') {
                                                                $preenchimento_4 = 1;
                                                            }
                                                            echo "<center><font color = '" . $cor . "'; size = 2> <strong>" . $resultado_agenda[hora] . ":" . $resultado_agenda[minutos] . "</strong></font><BR></center>";
                                                        }
                                                    }//fim calcula

                                                }
                                                //echo $abonado;

                                                //if($qtd_registro>0){


                                                //}
                                                $d_par = $d - 1;

                                                //antes $d++;


                                                echo "</tr></table>";
//echo $qtd_registro;
                                                $qtd_registro_compara = $qtd_registro;

                                                //echo $qtd_registro."-".$qtd_total_registros."--".$total_horas_periodo."-".$total_horas_grade."-".$justifica."-".$tdias;
                                                if (($qtd_registro <= $qtd_total_registros || $total_horas_periodo < $total_horas_grade || $justifica == 1) && $tdias <= 0 && $nao_calcula == 0) {
                                                    //echo $qtd_registro."<br>";
                                                    if ($resultado_consulta_grade[tipo_grade] == 0 && $qtd_registro < 4) {

                                                        $mostra_botao = 1;
                                                    }


                                                    if ($qtd_registro == 1) {
                                                        if ($resultado_consulta_grade[tipo_grade] == 0) {
                                                            echo "<center><font color = '#666666'; size = 2>--:--</font></center>
								      <center><font color = '#666666'; size = 2>--:--</font></center>
									  <center><font color = '#666666'; size = 2>--:--</font></center>";
                                                        } else {
                                                            echo "<center><font color = '#666666'; size = 2>--:--</font></center>";
                                                        }
                                                    }

                                                    if ($resultado_consulta_grade[tipo_grade] == 0) {

                                                        if ($qtd_registro == 2) {
                                                            echo "<center><font color = '#666666'; size = 2>--:--</font></center>
								      <center><font color = '#666666'; size = 2>--:--</font></center>";
                                                        }
                                                        if ($qtd_registro == 3) {
                                                            echo "<center><font color = '#666666'; size = 2>--:--</font></center>";
                                                        }
                                                    }

                                                    $qtd_registro_foto = $qtd_registro;
                                                    if ($resultado_consulta_grade[tipo_grade] == 0) {
                                                        $qtd_registro = 4;
                                                    } else {
                                                        $qtd_registro = 2;
                                                    }


                                                    if (($preenchimento_1 + $preenchimento_2 + $preenchimento_3 + $preenchimento_4) == 0) {
                                                        $zera_periodo_1 = 1;
                                                        $zera_periodo_2 = 1;
                                                    }

                                                    if (($preenchimento_1 + $preenchimento_2 + $preenchimento_3 + $preenchimento_4) == 1) {
                                                        $zera_periodo_1 = 1;
                                                        $zera_periodo_2 = 1;
                                                    }
                                                    if (($preenchimento_1 + $preenchimento_2 + $preenchimento_3 + $preenchimento_4) == 2) {
                                                        $zera_periodo_2 = 1;
                                                    }
                                                    if (($preenchimento_1 + $preenchimento_2 + $preenchimento_3 + $preenchimento_4) == 3) {
                                                        $zera_periodo_2 = 1;
                                                    }
//echo $preenchimento_1." - ".$preenchimento_2." - ".$preenchimento_3." - ".$preenchimento_4."<BR>";
//echo $nao_calcula." - ".$zera_periodo_2;
//if($abonado==2||$abonado==3){
//	$nao_calcula=1;
//	echo "AQUI";
//}
                                                    if ($nao_calcula == 0) {

                                                        if ($resultado_consulta_grade[tipo_grade] == 2) {
//	echo $zera_periodo_2;
                                                            if ($zera_periodo_1 == 1) {
                                                                $difDeHora = 0;
                                                            } else {
                                                                $difDeHora = ($hora_2 - $hora_1);
                                                            }
                                                        } else {

                                                            //echo $zera_periodo_1."-".$zera_periodo_2."-".$difDeHora;


                                                            if ($resultado_consulta_grade[tipo_grade] == 0) {

                                                                if ($zera_periodo_1 == 1 && $zera_periodo_2 == 0) {

                                                                    //	$difDeHora = 0;
                                                                    //echo "a";
                                                                    $difDeHora = ($hora_4 - $hora_3);
                                                                }
                                                                if ($zera_periodo_1 == 0 && $zera_periodo_2 == 1) {

                                                                    //	$difDeHora = 0;

                                                                    $difDeHora = ($hora_2 - $hora_1);
                                                                    $mostra_botao = 1;
                                                                }
                                                                if ($zera_periodo_1 == 1 && $zera_periodo_2 == 1) {
                                                                    //	echo "c";
                                                                    $difDeHora = 0;
                                                                }
                                                                if ($zera_periodo_1 == 0 && $zera_periodo_2 == 0) {
                                                                    //	echo "d";
                                                                    $difDeHora = ($hora_2 - $hora_1) + ($hora_4 - $hora_3);
                                                                    //echo "bb";
                                                                }
                                                            }

                                                            if ($qtd_registro_compara == 3) {
                                                                $difDeHora = ($hora_2 - $hora_1);
                                                            }
                                                        }
                                                        if ($qtd_registro_compara <= 1) {
                                                            $difDeHora = 0;
                                                        }

                                                        $tempo = $difDeHora / (60 * 60);
                                                        //echo 60*$tempo."-".$total_horas_grade;
                                                        if (60 * $tempo < ($total_horas_grade - 30)) {

                                                            $mostra_botao = 1;
                                                        }
                                                        $qtd_tempo = $tempo;
                                                        $tempo = explode('.', $tempo); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
                                                        $hora = $tempo[0];
                                                        @$minutos = (float)(0) . '.' . $tempo[1]; // Aqui forçamos a conversão para float, para não ter erro.
                                                        $minutos = round($minutos * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.

                                                        $minutos = explode('.', $minutos); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos

                                                        $minutos = $minutos[0];
                                                        if ($minutos < 10) {
                                                            $minutos = "0" . $minutos;
                                                        }

                                                        $total_dia = $hora . ':' . $minutos;
                                                        $total_mes = $total_mes + $difDeHora;

//if($zera_periodo_2==1){
//	$total_horas_periodo_2=0;
//}

//echo ($difDeHora/60)." = ".$total_horas_periodo_1 ."-".$total_horas_periodo_2;
                                                        if ($x > 1 && $x < 7 && $nao_calcula == 0) {
                                                            if ($resultado_consulta_grade[tipo_grade] == 0) {

                                                                //$total_acum = $total_acum + (($difDeHora/60) - abs(($total_horas_periodo_1+$total_horas_periodo_2)));
                                                                $total_acum = $difDeHora + $total_acum;
                                                            } else {

                                                                //$total_acum = $total_acum + (($difDeHora/60) - abs(($total_horas_periodo_1)));
                                                                $total_acum = $difDeHora / 60 + $total_acum;
                                                            }
                                                        }
//echo $total_acum."<BR>";
//." = ".($total_horas_periodo_1+$total_horas_periodo_2)."-".($difDeHora/60);
                                                        $total_acum3 = $total_acum;
                                                        $tempo3 = $total_acum3 / (60 * 60);

                                                        $tempo3 = explode('.', $tempo3); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
                                                        $hora3 = $tempo3[0];
                                                        @$minutos3 = (float)(0) . '.' . $tempo3[1]; // Aqui forçamos a conversão para float, para não ter erro.
                                                        $minutos3 = $minutos3 * 60; // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.

                                                        $minutos3 = explode('.', $minutos3); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
                                                        $minutos3 = $minutos3[0];
                                                        if ($minutos3 < 10) {
                                                            $minutos3 = "0" . $minutos3;
                                                        }

                                                        $total_acum_mostra = $hora3 . ':' . $minutos3;
                                                        if ($total_dia < 10) {
                                                            $total_dia = "0" . $total_dia;
                                                        }

                                                    }//fim calcula
//echo $hora;
//echo $qtd_total_registros;
                                                    if ($qtd_total_registros == 2) {
                                                        $qtd_compara = $total_horas_periodo_1 / 60;
                                                    } else {
                                                        $qtd_compara = ($total_horas_periodo_1 + $total_horas_periodo_2) / 60;
                                                    }
//echo $total_horas_periodo_1." - ".$total_horas_periodo_2;

                                                    if ($qtd_tempo < $qtd_compara) {
                                                        $mostra_botao = 1;
                                                        //echo $qtd_tempo."_".$qtd_compara;
                                                    }

//echo $total_horas_periodo_1." - ";
//echo $total_acum_mostra;
                                                    //echo ($total_horas_periodo_1+$total_horas_periodo_2);
                                                    // $qtd_registro;
                                                    //echo $qtd_registro;
                                                    //	echo $nao_calcula;

                                                    if ($nao_calcula == 0) {
                                                        //if($qtd_registro_foto>1&&$d_busca>=7&&$mes_p>=1&&$ano_p>=2013){
                                                        if ($qtd_registro_foto > 1 && $mes_p >= 1 && $ano_p >= 2013) {
                                                            echo "<div style='cursor:pointer;' onClick='AbreFotos(" . $d_busca . "," . $mes_p . "," . $ano_p . "," . $id_usr . ")'><center><font color = '#666666'>_____________ <img src='../images/camera.png'><br>";
                                                        } else {
                                                            echo "<div style='cursor:pointer;' onClick='')><center><font color = '#666666'>_______________<img src='../images/camera_espaco.png'><br>";
                                                        }

                                                        echo "<center>Total:  " . $total_dia . " hs</div>";//<br>Acum: ".$total_acum_mostra." hs</font></center>";
                                                    }
//echo $justifica;
                                                    //	 if($achou_registro==""){
                                                    //echo $mostra_botao;
                                                    //	echo $mostra_botao."-".$nao_calcula;
//echo "<BR>".$_SESSION["usr_gerente"]."-".($status_abonado+$status_cortado+$status_nao_abonado)."-".$achou_registro."-".$nao_calcula."-".$mostra_botao;
//echo "<BR>".$achou_registro;
//echo "<br><center><strong><font color = '#0066ff';' size = 1><div style='cursor:pointer;' onClick='AbreJustifivativa(".$id_usr.",".$d_par.",".$mes_p.",".$ano_p.")';>Aguardando abono</div></font></strong><center>";
                                                    $arrData = explode("/", $data_calendario);
                                                    $arrData[2]--;

                                                    if (diaPrecisaJustificativa($id_usr, implode("/", array_reverse($arrData)))) {
                                                        $mostra_botao = 1;
                                                        if ($_SESSION["usr_gerente"] == 0 && ($status_abonado + $status_cortado + $status_nao_abonado) == 0 && $achou_registro == 0 && $mostra_botao == 1 && ($_SESSION["sessao_id_usr"] == $id_usr)) {
                                                            echo "<center><strong style='display:block'><font color = '#666666';' size = 1><br><div style='cursor:pointer;' onClick='AbreJanela(" . $id_usr . "," . $d_par . "," . $mes_p . "," . $ano_p . ")';><font color = '#FF0000'>Justificar</font></div></font></strong></center><br>";
                                                        }
                                                        if ($_SESSION["usr_gerente"] >= 1 && $id_usr == $_SESSION["sessao_id_usr"] && $status_abonado == 0 && $achou_registro == 0 && $mostra_botao == 1) {
                                                            echo "<center><strong style='display:block'><font color = '#666666';' size = 1><br><div style='cursor:pointer;' onClick='AbreJanela(" . $id_usr . "," . $d_par . "," . $mes_p . "," . $ano_p . ")';><font color = '#FF0000'>Justificar</font></div></font></strong></center><br>";
                                                        }
                                                    }
                                                    //if($_SESSION["usr_gerente"]==0&&$achou_registro!=0&&$nao_calcula==0){
                                                    if ($_SESSION["usr_gerente"] == 0 && ($status_abonado + $status_cortado + $status_nao_abonado) == 0 && $achou_registro != 0) {
                                                        echo "<br><center><strong style='display:block'><font color = '#0066ff';' size = 1><div style='cursor:pointer;' onClick='AbreJustifivativa(" . $id_usr . "," . $d_par . "," . $mes_p . "," . $ano_p . ")';>Aguardando abono</div></font></strong><center>";
                                                    }
                                                    if ($_SESSION["usr_gerente"] == 0 && ($status_abonado + $status_cortado + $status_nao_abonado) != 0 && $achou_registro != 0) {
                                                        echo "<br><center><strong style='display:block'><font color = '#0066ff';' size = 1><div style='cursor:pointer;' onClick='AbreJustifivativa(" . $id_usr . "," . $d_par . "," . $mes_p . "," . $ano_p . ")';>";
                                                        if ($existe_abonado_0 == 0 & $status_cortado == 0) {
                                                            echo "<font color = '#009933' size = 1 style='display:block'>Abonado</font>";
                                                        }
                                                        if ($existe_abonado_0 > 0) {
                                                            echo "Aguardando abono";
                                                        }
                                                        if ($status_cortado > 0) {
                                                            echo "<font color = 'red' style='display:block'>Ponto Cortado</font>";
                                                        }
                                                        echo "</div></font></strong><center>";
                                                    }

                                                    if (false && temHoraNaoAutorizada($id_usr, implode("/", array_reverse($arrData)))) {
                                                        echo "<font color = 'red' style='display:block'>Horas não autorizadas</font>";
                                                    }

                                                    if ($id_usr == 25 && $mes_p == 03 && $ano_p == 2013 && $d_par == 14) {
                                                        //echo "<font color = 'red'>Ponto Cortado</font>";
                                                    }
                                                    //echo $id_usr."-".$mes_p."-".$ano_p."-".$d_par;
                                                    //}
                                                    //	echo $resultado_consulta_registro[abonado];
                                                    // if($achou_registro!=""){
                                                    //echo $achou_registro;
                                                    //aqui antes
                                                    //}


                                                }
                                                //echo "<br>".$_SESSION["usr_gerente"]."-".($status_abonado+$status_cortado+$status_nao_abonado)."-".$nao_calcula."-".$qtd_justificativas."-".$achou_registro;
                                                if ($_SESSION["usr_gerente"] >= 1 && ($status_abonado + $status_cortado + $status_nao_abonado) == 0 && $achou_registro != 0 && $nao_calcula == 0 && $achou_registro != 0) {
                                                    echo "<br><center><strong><font color = '#0066ff';' size = 1><div style='cursor:pointer;' onClick='AbreJustifivativa(" . $id_usr . "," . $d_par . "," . $mes_p . "," . $ano_p . ")';>Aguardando abono</div></font></strong><center>";
                                                }

                                                if ($_SESSION["usr_gerente"] >= 1 && ($status_abonado + $status_cortado + $status_nao_abonado) != 0 && $achou_registro != 0) {
                                                    echo "<br><center><strong><font color = '#0066ff';' size = 1><div style='cursor:pointer;' onClick='AbreJustifivativa(" . $id_usr . "," . $d_par . "," . $mes_p . "," . $ano_p . ")';>";

                                                    if ($existe_abonado_0 == 0 && $status_cortado == 0) {
                                                        echo "<font color = '#009933';' size = 1>Abonado</font>";
                                                    }
                                                    if ($existe_abonado_0 > 0) {
                                                        echo "Aguardando abono";
                                                    }
                                                    if ($status_cortado > 0) {
                                                        echo "<font color='red'>Ponto Cortado</font>";
                                                    }
                                                    echo "</div></font></strong><center>";
                                                }
                                                if ($id_usr == 25 && $mes_p == 03 && $ano_p == 2013 && $d_par == 14) {
                                                    //echo "<font color = 'red'>Ponto Cortado</font>";
                                                }
                                                //echo $status_abonado;
                                                //echo $id_usr."-".$mes_p."-".ano_p."-".d_par;
                                                if ($status_abonado == 4 && $nao_calcula == 0) {
                                                    echo "<br><center><strong><font color = '#009933';' size = 1><div style='cursor:pointer;' onClick='AbreJustifivativa(" . $id_usr . "," . $d_par . "," . $mes_p . "," . $ano_p . ")';>Abonado</div></font></strong><center>";
                                                }
                                                //if($status_cortado==4||($id_usr==25&&$mes_p==03&&$ano_p==2013&&$d_par==14)){
                                                if ($status_cortado == 4) {
                                                    //echo "<center><font color = '#666666'>_________________<br>
                                                    //	Total:  00:00 hs<br>Acum: 00:00 hs</font></center>";
                                                    echo "<br><center><strong><font color = '#FF0000';' size = 1><div style='cursor:pointer;' onClick='AbreJustifivativa(" . $id_usr . "," . $d_par . "," . $mes_p . "," . $ano_p . ")';>Ponto Cortado</div></font></strong><center>";
                                                }
                                                //echo $resultado_consulta_registro[abonado]."-".$nao_calcula;
                                                if ($status_nao_abonado == 4) {
                                                    echo "<br><center><strong><font color = '#FF0000';' size = 1><div style='cursor:pointer;' onClick='AbreJustifivativa(" . $id_usr . "," . $d_par . "," . $mes_p . "," . $ano_p . ")';>Não Abonado</div></font></strong><center>";
                                                }
                                                echo "<br>";
                                                if ($nao_calcula == 1) {
                                                    echo "<br><br><br><strong><font color = '#ffffff'><center>" . $resultado_evento[titulo] . '<br />' . $resultado_evento[descricao] . "</center></font></strong>";
                                                }

                                            }//sabado e domingo


                                        } else {
                                            echo("<td> </td>");
                                        }
                                    }
                                    for (; $x <= 7; $x++) {
                                        echo("<td> </td>");
                                    }
                                    echo("</tr>");

                                }



                                ?>
                                <tr>
                                    <td colspan="7" bgcolor="#FFFFFF">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="96%" height="30">&nbsp; <img src="../images/indice.jpg"
                                                                                        width="18" height="18"
                                                                                        align="absmiddle">
                                                    <a href="#"> <font size="1" color="#666666">Visualizar
                                                            compromissos por lista (<font color="#FF0000">Breve</font>)</font></a><a
                                                        href="#">&nbsp;&nbsp;</a>&nbsp;
                                                    <img src="../images/print.png" width="16" height="16"
                                                         align="absmiddle">
                                                    <a href="#"><font size="1" color="#666666">Imprimir</font></a>
                                                    <strong> <a href="#">&nbsp;</a><a href="#">&nbsp;</a><a href="#">
                                                            &nbsp;</a></strong></td>
                                                <td width="4%" align="right"><font size="1" color="#666666">&nbsp;&nbsp;&nbsp;</font>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        <?php } else {
                            ?>
                            <p>&nbsp;</p><p>&nbsp;</p>
                            <table width='523' border='0' align="center" cellpadding='2' cellspacing='0'>
                                <tr>
                                    <td width="82" align="center"><img src="../images/demo.png" width="71"
                                                                       height="71"></td>
                                    <td width="433" align="left"><font color="#FF0000" size="3"><strong>Selecione
                                                o funcion&aacute;rio e per&iacute;odo acima!</strong></font></td>
                                </tr>
                            </table>
                        <?php } ?>


                        <br> <br>
                        <br>

                        <div align="center"></div>
                        <p align="centencr">&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>
                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Busca</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <form name="form_busca" method="post" action="frm_lista_ocorrencias.php">
                                        <font color="#FF6600" size="1"><strong>Informe texto de
                                                busca:</strong></font><br>
                                        <font color="#FF6600" size="1"><strong>
                                                <input name="status_atual" type="text" id="status_atual" size="3"
                                                       style="display:none" value="<?php echo $status_atual; ?>">
                                                <input name="selMES" type="text" id="selMES" size="3"
                                                       value="<?php echo $MES; ?>" style="display:none">
                                                <input name="selANO" type="text" id="selANO" size="3"
                                                       value="<?php echo $ANO; ?>" style="display:none">
                                                <input name="local_atual" type="text" id="local_atual" size="3"
                                                       value="<?php echo $local_atual; ?>" style="display:none">
                                                <input name="enviados" type="text" id="enviados" size="3"
                                                       value="<?php echo $enviados; ?>" style="display:none">
                                                <input name="texto" type="text" id="texto" size="3"
                                                       value="<?php echo $texto; ?>" style="display:none">
                                            </strong></font>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td><input type="text" name="texto_busca" style="width:130px"
                                                           value="<?php echo $texto_busca; ?>" onKeyPress="fncEnter();">
                                                </td>
                                                <td><a href="#" onClick="fncBusca();"><img src="../images/ok_bt.gif"
                                                                                           width="27" height="15"
                                                                                           border="0"></a></td>
                                            </tr>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Resumo do funcion&aacute;rio</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <table width='100%' border='0' cellspacing='0' cellpadding='2'>
                                        <tr>
                                            <td colspan="4">
                                                <?php if ($foto == 1) { ?>
                                                    <img src="../fotos/<?php echo $id_usr . "_usr.jpg" ?>" width="88">
                                                <?php } ?>
                                                <?php if ($foto == 0) { ?>
                                                    <img src="../images/nouser.jpg" width="88" height="88">
                                                <?php } ?>
                                            </td>
                                            <td width="176%" colspan="4" align="center" valign="middle">
                                                <table width='100%' border='0' cellspacing='0' cellpadding='2'>
                                                    <tr>
                                                        <td width="704%" colspan="4" align="center"><font
                                                                color="#0066FF"><strong><?php echo $nome[0] . " " . $nome[count($nome) - 1]; ?>
                                                                    <font size="1"><br>
                                                                    </font></strong></font><font color="#999999">_________</font><font
                                                                color="#0066FF"><strong>
                                                                </strong></font></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" align="center"><font
                                                                color="#999999"><strong><font color="#333333" size="1">
                                                                        <?php
                                                                        echo $entrada_1 . "<br>";
                                                                        echo $saida_1 . "<br>";
                                                                        if ($resultado_consulta_grade[tipo_grade] == 0) {
                                                                            echo $entrada_2 . "<br>";
                                                                            echo $saida_2 . "<br>";
                                                                        }; ?>
                                                                    </font></strong></font></td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">&nbsp;</td>
                                            <td colspan="4" align="right">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Quadro de horas -<font color="#333333"><strong>
                                            <?php echo $mes_ex; ?></strong></font></td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa_y'>
                                    <table width='100%' border='0' cellspacing='0' cellpadding='2'>
                                        <tr>
                                            <td colspan="4">Abonadas:</td>
                                            <td width="41%" colspan="4" valign="middle"><strong><font
                                                        color="#009933"><?php echo $total_3; ?></font></strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">Limite legal:</td>
                                            <td colspan="4" valign="middle"><strong><font
                                                        color="#FF6600"><?php echo $total_4; ?></font></strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">N&atilde;o Abonadas:</td>
                                            <td colspan="4" valign="middle"><strong><font
                                                        color="#FF0000"><?php echo $total_2; ?></font></strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">Aguardando:</td>
                                            <td colspan="4" valign="middle"><strong><font
                                                        color="#0066FF"><?php echo $total_1; ?></font></strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">&nbsp;</td>
                                            <td colspan="4" valign="middle">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><strong>Total Horas:</strong></td>
                                            <td colspan="4" valign="middle"><strong>
                                                    <?php
                                                    echo $total_acum_mostra; ?>
                                                    hs </strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <p align="right"></p>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Legenda</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <table width='100%' border='0' cellspacing='0' cellpadding='2'>
                                        <tr>
                                            <td colspan="4"><img src="../images/item0.jpg" width="16" height="16">
                                            </td>
                                            <td width="704%" colspan="4" valign="middle">Normal</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><img src="../images/item2.jpg" width="16" height="16">
                                            </td>
                                            <td colspan="4">Atraso</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><img src="../images/item3.jpg" width="16" height="16">
                                            </td>
                                            <td colspan="4">Aguardando Abono</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><img src="../images/item1.jpg" width="16" height="16">
                                            </td>
                                            <td colspan="4">Abonado</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <p align="right">
                            <?php if ($_SESSION["sessao_rh"] == 1){ ?>
                        </p>

                        <div align="center"><a href="lista_online.php"><img src="../images/icone4.png" width="110"
                                                                            height="110" border="0"></a><br>
                            Lista on-line
                        </div>
                        <?php } ?>
                        <br>

                        <p align="center"><a href="../ajuda/" target="_blank"><img
                                    src="../images/respostas.png" alt="Manual do Sistema"
                                    width="87" height="62" border="0" class="icon"></a></p>

                        <p align="center"><a href="../ajuda/" target="_blank">Manual
                                do Sistema</a></p>

                        <p align="right">&nbsp; </p>

                        <p align="right">&nbsp; &nbsp;</p></td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                        de Ponto Eletrônico <?= date('Y') ?> - SED&reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>


<?php
((is_null($___mysqli_res = mysqli_close($conexao))) ? false : $___mysqli_res);
?>

<form name="abre_janela" method="post" action="justificar.php" target="myNewWin">
    <input type="hidden" name="id_usr" value="<?php echo $id_usr; ?>">
    <input type="hidden" name="dia">
    <input type="hidden" name="mes">
    <input type="hidden" name="ano">
    <input type="hidden" name="id_depto">
</form>

<form name="mostra_fotos" method="post" action="mostra_fotos.php" target="myNewWin7">
    <input type="hidden" name="id_usr" value="<?php echo $id_usr; ?>">
    <input type="hidden" name="dia">
    <input type="hidden" name="mes">
    <input type="hidden" name="ano">
</form>

<form name="abre_janela2" method="post" action="mostra_justificativa.php" target="myNewWin">
    <input type="hidden" name="id_justificativa">
    <input type="hidden" name="id_usr">
    <input type="hidden" name="dia">
    <input type="hidden" name="mes">
    <input type="hidden" name="ano">
    <input type="hidden" name="id_depto">

</form>
<form name="ocorrencias" method="post" action="frm_justificativa.php">
    <input type="hidden" name="id_depto">
    <input type="hidden" name="id_usr">
    <input type="hidden" name="mes_p">
    <input type="hidden" name="ano_p">
</form>

<form name="abre_janela4" method="post" action="mostra_fotos_todas.php" target="myNewWin">
    <input type="hidden" name="mes">
    <input type="hidden" name="ano">
    <input type="hidden" name="id_usr">
</form>

<iframe name="janela" width="800" height="600" style="display:none"></iframe>
</body>
</html>
