﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 && $_SESSION["sessao_id_usr"] == 0) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.close();
    </script>
    <?php

    die;
}

require_once("../funcoes/conexao.php");


$id_depto = $_POST["id_depto"];
if ($id_depto == '') {
    $id_depto = $_SESSION["sessao_id_depto"];
}

$mes_p = $_POST["mes_p"];
if ($mes_p == "") {
    $mes_p = date("m");
}

$dia = date("d");
$ano_p = $_POST["ano_p"];

if ($ano_p == "") {
    $ano_p = date("Y");
}

$sql_depto0 = "select * from depto where id_depto = " . $id_depto;
$dados_depto0 = mysqli_query( $conexao, $sql_depto0);
$resultado_depto0 = mysqli_fetch_array($dados_depto0);

$sql_depto1 = "select nome from usuarios inner join depto on depto.id_depto = usuarios.id_depto where depto.id_depto = " . $id_depto . " and gerente = 1 and (usuarios.id_usr <> 4 and usuarios.id_usr <> 426 and usuarios.id_usr <> 427)";;
$dados_depto1 = mysqli_query( $conexao, $sql_depto1);
$resultado_depto1 = mysqli_fetch_array($dados_depto1);

$sql_depto2 = "select count(id_usr) as total from usuarios where id_depto = " . $id_depto;
$dados_depto2 = mysqli_query( $conexao, $sql_depto2);
$resultado_depto2 = mysqli_fetch_array($dados_depto2);

$sql_depto3 = "select count(usuarios.id_usr) as total,usuarios.id_usr,nome from p_justificativa inner join usuarios on usuarios.id_usr = p_justificativa.id_usr inner join depto on depto.id_depto = usuarios.id_depto where depto.id_depto = " . $id_depto . " and month(p_justificativa.data_justificativa) = " . $mes_p . " and year(p_justificativa.data_justificativa) = " . $ano_p . " group by usuarios.id_usr order by total desc limit 0,1";
$dados_depto3 = mysqli_query( $conexao, $sql_depto3);
$resultado_depto3 = mysqli_fetch_array($dados_depto3);


$sql_contador0 = "select count(id_justificativa) as qtd_total from p_justificativa inner join depto on depto.id_depto = id_depto where abonado = 0 and month(p_justificativa.data_justificativa) = " . $mes_p . " and year(p_justificativa.data_justificativa) = " . $ano_p . " and id_depto = " . $id_depto;
$dados_contador0 = mysqli_query( $conexao, $sql_contador0);
$resultado_contador0 = mysqli_fetch_array($dados_contador0);

//$sql_contador1			= "select count(id_justificativa) as qtd_total from p_justificativa  inner join depto on depto.id_depto = id_depto where abonado = 1 and id_depto = ".$id_depto;
$sql_contador1 = "select count(id_justificativa) as qtd_total from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on depto.id_depto = usuarios.id_depto where abonado = 1 and month(p_justificativa.data_justificativa) = " . $mes_p . " and year(p_justificativa.data_justificativa) = " . $ano_p . " and usuarios.id_depto = " . $id_depto;

$dados_contador1 = mysqli_query( $conexao, $sql_contador1);
$resultado_contador1 = mysqli_fetch_array($dados_contador1);

$sql_contador2 = "select count(p_justificativa.id_tipo_justificativa) as qtd_total, titulo from p_justificativa inner join p_tipo_justificativa on p_justificativa.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa  inner join depto on depto.id_depto = id_depto  where id_depto = " . $id_depto . " and month(p_justificativa.data_justificativa) = " . $mes_p . " and year(p_justificativa.data_justificativa) = " . $ano_p . " group by p_justificativa.id_tipo_justificativa order by qtd_total desc limit 0,1";
$dados_contador2 = mysqli_query( $conexao, $sql_contador2);
$resultado_contador2 = mysqli_fetch_array($dados_contador2);


$sql_contador0 = "select count(id_justificativa) as qtd_total,nome,(select count(id_justificativa) from p_justificativa where id_usr = p.id_usr and abonado = 1) as abonados  from p_justificativa p inner join usuarios on p.id_usr = usuarios.id_usr where id_depto = " . $id_depto . " and month(p.data_justificativa) = " . $mes_p . " and year(p.data_justificativa) = " . $ano_p . " group by p.id_usr";
$dados_contador0 = mysqli_query( $conexao, $sql_contador0);
//echo $sql_contador0;
//$resultado_depto	= mysql_fetch_array($dados_contador0);

?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">

<html>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart"]});
    google.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Ocorrências', 'Quantitativo'],
            <?php while($resultado_depto = mysqli_fetch_array($dados_contador0)){
            echo "['".$resultado_depto[nome]."',".number_format($resultado_depto[qtd_total],0,",",".")."],";
            } ?>
        ]);

        var options = {
            title: 'Gráfico de Ocorrências deste departamento',
            is3D: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }

</script>


<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_preloadImages() { //v3.0
        var d = document;
        if (d.images) {
            if (!d.MM_p) d.MM_p = new Array();
            var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
            for (i = 0; i < a.length; i++)
                if (a[i].indexOf("#") != 0) {
                    d.MM_p[j] = new Image;
                    d.MM_p[j++].src = a[i];
                }
        }
    }
    function abre_deptos(id_depto) {
        document.abre_lista.id_depto.value = id_depto;
        document.abre_lista.submit();

    }
    function fncPainel(tipo) {
        document.abre_janela.tipo_ocorrencia.value = tipo;
        document.abre_janela.enviados.value = tipo;
        document.abre_janela.submit();
    }
    //-->
</script>
<head>

    <title>Principal</title>
</head>


<body onLoad="fncColoca();">
<meta name="title" content="SED - SED"/>
<meta name="url" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>"/>
<meta name="description"
      content="SED-GO - A SED de Goiás implementou em um projeto altamente inovador de acompanhamento de Processos totalmente digital."/>
<meta name="keywords"
      content="Sistema de Ponto Eletrônico,sistema,ouvidoria,corregedoria,controladoria,processos,protocolo,jedi,workflow,processo eletronico,gerencial,manifestação,acompanhamento,encaminhamento,controle,pesquisa,histórico,governo de goiás,sistema de acompanhamento de processos,sistema gerencial,marcelo roncato"/>

<meta name="autor" content="Marcelo Roncato"/>
<meta name="company" content="SED"/>
<meta name="revisit-after" content="5"/>
<script language="JavaScript" type="text/javascript">
    function fncColoca() {
        setTimeout("document.formulario.id_depto.value='<?php echo $id_depto?>';", 600);
        setTimeout("document.formulario.mes_p.value='<?php echo $mes_p?>';", 900);
        setTimeout("document.formulario.ano_p.value='<?php echo $ano_p?>';", 1100);
    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
//window.location.href="frm_Lista_processos.php?texto_busca="+document.form_busca.texto_busca.value;
    }


    function fncFechaMensagem() {
        mensagem.style.display = 'none';
    }

    function fncOpcoes2() {
        status_opcoes.style.display = '';
    }
    function Fechar() {
        status_opcoes.style.display = 'none';
    }
    function Abre2(id_processo) {
        window.open("processo.php?id_processo=" + id_processo, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=1070,height=700,top=10,left=20");
    }
    function fncEnter() {
        if (window.event.keyCode == 13) {
            fncBusca();
        }
    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
//document.form_busca.id_gerencia.value = document.lista.id_gerencia.value;
//document.form_busca.selMES.value = document.lista.selMES.value;
//document.form_busca.selANO.value = document.lista.selANO.value;


        document.form_busca.submit();
//window.location.href="frm_Lista_processos.php?texto_busca="+document.form_busca.texto_busca.value;

    }

</script>
<?php require_once("frm_topo.php"); ?>

<br>
<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg">
                        <table width="500" border="0" cellpadding="3" cellspacing="0">
                            <tr>
                                <td height="10">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right"><strong><font size="2"><font
                                                    color="#333333"></font></font></strong></div>
                                    <strong><font color="#666666" size="4">&nbsp;&nbsp;Estat&iacute;stica</font><br>
                                        <font color="#666666" size="1"> </font></strong></td>
                            </tr>
                        </table>
                    </td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" valign="top" bgcolor="#FFFFFF"><BR>
                        <table width="790" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
                            <tr valign="bottom">
                                <td height="25" class="Titulo_rel">
                                    <table width="790" border="0" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td width="194">
                                                <form name="formulario" method="post" action="grafico_depto.php">
                                                    <strong><font style="font-size:10px" color="#333333">
                                                            &nbsp;&nbsp;<font
                                                                size="2">Ger&ecirc;ncia:</font></font><font
                                                            color="#666666">
                                                            <select name="id_depto" id="id_depto" style="width:250px">
                                                                <option value="0" selected>Todas Unidades</option>
                                                                <?php
                                                                if ($_SESSION["sessao_rh"] == 0) {
                                                                    if ($_SESSION["usr_gerente"] == 1) {
                                                                        $sql_und = "select id_depto,depto,sigla from depto where ativo = 1 and id_depto = " . $_SESSION["sessao_id_depto"] . " order by sigla;";
                                                                    }
                                                                    if ($_SESSION["usr_gerente"] == 2) {
                                                                        $sql_und = "select id_depto,depto,sigla from depto where ativo = 1 and id_superintendencia  like concat(trim(TRAILING '0' from '{$_SESSION["sessao_id_superintendencia"]}'),'%') order by sigla;";
                                                                    }

                                                                } else {
                                                                    $sql_und = "select id_depto,depto,sigla from depto where ativo = 1 order by sigla;";
                                                                }
                                                                $dados_und = mysqli_query( $conexao, $sql_und);
                                                                while ($resultado_und = mysqli_fetch_array($dados_und)) {
                                                                    ?>
                                                                    <option
                                                                        value="<?php echo $resultado_und[id_depto]; ?>"><?php echo $resultado_und[depto]; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                            em:</font></strong> <strong>
                                                        <select name="mes_p" id="mes_p">
                                                            <option value="13">Todos</option>
                                                            <option value="01">Janeiro</option>
                                                            <option value="02">Fevereiro</option>
                                                            <option value="03">Mar&ccedil;o</option>
                                                            <option value="04">Abril</option>
                                                            <option value="05">Maio</option>
                                                            <option value="06">Junho</option>
                                                            <option value="07">Julho</option>
                                                            <option value="08">Agosto</option>
                                                            <option value="09">Setembro</option>
                                                            <option value="10">Outubro</option>
                                                            <option value="11">Novembro</option>
                                                            <option value="12">Dezembro</option>
                                                        </select>
                                                    </strong> <strong><font color="#666666">de:</font></strong>
                                                    <select name="ano_p" id="ano_p">
                                                        <?php echo montarComboAnos(); ?>
                                                    </select>
                                                    <strong><font color="#666666"><a href="#"
                                                                                     onClick="document.formulario.submit();"><img
                                                                    src="../images/ok_bt.gif" width="27" height="15"
                                                                    border="0"
                                                                    title="Realiza o filtro"></a></font></strong>
                                                    <strong><font color="#666666"> </font></strong>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="Itens_rel_s_traco">
                                <td height="312"><p>&nbsp;</p>
                                    <table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="311" align="center">
                                                <div align="center">
                                                    <table width='314' border='0' align='right' cellpadding='0'
                                                           cellspacing='0'>
                                                        <tr>
                                                            <td>
                                                                <DIV class="commit commit-tease js-details-container">
                                                                    <font size="2"><strong><font
                                                                                color="#0033FF"><?php echo $resultado_depto0[sigla]; ?></font></strong><font
                                                                            color="#0033FF"><br>
                                                                            <font
                                                                                size="1"><?php echo $resultado_depto0[depto]; ?>
                                                                            </font></font><br>
                                                                    </font>

                                                                    <DIV class="commit-meta">
                                                                        <table width='100%' border='0' cellspacing='0'
                                                                               cellpadding='2'>
                                                                            <tr>
                                                                                <td width="31%" align="center">
                                                                                    <div align="left"><strong><font
                                                                                                color="#666666">Gerente:</font></strong>
                                                                                    </div>
                                                                                </td>
                                                                                <td width="69%">
                                                                                    <div align="left"><strong><font
                                                                                                color="#009933"><?php echo $resultado_depto1[nome] ?></font>
                                                                                        </strong></div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div align="left"><strong><font
                                                                                                color="#666666">Servidores:</font></strong>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <div align="left"><strong><font
                                                                                                color="#009933"><?php echo $resultado_depto2[total] ?></font></strong>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2"
                                                                                    style='border:none;border-bottom:solid #999999 1pt;'>
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <div align="left"><font
                                                                                            color="#666666"><strong>Funcion&aacute;rio
                                                                                                com maior incid&ecirc;ncia: </strong></font>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2"><strong><font
                                                                                            color="#FF0000"><?php echo $resultado_depto3[nome] ?></font></strong>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <strong><font color="#333333"> </font></strong></div>
                                            </td>
                                            <td width="15" valign="top">&nbsp;</td>
                                            <td width="374" valign="top">
                                                <div align="center">
                                                    <table width='314' border='0' align='right' cellpadding='0'
                                                           cellspacing='0'>
                                                        <tr>
                                                            <td>
                                                                <DIV class="commit commit-tease js-details-container">
                                                                    <font color="#000033"><strong><font size="2">Resumo
                                                                                anual deste departamento</font></strong>
                                                                        <br>
                                                                        <br>
                                                                    </font>

                                                                    <DIV class="commit-meta">
                                                                        <table width='100%' border='0' cellspacing='0'
                                                                               cellpadding='2'>
                                                                            <tr>
                                                                                <td width="4%" align="center">
                                                                                    <strong><font color="#666666">Justificativas:</font></strong>
                                                                                </td>
                                                                                <td width="96%"><strong><font
                                                                                            color="#FF0000"><?php echo $resultado_contador1[qtd_total] ?></font>
                                                                                    </strong></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><strong><font color="#666666">Abonos:</font></strong>
                                                                                </td>
                                                                                <td><strong><font
                                                                                            color="#FF0000"><?php echo $resultado_contador1[qtd_total] ?></font></strong>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2"
                                                                                    style='border:none;border-bottom:solid #999999 1pt;'>
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2"><font
                                                                                        color="#666666"><strong>Maior
                                                                                            incid&ecirc;ncia: </strong></font>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2"><strong><font
                                                                                            color="#FF0000"><?php echo $resultado_contador2[titulo] ?></font></strong>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="chart_div" style="width: 800px; height: 400px;"></div>
                                    <table width="124" height="25" border="0" align="center" cellpadding="0"
                                           cellspacing="0">
                                        <tr>
                                            <td align="center">
                                                <div id="resultado1" class="tryit" onClick="window.print();">
                                                    Imprimir
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="Sub_titulo_rel">
                                <td height="15">&nbsp;</td>
                            </tr>
                        </table>
                        <p>&nbsp;</p>
                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Busca</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <form name="form_busca" method="post" action="frm_Lista_processos.php">
                                        <font color="#FF6600" size="1"><strong>Informe texto de
                                                busca:</strong></font><br>
                                        <font color="#FF6600" size="1"><strong>
                                                <input name="status_atual" type="text" id="status_atual" size="3"
                                                       style="display:none" value="<?php echo $status_atual; ?>">
                                                <input name="selMES" type="text" id="selMES" size="3"
                                                       value="<?php echo $MES; ?>" style="display:none">
                                                <input name="selANO" type="text" id="selANO" size="3"
                                                       value="<?php echo $ANO; ?>" style="display:none">
                                                <input name="local_atual" type="text" id="local_atual" size="3"
                                                       value="<?php echo $local_atual; ?>" style="display:none">
                                                <input name="enviados" type="text" id="enviados" size="3"
                                                       value="<?php echo $enviados; ?>" style="display:none">
                                                <input name="texto" type="text" id="texto" size="3"
                                                       value="<?php echo $texto; ?>" style="display:none">
                                            </strong></font>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td><input type="text" name="texto_busca" style="width:130px"
                                                           value="<?php echo $texto_busca; ?>" onKeyPress="fncEnter();">
                                                </td>
                                                <td><a href="#" onClick="fncBusca();"><img src="../images/ok_bt.gif"
                                                                                           width="27" height="15"
                                                                                           border="0"></a></td>
                                            </tr>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                            <br>
                            <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                                <tr>
                                    <td class='Titulo_caixa'> Ranking Deptos em %</td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" class='Corpo_caixa'>
                                        <table width='100%' border='0' cellspacing='0' cellpadding='2'>
                                            <tr>
                                                <td colspan="2"><p><font color="#FF6600"><strong>10
                                                                primeiros</strong></font></p>
                                                </td>
                                            </tr>
                                            <?php

                                            //$sql_processos2 = "select count(id_usr) as total,sigla,depto.id_depto from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto group by depto.id_depto order by total desc limit 0,10";
                                            //$dados_processos2 = mysql_query($sql_processos2, $conexao);

                                            //$sql_processos2 = "select count(id_justificativa) as total,sigla,depto.id_depto from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto group by depto.id_depto order by total desc limit 0,10";
                                            $sql_processos2 = "select depto.id_depto,depto.sigla,(count(id_justificativa)*100)/(select count(id_usr) from usuarios where id_depto = depto.id_depto) as percentual from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on depto.id_depto = usuarios.id_depto group by usuarios.id_depto order by percentual desc limit 0,10";
                                            $dados_processos2 = mysqli_query( $conexao, $sql_processos2);
                                            while ($resultado_processos2 = mysqli_fetch_array($dados_processos2)) {
                                                ?>
                                                <tr>
                                                    <td width="4%" align="center"><img src="../images/check.jpg"
                                                                                       width="15" height="17"></td>
                                                    <td width="96%" class="Itens_normal">
                                                        <div style="cursor:pointer;"
                                                             onClick="abre_deptos(<?php echo $resultado_processos2[id_depto] ?>);"
                                                             id="<?php echo $resultado_processos2[id_depto] ?>">
                                                            <p><strong><font
                                                                        color="#009933"> <?php echo $resultado_processos2[sigla] . " - " . number_format($resultado_processos2[percentual], 0, ",", ".") . "%"; ?></font></strong>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <tr align="right">
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        <?php } ?>
                        <br>

                        <p align="right"> &nbsp; &nbsp;</p></td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                        de Ponto Eletrônico <?=date('Y')?> - SED&reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<form name="abre_janela" method="post" action="frm_Lista_ocorrencias.php">
    <input type="hidden" name="tipo_ocorrencia">
    <input type="hidden" name="enviados">

</form>
<form name="abre_lista" method="post" action="frm_Lista_ocorrencias.php">
    <input type="hidden" name="id_depto">
    <input type="hidden" name="enviados" value="0">
    <input type="hidden" name="tipo_ocorrencia" value="0">


</form>

<?php
((is_null($___mysqli_res = mysqli_close($conexao))) ? false : $___mysqli_res);
?>
</body>
</html>
