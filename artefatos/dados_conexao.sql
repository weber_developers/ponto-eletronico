CREATE TABLE `dadosconexao` (
  `id`               BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dados`            VARCHAR(255)        NOT NULL,
  `data_inclusao`    DATETIME            NULL     DEFAULT NULL,
  `dados_adicionais` TEXT                NULL     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  COLLATE ='utf8_general_ci'
  ENGINE =InnoDB;


ALTER TABLE `p_registro`
ADD COLUMN `dadosConexaoId` BIGINT UNSIGNED NULL DEFAULT NULL
AFTER `tsp`,
ADD INDEX `dadosConexaoId` (`dadosConexaoId`);
