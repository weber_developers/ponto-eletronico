@ECHO OFF
SET PWD=%cd%
SET PWD=%PWD:\=/%
SET PWD=%PWD:C:=/c%
SET PWD=%PWD:c:=/c%
SET TARGET=%1
IF %TARGET%.==. SET TARGET=dev

SET DOCKER_FILES_PATH=%PWD%/../docker-files/ponto

docker-compose -p ponto stop
docker-compose -p ponto rm
docker-compose -p ponto up --build -d