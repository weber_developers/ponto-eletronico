ALTER TABLE `usuarios` ADD COLUMN `nascimento` VARCHAR(50) NULL AFTER `id_chefe`;
ALTER TABLE `usuarios` ADD COLUMN `data_cadastro` VARCHAR(50) NULL DEFAULT NULL AFTER `nascimento`;
ALTER TABLE `usuarios` ADD COLUMN `nome_reduzido` VARCHAR(50) NULL DEFAULT NULL AFTER `data_cadastro`;
ALTER TABLE `usuarios` CHANGE COLUMN `cargo` `cargo` VARCHAR(50) NULL DEFAULT NULL AFTER `nome`;
ALTER TABLE `usuarios` CHANGE COLUMN `terceirizado` `terceirizado` INT(1) NULL DEFAULT NULL FIRST;
ALTER TABLE `usuarios`
	CHANGE COLUMN `id_usr` `id_usr` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT AFTER `terceirizado`,
	ADD PRIMARY KEY (`id_usr`);