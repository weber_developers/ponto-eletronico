FROM php-zendserver

ENV ADMINISTRATOR_EMAIL=
ENV WEBMASATER_EMAIL=
ENV FROM_EMAIL=
ENV SMTP_HOST=
ENV SMTP_PORT=
ENV SMTP_CONNECTION_MODE=
ENV SMTP_USERNAME=
ENV SNTP_PASSWORD=

RUN apt-get update && apt-get install -y mysql-client git
#RUN git clone https://mendonca2709@bitbucket.org/weber_developers/ponto-eletronico.git /home/ponto
#RUN cp -fR /home/ponto/* /var/www/html/

#COPY ./ponto.sql /home/ponto.sql
#COPY ./popular-banco-de-dados.sh /home/popular-banco-de-dados.sh

#RUN chmod a+x /home/ponto.sql
#RUN chmod a+x /home/popular-banco-de-dados.sh

WORKDIR /var/www/html