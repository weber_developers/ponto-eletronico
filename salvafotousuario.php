<?php

require_once("funcoes/conexao.php");

if (empty($_REQUEST['matricula'])) {
    $path = $_SESSION['config']->pastaFotosUsuarios . "fotos_" . md5(date('YmdHis').rand(0,10000)) . ".jpg";
    $r = move_uploaded_file($_FILES['webcam']['tmp_name'], $path);
    $image = imagecreatefromjpeg($path);
    imagejpeg($image, $path, 100);
    echo $path;
}else{
    $usuario = getUsuario($_REQUEST['matricula']);
    if ($usuario->id_usr) {
        //gravo imagem do cadastro do usuario
        $image = imagecreatefromjpeg($_REQUEST['nome_arquivo']);
        imagejpeg($image, $_SESSION['config']->pastaFotosUsuarios . $usuario->matricula . '.jpg', 100);
        unlink($_REQUEST['nome_arquivo']);
    }
}
