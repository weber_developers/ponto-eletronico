$(function(){
	customDatePickerInicializar();
});

function customDatePickerSetFocus(id) {
	$("[id='" + id + "_text']").focus();
}
function customDatePickerInicializar() {
	$("[modificador='customDatePicker']:not([id*=clone])").each(function (index, element) {
		var sId = $(element).attr('id');

		if ($(element).attr("inicializado") != "true") {
			$(element).datebox({
				onSelect: function (date) {
					var sText = sId + "_text";
					var sValue = $("[id='" + sText + "']").val();
					if($(element).attr("readonly") == undefined) {
						customDatePickerSetDateHidden(sId, parseDateToStr(date));
					} else {
						customDatePickerSetDate(sId, customDatePickerGetValueStrFromParts(sId));
					}
				}
			});
			$(element).attr("inicializado", "true");
			customDatePickerPreparar(sId);
		}
	});
}

function customDatePickerPreparar(sId) {
	if (sId.indexOf("clone") < 0) {
		var oContainer = $("[id='" + sId + "_container" + "']");
		$(oContainer).each(function (index, element) {
			var oObjTextList = $(element).find(".combo-text");
			var oObjValueList = $(element).find(".combo-value");

			var oObjText;
			var oObjValue;
			oObjTextList.each(function (index, element) {
				oObjText = $(element);
			});
			oObjValueList.each(function (index, element) {
				oObjValue = $(element);
			});
			if ($(oObjText).length) {
				if ($(oObjText).attr("preparado") != "true") {
					if ($(oObjValue).length) {
						$(oObjText).mask('00/00/0000');

						var oObjOriginal = $("[id='" + sId + "']");
						var fncOnChange = $(oObjOriginal)[0].onchange;
						$(oObjText).change(fncOnChange);

						if ($(oObjOriginal).attr('required') != null) {
							$(oObjText).attr('required', 'required');
							$(oObjOriginal).removeAttr("required");
						}
					}
					$(oObjText).attr("id", sId + '_text');
					$(oObjText).attr("preparado", "true");
				}
			}
		});
	}
}

function setValueDateParts(_oObjText) {
	var id = _oObjText.id.replace('_text','');
	var sValue = $("[name='" + id + "']").val();
	customDatePickerSetDateHidden(id, sValue);
}

function customDatePickerGetValueStr(id) {
	var sValue = $("[name='" + id + "']").val();

	return sValue;
}

function customDatePickerGetValueDate(id) {
	return parseStrToDate(customDatePickerGetValueStr(id));
}


function customDatePickerGetValueDateFromParts(sId) {
	var oTxtDay = $("[id='" + sId + "_day']");
	oTxtDay.each(function (index, element) {
		oTxtDay = $(element).val();
	});
	var oTxtMonth = $("[id='" + sId + "_month']");
	oTxtMonth.each(function (index, element) {
		oTxtMonth = $(element).val();
	});
	var oTxtYear = $("[id='" + sId + "_year']"); if($(oTxtYear).length) $(oTxtYear).val();
	oTxtYear.each(function (index, element) {
		oTxtYear = $(element).val();
	});
	return parseStrToDate(oTxtDay + "/" + oTxtMonth + "/" + oTxtYear);
}

function customDatePickerGetValueStrFromParts(sId) {
	var oTxtDay = $("[id='" + sId + "_day']");
	oTxtDay.each(function (index, element) {
		oTxtDay = $(element).val();
	});
	var oTxtMonth = $("[id='" + sId + "_month']");
	oTxtMonth.each(function (index, element) {
		oTxtMonth = $(element).val();
	});
	var oTxtYear = $("[id='" + sId + "_year']"); if($(oTxtYear).length) $(oTxtYear).val();
	oTxtYear.each(function (index, element) {
		oTxtYear = $(element).val();
	});
	return (oTxtDay + "/" + oTxtMonth + "/" + oTxtYear);
}

function customDatePickerSetDate(sId, sValue) {
	var oContainer = $("[id='" + sId + "_container" + "']");
	$(oContainer).each(function (index, element) {
		var oObjTextList = $(element).find(".combo-text");
		var oObjValueList = $(element).find(".combo-value");

		var oObjText;
		var oObjValue;
		oObjTextList.each(function (index, element) {
			oObjText = $(element);
		});
		oObjValueList.each(function (index, element) {
			oObjValue = $(element);
		});
		if ($(oObjText).length) {
			$(oObjText).val(sValue);
			if ($(oObjValue).length) {
				$(oObjValue).val(sValue);
			}
		}
	});
	customDatePickerSetDateHidden(sId, sValue);
}

function customDataPickerSetReadonly(sId, bReadonly) {
	var oContainer = $("[id='" + sId + "_container" + "']");
	$(oContainer).each(function (index, element) {
		var oObjTextList = $(element).find(".combo-text");
		var oObjValueList = $(element).find(".combo-value");
		var oObjArrowList = $(element).find(".combo-arrow");

		var oObjText;
		var oObjValue;
		var oObjArrow;

		oObjTextList.each(function (index, element) {
			oObjText = $(element);
		});
		oObjValueList.each(function (index, element) {
			oObjValue = $(element);
		});
		oObjArrowList.each(function (index, element) {
			oObjArrow = $(element);
		});

		if (bReadonly) {
			$("[id='" + sId + "']").attr('readonly','readonly');
			oObjText.attr('readonly','readonly');
			oObjArrow.hide();
		} else {
			$("[id='" + sId + "']").removeAttr('readonly');
			oObjText.removeAttr('readonly');
			oObjArrow.show();
		}
	});
}

function customDatePickerSetDateHidden(id, sValue) {
	var day = '';
	var month = '';
	var year = '';
	var hours = '';
	var minutes = '';
	var seconds = '';

	var bAtualizar = false;
	if (sValue == null || sValue == 'null' || sValue.length == 0) {
		bAtualizar = true;
	} else if (sValue.length == 10) {
		var arrData = sValue.split("/");
		if (arrData.length >= 1) {day = arrData[0];}
		if (arrData.length >= 2) {month = arrData[1];}
		if (arrData.length >= 3) {year = arrData[2];}
		hours = 23;
		minutes = 59;
		seconds = 59;

		bAtualizar = true;
		if (month.length >= 2 && parseInt(month) > 12) {
			bAtualizar = false;
			alert('O mês informado para a data não é válido: ' + month);
			customDatePickerSetFocus(id);
			customDatePickerSetDate(id,'');
		}
	}

	if (bAtualizar) {
		var sId = id;
		var oTxtDay = $("[id='" + sId + "_day']"); if($(oTxtDay).length) $(oTxtDay).val(day);
		var oTxtMonth = $("[id='" + sId + "_month']"); if($(oTxtMonth).length) $(oTxtMonth).val(month);
		var oTxtYear = $("[id='" + sId + "_year']"); if($(oTxtYear).length) $(oTxtYear).val(year);
		var oTxtHour = $("[id='" + sId + "_hour']"); if($(oTxtHour).length) $(oTxtHour).val(hours);
		var oTxtMinute = $("[id='" + sId + "_minute']"); if($(oTxtMinute).length) $(oTxtMinute).val(minutes);
		var oTxtSecond = $("[id='" + sId + "_second']"); if($(oTxtSecond).length) $(oTxtSecond).val(seconds);
	}
}

function customDataPickerDisabled(sId) {
	$("[id='" + sId + "']").datebox({
		disabled: true
	});
}

function customDataPickerEnabled(sId) {
	$("[id='" + sId + "']").datebox({
		disabled: false
	});
}

function showCalendar(idBtn) {
	/* não há como mostrar o calendário */
}

function desabilitarComponenteDataPicker(componente) {
	$(componente).datebox({
		disabled: true
	});
}

function habilitarComponenteDataPicker(componente) {
	$(componente).datebox({
		disabled: false
	});
}