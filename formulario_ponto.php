<?php
/**
 * Created by PhpStorm.
 * User: TMP-mmendonca
 * Date: 02/03/2015
 * Time: 08:31
 */

require_once 'funcoes/conexao.php';
require_once 'funcoes/funcoes.php';
?>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        $('#matricula').mask('999.999.999-99');
        $('#matricula').keyup(function (e) {
            if ($('#matricula').val().length == 14) {
                $('#registrar-ponto').attr('disabled', false);
            }else{
                $('#registrar-ponto').attr('disabled', true);
            }
            if (e.which == 13 || e.keyCode == 13) {
                if ($('#matricula').val().length < 14) {
                    $('.informe-cpf').parent().effect('highlight', 1000);
                    $(this).val('');
                    $(this).focus();
                    return false;
                }
                tirarFoto();
            }
        });
        $('#matricula').focus();
        $('#matricula').keydown(function (e) {
            if (e.which == 13 || e.keyCode == 13) {
                e.preventDefault();
            }
        });
        $('body').keyup(function (e) {
            if (e.which == 27 || e.keyCode == 27) {
                window.location.href = 'index.php';
            }
        });
        $('.verificar-registros').click(function () {
            $('#modal-registros').find('.modal-body').html('<h2>Por favor aguarde...</h2>');
            $.post(
                'registros_do_dia.php',
                {
                    id_usr: $('#matricula').val()
                },
                function (r) {
                    $('#modal-registros').find('.modal-title').html(r.title);
                    $('#modal-registros').find('.modal-body').html(r.html);
                    $('#matricula').val('');
                    $('#registrar-ponto').attr('disabled', true);
                }, "json"
            );
            $('#modal-registros').modal('show');
        });
        setTimeout('updateTime()', 10);
        Webcam.set({
            width: 400,
            height: 300,
            image_format: 'jpeg',
            jpeg_quality: 90
        });
        Webcam.attach('#my_camera');
        $('video').addClass('img-thumbnail');
    });
    function tirarFoto() {
        sorria();
    }
    function sorria() {
        Webcam.snap(function (data_uri) {
            Webcam.upload(data_uri, 'salvafoto.php', function (code, text) {
                $.post(
                    'salvafoto.php',
                    {
                        matricula: $('#matricula').val(),
                        nome_arquivo: text,
                        grava_foto: $('#gravaFoto').val(),
                        dadosConexao: $('#dadosConexao').val()
                    },
                    function (r) {
                        document.write(r);
                    }
                );
            });
        });
    }
    updateTime = function () {
        $.post('horas.php', {}, function (r) {
            $('#horas').html(r);
        });
        setTimeout('updateTime()', 15000);
    }
    function click(event) {
        if (event.button == 2 || event.button == 3) {
            oncontextmenu = 'return false';
            alert('Botão do mouse incorreto!\n\nEssa ação foi registrada para este usuário.');
        }
    }

    function setFocus() {
        document.registro_ponto.matricula.focus();

    }
    function inicio() {
        window.location.href = 'index.php';
    }

</script>
<script src="./js/deployJava.js"></script>
<style>
    #loginForm {
        -webkit-box-shadow: 0px 0px 20px -2px rgba(191, 191, 191, 1);
        -moz-box-shadow: 0px 0px 20px -2px rgba(191, 191, 191, 1);
        box-shadow: 0px 0px 20px -2px rgba(191, 191, 191, 1);
    }
</style>
<?php if (isVPN() || $_REQUEST['vpn']) { ?>
    <script>
        function write(i) {
            $('#dadosConexao').text(i);
        }
    </script>
    <applet code="MyApplet.class" height="1" width="1" archive="meuapplet.jar"></applet>
<?php } ?>

<form action="frm_confirma_ponto.php" method="post" name="registro_ponto" id="loginForm">
    <input type="hidden" id="gravaFoto" name="gravaFoto"/>
    <textarea name="dadosConexao" id="dadosConexao" style="display:none;visibility: hidden;"></textarea>
    <table class="table">
        <tr>
            <th colspan="2">
                <h2 class="text-center">
                    <?php echo getDiaSemana(); ?>, <?php echo date('d'); ?>
                    de <?php echo getMes(); ?>
                    de <?php echo date('Y'); ?>
                </h2>
            </th>
        </tr>
        <tr>
            <td class="col-lg-6">
                <div id="my_camera" style="margin-left:80px;"></div>
            </td>
            <td class="col-lg-6">
                <div id="tem-foto" class="alert alert-success hide">
                    <h1 class="text-center">Você já tem foto cadastrada!</h1>
                </div>
                <div id="nao-encontrado" class="alert alert-danger hide">
                    <h1 class="text-center">Cadastro não encontrado!</h1>
                </div>
                <div id="formulario-foto" class="row hide">
                    <div class="col-lg-12">
                        <h3 class="alert alert-info text-center">Seu cadastro está sem foto!</h3>

                        <h3 class="text-justify">
                            Por favor, posicione-se em frente a câmera e pressione
                            <a href="javascript:void(0);" class="btn btn-success">ENTER</a>
                            para capturar a foto.
                            Esta foto ficará no seu cadastro no sistema de ponto.
                        </h3>

                        <p>Esta foto só poderá ser alterada com solicitação formal à Gerência de Gestão de Pessoas</p>
                    </div>
                </div>
                <div id="formulario-ponto" class="row">
                    <div class="col-lg-12">
                        <div id="horas" class="text-center  alert alert-info" style="font-size:3em;"></div>
                    </div>
                    <div class="col-lg-12">
                        <h3 class="informe-cpf">Informe seu CPF</h3>
                    </div>
                    <div class="col-lg-12">
                        <input name="matricula"
                               id="matricula"
                               type="text"
                               class="form-control input-lg"
                               autocomplete="off"
                            />
                    </div>
                    <div class="col-lg-12"><br/></div>
                    <div class="col-lg-12">
                        <a href="javascript:void(0);" id="registrar-ponto"
                           class="btn btn-success btn-lg btn-block"
                           disabled
                           onclick="javascript:tirarFoto();">
                            Registrar Ponto
                        </a>
                    </div>
                </div>
        </tr>
        <tr>
            <td colspan="2"><h2 class="text-center">&nbsp;</h2></td>
        </tr>
    </table>
</form>
<div class="row-fluid">
    <div class="col-lg-6">
        <a href="javascript:void(0);" class="btn btn-primary btn-lg btn-block verificar-registros">
            <i class="glyphicon glyphicon-time"></i> Verificar meus registros
        </a>
    </div>
    <div class="col-lg-6">
        <a href="atualiza_foto.php" class="btn btn-primary btn-lg btn-block">
            <i class="glyphicon glyphicon-camera"></i> Atualizar minha foto
        </a>
    </div>
</div>
<div id="modal-registros" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->
<iframe width="500" height="500" name="Verifica" style="display:none"></iframe>