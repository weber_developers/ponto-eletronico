﻿<?php session_start();
require_once 'funcoes/conexao.php';
require_once 'funcoes/funcoes.php';
ob_start();
?>
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <link rel="stylesheet" type="text/css" media="screen" href="./css/estilos.css">

    <script language="JavaScript" type="text/javascript">
        setTimeout("fncChama()", 2000);
        function fncChama() {
            window.location.href = 'index.php';
        }
        function fncEnter() {
            window.location.href = 'index.php';
        }
    </script>
    <div class="alert alert-success">
        <h1 class="text-center">Sua foto foi atualizada!</h1>
    </div>
<?php
$html = ob_get_clean();
include 'index.php';