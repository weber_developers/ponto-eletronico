Bizagi.AppModel = {
    "personalized": false,
    "userLogoName": "\\libs\\img\\biz-ex-logo.png",
    "bizagiUrl": "http://www.bizagi.com",
    "productName": "Bizagi Modeler",
    "modelName": "Modelo de Processos",
    "publishDate": "02/02/2015 14:42:23",
    "pages": [{
        "id": "db0ee308-8875-4342-880b-d166f3a26207",
        "name": "Bater o Ponto",
        "version": "1.0",
        "author": "TMP-mmendonca",
        "image": "files\\diagrams\\Bater_o_Ponto.png",
        "isSubprocessPage": false,
        "elements": [{
            "id": "40c755f1-aee6-4969-821c-a52e0e9ea125",
            "name": "Registro de Ponto",
            "elementImage": "files\\bpmnElements\\Participant.png",
            "imageBounds": {
                "points": [{"x": 20.0, "y": 20.0}],
                "radius": 0.0,
                "height": 398.0,
                "width": 50.0,
                "shape": "rect"
            },
            "elementType": "Participant",
            "properties": [],
            "pageElements": [{
                "id": "2fd19a3d-9cd8-4bf2-81f0-faf9d5f63ed8",
                "name": "Servidor deseja inicar ou encerrar o turno",
                "elementImage": "files\\bpmnElements\\NoneStart.png",
                "imageBounds": {
                    "points": [{"x": 175.0, "y": 95.0}],
                    "radius": 15.0,
                    "height": 30.0,
                    "width": 30.0,
                    "shape": "circle"
                },
                "elementType": "NoneStart"
            }, {
                "id": "c79ce6d1-ed3a-4511-92f6-3e9661df1f02",
                "name": "Informar matrícula",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 245.0, "y": 80.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "8f6891bf-0410-4e55-83f1-4e92dc762e65",
                "name": "Posicionar em frente a camera",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 370.0, "y": 80.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "880c7782-5b0e-49d6-9d91-02b6e12640dc",
                "name": "Clicar em Registrar Ponto",
                "description": "<p style=\"text-align:left;text-indent:0pt;margin:0pt 0pt 0pt 0pt;\"><span style=\"color:#000000;background-color:transparent;font-family:Segoe UI;font-size:8pt;font-weight:normal;font-style:normal;\">Esperar alguns segundos equanto a camera captura a imagem</span></p>",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 501.0, "y": 80.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "6023d727-5157-41c1-a9a2-997d25765de3",
                "name": "Validar  matrícula",
                "description": "",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 637.0, "y": 303.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "3b1e3bdf-732e-4c96-bc58-d58ba02b9ecf",
                "name": "Validar hoário",
                "description": "<p style=\"text-align:left;text-indent:0pt;margin:0pt 0pt 0pt 0pt;\"><span style=\"color:#000000;background-color:transparent;font-family:Segoe UI;font-size:8pt;font-weight:normal;font-style:normal;\">O sistema informa se o horário está fora da grade programada.</span></p><p style=\"text-align:left;text-indent:0pt;margin:0pt 0pt 0pt 0pt;\"><span style=\"color:#000000;background-color:transparent;font-family:Segoe UI;font-size:8pt;font-weight:normal;font-style:normal;\"> </span></p>",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 756.0, "y": 303.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "c0a1d011-5e6d-41af-82c0-b9dd9653fea3",
                "name": "Horário Válido?",
                "elementImage": "files\\bpmnElements\\ExclusiveGateway.png",
                "imageBounds": {
                    "points": [{"x": 888.0, "y": 313.0}],
                    "radius": 0.0,
                    "height": 40.0,
                    "width": 40.0,
                    "shape": "poly"
                },
                "elementType": "ExclusiveGateway",
                "properties": [],
                "pageElements": [{"name": "Não", "elementType": "SequenceFlow", "properties": []}, {
                    "name": "Sim",
                    "elementType": "SequenceFlow",
                    "properties": []
                }]
            }, {
                "id": "dbc3fccb-2169-415f-a037-7efa5657cd9a",
                "name": "Emite alerta para o servidor sobre ponto fora da grade",
                "description": "<p style=\"text-align:left;text-indent:0pt;margin:0pt 0pt 0pt 0pt;\"><span style=\"color:#000000;background-color:transparent;font-family:Segoe UI;font-size:8pt;font-weight:normal;font-style:normal;\">Emails são disparados para os gestores do servidor caso o registro precise de abono.</span></p><p style=\"text-align:left;text-indent:0pt;margin:0pt 0pt 0pt 0pt;\"><span style=\"color:#000000;background-color:transparent;font-family:Segoe UI;font-size:8pt;font-weight:normal;font-style:normal;\">É possível ver a lista posteriormente pelo sistema.</span></p>",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 989.0, "y": 292.5}],
                    "radius": 0.0,
                    "height": 81.0,
                    "width": 135.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "2a4d483e-d05b-45c1-bd79-2e48f2484d16",
                "name": "Confirmar o registro de ponto ",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 1155.0, "y": 99.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "8fc2e625-b65a-46ab-b426-a7839a7f1fd5",
                "name": "Ponto Registrado",
                "elementImage": "files\\bpmnElements\\NoneEnd.png",
                "imageBounds": {
                    "points": [{"x": 1311.0, "y": 114.0}],
                    "radius": 15.0,
                    "height": 30.0,
                    "width": 30.0,
                    "shape": "circle"
                },
                "elementType": "NoneEnd"
            }, {
                "id": "c6c20dd6-3b2b-4ef7-a014-e566c9f026e1",
                "name": "Servidor",
                "elementImage": "files\\bpmnElements\\Lane.png",
                "imageBounds": {
                    "points": [{"x": 70.0, "y": 20.0}],
                    "radius": 0.0,
                    "height": 198.0,
                    "width": 30.0,
                    "shape": "rect"
                },
                "elementType": "Lane"
            }, {
                "id": "6214c575-d982-4282-8fdd-544fafcaba6f",
                "name": "Sistema",
                "elementImage": "files\\bpmnElements\\Lane.png",
                "imageBounds": {
                    "points": [{"x": 70.0, "y": 218.0}],
                    "radius": 0.0,
                    "height": 200.0,
                    "width": 30.0,
                    "shape": "rect"
                },
                "elementType": "Lane"
            }]
        }]
    }, {
        "id": "a31aa893-b2bb-41aa-97ca-10001d5be6da",
        "name": "Justificar o Ponto",
        "version": "1.0",
        "author": "TMP-mmendonca",
        "image": "files\\diagrams\\Justificar_o_Ponto.png",
        "isSubprocessPage": false,
        "elements": [{
            "id": "83e7eeb4-640a-49d9-a40f-4a9e18f06c1c",
            "name": "Justificar Ponto",
            "elementImage": "files\\bpmnElements\\Participant.png",
            "imageBounds": {
                "points": [{"x": 20.0, "y": 20.0}],
                "radius": 0.0,
                "height": 422.0,
                "width": 50.0,
                "shape": "rect"
            },
            "elementType": "Participant",
            "properties": [],
            "pageElements": [{
                "id": "16dc0123-a653-498d-b6cf-8338a4479736",
                "name": "Fazer login em ponto/adm",
                "elementImage": "files\\bpmnElements\\NoneStart.png",
                "imageBounds": {
                    "points": [{"x": 126.0, "y": 115.5}],
                    "radius": 16.5,
                    "height": 33.0,
                    "width": 33.0,
                    "shape": "circle"
                },
                "elementType": "NoneStart"
            }, {
                "id": "343e4749-40f9-4931-9711-704711c835bc",
                "name": "Clicar em  Justificativas no menu horizontal",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 216.0, "y": 99.5}],
                    "radius": 0.0,
                    "height": 65.0,
                    "width": 102.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "b4766fbe-a8ed-475d-befe-01bd489d7607",
                "name": "Identificar dias que precisam justificativas",
                "description": "",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 364.0, "y": 100.5}],
                    "radius": 0.0,
                    "height": 63.0,
                    "width": 98.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "9153916c-86c6-485d-adcc-480d510bfd37",
                "name": "Clicar em Justificar",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 515.0, "y": 104.5}],
                    "radius": 0.0,
                    "height": 55.0,
                    "width": 101.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "9593a716-6945-4a0f-9efe-c19049949d2f",
                "name": "Preencher os campos do formulário",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 676.0, "y": 103.0}],
                    "radius": 0.0,
                    "height": 58.0,
                    "width": 89.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "1fa9e96b-994d-4dbe-a89a-7cd8bfdaeabc",
                "name": "Enviar e Fechar",
                "elementImage": "files\\bpmnElements\\NoneEnd.png",
                "imageBounds": {
                    "points": [{"x": 845.0, "y": 117.0}],
                    "radius": 15.0,
                    "height": 30.0,
                    "width": 30.0,
                    "shape": "circle"
                },
                "elementType": "NoneEnd"
            }, {
                "id": "57e7c536-ba69-4b9d-a5d0-bce72f326a1d",
                "name": "Servidor",
                "elementImage": "files\\bpmnElements\\Lane.png",
                "imageBounds": {
                    "points": [{"x": 70.0, "y": 20.0}],
                    "radius": 0.0,
                    "height": 422.0,
                    "width": 30.0,
                    "shape": "rect"
                },
                "elementType": "Lane"
            }]
        }]
    }, {
        "id": "4c98fad4-b852-4f40-b619-a436a3f78a20",
        "name": "Imprimir Ficha Individual",
        "version": "1.0",
        "author": "TMP-mmendonca",
        "image": "files\\diagrams\\Imprimir_Ficha_Individual.png",
        "isSubprocessPage": false,
        "elements": [{
            "id": "69ebe43c-5576-4a70-a6d4-a00882bc38d5",
            "name": "Imprimir Folha Individual",
            "elementImage": "files\\bpmnElements\\Participant.png",
            "imageBounds": {
                "points": [{"x": 20.0, "y": 20.0}],
                "radius": 0.0,
                "height": 550.0,
                "width": 50.0,
                "shape": "rect"
            },
            "elementType": "Participant",
            "properties": [],
            "pageElements": [{
                "id": "36ab56a8-39a8-4fea-9ad5-37f8c3998ee8",
                "name": "Acessar o Sistema",
                "description": "<p style=\"text-align:left;text-indent:0pt;margin:0pt 0pt 0pt 0pt;\"><span style=\"color:#000000;background-color:transparent;font-family:Segoe UI;font-size:8pt;font-weight:normal;font-style:normal;\">A ficha individual é necessária para o RH fechar os pontnos do mês. </span></p><p style=\"text-align:left;text-indent:0pt;margin:0pt 0pt 0pt 0pt;\"><span style=\"color:#000000;background-color:transparent;font-family:Segoe UI;font-size:8pt;font-weight:normal;font-style:normal;\">Ela precisa ser impressa no final do mês, com as ocorrências resolvidas, assinada e entregue para o Gestor de Ponto do Departamento.</span></p>",
                "elementImage": "files\\bpmnElements\\NoneStart.png",
                "imageBounds": {
                    "points": [{"x": 130.0, "y": 93.0}],
                    "radius": 15.0,
                    "height": 30.0,
                    "width": 30.0,
                    "shape": "circle"
                },
                "elementType": "NoneStart"
            }, {
                "id": "63098773-f9d9-42d3-bd92-1cd6206fc7cd",
                "name": "Clicar em Ficha de Frequência",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 237.0, "y": 78.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "4fde0294-1a37-40f3-b75a-257adcf7a975",
                "name": "Escolher o Mês e Ano",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 386.0, "y": 78.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "dc27726f-1a52-4760-b0d2-a8a2a322aa9a",
                "name": "Clicar em Gerar Folha",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 574.0, "y": 78.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "219127e8-a89f-4344-bb07-ec9ec19e759d",
                "name": "Verificar Justificativas não Abonadas",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 702.0, "y": 428.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "9672bfa3-8ae0-4408-9841-295383eccd50",
                "name": "Existem Justificativas não Abonadas?",
                "elementImage": "files\\bpmnElements\\ExclusiveGateway.png",
                "imageBounds": {
                    "points": [{"x": 841.0, "y": 438.0}],
                    "radius": 0.0,
                    "height": 40.0,
                    "width": 40.0,
                    "shape": "poly"
                },
                "elementType": "ExclusiveGateway",
                "properties": [],
                "pageElements": [{"name": "Sim", "elementType": "SequenceFlow", "properties": []}, {
                    "name": "Não",
                    "elementType": "SequenceFlow",
                    "properties": []
                }]
            }, {
                "id": "d2c54203-06b7-4c48-809e-2cd8e3d5bc1b",
                "name": "Emitir Alerta para o Servidor",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 994.0, "y": 428.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "3928eb16-1d54-4d8b-b619-66836e76292f",
                "name": "Finalizar",
                "elementImage": "files\\bpmnElements\\NoneEnd.png",
                "imageBounds": {
                    "points": [{"x": 1132.0, "y": 443.0}],
                    "radius": 15.0,
                    "height": 30.0,
                    "width": 30.0,
                    "shape": "circle"
                },
                "elementType": "NoneEnd"
            }, {
                "id": "b70b5658-a08a-40ca-9597-bd543511b080",
                "name": "Gerar a Ficha para Impressão",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 1011.0, "y": 100.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "69f035bb-88e5-4b6a-9aee-cd4f400d3299",
                "name": "Imprimir",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 1170.0, "y": 99.0}],
                    "radius": 0.0,
                    "height": 62.0,
                    "width": 92.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "2959a546-6000-45ae-9b3a-bdf18c4795d6",
                "name": "Assinar e Entregar para RH",
                "elementImage": "files\\bpmnElements\\NoneEnd.png",
                "imageBounds": {
                    "points": [{"x": 1330.0, "y": 115.0}],
                    "radius": 15.0,
                    "height": 30.0,
                    "width": 30.0,
                    "shape": "circle"
                },
                "elementType": "NoneEnd"
            }, {
                "id": "0201bb2e-959e-430f-b04b-51e248d9c932",
                "name": "Servidor",
                "elementImage": "files\\bpmnElements\\Lane.png",
                "imageBounds": {
                    "points": [{"x": 70.0, "y": 20.0}],
                    "radius": 0.0,
                    "height": 350.0,
                    "width": 30.0,
                    "shape": "rect"
                },
                "elementType": "Lane"
            }, {
                "id": "5cce2732-5408-4b75-ae0c-6ad3657142dc",
                "name": "Sistema",
                "elementImage": "files\\bpmnElements\\Lane.png",
                "imageBounds": {
                    "points": [{"x": 70.0, "y": 370.0}],
                    "radius": 0.0,
                    "height": 200.0,
                    "width": 30.0,
                    "shape": "rect"
                },
                "elementType": "Lane"
            }]
        }]
    }, {
        "id": "1e8dd80d-d9b0-4454-9f94-39046a6fbf0f",
        "name": "Resolver Ocorrência",
        "version": "1.0",
        "author": "TMP-mmendonca",
        "image": "files\\diagrams\\Resolver_Ocorrencia.png",
        "isSubprocessPage": false,
        "elements": [{
            "id": "1d5821a2-46ca-444f-96c9-d817be24393a",
            "name": "Resolver Ocorrência",
            "elementImage": "files\\bpmnElements\\Participant.png",
            "imageBounds": {
                "points": [{"x": 20.0, "y": 20.0}],
                "radius": 0.0,
                "height": 350.0,
                "width": 50.0,
                "shape": "rect"
            },
            "elementType": "Participant",
            "properties": [],
            "pageElements": [{
                "id": "0fc0d672-fe31-4e40-b70e-1a77e3cfd185",
                "name": "Acessar o Sistema",
                "elementImage": "files\\bpmnElements\\NoneStart.png",
                "imageBounds": {
                    "points": [{"x": 141.0, "y": 151.0}],
                    "radius": 15.0,
                    "height": 30.0,
                    "width": 30.0,
                    "shape": "circle"
                },
                "elementType": "NoneStart"
            }, {
                "id": "02cae0f2-0233-4f8c-84e9-efc865b90da9",
                "name": "Clicar em  Ocorrências",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 275.0, "y": 136.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "036a05eb-8120-4387-90f9-c49702a8a081",
                "name": "Filtrar de acordo com a necessidade",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 438.0, "y": 136.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "709c28f7-749a-4ed1-b9b1-b577b1441b27",
                "name": "Clicar no calendário do servidor desejado",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 603.0, "y": 136.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "559b3dcd-1778-412a-850e-3b0c73ef9213",
                "name": "Clicar em \"Aguardando Abono\"",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 779.0, "y": 136.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "0f755085-e9e8-4760-b79a-5a6ceac07e98",
                "name": "Gateway",
                "elementImage": "files\\bpmnElements\\InclusiveGateway.png",
                "imageBounds": {
                    "points": [{"x": 932.0, "y": 146.0}],
                    "radius": 0.0,
                    "height": 40.0,
                    "width": 40.0,
                    "shape": "poly"
                },
                "elementType": "InclusiveGateway",
                "properties": [],
                "pageElements": [{
                    "name": "Cortar Ponto",
                    "elementType": "SequenceFlow",
                    "properties": []
                }, {"name": "Abonar Ponto", "elementType": "SequenceFlow", "properties": []}, {
                    "name": "Não Abonar",
                    "elementType": "SequenceFlow",
                    "properties": []
                }]
            }, {
                "id": "fd7055ac-e146-41ce-b3ac-246f3c2b737c",
                "name": "Abonar Ponto",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 1013.0, "y": 42.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "1ae31f78-67e5-4b9c-b4d4-673c31cf9952",
                "name": "Gateway",
                "elementImage": "files\\bpmnElements\\InclusiveGateway.png",
                "imageBounds": {
                    "points": [{"x": 1182.0, "y": 146.0}],
                    "radius": 0.0,
                    "height": 40.0,
                    "width": 40.0,
                    "shape": "poly"
                },
                "elementType": "InclusiveGateway",
                "properties": [],
                "pageElements": [{"name": "Finalizar", "elementType": "SequenceFlow", "properties": []}]
            }, {
                "id": "7f00f71a-3ffa-468b-9c68-2f8d5e7534b8",
                "name": "Finalizar",
                "elementImage": "files\\bpmnElements\\NoneEnd.png",
                "imageBounds": {
                    "points": [{"x": 1294.0, "y": 151.0}],
                    "radius": 15.0,
                    "height": 30.0,
                    "width": 30.0,
                    "shape": "circle"
                },
                "elementType": "NoneEnd"
            }, {
                "id": "57dfb214-e11b-478a-ba7e-b76ec5b851ef",
                "name": "Não Abonar",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 1013.0, "y": 248.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "08b62894-f81f-4479-8eac-c6937dbc79a0",
                "name": "Cortar Ponto",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 1009.0, "y": 136.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "8c81063a-1c49-410b-87b5-accf91f698d1",
                "name": "Gestorde Ponto",
                "elementImage": "files\\bpmnElements\\Lane.png",
                "imageBounds": {
                    "points": [{"x": 70.0, "y": 20.0}],
                    "radius": 0.0,
                    "height": 350.0,
                    "width": 30.0,
                    "shape": "rect"
                },
                "elementType": "Lane"
            }]
        }]
    }, {
        "id": "b2d81c4b-3af1-4729-a278-c56b9d0eea42",
        "name": "Imprimir Ficha do Departamento",
        "version": "1.0",
        "author": "TMP-mmendonca",
        "image": "files\\diagrams\\Imprimir_Ficha_do_Departamento.png",
        "isSubprocessPage": false,
        "elements": [{
            "id": "04ae8393-26b6-4546-8aba-dbe5f4338034",
            "name": "Imprimir Ficha do Departamento",
            "elementImage": "files\\bpmnElements\\Participant.png",
            "imageBounds": {
                "points": [{"x": 20.0, "y": 20.0}],
                "radius": 0.0,
                "height": 209.0,
                "width": 50.0,
                "shape": "rect"
            },
            "elementType": "Participant",
            "properties": [],
            "pageElements": [{
                "id": "c44fa3af-a40e-4d85-a898-e5514a714b54",
                "name": "Acessar o Sistema",
                "elementImage": "files\\bpmnElements\\NoneStart.png",
                "imageBounds": {
                    "points": [{"x": 144.0, "y": 105.0}],
                    "radius": 15.0,
                    "height": 30.0,
                    "width": 30.0,
                    "shape": "circle"
                },
                "elementType": "NoneStart"
            }, {
                "id": "4ecbf558-4f02-41e1-b82a-783a41cf2cfd",
                "name": "Clicar em Ficha de Frequência",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 224.0, "y": 90.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "3ef938d5-dcdf-47f7-a259-578d89025c83",
                "name": "Escolher Departamentos no menu dirieto",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 375.0, "y": 89.5}],
                    "radius": 0.0,
                    "height": 61.0,
                    "width": 95.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "b762774d-004d-4e5f-993e-575db1745d41",
                "name": "Filtrar e Imprimir",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 521.0, "y": 90.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "21d6dc16-4a9d-4986-908d-5b20d4f66768",
                "name": "Event",
                "elementImage": "files\\bpmnElements\\NoneEnd.png",
                "imageBounds": {
                    "points": [{"x": 663.0, "y": 105.0}],
                    "radius": 15.0,
                    "height": 30.0,
                    "width": 30.0,
                    "shape": "circle"
                },
                "elementType": "NoneEnd"
            }, {
                "id": "3e4cd846-4528-4207-9317-b5e4b981486a",
                "name": "Gestor de Ponto",
                "elementImage": "files\\bpmnElements\\Lane.png",
                "imageBounds": {
                    "points": [{"x": 70.0, "y": 20.0}],
                    "radius": 0.0,
                    "height": 209.0,
                    "width": 30.0,
                    "shape": "rect"
                },
                "elementType": "Lane"
            }]
        }]
    }, {
        "id": "e60e7a89-3ccd-4858-8ecb-db477628660a",
        "name": "Cadastro de Feriados e Pontos Facultativos",
        "version": "1.0",
        "author": "TMP-mmendonca",
        "image": "files\\diagrams\\Cadastro_de_Feriados_e_Pontos_Facultativos.png",
        "isSubprocessPage": false,
        "elements": [{
            "id": "790f6d1b-7c75-4235-82b0-ead299e89178",
            "name": "Cadastro de Férias, Feriados e Pontos Facultativos",
            "description": "",
            "elementImage": "files\\bpmnElements\\Participant.png",
            "imageBounds": {
                "points": [{"x": 20.0, "y": 20.0}],
                "radius": 0.0,
                "height": 412.0,
                "width": 50.0,
                "shape": "rect"
            },
            "elementType": "Participant",
            "properties": [],
            "pageElements": [{
                "id": "53712b43-d957-46b7-a766-093723d742c0",
                "name": "Acessar o Sistema",
                "elementImage": "files\\bpmnElements\\NoneStart.png",
                "imageBounds": {
                    "points": [{"x": 142.0, "y": 180.0}],
                    "radius": 15.0,
                    "height": 30.0,
                    "width": 30.0,
                    "shape": "circle"
                },
                "elementType": "NoneStart"
            }, {
                "id": "4a4280e3-acfd-4109-81f5-f13be3be7b38",
                "name": "Clicar em Configurações",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 246.0, "y": 165.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "02328f3e-7d59-412d-a5fd-d436db18bab4",
                "name": "Clicar em  Cadastro de Eventos",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 402.0, "y": 165.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "20b339bd-77f1-4ab1-9b26-f9e48b9faf2d",
                "name": "Informar a Data e o Tipo do Evento",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 556.0, "y": 165.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "3e09eec8-d625-462f-8077-179f3bfd94da",
                "name": "Evento é para todos departamentos?",
                "elementImage": "files\\bpmnElements\\ExclusiveGateway.png",
                "imageBounds": {
                    "points": [{"x": 720.0, "y": 175.0}],
                    "radius": 0.0,
                    "height": 40.0,
                    "width": 40.0,
                    "shape": "poly"
                },
                "elementType": "ExclusiveGateway",
                "properties": [],
                "pageElements": [{"name": "Não", "elementType": "SequenceFlow", "properties": []}, {
                    "name": "Sim",
                    "elementType": "SequenceFlow",
                    "properties": []
                }]
            }, {
                "id": "8ac6ec6e-2989-428e-885f-964ba78e22bc",
                "name": "Informe os dados do evento",
                "description": "<p style=\"text-align:left;text-indent:0pt;margin:0pt 0pt 0pt 0pt;\"><span style=\"color:#000000;background-color:transparent;font-family:Segoe UI;font-size:8pt;font-weight:normal;font-style:normal;\">Descrição</span></p><p style=\"text-align:left;text-indent:0pt;margin:0pt 0pt 0pt 0pt;\"><span style=\"color:#000000;background-color:transparent;font-family:Segoe UI;font-size:8pt;font-weight:normal;font-style:normal;\">Anexo opcional</span></p><p style=\"text-align:left;text-indent:0pt;margin:0pt 0pt 0pt 0pt;\"><span style=\"color:#000000;background-color:transparent;font-family:Segoe UI;font-size:8pt;font-weight:normal;font-style:normal;\">Se o evento vai durar mais de 1 dia, inforome a data fim do evento</span></p>",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 1250.0, "y": 260.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "13987761-260d-4310-adec-04c85a95ba03",
                "name": "Registrar e Finalizar",
                "elementImage": "files\\bpmnElements\\NoneEnd.png",
                "imageBounds": {
                    "points": [{"x": 1412.0, "y": 275.0}],
                    "radius": 15.0,
                    "height": 30.0,
                    "width": 30.0,
                    "shape": "circle"
                },
                "elementType": "NoneEnd"
            }, {
                "id": "5222cfec-c32c-4d78-b939-9800392bd7c9",
                "name": "Informar Departamento",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 827.0, "y": 260.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "7013e963-33f4-4eb2-99f8-fc11d98b12fb",
                "name": "Evento é para todos os servidores do departamento?",
                "elementImage": "files\\bpmnElements\\ExclusiveGateway.png",
                "imageBounds": {
                    "points": [{"x": 974.0, "y": 270.0}],
                    "radius": 0.0,
                    "height": 40.0,
                    "width": 40.0,
                    "shape": "poly"
                },
                "elementType": "ExclusiveGateway",
                "properties": [],
                "pageElements": [{"name": "Sim", "elementType": "SequenceFlow", "properties": []}, {
                    "name": "Não",
                    "elementType": "SequenceFlow",
                    "properties": []
                }]
            }, {
                "id": "f95b7fe7-886b-4258-b3ee-4e1f1ba2a09a",
                "name": "Informar o servidor",
                "elementImage": "files\\bpmnElements\\AbstractTask.png",
                "imageBounds": {
                    "points": [{"x": 1094.0, "y": 366.0}],
                    "radius": 0.0,
                    "height": 60.0,
                    "width": 90.0,
                    "shape": "rect"
                },
                "elementType": "AbstractTask",
                "properties": []
            }, {
                "id": "390db834-566e-4846-833e-c4cd306571e6",
                "name": "Gestor de RH",
                "elementImage": "files\\bpmnElements\\Lane.png",
                "imageBounds": {
                    "points": [{"x": 70.0, "y": 20.0}],
                    "radius": 0.0,
                    "height": 412.0,
                    "width": 30.0,
                    "shape": "rect"
                },
                "elementType": "Lane"
            }]
        }]
    }],
    "texts": {
        "tableOfContents": "Conteúdo",
        "pageNumber": "Página",
        "pageNumberLabelOf": "de",
        "version": "Versão",
        "author": "Autor",
        "description": "Descrição",
        "mainPool": "Processo Principal",
        "mainPoolDescription": "Descrição do Processo Principal",
        "processDiagrams": "Diagramas do Processo",
        "processElements": "Elementos do Processo",
        "elements": "Elementos",
        "defaultElementName": "Elemento",
        "performers": "Executores",
        "connectors": "Conectores",
        "connector": "Conector",
        "home": "Sistema de Ponto",
        "search": "Pesquisar",
        "goToParentProcess": "<< Voltar",
        "visitBizagi": "Visite bizagi.com",
        "contains": "Contém {0} Sub-Processos",
        "showAll": "Mostrar Todos",
        "fullScreen": "Tela Cheia",
        "zoomIn": "Zoom In",
        "zoomOut": "Zoom Out",
        "close": "Fechar",
        "menu": "Menu: ",
        "errorPage": "Error when visualizing page",
        "process": "Processo",
        "subProcess": "Sub-Processos Publicados",
        "contain": "Contém",
        "checkAttributes": "Check attributes",
        "checkOverview": "Check overview",
        "unavailableResource": "Unavailable resource",
        "localResource": "Resource can be accessed locally",
        "performer": "Performer",
        "linktoimage": "Link to Image",
        "presentationAction": "Presentation Actions",
        "searchGlobal": "Pesquisar",
        "searchLocal": "Pesquisar neste processo",
        "searchResults": "Nenhum resultado encontrado",
        "titlePage": "Iniciar",
        "emptyElement": "This element has not yet been documented",
        "unsupported": "Your browser does not support content displayed by this page. <br> We recommend you upgrading your browser.",
        "details": "Details",
        "expand": "Expand",
        "mainPoolProperties": "Main Process properties",
        "cannotVisualize": "The page cannot be displayed",
        "resourceNotFound": "The requested resource was not found:",
        "applyTheme": "Applying new theme"
    },
    "searchMap": [{
        "containerId": "1e8dd80d-d9b0-4454-9f94-39046a6fbf0f",
        "containerName": "Resolver Ocorrência",
        "isSubprocess": false,
        "elements": [{"id": "bc3ac72c-e27b-4480-9831-7c965c43217a"}, {"id": "e6e2ba12-5c3d-4fe1-bc70-5470df3c7cc6"}, {
            "id": "1d5821a2-46ca-444f-96c9-d817be24393a",
            "value": "Resolver Ocorrência"
        }, {
            "id": "8c81063a-1c49-410b-87b5-accf91f698d1",
            "value": "Gestorde Ponto"
        }, {
            "id": "0fc0d672-fe31-4e40-b70e-1a77e3cfd185",
            "value": "Acessar o Sistema"
        }, {
            "id": "02cae0f2-0233-4f8c-84e9-efc865b90da9",
            "value": "Clicar em  Ocorrências"
        }, {
            "id": "036a05eb-8120-4387-90f9-c49702a8a081",
            "value": "Filtrar de acordo com a necessidade"
        }, {
            "id": "709c28f7-749a-4ed1-b9b1-b577b1441b27",
            "value": "Clicar no calendário do servidor desejado"
        }, {
            "id": "559b3dcd-1778-412a-850e-3b0c73ef9213",
            "value": "Clicar em \"Aguardando Abono\""
        }, {"id": "0f755085-e9e8-4760-b79a-5a6ceac07e98", "value": ""}, {
            "id": "fd7055ac-e146-41ce-b3ac-246f3c2b737c",
            "value": "Abonar Ponto"
        }, {
            "id": "57dfb214-e11b-478a-ba7e-b76ec5b851ef",
            "value": "Não Abonar"
        }, {
            "id": "08b62894-f81f-4479-8eac-c6937dbc79a0",
            "value": "Cortar Ponto"
        }, {"id": "1ae31f78-67e5-4b9c-b4d4-673c31cf9952", "value": ""}, {
            "id": "7f00f71a-3ffa-468b-9c68-2f8d5e7534b8",
            "value": "Finalizar"
        }, {"id": "5bc1926c-45ba-4776-b3fb-4808f7881d1d"}]
    }, {
        "containerId": "4c98fad4-b852-4f40-b619-a436a3f78a20",
        "containerName": "Imprimir Ficha Individual",
        "isSubprocess": false,
        "elements": [{
            "id": "69ebe43c-5576-4a70-a6d4-a00882bc38d5",
            "value": "Imprimir Folha Individual"
        }, {
            "id": "0201bb2e-959e-430f-b04b-51e248d9c932",
            "value": "Servidor"
        }, {
            "id": "5cce2732-5408-4b75-ae0c-6ad3657142dc",
            "value": "Sistema"
        }, {
            "id": "36ab56a8-39a8-4fea-9ad5-37f8c3998ee8",
            "value": "Acessar o Sistema"
        }, {
            "id": "63098773-f9d9-42d3-bd92-1cd6206fc7cd",
            "value": "Clicar em Ficha de Frequência"
        }, {
            "id": "4fde0294-1a37-40f3-b75a-257adcf7a975",
            "value": "Escolher o Mês e Ano"
        }, {
            "id": "dc27726f-1a52-4760-b0d2-a8a2a322aa9a",
            "value": "Clicar em Gerar Folha"
        }, {
            "id": "219127e8-a89f-4344-bb07-ec9ec19e759d",
            "value": "Verificar Justificativas não Abonadas"
        }, {
            "id": "9672bfa3-8ae0-4408-9841-295383eccd50",
            "value": "Existem Justificativas não Abonadas?"
        }, {
            "id": "d2c54203-06b7-4c48-809e-2cd8e3d5bc1b",
            "value": "Emitir Alerta para o Servidor"
        }, {
            "id": "b70b5658-a08a-40ca-9597-bd543511b080",
            "value": "Gerar a Ficha para Impressão"
        }, {
            "id": "69f035bb-88e5-4b6a-9aee-cd4f400d3299",
            "value": "Imprimir"
        }, {
            "id": "2959a546-6000-45ae-9b3a-bdf18c4795d6",
            "value": "Assinar e Entregar para RH"
        }, {"id": "3928eb16-1d54-4d8b-b619-66836e76292f", "value": "Finalizar"}]
    }, {
        "containerId": "a31aa893-b2bb-41aa-97ca-10001d5be6da",
        "containerName": "Justificar o Ponto",
        "isSubprocess": false,
        "elements": [{
            "id": "83e7eeb4-640a-49d9-a40f-4a9e18f06c1c",
            "value": "Justificar Ponto"
        }, {
            "id": "57e7c536-ba69-4b9d-a5d0-bce72f326a1d",
            "value": "Servidor"
        }, {
            "id": "16dc0123-a653-498d-b6cf-8338a4479736",
            "value": "Fazer login em ponto/adm"
        }, {
            "id": "343e4749-40f9-4931-9711-704711c835bc",
            "value": "Clicar em  Justificativas no menu horizontal"
        }, {
            "id": "b4766fbe-a8ed-475d-befe-01bd489d7607",
            "value": "Identificar dias que precisam justificativas"
        }, {
            "id": "9153916c-86c6-485d-adcc-480d510bfd37",
            "value": "Clicar em Justificar"
        }, {
            "id": "9593a716-6945-4a0f-9efe-c19049949d2f",
            "value": "Preencher os campos do formulário"
        }, {
            "id": "1fa9e96b-994d-4dbe-a89a-7cd8bfdaeabc",
            "value": "Enviar e Fechar"
        }, {"id": "da2435cf-8f7e-4859-95bd-47f49136e477"}, {"id": "5778207c-3648-445d-b5e1-a1c4495b07e1"}, {"id": "70f9e104-b673-42f6-b4b3-30f6415fc813"}, {"id": "8995f66a-f139-4742-ab0d-901c4af0c4a8"}]
    }, {
        "containerId": "b2d81c4b-3af1-4729-a278-c56b9d0eea42",
        "containerName": "Imprimir Ficha do Departamento",
        "isSubprocess": false,
        "elements": [{
            "id": "04ae8393-26b6-4546-8aba-dbe5f4338034",
            "value": "Imprimir Ficha do Departamento"
        }, {
            "id": "3e4cd846-4528-4207-9317-b5e4b981486a",
            "value": "Gestor de Ponto"
        }, {
            "id": "c44fa3af-a40e-4d85-a898-e5514a714b54",
            "value": "Acessar o Sistema"
        }, {
            "id": "4ecbf558-4f02-41e1-b82a-783a41cf2cfd",
            "value": "Clicar em Ficha de Frequência"
        }, {
            "id": "3ef938d5-dcdf-47f7-a259-578d89025c83",
            "value": "Escolher Departamentos no menu dirieto"
        }, {
            "id": "b762774d-004d-4e5f-993e-575db1745d41",
            "value": "Filtrar e Imprimir"
        }, {"id": "21d6dc16-4a9d-4986-908d-5b20d4f66768", "value": ""}]
    }, {
        "containerId": "db0ee308-8875-4342-880b-d166f3a26207",
        "containerName": "Bater o Ponto",
        "isSubprocess": false,
        "elements": [{"id": "9c67e271-d505-4b53-ba9a-fc3343608506"}, {"id": "116f7bcd-ea93-48e8-b6a0-79c4ff19f3c6"}, {
            "id": "40c755f1-aee6-4969-821c-a52e0e9ea125",
            "value": "Registro de Ponto"
        }, {
            "id": "c6c20dd6-3b2b-4ef7-a014-e566c9f026e1",
            "value": "Servidor"
        }, {
            "id": "6214c575-d982-4282-8fdd-544fafcaba6f",
            "value": "Sistema"
        }, {
            "id": "2fd19a3d-9cd8-4bf2-81f0-faf9d5f63ed8",
            "value": "Servidor deseja inicar ou encerrar o turno"
        }, {
            "id": "c79ce6d1-ed3a-4511-92f6-3e9661df1f02",
            "value": "Informar matrícula"
        }, {
            "id": "8f6891bf-0410-4e55-83f1-4e92dc762e65",
            "value": "Posicionar em frente a camera"
        }, {
            "id": "880c7782-5b0e-49d6-9d91-02b6e12640dc",
            "value": "Clicar em Registrar Ponto"
        }, {
            "id": "6023d727-5157-41c1-a9a2-997d25765de3",
            "value": "Validar  matrícula"
        }, {
            "id": "3b1e3bdf-732e-4c96-bc58-d58ba02b9ecf",
            "value": "Validar hoário"
        }, {
            "id": "c0a1d011-5e6d-41af-82c0-b9dd9653fea3",
            "value": "Horário Válido?"
        }, {
            "id": "dbc3fccb-2169-415f-a037-7efa5657cd9a",
            "value": "Emite alerta para o servidor sobre ponto fora da grade"
        }, {
            "id": "2a4d483e-d05b-45c1-bd79-2e48f2484d16",
            "value": "Confirmar o registro de ponto "
        }, {"id": "8fc2e625-b65a-46ab-b426-a7839a7f1fd5", "value": "Ponto Registrado"}]
    }, {
        "containerId": "e60e7a89-3ccd-4858-8ecb-db477628660a",
        "containerName": "Cadastro de Feriados e Pontos Facultativos",
        "isSubprocess": false,
        "elements": [{
            "id": "790f6d1b-7c75-4235-82b0-ead299e89178",
            "value": "Cadastro de Férias, Feriados e Pontos Facultativos"
        }, {
            "id": "390db834-566e-4846-833e-c4cd306571e6",
            "value": "Gestor de RH"
        }, {
            "id": "53712b43-d957-46b7-a766-093723d742c0",
            "value": "Acessar o Sistema"
        }, {
            "id": "4a4280e3-acfd-4109-81f5-f13be3be7b38",
            "value": "Clicar em Configurações"
        }, {
            "id": "02328f3e-7d59-412d-a5fd-d436db18bab4",
            "value": "Clicar em  Cadastro de Eventos"
        }, {
            "id": "20b339bd-77f1-4ab1-9b26-f9e48b9faf2d",
            "value": "Informar a Data e o Tipo do Evento"
        }, {
            "id": "3e09eec8-d625-462f-8077-179f3bfd94da",
            "value": "Evento é para todos departamentos?"
        }, {
            "id": "8ac6ec6e-2989-428e-885f-964ba78e22bc",
            "value": "Informe os dados do evento"
        }, {
            "id": "5222cfec-c32c-4d78-b939-9800392bd7c9",
            "value": "Informar Departamento"
        }, {
            "id": "7013e963-33f4-4eb2-99f8-fc11d98b12fb",
            "value": "Evento é para todos os servidores do departamento?"
        }, {
            "id": "f95b7fe7-886b-4258-b3ee-4e1f1ba2a09a",
            "value": "Informar o servidor"
        }, {"id": "13987761-260d-4310-adec-04c85a95ba03", "value": "Registrar e Finalizar"}]
    }]
}