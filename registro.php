﻿<?php session_start();
$bt = $_GET["bt"]; ?>
<head><title>Registro de Ponto</title>

    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <link rel="stylesheet" type="text/css" media="screen" href="./css/estilos.css">
    <script language="JavaScript" src="./funcoes/funcao.js"></script>
    <script language="javascript" type="text/javascript">
        function click() {
            if (event.button == 2 || event.button == 3) {
                oncontextmenu = 'return false';
                alert('Botão do mouse incorreto!\n\nEssa ação foi registrada para este usuário.');
            }
        }
        function keypresed() {
//alert('Tecla Desabilitada!\n\nEssa ação foi registrada para este usuário.');
        }
        //document.onkeydown=keypresed;
        document.onmousedown = click
        document.oncontextmenu = new Function("return false;")

        document.onkeydown = function (e) {
//alert(e.which);
            if (e.which == 33 || e.which == 34 || e.which == 35 || e.which == 36 || e.which == 37 || e.which == 38 || e.which == 39 || e.which == 40) {
                alert('Teclado numérico desabilitado ou tecla inválida!');
            }
            if (e.which != 144 && e.which != 33 && e.which != 34 && e.which != 35 && e.which != 36 && e.which != 37 && e.which != 38 && e.which != 39 && e.which != 40 && e.which != 116 && e.which != 8 && e.which != 46 && e.which != 13 && e.which != 48 && e.which != 49 && e.which != 50 && e.which != 51 && e.which != 52 && e.which != 53 && e.which != 54 && e.which != 55 && e.which != 56 && e.which != 57 && e.which != 96 && e.which != 97 && e.which != 98 && e.which != 99 && e.which != 100 && e.which != 101 && e.which != 102 && e.which != 103 && e.which != 104 && e.which != 105) {
                document.registro_ponto.matricula.value = '';
                alert('Tecla Desabilitada!\n\nEssa ação foi registrada para este usuário.');
            }
        }

        function fncEnter() {
            if (window.event.keyCode == 13) {
                if (document.registro_ponto.matricula.value == '') {
                    alert("Informe sua matrícula!");
                    document.registro_ponto.matricula.focus();
                    return false;
                }
                document.registro_ponto.submit();
            }
        }

        function setFocus() {
            document.registro_ponto.matricula.focus();

        }
        function inicio() {
            window.location.href = 'index.php';
        }

        function fncValida() {

            if (document.registro_ponto.matricula.value == '') {
                alert("Informe sua matrícula!");
                document.registro_ponto.matricula.focus();
                return false;
            }
            document.registro_ponto.submit();
        }
    </script>
</head>
<body onLoad="click();">


<BR> <BR> <BR>

<form action="frm_confirma_ponto.php" method="post" name="registro_ponto" id="loginForm">
    <table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="680" height="80" background="./images/header.jpg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="93%"><strong><font color="#333333" size="4">&nbsp;&nbsp;&nbsp;&nbsp;Registro
                                                de Ponto</font></strong></td>
                                    <td width="7%" rowspan="2" valign="bottom">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="bottom">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><font
                                                color="#666666" size="1">&nbsp;
                                                <?php

                                                $hoje = getdate();

                                                switch ($hoje['wday']) {
                                                    case 0:
                                                        echo "Domingo, ";
                                                        break;
                                                    case 1:
                                                        echo "Segunda-Feira, ";
                                                        break;
                                                    case 2:
                                                        echo "Terça-Feira, ";
                                                        break;
                                                    case 3:
                                                        echo "Quarta-Feira, ";
                                                        break;
                                                    case 4:
                                                        echo "Quinta-Feira, ";
                                                        break;
                                                    case 5:
                                                        echo "Sexta-Feira, ";
                                                        break;
                                                    case 6:
                                                        echo "Sábado, ";
                                                        break;
                                                }

                                                echo $hoje['mday'];
                                                switch ($hoje['mon']) {
                                                    case 1:
                                                        echo " de Janeiro de ";
                                                        break;
                                                    case 2:
                                                        echo " de Fevereiro de ";
                                                        break;
                                                    case 3:
                                                        echo " de Março de ";
                                                        break;
                                                    case 4:
                                                        echo " de Abril de ";
                                                        break;
                                                    case 5:
                                                        echo " de Maio de ";
                                                        break;
                                                    case 6:
                                                        echo " de Junho de ";
                                                        break;
                                                    case 7:
                                                        echo " de Julho de ";
                                                        break;
                                                    case 8:
                                                        echo " de Agosto de ";
                                                        break;
                                                    case 9:
                                                        echo " de Setembro de ";
                                                        break;
                                                    case 10:
                                                        echo " de Outubro de ";
                                                        break;
                                                    case 11:
                                                        echo " de Novembro de ";
                                                        break;
                                                    case 12:
                                                        echo " de Dezembro de ";
                                                        break;
                                                }

                                                echo $hoje['year'] . ".";
                                                ?>
                                                <input name="bt" type="text" style="display:none"
                                                       value="<?php echo $bt; ?>">
                                            </font></strong></td>
                                </tr>
                            </table>

                        </td>
                        <td width="20" background="./images/header_rightcap.jpg">&nbsp;</td>
                    </tr>
                </table>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="880" background="./images/contentshadow.gif" height="2"></td>
                    </tr>
                </table>
                <table width="699" height="13" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="3" height="5"></td>
                        <td width="12" height="4" background="./images/rightside.gif"></td>
                        <td width="890" align="center" valign="top" bgcolor="#FFFFFF"><BR>

                            <p><font size="2"><strong><font color="#FF0000">Informe sua matrícula,
                                            posicione-se para registro de sua foto e <br>
                                            clique no botão Registrar Ponto!</font></strong> <br>
                                    <br>
                                </font></p>
                            <table width="536" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2" align="center">
                                        <script>exibeFash('cap127.swf', 560, 200)</script>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="246" align="center">
                                        <iframe scrolling="no" src="horario.php" width="150" height="50"
                                                frameborder="0"></iframe>
                                    </td>
                                    <td width="284" valign="bottom">&nbsp;</td>
                                </tr>
                            </table>
                            <br>

                            <p><font color="#990000"></font></p>

                            <p>&nbsp;</p></td>
                        <td width="4" valign="top" background="./images/sidebar.gif"><br>
                        </td>
                    </tr>
                </table>
                <table width="700" height="59" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="1" colspan="2"></td>
                    </tr>
                    <tr>
                        <td width="596" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                            de Ponto Eletrônico &reg; 2013
                        </td>
                        <td width="2" class="Fundo_caixa_canto_jpg"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">&nbsp;</td>
        </tr>
    </table>
</form>
<BR>
<iframe width="500" height="500" name="Verifica" style="display:none"></iframe>
</body>

</html>