<?php
/**
 * Created by PhpStorm.
 * User: marcelo-tm
 * Date: 06/04/2015
 * Time: 07:34
 */
ini_set('error_reporting', E_ERROR);

if ($argv[1] == 'path') {
    //$_SERVER['DOCUMENT_ROOT'] = 'C:/wamp/www/desenvolvimento/fontes/gpPonto/';
    $_SERVER['DOCUMENT_ROOT'] = $argv[2];
}

require_once 'conexao.php';
require_once 'funcoes.php';

$hoje = new DateTime();

foreach (getGerentes() as $usuario) {
    $servidores = implode(",", array_keys($usuario->getMeusServidoresArray(null, true)));
    $html = '';
    if ($servidores) {
        $ocorrencias = R::findAll(BaseObject::OCORRENCIA, "id_usr in ({$servidores}) and data_inclusao like '{$hoje->format('Y-m-d')}%'");
        //$ocorrencias = R::findAll(BaseObject::OCORRENCIA, "id_usr in ({$servidores})");
        if ($ocorrencias) {
            foreach ($ocorrencias as $ocorrencia) {
                $html .= "{$ocorrencia->descricao} <br />";
            }
            ob_start();
            include './emailOcorrencia.php';
            $tpl = ob_get_clean();
            $tpl = str_replace('{ocorrencias}', $html, $tpl);
            enviarEmail($usuario->email, utf8_decode('Ocorrências de Ponto do dia ' . date('d/m/Y')), $tpl);
        }
    }
}