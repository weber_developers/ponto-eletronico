<?php
ini_set('error_reporting',E_ERROR);
date_default_timezone_set("UTC");
session_start();
include_once('mysql2i.class.php');
$_SESSION['config'] = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/config/properties.json'));
$conexao = ($GLOBALS["___mysqli_ston"] = mysqli_connect($_SESSION['config']->conexao->uri,  $_SESSION['config']->conexao->usuario,  $_SESSION['config']->conexao->senha)) or print (mysqli_error($GLOBALS["___mysqli_ston"]));
((bool)mysqli_set_charset($GLOBALS["___mysqli_ston"], "UTF8"));
mysqli_select_db( $conexao, $_SESSION['config']->conexao->banco) or print(mysqli_error($GLOBALS["___mysqli_ston"]));
//date_default_timezone_set('America/Sao_Paulo');
require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/BaseObject.php';
R::setup("mysql:host={$_SESSION['config']->conexao->uri};dbname={$_SESSION['config']->conexao->banco}", $_SESSION['config']->conexao->usuario, $_SESSION['config']->conexao->senha); //for both mysql or mariaDB
R::ext('pdispense', function ($type) {
    return R::getRedBean()->dispense($type);
});
require_once "funcoes.php";
$oUsuario = getUsuario();
//if (strpos($_SERVER['SCRIPT_FILENAME'], '/adm/') === false) {
//    require_once "./php-skydrive/src/functions.inc.php";
//    $token = skydrive_tokenstore::acquire_token(); // Call this function to grab a current access_token, or false if none is available.
//    if (!$token) { // If no token, prompt to login. Call skydrive_auth::build_oauth_url() to get the redirect URL.
//        echo "<div>";
//        echo "<img src='statics/key-icon.png' width='32px' style='vertical-align: middle;'>&nbsp";
//        echo "<span style='vertical-align: middle;'><a href='" . skydrive_auth::build_oauth_url() . "'>Login no SkyDrive</a></span>";
//        echo "</div>";
//        exit;
//    } else { // Otherwise, if we have a token, use it to create an object and start calling methods to build our page.
//        $_SESSION['skydrive'] = new skydrive($token);
//    }
//}
?>
